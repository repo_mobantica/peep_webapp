<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>RealEstate | Material List</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Material List:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Store</a></li>
        <li class="active"> Material List</li>
      </ol>
    </section>
    <!-- Main content -->
	<form name="StorePurchesform" id="StorePurchesform" action="${pageContext.request.contextPath}/MaterialPurchesedByStore" onSubmit="return validate()" method="post">


<section class="content">
   
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <span id="statusSpan" style="color:#FF0000"></span>
        <!-- /.box-header -->
        
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                  <span id="statusSpan" style="color:#FF0000"></span>
                   
                  <div class="box-body">
                     <div class="row">
                      <div class="col-xs-2">
				      <label for="storeId">Store Id </label>
		 		     <input type="text" class="form-control" id="storeId" name="storeId" value="${materialpurchesDetails[0].storeId}" readonly>
     			    </div>               
              		 </div>
            	  </div> 
           <div class="box-body"><!-- Firm -->
                <div class="row">
                  
			     
                </div>
              </div>
              
            </div>
          </div>
        </div><!-- Col-12 end -->
        
        <div class="panel box box-danger"></div>
        
        <div class="box-body">
          <div class="row">
			
			<div class="col-md-12">
              <h2 class="box-title">Material List</h2>
          <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
        	
              <table id="materialDetailsTable" class="table table-bordered">
                <thead>
                <tr bgcolor="#4682B4">
               		<th style="width:40px">Sr.No</th>
               		<th style="width:40px">item Id</th>
                  	<th style="width:150px">Item Category</th>
                  	<th style="width:150px">Item Sub-Category</th>
                  	<th style="width:150px">Item Name</th>
                  	<th style="width:50px">Unit</th>
                  	<th style="width:50px">Size</th>
                    <th style="width:150px">Brand</th>
                    <th style="width:80px">Qty</th>
                    <th style="width:30px" align="center">Check</th>
                    <th style="width:200px">Remark</th>
                  </tr>
                </thead>
                <tbody>
                
                  <c:forEach items="${materialDetails}" var="materialDetails" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      <td>${loopStatus.index+1}</td>
                      <td >${materialDetails.itemId} </td>
                      <td>${materialDetails.itemMainCategoryName}</td>
                      <td>${materialDetails.itemCategoryName}</td>
                      <td>${materialDetails.itemName}</td>
                      <td>${materialDetails.itemUnit}</td>
                      <td>${materialDetails.itemSize}</td>
                      <td>${materialDetails.itemBrandName}</td>
                      <td>${materialDetails.itemQuantity}</td>
                      <td contenteditable='true'></td>
                       <td contenteditable='true'></td>
                     <!--  <td align="center"><input type="checkbox" name="checkStatus" id="checkStatus" value="COMPLETE"></td>  
                     <td align="center"><textarea class="form-control" rows="1" id="remark" name="remark"> </textarea></td>  
                       --> </tr>
				 </c:forEach>
				 
             <%-- 
                  <c:forEach items="${materialDetails}" var="materialDetails" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                      <td>${loopStatus.index+1}</td>
                      <td >${materialDetails.itemId}<input type="hidden" class="form-control" id="itemId" name="itemId" value="${materialDetails.itemId}" readonly>
     			    </td>
                      <td>${materialDetails.itemMainCategoryName}</td>
                      <td>${materialDetails.itemCategoryName}</td>
                      <td>${materialDetails.itemName} <input type="hidden" class="form-control" id="itemName" name="itemName" value="${materialDetails.itemName}" readonly></td>
                      <td>${materialDetails.itemUnit} <input type="hidden" class="form-control" id="itemUnit" name="itemUnit" value="${materialDetails.itemUnit}" readonly></td>
                      <td>${materialDetails.itemSize} <input type="hidden" class="form-control" id="itemSize" name="itemSize" value="${materialDetails.itemSize}" readonly></td>
                      <td>${materialDetails.itemBrandName}  <input type="hidden" class="form-control" id="itemBrandName" name="itemBrandName" value="${materialDetails.itemBrandName}" readonly></td>
                      <td>${materialDetails.itemQuantity}  <input type="hidden" class="form-control" id="itemQuantity" name="itemQuantity" value="${materialDetails.itemQuantity}" readonly></td>
                      <td align="center"><input type="checkbox" name="checkStatus" id="checkStatus" value="COMPLETE"></td>  
                      <td align="center"><textarea class="form-control" rows="1" id="remark" name="remark"> </textarea></td>  
                      </tr>
				 </c:forEach>
				 
				  --%>
                </tbody>
              </table>
            </div>
           </div>
          </div>
         
				 
		  </div>
		  
		  <br/><br/><br/>
		  
				 <input type="hidden" id="creationDate" name="creationDate" value="">
				 <input type="hidden" id="updateDate" name="updateDate" value="">
				 <input type="hidden" id="userName" name="userName" value="">	
		  <div class="col-md-12">
		    <div class="box-body">
        <div class="row">
         <div class="col-xs-1">
         </div>
	       <div class="col-xs-4">
              <a href="StoreManagement"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
			</div>
			    
	   		<div class="col-xs-2">
	            <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			</div>
			    
			<div class="col-xs-3">
		  		<button type="button" class="btn btn-info pull-right" onclick="return AddAllData()">Submit</button>
			</div>
			
			   <div class="col-xs-3" hidden="hidden">
		  			<button type="submit" class="btn btn-info pull-right" name="submit" id="submit">Submit</button>
			    </div> 
			    
		 </div>
	  </div>
	</div>
       </div>
	 </div>
  </section>

	</form>
  </div>

 <%@ include file="footer.jsp" %>
 
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- page script -->
<script>
function AddAllData()
{
	
	
	var storeId = $('#storeId').val();
	var creationDate = $('#creationDate').val();
	var itemId;
	var itemUnit;
	var itemSize;
	var itemBrandName;
	var itemQuantity;
	var checkStatus;
	var remark;
	var oTable = document.getElementById('materialDetailsTable');
	var rowLength = oTable.rows.length;
	//alert(rowLength);
	for (var i = 1; i < rowLength; i++){

		   var oCells = oTable.rows.item(i).cells;

		   var cellLength = oCells.length;
		   
			itemId=oCells.item(1).innerHTML;
			itemUnit =oCells.item(5).innerHTML;
			itemSize = oCells.item(6).innerHTML;
			itemBrandName = oCells.item(7).innerHTML;
			itemQuantity = oCells.item(8).innerHTML;
			checkStatus= oCells.item(9).innerHTML;
			remark=oCells.item(10).innerHTML;
			//alert(itemQuantity);
			
		
			 $.ajax({

				url : '${pageContext.request.contextPath}/SaveAlllStoreDate',
				type : 'Post',
				data : { storeId : storeId, itemId : itemId, itemUnit : itemUnit, itemSize : itemSize, itemBrandName : itemBrandName, itemQuantity : itemQuantity, checkStatus : checkStatus, remark : remark, creationDate : creationDate},
				dataType : 'json',
				success : function(result)
						  {

						  }
				});
		
		}
	submitFunction();
}
function submitFunction()
{
submit.click();
}
function init()
{
	clearall();
	var n =  new Date();
	var y = n.getFullYear();
	var m = n.getMonth() + 1;
	var d = n.getDate();
	
	document.getElementById("creationDate").value = d + "/" + m + "/" + y;
	document.getElementById("updateDate").value = d + "/" + m + "/" + y;
	document.getElementById("userName").value ="Admin";
	

}

  $(function () {
    $('#materialDetailsTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
