<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Booking</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">
<%@ include file="headerpage.jsp" %>
 
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Booking Details
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Booking</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="bookingform" action="${pageContext.request.contextPath}/EditBooking" onSubmit="return validate()" method="Post" >
  
  <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
       <div  class="panel box box-danger"/>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
      	<span id="statusSpan" style="color:#FF0000"></span>
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label for="enquiryId">Enquiry Id</label>
                  <input type="text" class="form-control" id="enquiryId" placeholder="ID" name="enquiryId" value="${bookingDetails[0].enquiryId}"  readonly>
                </div>               
                    <div class="col-xs-2">
                  <label for="bookingId">Booking Id</label>
                  <input type="text" class="form-control" id="bookingId" placeholder="ID" name="bookingId" value="${bookingDetails[0].bookingId}"  readonly>
                </div>
              </div>
            </div>
				
				
				<div class="box-body">
              <div class="row">
                <div class="col-xs-2">
				<label for="bookingfirstname">First Name</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingfirstname" placeholder="First Name" name="bookingfirstname" value="${bookingDetails[0].bookingfirstname}">
                  <span id="bookingfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="bookingmiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="bookingmiddlename" placeholder="Middle Name"  name="bookingmiddlename" value="${bookingDetails[0].bookingmiddlename}">
                 <span id="bookingmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="bookinglastname">Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookinglastname" placeholder="Last Name" name="bookinglastname" value="${bookingDetails[0].bookinglastname}">
                 <span id="bookinglastnameSpan" style="color:#FF0000"></span>
                </div>
                
                
                
                <%-- <div id="maidenName1" hidden="hidden">
                <div class="col-xs-2">
				<label for="bookingmaidenfirstname">Maiden First Name</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingmaidenfirstname" placeholder="First Name" name="bookingmaidenfirstname" value="${bookingDetails[0].bookingmaidenfirstname}">
                  <span id="bookingmaidenfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="bookingmaidenmiddlename">Maiden Middle Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingmaidenmiddlename" placeholder="Middle Name"  name="bookingmaidenmiddlename"value="${bookingDetails[0].bookingmaidenmiddlename}" >
                 <span id="bookingmaidenmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="bookingmaidenlastname">Maiden Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingmaidenlastname" placeholder="Last Name" name="bookingmaidenlastname" value="${bookingDetails[0].bookingmaidenlastname}">
                 <span id="bookingmaidenlastnameSpan" style="color:#FF0000"></span>
                </div>
            </div>  --%>
              </div>
            </div>
				
				
				
            
			<div class="box-body">
              <div class="row">
				     <div class="col-xs-3">
			    <label>Mobile No(Primary)</label><label class="text-red">* </label>
			    
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="bookingmobileNumber1" data-mask id="bookingmobileNumber1" value="${bookingDetails[0].bookingmobileNumber1}"  >
                </div>
                   <span id="bookingmobileNumber1Span" style="color:#FF0000"></span>
				 </div>
				  <div class="col-xs-3">
			    <label>Mobile No 2</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="bookingmobileNumber2" data-mask id="bookingmobileNumber2"  value="${bookingDetails[0].bookingmobileNumber2}">
                </div>
                 <span id="bookingmobileNumber2Span" style="color:#FF0000"></span>
				 </div> 
				  <div class="col-xs-3">
			     <label for="bookingEmail">Email ID </label><label class="text-red">* </label>
				   <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control" placeholder="Email" name ="bookingEmail" id="bookingEmail" value="${bookingDetails[0].bookingEmail}">
              </div>
               <span id="bookingEmailSpan" style="color:#FF0000"></span>
			     </div> 
			     <div class="col-xs-3">
			    <label for="bookingOccupation">Occupation </label><label class="text-red">* </label>
		     <select class="form-control" name="bookingOccupation" id="bookingOccupation" >
				     <option selected="" value="Default">-Select Occupation-</option>
				     <option selected="" value="${bookingDetails[0].bookingOccupation}">${bookingDetails[0].bookingOccupation}</option>
                 	  <c:forEach var="occupationList" items="${occupationList}">
	                 	  <c:choose>
		                 	    <c:when test="${bookingDetails[0].bookingOccupation ne occupationList.occupationName}">
		                    		<option value="${occupationList.occupationName}">${occupationList.occupationName}</option>
		                    	</c:when>
	                      </c:choose>
				      </c:forEach>
				     			
				 </select>
                    <span id="bookingOccupationSpan" style="color:#FF0000"></span>
                </div> 
				 </div>
            </div>
		
				
		 <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			   <label for="bookingaddress">Address </label><label class="text-red">* </label>
                 <textarea class="form-control" rows="1" id="bookingaddress" placeholder="Address" name="bookingaddress"> ${bookingDetails[0].bookingaddress}</textarea>
                  <span id="bookingaddressSpan" style="color:#FF0000"></span>
			     </div> 
				  
                   <div class="col-xs-2">
                  <label>Country </label><label class="text-red">* </label>
    			  <select class="form-control" name="countryName" id="countryName" onchange="getStateList(this.value)">
				  <option selected="selected" value="${bookingDetails[0].countryName}">${bookingDetails[0].countryName}</option>
                    <c:forEach var="countryList" items="${countryList}">
                      <c:choose>
	                     <c:when test="${bookingDetails[0].countryName ne countryList.countryName}">
		                    <option value="${countryList.countryName}">${countryList.countryName}</option>
		                 </c:when>
		              </c:choose>
	                 </c:forEach>
                  </select>
                   <span id="countryNameSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>State </label><label class="text-red">* </label>
				  <select class="form-control" name="stateName" id="stateName" onchange="getCityList(this.Value)">
				 		 <option selected="" value="${bookingDetails[0].stateName}">${bookingDetails[0].stateName}</option>
                    <c:forEach var="stateList" items="${stateList}">
                     <c:choose>
                      <c:when test="${bookingDetails[0].stateName ne stateList.stateName}">
                        <option value="${stateList.stateName}">${stateList.stateName}</option>
                      </c:when>
                     </c:choose>
				    </c:forEach>
                  </select>
                   <span id="stateNameSpan" style="color:#FF0000"></span>
               </div> 
                  <div class="col-xs-2">
				  <label>City </label><label class="text-red">* </label>
                    <select class="form-control" name ="cityName" id="cityName" onchange="getLocationAreaList(this.value)">
				  <option selected="" value="${bookingDetails[0].cityName}">${bookingDetails[0].cityName}</option>
                     <c:forEach var="cityList" items="${cityList}">
                      <c:choose>
                       <c:when test="${bookingDetails[0].cityName ne cityList.cityName}">
                    		<option value="${cityList.cityName}">${cityList.cityName}</option>
                       </c:when>
                      </c:choose>
				     </c:forEach>
                  </select>
                  <span id="cityNameSpan" style="color:#FF0000"></span>
				  </div> 
           
				    <div class="col-xs-2">
				    <label>Area </label><label class="text-red">* </label>
				        <select class="form-control" name="locationareaName" id="locationareaName"onchange="getpinCode(this.value)">
				  	<option selected="" value="${bookingDetails[0].locationareaName}">${bookingDetails[0].locationareaName}</option>
                 	  <c:forEach var="locationareaList" items="${locationareaList}">
	                 	  <c:choose>
		                 	    <c:when test="${bookingDetails[0].locationareaName ne locationareaList.locationareaName}">
		                    		<option value="${locationareaList.locationareaName}">${locationareaList.locationareaName}</option>
		                    	</c:when>
	                      </c:choose>
				      </c:forEach>
                  </select>
                   <span id="locationareaNameSpan" style="color:#FF0000"></span>
				  </div> 
           
                  <div class="col-xs-1">
			   
                  <label for="bookingPincode">Pin Code </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingPincode" placeholder="Pin Code" name="bookingPincode" value="${bookingDetails[0].bookingPincode}" readonly>
                    <span id="bookingPincodeSpan" style="color:#FF0000"></span>
			     </div> 
			     </div>
			     </div>
		
			
	
          </div>
          
            
         	   <input type="hidden" id="bookingstatus" name="bookingstatus" value="Booking">
       	 	   <input type="hidden" id="enquirydbStatus" name="enquirydbStatus" value="${enquirydbStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="${bookingDetails[0].creationDate}">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value=""> 

     </div>
     </div>
    </div>
     
    
      <div class="box box-default">
       <div class="box-body">
          <div class="row">
            <div class="col-md-12">
		 
			  <div class="box-body">
              <div class="row">
              <h3>Project And Flat Details</h3>
                  <div class="col-xs-3">
				<label>Project Name</label><label class="text-red">* </label>
				<input type="text" class="form-control" id="projectName" placeholder="" name="projectName" value="${bookingDetails[0].projectName}" readonly>
                    <span id="projectNameSpan" style="color:#FF0000"></span>
			     </div> 
			         <div class="col-xs-3">
			     <label>Project Building</label><label class="text-red">* </label>
			     <input type="text" class="form-control" id="buildingName" placeholder="" name="buildingName" value="${bookingDetails[0].buildingName}" readonly>

	               <span id="buildingNameSpan" style="color:#FF0000"></span>
			     </div> 
			    <div class="col-xs-3">
			      <label>Wing </label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="wingName" placeholder="" name="wingName" value="${bookingDetails[0].wingName}" readonly>
       
                   <span id="wingNameSpan" style="color:#FF0000"></span>
                 </div>
                   <div class="col-xs-3">
			      <label>Floor Name</label><label class="text-red">* </label>
                <input type="text" class="form-control" id="floortypeName" placeholder="" name="floortypeName" value="${bookingDetails[0].floortypeName}" readonly>
       
                    <span id="floortypeNameSpan" style="color:#FF0000"></span>
			     </div>
				 </div>
            </div>


			  <div class="box-body">
              <div class="row">
                    <div class="col-xs-3">
			    	<label>Flat Number  </label><label class="text-red">* </label>
					 <input type="text" class="form-control" id="flatNumber" placeholder="" name="flatNumber" value="${bookingDetails[0].flatNumber}" readonly>
       
                    <span id="flatNumberSpan" style="color:#FF0000"></span>
			     </div>
			     
			     
			       <div class="col-xs-3">
			    <label>Flat Type  </label><label class="text-red">* </label>
				<input type="text" class="form-control" id="flatType" placeholder="" name="flatType" value="${bookingDetails[0].flatType}" readonly>
                    <span id="flatTypeSpan" style="color:#FF0000"></span>
			     </div> 
			       <div class="col-xs-3">
                  <label>Flat Facing  </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatFacing" placeholder="Flat Facing " name="flatFacing" value="${bookingDetails[0].flatFacing}"readonly>
			     <span id="flatFacingSpan" style="color:#FF0000"></span>
			     </div>
       
			   
			      <div class="col-xs-3">
                  <label for="flatarea">Flat Area in SQ.FT </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatareainSqFt" placeholder="Flat Area in SQ.FT " name="flatareainSqFt" value="${bookingDetails[0].flatareainSqFt}" readonly>
			       <span id="flatareainSqFtSpan" style="color:#FF0000"></span>
			     </div> 
			            </div>
            </div>
			
			 <div class="box-body">
              <div class="row">
           
			          	<div class="col-xs-3">
			 	   <label for="flatCostwithotfloorise">Flat Cost(Sq.Ft)</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatCostwithotfloorise" placeholder="Flat Total Cost" name="flatCostwithotfloorise" value="${bookingDetails[0].flatCostwithotfloorise}" readonly>
                   <span id="flatCostwithotflooriseSpan" style="color:#FF0000"></span>
			     </div> 
           			<div class="col-xs-3">
			 	   <label for="floorRise">Floor Rise</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="floorRise" placeholder="Flat Total Cost" name="floorRise" value="${bookingDetails[0].floorRise}" readonly>
                   <span id="floorRiseSpan" style="color:#FF0000"></span>
			     </div> 
			   
                  <div class="col-xs-3">
			    <label for="flatCost">Flat Cost Per Sq.Ft</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatCost" placeholder="Flat Total Cost" name="flatCost" onchange="getflatBasicCost(this.value)" value="${bookingDetails[0].flatCost}">
                   <span id="flatCostSpan" style="color:#FF0000"></span>
			     <input type="hidden" id="flatminimumCost" name="flatminimumCost">
			     </div>
			       <div class="col-xs-3">
                  <label for="flatcost">Flat Basic Cost </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatbasicCost" placeholder="Flat Cost " name="flatbasicCost" value="${bookingDetails[0].flatbasicCost}" readonly>
			     <span id="flatbasicCostSpan" style="color:#FF0000"></span>
			     </div>
              </div>
            </div>
            
            <div class="box-body">
              <div class="row">
               <div class="col-xs-3">
			    <label for="parkingFloor">Parking Floor</label> <label class="text-red">* </label>
                    <select class="form-control" name="parkingFloor" id="parkingFloor" onchange="getParkingNumber(this.value)" >
				  		<option selected="" value="${bookingDetails[0].parkingFloor}">${bookingDetails[0].parkingFloor}</option>
                  </select>
                   <span id="parkingFloorSpan" style="color:#FF0000"></span>
			     </div>
			          <div class="col-xs-3">
			    <label for="parkingNumber">Parking Zone</label> <label class="text-red">* </label>
                    <select class="form-control" name="parkingNumber" id="parkingNumber">
				  	<option selected="" value="${bookingDetails[0].parkingNumber}">${bookingDetails[0].parkingNumber}</option>
                  </select>
                   <span id="parkingNumberSpan" style="color:#FF0000"></span>
			     </div>
			       <div class="col-xs-3">
                  <label for="flatcost">Agent Name </label><label class="text-red">* </label>
 				 <select class="form-control" name="agentName" id="agentName">
				   <option selected="" value="${bookingDetails[0].agentName}">${bookingDetails[0].agentName}</option>
                 
                     <c:forEach var="agentList" items="${agentList}">
	    				  <c:choose>
				      	 <c:when test="${bookingDetails[0].agentName ne agentList.agentfirmName}">
                    	 <option value="${agentList.agentfirmName}">${agentList.agentfirmName}</option>
                    	 </c:when>
                       </c:choose>				       
                        </c:forEach>
               
                  </select>
			     <span id="agentNameSpan" style="color:#FF0000"></span>
			     </div>
              </div>
            </div>
            
          </div>
        
	 
    </div>
    </div>
        
           <div class="box-body">
               
              <div class="row">
                  <div class="col-xs-12">
                  	<div class="box-body">
              <div class="row">
              <h4><label>Amount Details</label></h4>
                  <div class="col-xs-3">
                   <div class="col-sm-12">
                  <label for="infrastructureCharge">Infrastructure Charges </label><label class="text-red">* </label>
                  </div>
                   <div class="col-sm-12">
                    <div class="form-group">
				<input type="text" class="form-control" id="infrastructureCharge" placeholder="Infrastructure Charges" name="infrastructureCharge" value="${bookingDetails[0].infrastructureCharge}" onchange="calculate(this.value)">      
				<span id="infrastructureChargeSpan" style="color:#FF0000"></span>     
                </div>
                </div>
                  </div>
                  
                  <div class="col-xs-3">
                   <div class="col-sm-12">
                 <label for="aggreementValue" >Aggreement Value</label><label class="text-red">* </label>
                  </div>
                   <div class="form-group">
                  <div class="col-sm-12">
                    <input type="text" class="form-control" id="aggreementValue1" placeholder="Aggreement Value " name="aggreementValue1" value="${bookingDetails[0].aggreementValue1}" onchange="calculate(this.value)" readonly>
                    <span id="aggreementValue1Span" style="color:#FF0000"></span>
                  </div>
                </div>
                  </div>
                 
                   <div class="col-xs-3">
                   <div class="col-sm-12">
                  <label for="stampDuty" >Stamp Duty (6%)</label><label class="text-red">* </label>
                  </div>
                  <div class="form-group">
				   <div class="col-sm-4">
					<input type="text" class="form-control" id="stampDutyPer" name="stampDutyPer" value="${bookingDetails[0].stampDutyPer}" onchange="calculate(this.value)">  
					<span id="stampDutyPerSpan" style="color:#FF0000"></span>  
				  </div>
                  <div class="col-sm-8">
				<input type="text" class="form-control" id="stampDuty1" placeholder="Stamp Duty" name="stampDuty1" value="${bookingDetails[0].stampDuty1}" onchange="calculate(this.value)" readonly>   
				<span id="stampDuty1Span" style="color:#FF0000"></span>       
				  </div>
                </div>
			</div>
				  <div class="col-xs-3">
                   <div class="col-sm-12">
                   <label for="registrationCost" >Registration Cost(1%)</label><label class="text-red">* </label>
                  </div>
                    <div class="form-group">
					<div class="col-sm-4">
					<input type="text" class="form-control" id="registrationPer" name="registrationPer" value="${bookingDetails[0].registrationPer}" onchange="calculate(this.value)">  
					<span id="gstCostSpan" style="color:#FF0000"></span>  
				  </div>
                  <div class="col-sm-8">
					<input type="text" class="form-control" id="registrationCost1" placeholder="Registration" name="registrationCost1" value="${bookingDetails[0].registrationCost1}" onchange="calculate(this.value)" readonly>  
					<span id="registrationCost1Span" style="color:#FF0000"></span>        
				  </div>
                </div>
					</div>
					
                  </div>
                  </div> 
               <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                   <div class="col-sm-12">
                     <label for="handlingCharges" >Handling Charges</label><label class="text-red">* </label>
                  </div>
                 <div class="form-group">
                  <div class="col-sm-12">
					<input type="text" class="form-control" id="handlingCharges" placeholder="Handling Charges" name="handlingCharges" value="${bookingDetails[0].handlingCharges}" onchange="calculate(this.value)"> 
					<span id="handlingChargesSpan" style="color:#FF0000"></span>       
				  </div>
                </div>
				</div>
					
                   <div class="col-xs-3">
                    <div class="col-sm-12">
                  <label for="gstCost" >GST Amount </label><label class="text-red">* </label>
                  </div>
                   
                  <div class="form-group">
 				<div class="col-sm-4">
					<input type="text" class="form-control" id="gstCost" name="gstCost" value="${bookingDetails[0].gstCost}" onchange="calculate(this.value)">  
					<span id="gstCostSpan" style="color:#FF0000"></span>  
				  </div>
                  <div class="col-sm-8">
					<input type="text" class="form-control" id="gstAmount1" placeholder="GST Amount"  name="gstAmount1" value="${bookingDetails[0].gstAmount1}" readonly>     
					<span id="gstAmount1Span" style="color:#FF0000"></span>  
				  </div>
                </div>
				</div>
           
                <div class="col-xs-3">
                   <div class="col-sm-12">
                 <label for="grandTotal">Grand Total</label><label class="text-red">* </label>
                  </div>
                     <div class="form-group">
                  <div class="col-sm-12">
				<input type="text" class="form-control" id="grandTotal1" placeholder="Grand Total" name="grandTotal1" value="${bookingDetails[0].grandTotal1}" readonly>         
			  <span id="grandTotal1Span" style="color:#FF0000"></span>
				  </div>
                </div>
                </div>
                
                <div class="col-xs-3">
                   <div class="col-sm-12">
                 <label for="grandTotal">TDS 1% Amount</label><label class="text-red">* </label>
                  </div>
                     <div class="form-group">
                  <div class="col-sm-12">
				<input type="text" class="form-control" id="tds" placeholder="TDS Amount" name="tds" value="${bookingDetails[0].tds}" readonly>         
			  <span id="grandTotal1Span" style="color:#FF0000"></span>
				  </div>
                </div>
                </div>
             </div>
          </div>
        </div>
      </div>
    </div>
</div>
         <div class="box-body">
              <div class="row">
              </br>
                     <div class="col-xs-4">
                	<a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
			     
              </div>
			</div>
    </section>
  
  
  

	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>
/* 
function MaidenFemale()
{
	if((document.bookingform.bookingMarried[0].checked == true ) && (document.bookingform.bookingGender[1].checked == true ))
	{
 		document.getElementById('maidenName1').style.display ='block';
	}
}

function MaidenMale()
{
	
	 	document.getElementById('maidenName1').style.display ='none';
} */
function chequePayment(){
	document.getElementById("labelchequeNumber").innerHTML="Cheque Number";
	  document.getElementById('cheque').style.display ='block';
	  document.bookingform.chequeNumber.value="";
	}
function ddPayment(){
	document.getElementById("labelchequeNumber").innerHTML="DD Number";
	  document.getElementById('cheque').style.display ='block';
	  document.bookingform.chequeNumber.value="";
	}
function onlinePayment(){
	document.getElementById("labelchequeNumber").innerHTML="RTGS/NEFT/IMPS Number";
	  document.getElementById('cheque').style.display ='block';
	  document.bookingform.chequeNumber.value="";
	}
function cashPayment(){
	  document.getElementById('cheque').style.display ='none';
	  document.bookingform.chequeNumber.value="";
	}





function clearall(){
	$('#bookingfirstNameSpan').html('');
	$('#bookingmiddlenameSpan').html('');
	$('#bookinglastnameSpan').html('');
	$('#bookingaddressSpan').html('');
	$('#countryNameSpan').html('');
	$('#stateNameSpan').html('');
	$('#cityNameSpan').html('');
	$('#locationareaNameSpan').html('');
	$('#bookingPincodeSpan').html('');
	$('#bookingEmailSpan').html('');
	$('#bookingmobileNumber1Span').html('');
	$('#bookingmobileNumber2Span').html('');
	$('#bookingOccupationSpan').html('');

	$('#projectNameSpan').html('');
	$('#buildingNameSpan').html('');
	$('#wingNameSpan').html('');
	$('#floortypeNameSpan').html('');
	$('#flatTypeSpan').html('');
	$('#flatNumberSpan').html('');
	$('#flatFacingSpan').html('');
	$('#flatareainSqFtSpan').html('');
	$('#flatbasicCostSpan').html('');
	$('#aggreementValue1Span').html('');
	$('#discount1Span').html('');
	$('#stampDuty1Span').html('');
	$('#registrationCost1Span').html('');
	$('#gstAmount1Span').html('');
	$('#handlingChargesSpan').html('');
	$('#infrastructureChargeSpan').html('');
	
	$('#grandTotal1Span').html('');
	$('#bookingAmount1Span').html('');
	$('#paymentModeSpan').html('');
	$('#chequeNumberSpan').html('');
	$('#chequebankNameSpan').html('');
	
}
function changeStatus()
{
	//$('#enquirydbStatusSpan').html('');
}

function getpinCode()
{

	 $("#bookingPincode").empty();
	 var locationareaName = $('#locationareaName').val();
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		//data : { locationareaName : locationareaName, cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								if(result[i].countryName==countryName)
									{
									if(result[i].stateName==stateName)
										{
											if(result[i].cityName==cityName)
											{
												if(result[i].locationareaName==locationareaName)
												{
													 $('#bookingPincode').val(result[i].pinCode);
												}
											}
										}
									}
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getflatBasicCost()
{
	$('#flatCostSpan').html('');
	 var flatareainSqFt1 = $('#flatareainSqFt').val();
	 var flatCost1 = $('#flatCost').val();
	 var flatminimumCost1 = $('#flatminimumCost').val();
	 var floorRise1 = $('#floorRise').val();
	 var flatareainSqFt= parseInt(flatareainSqFt1);
	 var flatCost= parseInt(flatCost1);
	 var flatminimumCost= parseInt(flatminimumCost1);
	 
	 var floorRise= parseInt(floorRise1);
	 var flatCostwithotfloorise;
	 var flatBasiccost;
	 
	 if(flatCost<flatminimumCost)
		 {
		 flatBasiccost=flatareainSqFt*flatminimumCost;
		 flatCostwithotfloorise=flatminimumCost-floorRise;
		 document.bookingform.flatCost.value=flatminimumCost;
		 $('#flatCostSpan').html('Value should be '+flatminimumCost+' or above..!');
		 }
	 else
		 {
		 flatBasiccost=flatareainSqFt*flatCost;
		 flatCostwithotfloorise=flatCost-floorRise;
		 }
	 	document.bookingform.flatbasicCost.value=flatBasiccost;
		document.bookingform.flatCostwithotfloorise.value=flatCostwithotfloorise;
		calculate();
}
function calculate()
{
	
	 var flatbasicCost = Number($('#flatbasicCost').val());
	 var registrationPer = Number($('#registrationPer').val());
	 var stampDutyPer = Number($('#stampDutyPer').val());
	 
	var infrastructureCharge = Number($('#infrastructureCharge').val());
	var aggreementValue1 = flatbasicCost+infrastructureCharge;
	
	var gstCost =Number($('#gstCost').val());
	var stampDuty1 = (aggreementValue1/100)*stampDutyPer;
	var registrationCost2=0;
	if(aggreementValue1>3000000)
		{
		registrationCost2=30000;
		}
	else
		{
		registrationCost2= (aggreementValue1/100)*registrationPer;
		}
	var registrationCost1 =registrationCost2;
	
	
	var gstAmount1 = (aggreementValue1/100)*gstCost;
	var handlingCharges = Number($('#handlingCharges').val());
	var grandTotal1 = aggreementValue1+ stampDuty1+registrationCost1+handlingCharges+gstAmount1;
	var bookingAmount1=(aggreementValue1/100)*10;
	
	aggreementValue1=aggreementValue1.toFixed(0);
	registrationCost1=registrationCost1.toFixed(0);
	stampDuty1=stampDuty1.toFixed(0);
	grandTotal1=grandTotal1.toFixed(0);
	bookingAmount1=bookingAmount1.toFixed(0);
	gstAmount1=gstAmount1.toFixed(0);
	
	document.bookingform.gstAmount1.value=gstAmount1;
	document.bookingform.registrationCost1.value=registrationCost1;
	document.bookingform.handlingCharges.value=handlingCharges;
	document.bookingform.stampDuty1.value=stampDuty1;
	document.bookingform.aggreementValue1.value=aggreementValue1;
	document.bookingform.grandTotal1.value=grandTotal1;
	if(grandTotal1>5000000)
	{
	tds=(grandTotal1)/100;
	tds=tds.toFixed(0);
	}
else 
	{
	tds=0;
	}
document.bookingform.tds.value=tds;

}
function getParkingNumber()
{
	 
	 $("#parkingNumber").empty();
	 var projectName = $('#projectName').val();
	 var buildingName = $('#buildingName').val();
	 var wingName = $('#wingName').val();
	 var floortypeName = $('#parkingFloor').val();
	 
	 $.ajax({

			url : '${pageContext.request.contextPath}/getParkingNumber',
			type : 'Post',
			data : { projectName : projectName, buildingName : buildingName, wingName: wingName, floortypeName : floortypeName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select parking Number-");
								$("#parkingNumber").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].parkingNumber).text(result[i].parkingNumber);
								    $("#parkingNumber").append(option);
								} 
							} 
							else
							{
								alert("failure111");
							}

						}
			});	
}
function validate()
{ 
	
	clearall();
		//validation for first name
		if(document.bookingform.bookingfirstname.value=="")
		{
			$('#bookingfirstnameSpan').html('First name should not be empty..!');
			document.bookingform.bookingfirstname.focus();
			return false;
		}
		else if(document.bookingform.bookingfirstname.value.match(/^[\s]+$/))
		{
			$('#bookingfirstnameSpan').html('First name should not be empty..!');
			document.bookingform.bookingfirstname.value="";
			document.bookingform.bookingfirstname.focus();
			return false;
		}

	
			
		//validation for last name
		if(document.bookingform.bookinglastname.value=="")
		{
			 $('#bookinglastnameSpan').html('Last name should not be empty..!');
			document.bookingform.bookinglastname.focus();
			return false;
		}
		else if(document.bookingform.bookinglastname.value.match(/^[\s]+$/))
		{
			 $('#bookinglastnameSpan').html('Last name should not be empty..!');
			document.bookingform.bookinglastname.focus();
			return false;
		}
	
		
		
		//validation for address
		if(document.bookingform.bookingaddress.value=="")
		{
			 $('#bookingaddressSpan').html('Please, enter address..!');
			document.bookingform.bookingaddress.focus();
			return false;
		}
		else if(document.bookingform.bookingaddress.value.match(/^[\s]+$/))
		{
			 $('#bookingaddressSpan').html('Please, enter employee address name..!');
			document.bookingform.bookingaddress.focus();
			return false;
		}
	
		
		//validation for country name
		if(document.bookingform.countryName.value=="Default")
		{
			 $('#countryNameSpan').html('Please, select country name..!');
			document.bookingform.countryName.focus();
			return false;
		}
		
		//validation for state name
		if(document.bookingform.stateName.value=="Default")
		{
			 $('#stateNameSpan').html('Please, select state name..!');
			document.bookingform.stateName.focus();
			return false;
		}
		
		//validation for city name
		if(document.bookingform.cityName.value=="Default")
		{
			 $('#cityNameSpan').html('Please, select city name..!');
			document.bookingform.cityName.focus();
			return false;
		}
		
		//validation for location area name
		if(document.bookingform.locationareaName.value=="Default")
		{
			 $('#locationareaNameSpan').html('Please, select location name..!');
			document.bookingform.locationareaName.focus();
			return false;
		}
		
		
		
		//validation for email
		if(document.bookingform.bookingEmail.value=="")
		{
			 $('#bookingEmailSpan').html('Email Id should not be blank..!');
			document.bookingform.bookingEmail.focus();
			return false;
		}
		else if(!document.bookingform.bookingEmail.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			 $('#bookingEmailSpan').html(' enter valid email id..!');
			document.bookingform.bookingEmail.value="";
			document.bookingform.bookingEmail.focus();
			return false;
		}
		
		
		//validation for mobile number 1
		if(document.bookingform.bookingmobileNumber1.value=="")
		{
			 $('#bookingmobileNumber1Span').html('Please, enter primary mobile number..!');
			document.bookingform.bookingmobileNumber1.value="";
			document.bookingform.bookingmobileNumber1.focus();
			return false;
		}
		else if(!document.bookingform.bookingmobileNumber1.value.match(/^[(]{1}[+]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
		{
			 $('#bookingmobileNumber1Span').html(' enter valid primary mobile number..!');
			document.bookingform.bookingmobileNumber1.value="";
			document.bookingform.bookingmobileNumber1.focus();
			return false;	
		}
		
		//validation for mobile number 2
		if(document.bookingform.bookingmobileNumber2.value.length!=0)
		{
			if(!document.bookingform.bookingmobileNumber2.value.match(/^[(]{1}[+]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
			{
				 $('#bookingmobileNumber2Span').html(' enter valid secondary mobile number..!');
				document.bookingform.bookingmobileNumber2.value="";
				document.bookingform.bookingmobileNumber2.focus();
				return false;	
			}
		}
		
		//validation for pan card
		/*
		if(document.bookingform.bookingPancardno.value=="")
		{
			 $('#bookingPancardnoSpan').html('Please, enter PAN number..!');
			document.bookingform.bookingPancardno.value="";
			document.bookingform.bookingPancardno.focus();
			return false;
		}
		else if(!document.bookingform.bookingPancardno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
		{
			 $('#bookingPancardnoSpan').html(' PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
			document.bookingform.bookingPancardno.value="";
			document.bookingform.bookingPancardno.focus();
			return false;	
		}
		
		//validation for aadhar number
			
		if(document.bookingform.bookingAadharno.value=="")
		{
			 $('#bookingAadharnoSpan').html('Please, enter Aadhar number..!');
			document.bookingform.bookingAadharno.value="";
			document.bookingform.bookingAadharno.focus();
			return false;
		}
		else if(!document.bookingform.bookingAadharno.value.match(/^\d{4}\d{4}\d{4}$/))
		{
			 $('#bookingAadharnoSpan').html('Aadhar card number should be 12 digit number only..!');
			document.bookingform.bookingAadharno.value="";
			document.bookingform.bookingAadharno.focus();
			return false;	
		}
		
		*/
		//validation for occupation
		if(document.bookingform.bookingOccupation.value=="Default")
		{
			 $('#bookingOccupationSpan').html('Please, select proper occupation..!');
			document.bookingform.bookingOccupation.focus();
			return false;
		}
	/*	
		//validation for enquiry person company name
		if(document.bookingform.bookingcompanyName.value=="")
		{
			 $('#bookingcompanyNameSpan').html('Please, enter company name..!');
			document.bookingform.bookingcompanyName.focus();
			return false;
		}
		else if(document.bookingform.bookingcompanyName.value.match(/^[\s]+$/))
		{
			 $('#bookingcompanyNameSpan').html('Please, enter company name..!');
			document.bookingform.bookingcompanyName.value="";
			document.bookingform.bookingcompanyName.focus();
			return false;
		}
		else if(!document.bookingform.bookingcompanyName.value.match(/^[A-Za-z\s.]+[\s]{0,10}$/))
		{
			 $('#bookingcompanyNameSpan').html('Please, enter company name..!');
			document.bookingform.bookingcompanyName.value="";
			document.bookingform.bookingcompanyName.focus();
			return false;
		}
		
		//validation for enquiry person office number
		if(document.bookingform.bookingofficeNumber.value=="")
		{
			 $('#bookingofficeNumberSpan').html('Please, enter office phone number..!');
			document.bookingform.bookingofficeNumber.value="";
			document.bookingform.bookingofficeNumber.focus();
			return false;
		}
		else if(!document.bookingform.bookingofficeNumber.value.match(/^\(?([0-9]{3})\)?[\s. ]?([0-9]{3})[-. ]?([0-9]{5})$/))
		{
			 $('#bookingofficeNumberSpan').html(' phone number must be 10 digit numbers only..!');
			document.bookingform.bookingofficeNumber.value="";
			document.bookingform.bookingofficeNumber.focus();
			return false;	
		}
		
		*/
	
			
		//validation for project name-----------------------------------------------------
		if(document.bookingform.projectName.value=="Default")
		{
			 $('#projectNameSpan').html('Please, select project name..!');
			document.bookingform.projectName.focus();
			return false;
		}
		
		//validation for project building
		if(document.bookingform.buildingName.value=="Default")
		{
			 $('#buildingNameSpan').html('Please, select project building name..!');
			document.bookingform.buildingName.focus();
			return false;
		}
		//validation for project wing
		if(document.bookingform.wingName.value=="Default")
		{
			 $('#wingNameSpan').html('Please, select project wing name..!');
			document.bookingform.wingName.focus();
			return false;
		}
		//validation for floor
		if(document.bookingform.floortypeName.value=="Default")
		{
			 $('#floortypeNameSpan').html('Please, select floor..!');
			document.bookingform.floortypeName.focus();
			return false;
		}
		
		//validation for flat type
		if(document.bookingform.flatType.value=="Default")
		{
			 $('#flatTypeSpan').html('Please, select flat type..!');
			document.bookingform.flatType.focus();
			return false;
		}
		
		//validation for flat number
		if(document.bookingform.flatNumber.value=="Default")
		{
			 $('#flatNumberSpan').html('Please, select flat number..!');
			document.bookingform.flatNumber.focus();
			return false;
		}
	
	
		
}

function init()
{
	//$('#enquirydbStatusSpan').html('');
	//clearall();
/*
	$.ajax({

		url : '${pageContext.request.contextPath}/getGstCostList',
		type : 'Post',
		data : { },
		dataType : 'json',
		success : function(result)
				  {
			 var gstCost1=0;
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								gstCost1=result[result.length-1].taxPercentage;
							} 
						} 
						else
						{
							alert("failure111");
						}
						document.bookingform.gstCost.value=gstCost1;
					}
		});
*/
	 var n = new Date();
	 var y = n.getFullYear();
	 var m = n.getMonth()+1;
	 var d = n.getDate();
	 
	 document.getElementById("creationDate").value = d + "/"+ m + "/" + y;
	 
	 document.getElementById("updateDate").value = d + "/"+ m + "/" + y;
	 
	/* document.getElementById("userName").value = "Admin" ; 
	 if(document.bookingform.enquirydbStatus.value == "Fail")
	 {
		 $('#enquirydbStatusSpan').html(' record is present already..!');
	 }
	 else if(document.bookingform.enquirydbStatus.value == "Success")
	 {
	 $('#statusSpan').html('Record added successfully..!');
	
	 }
   // document.bookingform.enqfirstName.focus();
	*/ 
	
	if(( document.bookingform.bookingGender[1].checked == true ))
	  {
		 document.getElementById('maidenName1').style.display ='block';
	  }
	
	if(( document.bookingform.paymentMode[0].checked == true ))
	  {	
		  document.getElementById('cheque').style.display ='none';
		  document.bookingform.chequeNumber.value="";
	  }
	if(( document.bookingform.paymentMode[1].checked == true ))
	  {	
		  document.getElementById("labelchequeNumber").innerHTML="Cheque Number";
		  document.getElementById('cheque').style.display ='block';
	  }
	if(( document.bookingform.paymentMode[2].checked == true ))
	  {	
		  document.getElementById("labelchequeNumber").innerHTML="DD Number";
		  document.getElementById('cheque').style.display ='block';
	  }
	if(( document.bookingform.paymentMode[3].checked == true ))
	  {	
		  document.getElementById("labelchequeNumber").innerHTML="RTGS/NEFT/IMPS Number";
		  document.getElementById('cheque').style.display ='block';
	  }
	
	
	
	
}


function getBankCode()
{
	
	 $("#branchName").empty();
	 $("#bankifscCode").empty();
	 var bankName = $('#chequebankName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBankCode',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
								$('#branchName').val(result[0].branchName);
								$('#bankifscCode').val(result[0].bankifscCode);
							
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}


function getStateList()
{
	 $("#stateName").empty();
	 $("#cityName").empty();
	 $("#locationareaName").empty();
	 
	 var countryName = $('#countryName').val();

		$.ajax({

			url : '${pageContext.request.contextPath}/getStateList',
			type : 'Post',
			data : { countryName : countryName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{

								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Location Area-");
								$("#locationareaName").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select City-");
								$("#cityName").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select State-");
								$("#stateName").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].stateName).text(result[i].stateName);
								    $("#stateName").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		
}//end of get State List


function getCityList()
{
	
$("#cityName").empty();
$("#locationareaName").empty();
var stateName = $('#stateName').val();
var countryName = $('#countryName').val();
$.ajax({

	url : '${pageContext.request.contextPath}/getCityList',
	type : 'Post',
	data : { stateName : stateName, countryName : countryName},
	dataType : 'json',
	success : function(result)
			  {
					if (result) 
					{
						var option = $('<option/>');
						option.attr('value',"Default").text("-Select Location Area-");
						$("#locationareaName").append(option);
						
						var option = $('<option/>');
						option.attr('value',"Default").text("-Select City-");
						$("#cityName").append(option);
						
						for(var i=0;i<result.length;i++)
						{
							var option = $('<option />');
						    option.attr('value',result[i].cityName).text(result[i].cityName);
						    $("#cityName").append(option);
						 } 
					} 
					else
					{
						alert("failure111");
						//$("#ajax_div").hide();
					}

				}
	});
}//end of get City List

function getLocationAreaList()
{
	 $("#locationareaName").empty();
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaName).text(result[i].locationareaName);
							    $("#locationareaName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get locationarea List


function getWingNameList()
{
	 $("#wingName").empty();
	 $("#floortypeName").empty();
	 $("#flatNumber").empty();
	 $("#flatType").empty();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 document.bookingform.flatFacing.value="";
		document.bookingform.flatareainSqFt.value="";
		document.bookingform.flatbasicCost.value="";
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							var option = $('<option/>');

			                var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingName).text(result[i].wingName);
							    $("#wingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}

function getBuildingList()
{
	 $("#buildingName").empty();
	 $("#wingName").empty();
	 $("#floortypeName").empty();
	 $("#flatNumber").empty();
	 $("#flatType").empty();
	 document.bookingform.flatFacing.value="";
		document.bookingform.flatareainSqFt.value="";
		document.bookingform.flatbasicCost.value="";
		document.bookingform.flatbasicCost.value="";
		 document.bookingform.flatCostwithotfloorise.value="";
	 var projectName = $('#projectName').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList1',
		type : 'Post',
		data : { projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building-");
							$("#buildingName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingName).text(result[i].buildingName);
							    $("#buildingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});

	$("#parkingFloor").empty();
	$.ajax({

		url : '${pageContext.request.contextPath}/getParkingFloor',
		type : 'Post',
		data : { projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
						
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Parking Floor-");
							$("#parkingFloor").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#parkingFloor").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Building List  


function getFloorNameList()
{
	 $("#floortypeName").empty();
	 $("#flatNumber").empty();
	 $("#flatType").empty();
	 var wingName = $('#wingName').val();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 document.bookingform.flatFacing.value="";
		document.bookingform.flatareainSqFt.value="";
		document.bookingform.flatbasicCost.value="";
		document.bookingform.flatCostwithotfloorise.value="";
		document.bookingform.floorRise.value="";
	 $.ajax({

		url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingName : wingName, buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);
						
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#floortypeName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}

function getFlatNumberList()
{
	document.bookingform.flatFacing.value="";
	document.bookingform.flatareainSqFt.value="";
	document.bookingform.flatbasicCost.value="";
	document.bookingform.flatCostwithotfloorise.value="";
	document.bookingform.floorRise.value="";
	 $("#flatNumber").empty();
	// var flatType = $('#flatType').val();
	 var floortypeName = $('#floortypeName').val();
		var wingName = $('#wingName').val();
		var buildingName = $('#buildingName').val();
		var projectName = $('#projectName').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getFlatNumberList',
		type : 'Post',
		data : {floortypeName: floortypeName ,wingName : wingName, buildingName : buildingName, projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].flatNumber).text(result[i].flatNumber);
							    $("#flatNumber").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Flat Number List   

/*
function getflattype()
{	
	document.enquiryform.flatFacing.value="";
document.enquiryform.flatareainSqFt.value="";
document.enquiryform.flatbasicCost.value="";
$("#flatNumber").empty();
$("#flatType").empty();

var option = $('<option/>');
option.attr('value',"Default").text("-Select Flat Number-");
$("#flatNumber").append(option);

var option = $('<option/>');
option.attr('value',"Default").text("-Select Flat Type-");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"RK").text("RK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"1 BHK").text("1 BHK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"2 BHK").text("2 BHK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"3 BHK").text("3 BHK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"4 BHK").text("4 BHK");
$("#flatType").append(option);

}
*/
function getFlatFacing()
{

	document.bookingform.flatFacing.value="";
	document.bookingform.flatareainSqFt.value="";
	document.bookingform.flatbasicCost.value="";
	document.bookingform.flatCost.value="";
	document.bookingform.flatminimumCost.value="";
	document.bookingform.flatType.value="";
	document.bookingform.flatCostwithotfloorise.value="";
	document.bookingform.floorRise.value="";
	var flatNumber = $('#flatNumber').val();
	var wingName = $('#wingName').val();
	var buildingName = $('#buildingName').val();
	var projectName = $('#projectName').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getFlatDetail',
		type : 'Post',
		data : {flatNumber : flatNumber , wingName : wingName, buildingName : buildingName, projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
						  for(var i=0;i<result.length;i++)
						  {
							  $('#flatFacing').val(result[i].flatfacingName);
							  $('#flatareainSqFt').val(result[i].flatAreawithLoadingInFt);
							  $('#flatbasicCost').val(result[i].flatbasicCost);
							  $('#flatCost').val(result[i].flatCost); 
							  $('#flatminimumCost').val(result[i].flatminimumCost); 
							  $('#flatType').val(result[i].flatType); 
							  $('#flatCostwithotfloorise').val(result[i].flatCostwithotfloorise); 
							  $('#floorRise').val(result[i].floorRise);
						  }
							
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Flat Number List


$(function () 
 {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    
    //Date picker
    $('#secondDob').datepicker({
      autoclose: true
    })
 $('#datepicker').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
