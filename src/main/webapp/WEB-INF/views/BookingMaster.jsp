<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Booking Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
  <!-- Font Awesome -->
  <!-- Ionicons -->
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <style type="text/css">
   tr.odd {background-color:#F0F8FF}
tr.even {background-color: #CCE5FF}
    </style>
    
   
    
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Booking Master Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Booking Master</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="bookingmasterform" action="${pageContext.request.contextPath}/BookingMaster" method="post">
   
      <section class="content">
      <div class="box box-default">
      
      <div class="box-body">
      <div class="row">
      <div class="col-xs-12">
      <div class="box-body">
          <div class="row">
         <%-- <div class="col-xs-2">
		<div class="input-group">
          <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="text" class="form-control pull-right" id="datepicker1" name="datepicker1">
                </div>
          		<span class="input-group-btn">
                <button type="button" name="search" id="search-btn" class="btn btn-flat" onclick="return Search()"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
        <span id="datepicker1Span" style="color:#FF0000"></span>
       </div> --%>
       
       <div class="col-xs-3">
       
       </div>
         <div class="col-xs-6">
       <a href="AddNewBooking"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Booking</button></a>
      </div>
      
      </div>
      </div>
       </div>
        <div class="col-xs-12">
       
        </br> </br>
        
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#BookingRemainingList" data-toggle="tab">Booking Remaining List</a></li>
              <li><a href="#AllBookingList" data-toggle="tab">All Booking List</a></li>
                <li><a href="#AllAggreementList" data-toggle="tab">All Aggreement List</a></li>
              <li><a href="#AllCustomerExtraChargesList" data-toggle="tab">All Customer Extra Charges List</a></li>
             <li><a href="#AllCustomerLoanList" data-toggle="tab">All Customer Loan List</a></li>
             <li><a href="#AllBookingCancelList" data-toggle="tab">All Booking Cancel List</a></li>
            </ul>
            
            
            
            <div class="tab-content">
              <div class="tab-pane active" id="BookingRemainingList">
                <section >
              <h4 class="page-header">Booking Remaining List</h4>
              <div  class="panel box box-danger">
              </br>
              <div class="table-responsive" >
        	  <table id="BookingRemainingListTable" class="table table-bordered">
                  <thead>
                  <tr bgcolor="#4682B4">
                  <th>Enquiry Id</th>
                  <th>Name</th>
				 
                  <th>Email Id</th>
				  <th>Mobile No</th>
				
				  <th>Flat Type</th>
				  <th>Status</th>
				  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody >
                   <c:forEach items="${enquiryList}" var="enquiryList" varStatus="loopStatus">
                  	<tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                   <td>${enquiryList.enquiryId}</td>
		               <td>${enquiryList.enqfirstName} ${enquiryList.enqmiddleName} ${enquiryList.enqlastName}</td>
		               
		               <td>${enquiryList.enqEmail}</td>
		               <td>${enquiryList.enqmobileNumber1}</td>
		              
		               <td>${enquiryList.flatType}</td>
		               <td>${enquiryList.enqStatus} Remaining</td>
		               <td><a href="${pageContext.request.contextPath}/AddBooking?enquiryId=${enquiryList.enquiryId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="For Booking"><i class="glyphicon glyphicon-edit"></i></a></td>
                     </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
                 </div> 
                </section>
			 </div>
		    
                      
              <div class="tab-pane" id="AllBookingList">
                <section >
              <h4 class="page-header">All Booking List</h4>
              <div  class="panel box box-danger">
              </br>
              <div class="table-responsive" >
            <table id="bookingListTable" class="table table-bordered">
                  <thead>
                  <tr bgcolor="#4682B4">
                  <th>Booking Id</th>
                  <th style="width:90px">Name</th>
                  <th>Email Id</th>
				  <th style="width:90px">Mobile No</th>
				  <th>Project </th>
				  <th>Building </th>
				  <th>Wing </th>
				  <th>Flat Number</th>
				  <th style="width:350px">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                   <c:forEach items="${bookingList}" var="bookingList" varStatus="loopStatus">
                   <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                   <td>${bookingList.bookingId}</td>
	               <td>${bookingList.bookingfirstname} ${bookingList.bookinglastname}</td>
	               <td>${bookingList.bookingEmail}</td>
	               <td>${bookingList.bookingmobileNumber1}</td>
	               <td>${bookingList.projectName}</td>
	               <td>${bookingList.buildingName}</td>
	               <td>${bookingList.wingName}</td>
	               <td>${bookingList.flatType}-${bookingList.flatNumber}</td>
	               <td>
	                  <a href="${pageContext.request.contextPath}/EditBooking?bookingId=${bookingList.bookingId}" class="btn btn-success btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
		               	
		               	<c:forEach var="loanList" items="${loanList}">
                       		 <c:choose>
                                <c:when test="${loanList.bookingId eq bookingList.bookingId}">
	                    		  <c:set var = "flag1" scope = "session" value = "${1}"/>
	                            </c:when>
	                         </c:choose>
	                    	</c:forEach>
		                    <c:if test = "${flag1 !=1}">
         					<a href="${pageContext.request.contextPath}/CustomerLoanDetails?bookingId=${bookingList.bookingId}" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Add Loan"><i class="fa fa-fw fa-bank"></i></a>
      						</c:if>
		                <c:set var = "flag1" scope = "session" value = "${0}"/>
		               <a href="${pageContext.request.contextPath}/BookingCancellationform?bookingId=${bookingList.bookingId}" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Booking Cancel"><i class="glyphicon glyphicon-remove"></i></a>
		                   <c:forEach var="extrachargesList" items="${extrachargesList}">
                       		 <c:choose>
                                <c:when test="${extrachargesList.bookingId eq bookingList.bookingId}">
	                    		  <c:set var = "flag2" scope = "session" value = "${1}"/>
	                            </c:when>
	                         </c:choose>
	                    	</c:forEach>
	                     <c:if test = "${flag2 !=1}">
		               <a href="${pageContext.request.contextPath}/AddExtraCharges?bookingId=${bookingList.bookingId}" class="btn bg-olive btn-sm" data-toggle="tooltip" title="Add Extra Charges"><i class="glyphicon glyphicon-plus"></i></a>
		              </c:if>
		              <c:set var = "flag2" scope = "session" value = "${0}"/>
		                <a href="${pageContext.request.contextPath}/FlatBookingLetter?bookingId=${bookingList.bookingId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Booking Letter"><span class="glyphicon glyphicon-print"></span></a>
		             	<c:forEach var="aggreementList" items="${aggreementList}">
                       		 <c:choose>
                                <c:when test="${aggreementList.bookingId eq bookingList.bookingId}">
	                    		  <c:set var = "flag3" scope = "session" value = "${1}"/>
	                            </c:when>
	                         </c:choose>
	                    	</c:forEach>
		                    <c:if test = "${flag3 !=1}">
         			    <a href="${pageContext.request.contextPath}/AddAggreementForm?bookingId=${bookingList.bookingId}" class="btn bg-purple btn-sm" data-toggle="tooltip" title="To Add Aggreement"><i class="glyphicon glyphicon-plus"></i></a>
		         	   |<a href="${pageContext.request.contextPath}/CustomerReceiptForm?bookingId=${bookingList.bookingId}" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add Payment"><span class="glyphicon glyphicon-piggy-bank"></span></a>
	               	   |<a href="${pageContext.request.contextPath}/CustomerPaymentHistory?bookingId=${bookingList.bookingId}" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Receipt"><span class="glyphicon glyphicon-print"></span></a>
		         	  </c:if>
		         	  <c:set var = "flag3" scope = "session" value = "${0}"/>
		        
	               </td>
                      </tr>
				   </c:forEach>
                 </tbody>
                </table>
    			 </div>
                 </div> 
                </section>
			 </div>
 
 
 
 <div class="tab-pane" id="AllAggreementList">
       <section>
              <h4 class="page-header">All Customer Aggreement List</h4>
              <div  class="panel box box-danger">
              </br>
              <div class="table-responsive">
        	  <table id="AllAggreementCustomerList" class="table table-bordered">
                  <thead>
                  <tr bgcolor="#4682B4">
                  <th style="width:100px">Id</th>
                  <th style="width:120px">Booking Id</th>
                  <th>Aggreement Name</th>
				  <th>Aggreement Date</th>
				  <th style="width:450px">Action</th>
                  </tr>
                  </thead>
                  <tbody >
                   <c:forEach items="${aggreementList}" var="aggreementList" varStatus="loopStatus">
                   <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                   <td>${aggreementList.aggreementId}</td>
	                   <td>${aggreementList.bookingId}</td>
	                   <td>${aggreementList.firstApplicantfirstname} ${aggreementList.firstApplicantmiddlename} ${aggreementList.firstApplicantlastname}</td>
	                   <td>${aggreementList.creationDate}</td>
		               
		             <td>
		               	<a href="${pageContext.request.contextPath}/EditAggreementForm?aggreementId=${aggreementList.aggreementId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
		              <%--  	<a href="${pageContext.request.contextPath}/ViewAggreementForm?aggreementId=${aggreementList.aggreementId}" class="btn bg-purple btn-sm" data-toggle="tooltip" title="Edit"><i class="fa fa-fw fa-street-view"></i></a>
		               --%>
		               |<a href="${pageContext.request.contextPath}/GeneratePaymentScheduler?bookingId=${aggreementList.bookingId}" class="btn bg-orange btn-sm" data-toggle="tooltip" title="Generate Payment Schedule"><i class="glyphicon glyphicon-plus"></i></a>
		               |<a href="${pageContext.request.contextPath}/DemandLetter?bookingId=${aggreementList.bookingId}" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Demand Letter"><span class="glyphicon glyphicon-print"></span></a>
	              
	               <c:forEach var="CustomercheckList" items="${CustomercheckList}">
                       	<c:choose>
                        <c:when test="${CustomercheckList.bookingId eq aggreementList.bookingId}">
	                    <c:set var = "flag4" scope = "session" value = "${1}"/>
	                     </c:when>
	                     </c:choose>
	                    	</c:forEach>
		                    <c:if test = "${flag4 ==1}">
         					|<a href="${pageContext.request.contextPath}/EditCustomerFlatCheckList?bookingId=${aggreementList.bookingId}" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Check List"><i class="fa fa-fw fa-check"></i></a>
      						|<a href="${pageContext.request.contextPath}/CustomerFlatCheckListPrint?bookingId=${aggreementList.bookingId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Print Check List"><i class="glyphicon glyphicon-print"></i></a>
      						</c:if> 
			              	<c:if test = "${flag4 !=1}">
			              	 |<a href="${pageContext.request.contextPath}/CustomerFlatCheckList?bookingId=${aggreementList.bookingId}" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Check List"><i class="fa fa-fw fa-check"></i></a>
			              	 </c:if> 
			              	 <c:set var = "flag4" scope = "session" value = "${0}"/>
			               	 
		               |<a href="${pageContext.request.contextPath}/CustomerReceiptForm?bookingId=${aggreementList.bookingId}" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add Payment"><span class="glyphicon glyphicon-piggy-bank"></span></a>
	               	   |<a href="${pageContext.request.contextPath}/CustomerPaymentHistory?bookingId=${aggreementList.bookingId}" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Receipt"><span class="glyphicon glyphicon-print"></span></a>
	               
		               </td> 
	               </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
              </div>
            </section>
		</div>
 
 
 <div class="tab-pane" id="AllCustomerExtraChargesList">
       <section>
              <h4 class="page-header">All Customer Extra Charges List</h4>
              <div  class="panel box box-danger">
              </br>
              <div class="table-responsive">
        	  <table id="AllExtraChargesList" class="table table-bordered">
                  <thead>
                  <tr bgcolor="#4682B4">
                  <th>Id</th>
                  <th>Booking Id</th>
                  <th>Name</th>
                  <th>Booking Date</th>
				  <th>Aggreement Value</th>
                  <th>Maintenance Cost</th>
				  <th>Parking Charges</th>
				  <th>Other Charges</th>
				  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody >
                   <c:forEach items="${extrachargesList}" var="extrachargesList" varStatus="loopStatus">
                   <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                   <td>${extrachargesList.chargesId}</td>
	                   <td>${extrachargesList.bookingId}</td>
	                   <td>${extrachargesList.bookingDate}</td>
	                   <td>${extrachargesList.customerName}</td>
		               <td>${extrachargesList.aggreementValue1} </td>
		               <td>${extrachargesList.maintenanceCost}</td>
		               <td>${extrachargesList.parkingCharges}</td>
		               <td>${extrachargesList.otherCharges}</td>
		               <td>
		               	   <a href="${pageContext.request.contextPath}/EditExtraCharges?chargesId=${extrachargesList.chargesId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
		               	  |<a href="${pageContext.request.contextPath}/MaintenanceReceipt?chargesId=${extrachargesList.chargesId}" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Print"><span class="glyphicon glyphicon-print"></span></a>
		               </td>
	               </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
              </div>
            </section>
		</div>
		
<div class="tab-pane" id="AllCustomerLoanList">
       <section>
              <h4 class="page-header">All Customer Loan List</h4>
              <div  class="panel box box-danger">
              </br>
              <div class="table-responsive">
        	  <table id="AllLoanList" class="table table-bordered">
                  <thead>
	                  <tr bgcolor="#4682B4">
		                  <th>Loan Id</th>
		                  <th>Booking Id</th>
		                  <th>Customer Name</th>
		                  <th>Total Amount</th>
						  <th>Aggreement Amount</th>
						  <th>Loan Amount</th>
		                  <th>Remaining Amount</th>
						  <th>Bank Name</th>
						  <th>Action</th>
	                  </tr>
                  </thead>
                  <tbody>
                  <c:forEach items="${loanList}" var="loanList" varStatus="loopStatus">
                   <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                   <td>${loanList.customerloanId}</td>
	                   <td>${loanList.bookingId}</td>
		               <td>${loanList.customerName}</td>
		               <td>${loanList.totalAmount}</td>
		               <td>${loanList.aggreementValue1}</td>
		               <td>${loanList.loanAmount}</td>
		               <td>${loanList.remainingAmount}</td>
		               <td>${loanList.bankName}</td>
		               <td><a href="${pageContext.request.contextPath}/EditCustomerLoanDetails?bookingId=${loanList.bookingId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Loan View"><i class="glyphicon glyphicon-eye-open"></i></a>
		               </td>
	             	</tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
              </div>
            </section>
		</div>
		
<div class="tab-pane" id="AllBookingCancelList">
       <section>
              <h4 class="page-header">All Booking Cancel List</h4>
              <div  class="panel box box-danger">
              </br>
              <div class="table-responsive">
        	  <table id="AllCancelList" class="table table-bordered">
                  <thead>
                  <tr bgcolor="#4682B4">
              	  <th>Booking Id</th>
                  <th>Customer Name</th>
                  <th>Email Id</th>
                  <th>Mobile Number</th>
				  <th>Booking Date</th>
				  <th>Cancel Date</th>
				  <th>Aggreement Value </th>
				  <th>Cancel Charge</th>
				  <th style="width:90px">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                   <c:forEach items="${cancelList}" var="cancelList" varStatus="loopStatus">
                   <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                   <td>${cancelList.bookingId}</td>
                   <td>${cancelList.customerName}</td>
                   <td>${cancelList.customerEmail}</td>
                   <td>${cancelList.customerMobile}</td>
	               <td>${cancelList.bookingDate} </td>
	               <td>${cancelList.cancelDate}</td>
	               <td>${cancelList.aggreementValue1}</td>
	               <td>${cancelList.cancelCharge}</td>
	               <td><a href="${pageContext.request.contextPath}/CancelAgainBooking?bookingId=${cancelList.bookingId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Again Booking"><i class="glyphicon glyphicon-edit"></i></a>
	               </td>
	               </tr>
				   </c:forEach>
                 </tbody>
                </table>
              </div>
              </div>
            </section>
		</div>		
	</div>
   </div>
 </div>
  </div>
  </div>
  </div>
  </section>
</form>
 </div>
  <%@ include file="footer.jsp" %>
 <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
       
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Bootstrap 3.3.7 -->
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script>
function Search()
{
	var creationDate = $('#datepicker1').val();
	$("#bookingListTable tr").detach();
		
	$.ajax({

		url : '${pageContext.request.contextPath}/getDateWiseBookingList',
		type : 'Post',
		data : { creationDate : creationDate},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#bookingListTable').append('<tr style="background-color: #4682B4;"><th>Booking Id</th><th >Name</th><th>Email Id</th><th>Mobile No</th><th>Project name</th><th>Building Name</th><th>Wing name</th><th>Floor</th><th>Flat Type</th><th>Flat Number</th><th style="width:350px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].bookingId;
									$('#bookingListTable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].bookingId+'</td><td>'+result[i].bookingfirstname+' '+result[i].bookingmiddlename+' '+result[i].bookinglastname+'</td><td>'+result[i].bookingEmail+'</td><td>'+result[i].bookingmobileNumber1+'</td><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].floortypeName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatNumber+'</td><td><a href="${pageContext.request.contextPath}/EditBooking?bookingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
								else
								{
									var id = result[i].bookingId;
									$('#bookingListTable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].bookingId+'</td><td>'+result[i].bookingfirstname+' '+result[i].bookingmiddlename+' '+result[i].bookinglastname+'</td><td>'+result[i].bookingEmail+'</td><td>'+result[i].bookingmobileNumber1+'</td><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].floortypeName+'</td><td>'+result[i].flatType+'</td><td>'+result[i].flatNumber+'</td><td><a href="${pageContext.request.contextPath}/EditBooking?bookingId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	
	
}
$(function () {
  //Initialize Select2 Elements
  $('.select2').select2()

  //Datemask dd/mm/yyyy
  $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
  //Datemask2 mm/dd/yyyy
  $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
  //Money Euro
  $('[data-mask]').inputmask()

  //Date range picker
  $('#reservation').daterangepicker()
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
  //Date range as a button
  $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
  )

  //Date picker
  $('#datepicker1').datepicker({
    autoclose: true
  })

  //iCheck for checkbox and radio inputs
  $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass   : 'iradio_minimal-blue'
  })
  //Red color scheme for iCheck
  $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
    checkboxClass: 'icheckbox_minimal-red',
    radioClass   : 'iradio_minimal-red'
  })
  //Flat red color scheme for iCheck
  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass   : 'iradio_flat-green'
  })

  //Colorpicker
  $('.my-colorpicker1').colorpicker()
  //color picker with addon
  $('.my-colorpicker2').colorpicker()

  //Timepicker
  $('.timepicker').timepicker({
    showInputs: false
  })
})
$(function () {
    $('#BookingRemainingListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
$(function () {
    $('#bookingListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
  $(function () {
    $('#AllExtraChargesList').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
   $(function () {
    $('#AllLoanList').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
  $(function () {
    $('#AllCancelList').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
    $(function () {
    $('#AllAggreementCustomerList').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
