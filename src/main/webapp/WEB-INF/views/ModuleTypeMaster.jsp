<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PEEP | ModuleType Master</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <section class="content-header">
      <h1>
        Module Type Master Details:
     
      </h1>
      
    </section>
    <!-- Main content -->
	<form name="modulemasterform" action="${pageContext.request.contextPath}/ModuleTypeMaster" onSubmit="return validate()" method="post">

    <section class="content">
	 <div class="box-body">
       
		   	 
			     <div class="box-body">
              <div class="row">
                 
				  <div class="col-xs-3">
				
            	 </div>
            	 
				<div class="col-xs-3">
			 		&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
			 		<a href="ModuleTypeMaster">  <button type="button" class="btn btn-default" value="reset" style="width:90px"> Reset</button></a>
                </div> 
              
              	<div class="col-xs-2">
			        <a href="AddModuleDetails"> <button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add New Module Type</button></a>
            	</div>
			    <!--  <div class="col-xs-3">
			       <a href="ImportNewCountry">  <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"> Import From Excel File</button></a>
                </div> -->  
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
              
         </div>
      <div class="row">
        <div class="col-xs-12">
      
      <div class="box" >
            <div class="box-header">
              <h3 class="box-title">Module List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="countryListTable" class="table table-bordered">
                <thead>
                <tr bgcolor="#4682B4">
                    <th style="width:300px">Module Name</th>
                    
                    <th style="width:50px">Action</th>
                      </tr>
                </thead>
                <tbody>
             
                  <c:forEach items="${moduleTypeList}" var="moduleTypeList" varStatus="loopStatus">
                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${moduleTypeList.moduleType}</td>
                        <td><a href="${pageContext.request.contextPath}/EditModuleType?moduleId=${moduleTypeList.id}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                        <a onclick="DeleteModuleType('${moduleTypeList.id}')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>
                 
                        </td>
                      </tr>
				   </c:forEach>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
 <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#countryListTable').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  


function DeleteModuleType(moduleTypeId)
{
	//alert("hii");
    var moduleTypeId = moduleTypeId;
    
    $('#countryListTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteModuleType',
		type : 'Post',
		data : {moduleTypeId : moduleTypeId},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
						/* 	$('#countryListTable').append('<tr style="background-color: #4682B4;"><th style="width:300px">Module Name</</th><th style="width:150px">Activity Name</</th><th style="width:150px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].id;
									$('#countryListTable').append('<tr class="even"><td>'+result[i].moduleType+'</td><td>'+result[i].activityName+'</td><td><a href="${pageContext.request.contextPath}/EditModuleType?moduleId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeletePatientInformation('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].id;
									$('#countryListTable').append('<tr class="odd"><td>'+result[i].moduleType+'</td><td>'+result[i].activityName+'</td><td><a href="${pageContext.request.contextPath}/EditModuleType?moduleId='+id+'" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>|<a onclick="DeletePatientInformation('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
										}
							
							 }  */
						   window.location = "${pageContext.request.contextPath}/ModuleTypeMaster";
						} 
						else
						{
							alert("failure111");
						}
					   $('#countryListTable').DataTable()
					      $('#example2').DataTable({
						      'paging'      : true,
						      'lengthChange': false,
						      'searching'   : false,
						      'ordering'    : true,
						      'info'        : true,
						      'autoWidth'   : false
						    })
				  } 

		});

}
</script>
</body>
</html>
