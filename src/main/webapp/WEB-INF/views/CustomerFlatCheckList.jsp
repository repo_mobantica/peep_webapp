<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Customer Flat CheckList</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- Google Font -->
    <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>


<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customer Flat CheckList Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Customer Flat CheckList</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="customerflatchecklistform" action="${pageContext.request.contextPath}/CustomerFlatCheckList" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
          
							
		 <div class="box-body">
              <div class="row">
               <div class="col-md-2">
                  <label>Ref No:</label>
                  <input type="text" class="form-control" id="checkListId" name="checkListId" value="${checklistCode}" readonly>
                </div>
        	     <div class="col-md-3">
        	     
        	     </div>
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h3><span class="fa fa-home"></span> 
                    <label>CHECK LIST:  </label> 
                   </h3> 
       		 	  </div>
			     </div> 
			      <div class="col-md-4">
                
                </div>
              </div>
              </div>
        	<input type="hidden" id="bookingId" name="bookingId" value="${bookingDetails[0].bookingId}">
        	<input type="hidden" id="aggreementId" name="aggreementId" value="${aggreementDetails[0].aggreementId}">
     <div class="box-body">
              <div class="row">
                   <div class="col-xs-3">
                   	<div class="input-group">
                 	<h4><span class="fa fa-university"></span> 
                   	<label >Project Name : ${bookingDetails[0].projectName}</label> 
                   	</h4> 
       		 		</div>
			      </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-certificate"></span> 
                    <label>Building Name : ${bookingDetails[0].buildingName} </label> 
                   </h4> 
       		 	  </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-cube"></span> 
                    <label>Wing Name : ${bookingDetails[0].wingName} </label> 
                    </h4> 
       		 	   </div>
			     </div>  
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-align-center"></span> 
                    <label >Floor Name : ${bookingDetails[0].floortypeName} </label> 
                    </h4> 
       		 	   </div>
			     </div>
			     
			     <div class="col-xs-3">
                   <div class="input-group">
                 	<h4><span class="fa fa-home"></span> 
                    <label >Flat Number : ${bookingDetails[0].flatNumber} </label> 
                    </h4> 
       		 	   </div>
			     </div>  
			     
				
			      
              </div>
            </div>   	
        	
        	<div class="box-body">
        	
              <div class="row">
                <div class="col-xs-3">
                </div>
              <div class="col-xs-5">
              <div class="table-responsive">
              <table id="checklisttable" class="table table-bordered table-striped">

                  <thead>
	                  <tr bgcolor=#4682B4>
	                  	<td style="width:50px"><b>Sr.No</b></td>
	                    <td style="width:200px"><b>ITEM INSPECTED</b></td>
	                    <td style="width:50px"><b>REMARKS</b></td>
	                  </tr>
                  </thead>
                  <tbody>
                      <tr>
	                      <td>1</td>
	                      <td>CEILINGS</td>
	                      <td><input type="checkbox" name="ceilings" id="ceilings" value="CEILINGS"></td>
	               	  </tr>
	               	  <tr>
	                      <td>2</td>
	                      <td>TILES</td>
	                      <td><input type="checkbox" name="tiles" id="tiles" value="TILES"></td>
	               	  </tr>
	               	  <tr>
	                      <td>3</td>
	                      <td>DOOR</td>
	                      <td><input type="checkbox" name="door" id="door" value="DOOR"></td>
	               	  </tr>
	               	  <tr>
	                      <td>4</td>
	                      <td>WINDOWS</td>
	                      <td><input type="checkbox" name="windows" id="windows" value="WINDOWS"></td>
	               	  </tr>
	               	  <tr>
	                      <td>5</td>
	                      <td>PLUMBING & SANITORY</td>
	                      <td><input type="checkbox" name="plumbingAndsanitory" id="plumbingAndsanitory" value="PLUMBING & SANITORY"></td>
	               	  </tr>
	               	  <tr>
	                      <td>6</td>
	                      <td>ELECTRIFICATION</td>
	                      <td><input type="checkbox" name="electrification" id="electrification" value="ELECTRIFICATION"></td>
	               	  </tr>
	               	  <tr>
	                      <td>7</td>
	                      <td>PAINTING(INTERNAL)</td>
	                      <td><input type="checkbox" name="painting" id="painting" value="PAINTING"></td>
	               	  </tr>
                 
                 </tbody>
                </table>
                </div>
                </div>
              </div>
            </div>
        	
		 <div class="box-body">
           <div class="row">
            <div class="col-md-3">
               <label>Main Door Key No</label> 
               <input type="text" class="form-control" id="mainDoorKeyNo" name="mainDoorKeyNo" placeholder="Main Door Key No">
                  <span id="mainDoorKeyNoSpan" style="color:#FF0000"></span>
             </div>
              <div class="col-md-3">
               <label>Inner Door Key No</label>
               <input type="text" class="form-control" id="innerDoorKeyNo" name="innerDoorKeyNo" placeholder="Inner Door Key No">
                  <span id="innerDoorKeyNoSpan" style="color:#FF0000"></span>
             </div>
              <div class="col-md-3">
               <label>Electric Bill Meter No</label> 
               <input type="text" class="form-control" id="electricBillMeterNo" name="electricBillMeterNo" placeholder="Electric Bill Meter No">
                  <span id="electricBillMeterNoSpan" style="color:#FF0000"></span>
             </div>
         	</div>
            </div>
            <div class="panel box box-danger"></div>
            <div class="box-body">
           <div class="row">
           <div class="col-md-3">
           	<h4><span class="fa fa-cube"></span> 
                <label>To Add More </label> 
              </h4> 
             </div>
           </div>
           </div>
           
             <div class="box-body">
           <div class="row">
            <div class="col-md-3">
               <label>Type</label> 
               <input type="text" class="form-control" id="type" name="type" placeholder="Type">
                  <span id="electricBillMeterNoSpan" style="color:#FF0000"></span>
           </div>
       
           <div class="col-md-3">
            <label>Details</label> 
	  			<div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-key"></i>
                  </div>
                  <input type="text" class="form-control" id="details" name="details" placeholder="Details">
                <span class="input-group-btn">
            	    <button type="button" class="btn btn-success" onclick="return AddMoreDetails()"><i class="fa fa-plus"></i>Add</button>
              	   </span>
                </div>
           </div>
           </div>
           </div>
           
           <div class="box-body">
           <div class="row">        
        	<div class="col-xs-12">
              <table class="table table-bordered" id="addMoreDetialsTable">
	              <tr bgcolor=#4682B4>
		              <th>Sr.No</th>
		              <th>Type</th>
		              <th>Details</th>
		              <th>Action</th>
	               </tr>

               	   <%-- <c:forEach items="${bankEmployeesList}" var="bankEmployeesList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
                        <td>${bankEmployeesList.employeeName}</td>
                        <td>${bankEmployeesList.employeeDesignation}</td>
                        <td>${bankEmployeesList.employeeEmail}</td>
                        <td>${bankEmployeesList.employeeMobileno}</td>
                        <td>
                        	<a href="${pageContext.request.contextPath}/EditBankEmployee?bankId=${bankEmployeesList.bankId}" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
                           |<a onclick="DeleteBankEmployee()" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                     </tr>
                    </c:forEach> --%>
              </table>
            </div>
           </div>
          </div>
            <div class="box-body">
            <div class="row">
            
                   
				<input type="hidden" id="creationDate" name="creationDate" value="">
				<input type="hidden" id="updateDate" name="updateDate" value="">
				<input type="hidden" id="userName" name="userName" value="">
              <!-- /.form-group -->
            </div>
            </div>
             </div>
    
            
            <!-- /.col -->
			
          </div>
		   	  	     <div class="box-body">
              <div class="row">
                      <div class="col-xs-4">
                	<a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-3">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
    
      </section>
	</form>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
<script>
function clearall()
{
	$('#locationareaNameSpan').html('');
	$('#cityNameSpan').html('');
	$('#stateNameSpan').html('');
	$('#countryNameSpan').html('');
	$('#pinCodeSpan').html('');
}

function validate()
{ 
	/* clearall();
		if(document.locationareaform.countryName.value=="Default")
		{
			$('#countryNameSpan').html('Please, select country name..!');
			document.locationareaform.countryName.focus();
			return false;
		}
		
		if(document.locationareaform.stateName.value=="Default")
		{
			$('#stateNameSpan').html('Please, select state name..!');
			document.locationareaform.stateName.focus();
			return false;
		}
		
		if(document.locationareaform.cityName.value=="Default")
		{
			$('#cityNameSpan').html('Please, select city name..!');
			document.locationareaform.cityName.focus();
			return false;
		}
		
		//validation for the location area name
		if(document.locationareaform.locationareaName.value=="")
		{
			//$('#locationareaNameSpan').html('Please, enter location area name..!');
			document.locationareaform.locationareaName.value="";
			document.locationareaform.locationareaName.focus();
			return false;
		}
		else if(document.locationareaform.locationareaName.value.match(/^[\s]+$/))
		{
			$('#locationareaNameSpan').html('Please, enter location area name..!');
			document.locationareaform.locationareaName.value="";
			document.locationareaform.locationareaName.focus();
			return false; 	
		}
	
		  //validation for pincode
		if(document.locationareaform.pinCode.value=="")
		{
			$('#pinCodeSpan').html('Please, enter valid 6 digit pincode');
			document.locationareaform.pinCode.focus();
			return false;
		}
		 */
}

function AddMoreDetails()
{

	//clearall();
	
	//validation for employee name
	if(document.customerflatchecklistform.type.value=="")
	{
		$('#typeSpan').html('Type name should not be empty..!');
		document.customerflatchecklistform.type.value="";
		document.customerflatchecklistform.type.focus();
		return false;
	}
	else if(document.customerflatchecklistform.type.value.match(/^[\s]+$/))
	{
		$('#typeSpan').html('Type name should not be empty..!');
		document.customerflatchecklistform.type.value="";
		document.customerflatchecklistform.type.focus();
		return false;
	}
	
	if(document.customerflatchecklistform.details.value=="")
	{
		$('#detailsSpan').html('Details should not be empty..!');
		document.customerflatchecklistform.details.value="";
		document.customerflatchecklistform.details.focus();
		return false;
	}
	else if(document.customerflatchecklistform.details.value.match(/^[\s]+$/))
	{
		$('#detailsSpan').html('Details should not be empty..!');
		document.customerflatchecklistform.details.value="";
		document.customerflatchecklistform.details.focus();
		return false;
	}
	
	$('#addMoreDetialsTable tr').detach();
	 
	 var checkListId = $('#checkListId').val();
	 var bookingId = $('#bookingId').val();
	 var aggreementId = $('#aggreementId').val();
	 var type = $('#type').val();
	 var details = $('#details').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/AddMoreCheckListDetails',
		type : 'Post',
		data : { checkListId : checkListId, bookingId : bookingId, aggreementId : aggreementId, type : type, details : details},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
							$('#addMoreDetialsTable').append('<tr style="background-color: #4682B4;"><th style="width:50px">Sr.No</th><th style="width:150px">Type</th><th style="width:150px">Detials</th><th style="width:100px">Action</th>');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									var id = result[i].typeId;
									$('#addMoreDetialsTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].type+'</td><td>'+result[i].details+'</td><td><a onclick="DeleteMoreDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
								else
								{
									var id = result[i].typeId;
									$('#addMoreDetialsTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].type+'</td><td>'+result[i].details+'</td><td><a onclick="DeleteMoreDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}
				  } 

		});
	 
	 $('#type').val("")
	 $('#details').val("");

}

function DeleteMoreDetails(typeId)
{
	var checkListId = $('#checkListId').val();
    
    $('#addMoreDetialsTable tr').detach();
    
    $.ajax({

		 url : '${pageContext.request.contextPath}/DeleteMoreCheckListDetails',
		type : 'Post',
		data : { checkListId : checkListId, typeId : typeId },
		dataType : 'json',
		success : function(result)
				  {
				   if (result) 
				   { 
						$('#addMoreDetialsTable').append('<tr style="background-color: #4682B4;"><th style="width:50px">Sr.No</th><th style="width:150px">Type</th><th style="width:150px">Detials</th><th style="width:100px">Action</th>');
					
						for(var i=0;i<result.length;i++)
						{ 
							if(i%2==0)
							{
								var id = result[i].typeId;
								$('#addMoreDetialsTable').append('<tr style="background-color: #F0F8FF;"><td>'+(i+1)+'</td><td>'+result[i].type+'</td><td>'+result[i].details+'</td><td><a onclick="DeleteMoreDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
							else
							{
								var id = result[i].typeId;
								$('#addMoreDetialsTable').append('<tr style="background-color: #CCE5FF;"><td>'+(i+1)+'</td><td>'+result[i].type+'</td><td>'+result[i].details+'</td><td><a onclick="DeleteMoreDetails(\''+id+'\')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> </td>');
							}
						
						 } 
					} 
					else
					{
						alert("failure111");
					}
		  } 

		});	
}

function init()
{
	clearall();
	 var n = new Date();
	 var y = n.getFullYear();
	 var m = n.getMonth()+1;
	 var d = n.getDate();
	 
	 document.getElementById("creationDate").value = d + "/"+ m + "/" + y;
	 
	 document.getElementById("updateDate").value = d + "/"+ m + "/" + y;
	 
	 document.getElementById("userName").value = "Admin" ; 
	 
	 if(document.locationareaform.locationAreaStatus.value == "Fail")
	 {
	  	//alert("Sorry, record is present already..!");
	 }
	 else if(document.locationareaform.locationAreaStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record added successfully..!');
	 }
	 
  	document.locationareaform.countryName.focus();
}



  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
