<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PEEP |Edit Patient Information</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="styleshee
  t" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">

	<%@ include file="headerpage.jsp" %>
  
  	<%@ include file="menu.jsp" %>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Patient Information:
   
      </h1>
     
    </section>

    <!-- Main content -->
	
<form name="patientform" action="${pageContext.request.contextPath}/EditPatientInformationData" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
              <!-- /.form-group -->
              <div class="box-body">
          <div class="row">
            <div class="col-md-4">
                  <label for="countyId">Patient Id</label>
                  <input type="text" class="form-control" id="id" name="id"  value="${patientDetails[0].id}" readonly>
                  </div>
                  </div>
                </div>
			
								
				
					<div class="box-body">
          <div class="row">
            <div class="col-md-6">
           		 <div>
                  <label for="firstName">First Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" value="${patientDetails[0].firstName}"><br>
                  <span id="firstNameSpan" style="color:#FF0000"></span>
                  </div>
                  
                  <div>
                   <label for="lastName">Last Name</label> <label class="text-red">* </label>
              	  <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name" value="${patientDetails[0].lastName}"><br>
              	  <span id="lastNameSpan" style="color:#FF0000"></span>
              	  </div>
              	  
              	  <div>
              	   <label for="emailId">Email ID</label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="emailId" name="emailId" placeholder="Email Id" value="${patientDetails[0].emailId}"><br>
                     <span id="emailIdSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div>
                 	<label for="phoneNUmber">Phone Number</label> <label class="text-red">* </label>  
                   <input type="text" class="form-control" id="phoneNUmber" name="phoneNUmber" placeholder="Phone Numaber" value="${patientDetails[0].phoneNUmber}"> <br>
                    <span id="phoneNUmberSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div>
                	<label for="socialSecurityNumber">Social Security Number</label> <label class="text-red">* </label>  
                   <input type="text" class="form-control" id="socialSecurityNumber" name="socialSecurityNumber" placeholder="Social Security Number" value="${patientDetails[0].socialSecurityNumber}">
                     <span id="socialSecurityNumberSpan" style="color:#FF0000"></span>
                   </div>
              </div>
                 
              <div class="col-md-6">
              
              
                    <div>
                     <label for="age">Age</label> <label class="text-red">* </label> 
                    <input type="text" class="form-control" id="age" name="age" placeholder="age" value="${patientDetails[0].age}">
                   
                    <span id="ageSpan" style="color:#FF0000"></span>
                   </div>                  <br>
                   
                   
                    <div>
                     <label for="dateOfBirth">Year Of Birth</label> <label class="text-red">* </label> 
                    <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" placeholder="Year Of Birth" value="${patientDetails[0].dateOfBirth}">
                   
                    <span id="dateOfBirthSpan" style="color:#FF0000"></span>
                   </div>                  <br>
                   
                    <div>
              		 <label for="zipCode">Zip Code</label> <label class="text-red">* </label> 
                    <input type="text" class="form-control" id="zipCode" name="zipCode" placeholder="Zip Code" value="${patientDetails[0].zipCode}"><br>
                        <span id="zipCodeSpan" style="color:#FF0000"></span>
                    </div>
                    
                    <div>
                    <label for="countryName">Country</label> <label class="text-red">* </label>   
                     <input type="text" class="form-control" id="country" name="country" placeholder="Country" value="${patientDetails[0].country}"><br>
                     <span id="countrySpan" style="color:#FF0000"></span>
                     </div>
                     <div>
                     
                    <label for="gender">Gender</label> <label class="text-red">* </label><br>
                  <%--   <c:if test="${patientDetails[0].gender}=='Male'">   --%>
                  <c:if test="${patientDetails[0].gender eq 'Male'}">
                     <input type="radio"  id="gender" name="gender" value="Male" checked> Male
                         <input type="radio" id="gender" name="gender" value="Female"> Female
                    </c:if>
                      <c:if test="${patientDetails[0].gender eq 'Female'}">
                     <input type="radio" id="gender" name="gender" value="Female" checked> Female 
                     <input type ="radio"  id="gender" name="gender" value="Male"> Male
                    </c:if>
                      <span id="genderSpan" style="color:#FF0000"></span>
                     </div><br>
                     
                     <div>
                     <label for="language"> Preferred  Language</label> <label class="text-red">* </label> <br></br> 
                     <input type="text" class="form-control" id="language" name="language" placeholder=" Preferred Language" value="${patientDetails[0].language}">
           			   <span id="languageSpan" style="color:#FF0000"></span>
           			  
           			  </div>
           	  </div>
               
               
                    
           </div>
           
            <div class="row">
            <div class="col-md-6">
           	   <div>
		                   <label for="cancerTypeId">Type of Cancer</label> <label class="text-red">* </label>  
		                   <br>
		                <%--   <c:forEach items="${cancerTypeList}" var="cancer">
		                  <input type="radio"  id="cancerTypeId" name="cancerTypeId" value="${cancer.id}"> ${cancer.cancerType}
							</c:forEach> --%>
							<c:forEach items="${cancerTypeList}" var="cancer">
							 <c:if test="${patientDetails[0].cancerTypeId eq cancer.id}">
	                     	<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		  <input type="radio"  id="cancerTypeId" name="cancerTypeId" value="${cancer.id}" checked> ${cancer.cancerType}
		                     	</div>
		                     	
	 						</div>
	 						
	 						
	 						</c:if>
	 						 <c:if test="${patientDetails[0].cancerTypeId ne cancer.id}">
	 						<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		  <input type="radio"  id="cancerTypeId" name="cancerTypeId" value="${cancer.id}" > ${cancer.cancerType}
		                     	</div>
		                     	
	 						</div>
	 						</c:if>
	 						</c:forEach>
		                 <br>
		                 <span id="cancerTypeIdSpan" style="color:#FF0000"></span>
                 </div>
                  
                <div>
	                   <br><br><label for="hospitalName">Name of Hospital where care is going on </label> <label class="text-red">* </label>
	                  	<input type="text" class="form-control" id="hospitalName" name="hospitalName" placeholder="Name of Hospital where care is going on"
	                  	value="${patientDetails[0].hospitalName}" ><br>
	                 	<span id="hospitalNameSpan" style="color:#FF0000"></span>
                 </div>
              	  
              	  <div>
	                  <label for="hospitalEmailid">Hospital Email Id</label> <label class="text-red">* </label>  
	                   <input type="text" class="form-control" id="hospitalEmailid" name="hospitalEmailid" placeholder="Hospital Emailid" 
	                   value="${patientDetails[0].hospitalEmailid}"><br>
	                   <span id="hospitalEmailidSpan" style="color:#FF0000"></span>
	               </div>
	               
                    
                   
                   
              </div>
                 
              <div class="col-md-6">
                  		<div>	
		 					<label for="hospitalPhysician">Name Of Physician</label> <label class="text-red">* </label>
		              	  <input type="text" class="form-control" id="hospitalPhysician" name="hospitalPhysician" placeholder="Name Of Physician"
		              	    value="${patientDetails[0].hospitalPhysician}"><br>
		            		<span id="hospitalPhysicianSpan" style="color:#FF0000"></span>
                    </div>       
                    
                   <div>	
		 					<label for="hospitalPhysician">Hospital Phone Number</label> <label class="text-red">* </label>
		              	  <input type="text" class="form-control" id="hosipitalPhoneNumber" name="hosipitalPhoneNumber" placeholder="Name Of Phone Number"
		              	    value="${patientDetails[0].hosipitalPhoneNumber}"><br>
		            		<span id="hosipitalPhoneNumberSpan" style="color:#FF0000"></span>
                    </div>
                    <div>  
	                       <label for="stageOfChemotherapy"> Stage of Chemotherapy </label> <label class="text-red">* </label> <br> 
	                       
	                        <c:if test="${patientDetails[0].stageOfChemotherapy eq 'Receiving'}">
	                     	<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Receiving" checked >Receiving
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Received" >Received 
		 						</div>
	 						</div>
	 						
	 						<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Planned" >Planned
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Not Sure" >Not Sure
		 						</div>
	 						</div>
	 						</c:if>
	 						
	 						    <c:if test="${patientDetails[0].stageOfChemotherapy eq 'Received'}">
	                     	<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Receiving"  >Receiving
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Received" checked>Received 
		 						</div>
	 						</div>
	 						
	 						<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Planned" >Planned
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Not Sure" >Not Sure
		 						</div>
	 						</div>
	 						</c:if>
	 						
	 						 <c:if test="${patientDetails[0].stageOfChemotherapy eq 'Planned'}">
	                     	<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Receiving"  >Receiving
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Received" >Received 
		 						</div>
	 						</div>
	 						
	 						<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Planned" checked>Planned
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Not Sure" >Not Sure
		 						</div>
	 						</div>
	 						</c:if>
	 						
	 						
	 						 <c:if test="${patientDetails[0].stageOfChemotherapy eq 'Not Sure'}">
	                     	<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Receiving"  >Receiving
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Received" >Received 
		 						</div>
	 						</div>
	 						
	 						<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Planned" >Planned
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Not Sure" checked >Not Sure
		 						</div>
	 						</div>
	 						</c:if>
	 						
	 						 <c:if test="${patientDetails[0].stageOfChemotherapy eq null}">
	                     	<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Receiving"  >Receiving
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Received" >Received 
		 						</div>
	 						</div>
	 						
	 						<div class="col-md-12">
		                     	<div class="col-sm-4">
		                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Planned" >Planned
		                     	</div>
		                     	<div class="col-sm-4">
		 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Not Sure" >Not Sure
		 						</div>
	 						</div>
	 						</c:if>
	 						<span id="stageOfChemotherapySpan" style="color:#FF0000"></span>
                    </div>
           	  </div>
                    
                    
           </div>
           
           
           
                </div>
                </div>
				<!-- <input type="hidden" id="status" name="status" value="${countryStatus}"> -->	
				<input type="hidden" id="creationDate" name="creationDate" value="${patientDetails[0].createdDate}">
				<input type="hidden" id="updateDate" name="updateDate " value="${patientDetails[0].updatedDate}">
			
				<input type="hidden" id="userName" name="userName" value = "<%= session.getAttribute("user") %>">
				
					
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		  	     <div class="box-body">
              <div class="row">
                <div class="col-xs-2"></div><div class="col-xs-1"></div>
                      <div class="col-xs-2">
                	<a href="PatienteMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-2">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-1">
			  				<button type="submit" class="btn btn-info " name="submit">Update</button>
			        </div> 
                 </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
           
      </div>
      <!-- /.box -->
			
			<!-- Design For View Table Of Country List -->
    
   
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
  
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
   
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function validate()
{
	   /*  if(document.patientform.firstName.value=="" )
		{
		  if( document.patientform.firstName.value.match(/^[\s]+$/) )
			  {
			  
			  $('#fistNameSapn').html('Please, enter First Name.!');
				document.patientform.firstName.value="";
				document.patientform.firstName.focus();
				return false; 	
			  }
			  $('#fistNameSapn').html('Please, enter First Name.!');
			document.patientform.firstName.value="";
			document.patientform.firstName.focus();
			return false; 	
		} */ 
		 if(document.patientform.firstName.value=="")
			{
			  $('#firstNameSpan').html('Please, enter First Name.!');
				document.patientform.firstName.focus();
				return false;
			}
			else if(document.patientform.firstName.value.match(/^[\s]+$/))
			{
				$('#firstNameSpan').html('Please, enter First Name..!');
				document.patientform.firstName.value="";
				document.patientform.firstName.focus();
				return false; 	
			}
	   
			  if(document.patientform.lastName.value=="")
			{
				  $('#lastNameSpan').html('Please, enter Last Name.!');
					document.patientform.lastName.focus();
					return false;
			}
			else if(document.patientform.lastName.value.match(/^[\s]+$/))
			{
				$('#lastNameSpan').html('Please, enter Last Name..!');
				document.patientform.lastName.value="";
				document.patientform.lastName.focus();
				return false; 	
			}
	  
			   if(document.patientform.country.value=="")
			{
			  $('#countrySpan').html('Please, enter Country Name.!');
				document.patientform.country.focus();
				return false;
			}
			else if(document.patientform.country.value.match(/^[\s]+$/))
			{
				$('#countrySpan').html('Please, enter Country Name..!');
				document.patientform.country.value="";
				document.patientform.country.focus();
				return false; 	
			}
			  
			    if(document.patientform.socialSecurityNumber.value=="")
			{
			  $('#socialSecurityNumberSpan').html('Please, enter Social SecurityNumber.!');
				document.patientform.socialSecurityNumber.focus();
				return false;
			}
			  
		  if(document.patientform.hospitalName.value=="")
			{
			  $('#hospitalNameSpan').html('Please, enter Hospital Name.!');
				document.patientform.hospitalName.focus();
				return false;
			}
			 else if(document.patientform.hospitalName.value.match(/^[\s]+$/))
			{
				$('#hospitalNameSpan').html('Please, enter Hospital Name..!');
				document.patientform.hospitalName.value="";
				document.patientform.hospitalName.focus();
				return false; 	
			} 
	  
		  if(document.patientform.hospitalPhysician.value=="")
			{
			  $('#hospitalPhysicianSpan').html('Please, enter Physician Name.!');
				document.patientform.hospitalPhysician.focus();
				return false;
			}
			 else if(document.patientform.hospitalPhysician.value.match(/^[\s]+$/))
			{
				$('#hospitalPhysicianSpan').html('Please, enter Physician Name..!');
				document.patientform.hospitalPhysician.value="";
				document.patientform.hospitalPhysician.focus();
				return false; 	
			} 
		  
		  
		  if(document.patientform.hospitalName.value=="")
			{
			  $('#hospitalNameSpan').html('Please, enter Physician Name.!');
				document.patientform.hospitalName.focus();
				return false;
			}
			 else if(document.patientform.hospitalName.value.match(/^[\s]+$/))
			{
				$('#hospitalNameSpan').html('Please, enter Physician Name..!');
				document.patientform.hospitalName.value="";
				document.patientform.hospitalName.focus();
				return false; 	
			}
		  
		  
		  if(document.patientform.hospitalName.value=="")
			{
			  $('#hospitalNameSpan').html('Please, enter Physician Name.!');
				document.patientform.hospitalName.focus();
				return false;
			}
			 else if(document.patientform.hospitalName.value.match(/^[\s]+$/))
			{
				$('#hospitalNameSpan').html('Please, enter Physician Name..!');
				document.patientform.hospitalName.value="";
				document.patientform.hospitalName.focus();
				return false; 	
			}
		  
	  if(document.patientform.emailId.value=="")
		{
		  
			 
		  $('#emailIdSpan').html('Please, enter Email Id.!');
			document.patientform.emailId.focus();
			return false;
		}
	  else
		  {
		  if(!document.patientform.emailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		  {
				 $('#emailIdSpan').html('Please, enter  Valid Email Id.!');
				 document.patientform.emailId.value="";
				document.patientform.emailId.focus();
				return false;
			  }
		  
		  }
	  
	  if(document.patientform.hospitalEmailid.value=="")
		{
		  
			 
		  $('#hospitalEmailidSpan').html('Please, enter Email Id.!');
			document.patientform.hospitalEmailid.focus();
			return false;
		}
	  else
		  {
		  if(!document.patientform.hospitalEmailid.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		  {
				 $('#hospitalEmailidSpan').html('Please, enter  Valid Email Id.!');
				 document.patientform.hospitalEmailid.value="";
				document.patientform.hospitalEmailid.focus();
				return false;
			  }
		  
		  }  
	  
	  if(document.patientform.phoneNUmber.value=="")
		{
			 $('#phoneNUmberSpan').html('Please, enter phone  number..!');
			document.patientform.phoneNUmber.value="";
			document.patientform.phoneNUmber.focus();
			return false;
		}
		else if(!document.patientform.phoneNUmber.value.match(/^\d{10}$/))
		{
			 $('#phoneNUmberSpan').html(' enter valid phone number..!');
			document.patientform.phoneNUmber.value="";
			document.patientform.phoneNUmber.focus();
			return false;	
		}
	  if(document.patientform.hosipitalPhoneNumber.value=="")
		{
			 $('#hosipitalPhoneNumberSpan').html('Please, enter phone  number..!');
			document.patientform.hosipitalPhoneNumber.value="";
			document.patientform.hosipitalPhoneNumber.focus();
			return false;
		}
		else if(!document.patientform.hosipitalPhoneNumber.value.match(/^\d{10}$/))
		{
			 $('#hosipitalPhoneNumberSpan').html(' enter valid phone number..!');
			document.patientform.hosipitalPhoneNumber.value="";
			document.patientform.hosipitalPhoneNumber.focus();
			return false;	
		}
	var check=true;
		
	var check1=true;
      $("input:radio").each(function(){
    	
          var name = $(this).attr("name");
          if($("input:radio[name=cancerTypeId]:checked").length == 0){
              check = false;
             
              $('#cancerTypeIdSpan').html('Please, select Atlest on radio buttton..!');
          }
         
      });

      $("input:radio").each(function(){
      	
    	  if($("input:radio[name=stageOfChemotherapy]:checked").length == 0){
              check1 = false;
              $('#stageOfChemotherapySpan').html('Please, select Atlest on radio buttton..!');
          }
    	
      });
      if(!check)
    	  {
    	  return false;	
    	  }
      if(!check1)
	  {
	  return false;	
	  }
     
	   /*  if(document.patientform.dateOfBirth.value=="")
		{
			 $('#dateOfBirthSpan').html('Please, enter Year of Birth..!');
			document.patientform.dateOfBirth.value="";
			document.patientform.dateOfBirth.focus();
			return false;
		}
		
	
		 if(document.patientform.zipCode.value=="")
			{
				 $('#zipCodeSpan').html('Please, enter ZipCode..!');
				document.patientform.zipCode.value="";
				document.patientform.zipCode.focus();
				return false;
			}
			else if(!document.patientform.zipCode.value.match(^[0-9])
			{
				 $('#zipCodeSpan').html(' enter valid ZipCode..!');
				document.patientform.zipCode.value="";
				document.patientform.zipCode.focus();
				return false;	
			} 
	  */
}
  
function init()
{	
	
	var n =  new Date();
	var y = n.getFullYear();
	var m = n.getMonth() + 1;
	var d = n.getDate();
	
	document.getElementById("updateDate").value = d + "/" + m + "/" + y;
	
	 if(document.patientform.status.value=="Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.patientform.status.value=="Success")
	 {
	    alert("Record added successfully..!");
	    
	 }
  	 document.patientform.moduleType.focus();
}

</script>
</body>
</html>
