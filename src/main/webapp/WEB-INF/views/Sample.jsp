<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PEEP |Add RedFlagQuestion Details</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="styleshee
  t" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" >

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">
  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add RedFlag Question Details :
      </h1>
    </section>

    <!-- Main content -->
<form name="redFlagQuestionForm" action="${pageContext.request.contextPath}/SaveRedFlagQuestionData" onSubmit="return validate()" method="POST">

 <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
         			<div class="box-body">
         				 
                		<div class="row">
                 			  
	            			<div class="col-md-6">
	             				   <div class="col-xs-12"><br>
	               						 <label for="redFlagQuestionName">Add Questions </label> <label class="text-red">* </label>
	                 					 <input type="text" class="form-control" id="redFlagQuestionName" name="redFlagQuestionName" placeholder="Question Name" >
	                					 <span id="questionNameSpan" style="color:#FF0000"></span>
              						</div>
	             			</div> 
	             			<div class="col-md-6"></div>
                		</div>
                		<div class="row">
                 			 
	            			<div class="col-md-6">
	             			 	 <div class="col-xs-12"><br>
				    				 
	                  						<label for="redFlagOptionName">Add Options </label> <label class="text-red">* </label><br>
	                  						 <input type="text" class="form-control" id="redFlagOptionName" name="redFlagOptionName" placeholder="Option Name" >
                   					
	                   
	                  				
	                  						<label for="optionName123">&nbsp;</label> <label class="text-red">    </label><br>
	                  						<input type="button" onclick="myFunction()" value="Add More Options"/><br><br>
                   					
			               			 <span id="optionNameSpan" style="color:#FF0000"></span>
	                 			</div>
	             			</div> 
                		</div>
                		
                		<div class="row">
                 			
	            			<div class="col-md-6">
	             			 	<div class="col-xs-12"><br>
	                 				<table id="myTable"  border="1" >
										<tr><th bgcolor="#3c8dbc"> <center>Option Name </center>    </th><th bgcolor="#3c8dbc">  <center>   Action </center>     </th>
											<col width="300">
  											<col width="300">
  										</tr>
  									</table>
	              				</div> 
	             			</div> 
                		</div>
                		
				
					</div>
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
		
          </div>
       </div> <!-- /.box-body -->
           
          <br><br><br>
		  	     <div class="box-body">
              <div class="row">
                      <div class="col-xs-2">
                	 <a href="ActivityMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a> 
			     </div>
				  <div class="col-xs-2">
                <input type="reset" class="btn btn-default" value="reset" style="width:90px" value="Reset"> 
              
			     </div>
					<div class="col-xs-1">
			  				<input type="submit" id ="submit" class="btn btn-info " name="submit" value="Submit"> 
			        </div> 
                 </div>
			  </div>
          <!-- /.row -->
        
       
      </div>
      <!-- /.box -->
			
			<!-- Design For View Table Of Country List -->
    
   
     
    </section>

    
  </div>
  
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
   
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function myFunction() { 
	var txtval = document.getElementById("optionName").value;
    var table = document.getElementById("myTable");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    cell1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ txtval;
    cell2.innerHTML = '<center><input type="button" class="remove_row" value="Delete"/></center>';
  //  document.getElementById("optionName").value="";
   // cell1.innerHTML = txtval;
   // cell2.innerHTML = '<a href="javascript:removeFunction()" class="btn btn-info btn-lg"> <span class="glyphicon glyphicon-plus-sign"></span>  </a>';
    //cell2.innerHTML = '<input type="button" class="remove_row" value="Delete"/>';
}
function myFunction1() { 
	var txtval = document.getElementById("videoLink").value;
//	alert(txtval);
    var table = document.getElementById("myTable1");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    cell1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ txtval;
    cell2.innerHTML = '<center><input type="button" class="remove_row" value="Delete"/></center>';
   // document.getElementById("videoLink").value="";
}




</script>
<script>
$(document).ready(function(){

	 $("#myTable").on('click','.remove_row',function(){
	       $(this).closest('tr').remove();
	     });

	});
	

</script>
<script>
(function() {
  document.getElementById("submit").onclick = function() {
    var table = document.getElementById("myTable");
  
    var options="";
    var videoLink="";
    
    for (var i = 1; i < table.rows.length; i++) {
    	
      options=options+table.rows[i].cells[0].innerHTML+",";
      options=options.replace(/&nbsp;/g, '');
     
    }
    document.getElementById("redFlagOptionName").value=options;
   /// alert("hii"+options+ " "+videoLink);
  };
})();
</script>
<script>
function myFunction() { 
	var txtval = document.getElementById("redFlagOptionName").value;
	if(txtval=="")
		{
		  $('#optionNameSpan').html('Please, Option cannot be blank..!');
			document.redFlagQuestionForm.redFlagOptionName.focus();
			
		}else
			{
		    var table = document.getElementById("myTable");
		    var row = table.insertRow(1);
		    var cell1 = row.insertCell(0);
		    var cell2 = row.insertCell(1);
		    cell1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ txtval;
		    cell2.innerHTML = '<center><input type="button" class="remove_row" value="Delete"/></center>';
		   document.getElementById("redFlagOptionName").value="";
			}

   // cell1.innerHTML = txtval;
   // cell2.innerHTML = '<a href="javascript:removeFunction()" class="btn btn-info btn-lg"> <span class="glyphicon glyphicon-plus-sign"></span>  </a>';
    //cell2.innerHTML = '<input type="button" class="remove_row" value="Delete"/>';
}
function validate()
{
	 
		
	  if(document.redFlagQuestionForm.redFlagQuestionName.value=="")
		{
		  $('#questionNameSpan').html('Please, enter Question..!');
			document.redFlagQuestionForm.redFlagQuestionName.focus();
			return false;
		}
	  
	  if(document.redFlagQuestionForm.redFlagOptionName.value=="")
		{
		  $('#optionNameSpan').html('Please, Enter Atlest One Option..!');
			document.redFlagQuestionForm.redFlagOptionName.focus();
			return false;
		}
	  
		
	 
}
</script>
</body>
</html>
