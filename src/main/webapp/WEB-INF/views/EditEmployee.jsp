<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Employee</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
     
  <style>
.alert {
    padding: 20px;
    background-color: #f44336;
    color: white;
    opacity: 1;
    transition: opacity 0.6s;
    margin-bottom: 15px;
}

.alert.success {background-color: #4CAF50;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
    margin-left: 15px;
    color: white;
    font-weight: bold;
    float: right;
    font-size: 22px;
    line-height: 20px;
    cursor: pointer;
    transition: 0.3s;
}

.closebtn:hover {
    color: black;
}
</style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>

<div class="wrapper">

	<%@ include file="headerpage.jsp" %>
  
  	<%@ include file="menu.jsp" %>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Employee Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Employee</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="employeeform" action="${pageContext.request.contextPath}/EditEmployee" method="post" onSubmit="return validate()">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
              <!-- /.form-group -->
             
			
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-4">
                  <label >Employee Id</label>
                  <input type="text" class="form-control"  id="employeeId"name="employeeId" value="${employeeDetails[0].employeeId}" readonly>
                </div>               
                
              </div>
            </div>
				
				
				<div class="box-body">
              <div class="row">
                <div class="col-xs-4">
				<label for="employeefirstname">First Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="employeefirstName" placeholder="First Name" name="employeefirstName" value="${employeeDetails[0].employeefirstName}">
                 <span id="employeefirstNameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-4">
				<label for="employeemiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="employeemiddleName" placeholder="Middle Name" name="employeemiddleName" value="${employeeDetails[0].employeemiddleName}">
                  <span id="employeemiddleNameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-4">
				<label for="employeelastname">Last Name </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="employeelastName" placeholder="Last Name" name="employeelastName" value="${employeeDetails[0].employeelastName}">
                  <span id="employeelastNameSpan" style="color:#FF0000"></span>
                </div>
              </div>
            </div>
				
				
				   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			      <label>Designation  </label> <label class="text-red">* </label>
			      
                  <select class="form-control" name="employeeDesignation" id="employeeDesignation">
                  
				  					<option value="Default">-Select Designation-</option>
				  					<option selected="" value="${employeeDetails[0].employeeDesignation}">${employeeDetails[0].employeeDesignation}</option>
				  		
                     <c:forEach var="designationList" items="${designationList}">
	                      <c:choose>
		                       <c:when test="${employeeDetails[0].employeeDesignation ne designationList.designationName}">
		                    	  	<option value="${designationList.designationName}">${designationList.designationName}</option>
		                       </c:when>
	                      </c:choose>
				     </c:forEach>
				     
                  </select>
                  
                   <span id="employeeDesignationSpan" style="color:#FF0000"></span>  
			     </div> 
				  <div class="col-xs-6">
			      <label>Date Of Birth </label> <label class="text-red">* </label>
				 <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="text" class="form-control pull-right" id="datepicker1" name="employeeDob" value="${employeeDetails[0].employeeDob}">
                   <span id="employeeDobSpan" style="color:#FF0000"></span>
                </div>
				   </div>
				 </div>
            </div>
				
				 <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			   <label for="radiobutton">Gender</label><br/>
			   
			   <c:choose>
			     <c:when test="${employeeDetails[0].employeeGender eq 'male'}">
                 	<input type="radio" class="minimal" name="employeeGender" id="employeeGender" value="male" checked="checked">Male
                 </c:when>
                 <c:otherwise>  
                 	<input type="radio" class="minimal" name="employeeGender" id="employeeGender" value="male">Male
                 </c:otherwise>
			   </c:choose>
			    		&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
				  		
			   <c:choose>
				  <c:when test="${employeeDetails[0].employeeGender eq 'female'}">
  					<input type="radio" class="minimal" name="employeeGender" id="employeeGender" value="female" checked="checked">Female
  				  </c:when>
  				  <c:otherwise>
  				  	<input type="radio" class="minimal" name="employeeGender" id="employeeGender" value="female">Female
  				  </c:otherwise>
  			   </c:choose>
  			     
                   <span id="employeeGenderSpan" style="color:#FF0000"></span>
                 </div> 
			 </div>
            </div>
				
				
				    <div class="box-body">
					  <h3>Address  Details</h3>
              <div class="row">
                  <div class="col-xs-12">
			   <label for="employeeaddress">Address </label> <label class="text-red">* </label>
                 <textarea class="form-control" rows="1" id="employeeAddress" placeholder="Address" name ="employeeAddress">${employeeDetails[0].employeeAddress} </textarea>
			       <span id="employeeAddressSpan" style="color:#FF0000"></span>
			     </div> 
				  
              </div>
            </div>
				
				
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label>Country </label> <label class="text-red">* </label>
                  <select class="form-control" name="countryName" id="countryName" onchange="getStateList(this.value)">
				  		
				  			<option selected="selected" value="${employeeDetails[0].countryName}">${employeeDetails[0].countryName}</option>
				  		
                    <c:forEach var="countryList" items="${countryList}">
                      <c:choose>
	                     <c:when test="${employeeDetails[0].countryName ne countryList.countryName}">
		                    <option value="${countryList.countryName}">${countryList.countryName}</option>
		                 </c:when>
		              </c:choose>
	                 </c:forEach>
	                 
                  </select>
                     <span id="countryNameSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-6">
				 <label>State </label> <label class="text-red">* </label>
                  <select class="form-control" name="stateName" id="stateName" onchange="getCityList(this.Value)">
				 		 <option selected="" value="${employeeDetails[0].stateName}">${employeeDetails[0].stateName}</option>
				 		 
                    <c:forEach var="stateList" items="${stateList}">
                     <c:choose>
                      <c:when test="${employeeDetails[0].stateName ne stateList.stateName}">
                        <option value="${stateList.stateName}">${stateList.stateName}</option>
                      </c:when>
                     </c:choose>
				    </c:forEach>
				      
                  </select>
                     <span id="stateNameSpan" style="color:#FF0000"></span>
				</div> 
              </div>
            </div>
				
			
			
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
				  <label>City </label> <label class="text-red">* </label>
                  <select class="form-control" name ="cityName" id="cityName" onchange="getLocationAreaList(this.value)">
                  
				  			<option selected="" value="${employeeDetails[0].cityName}">${employeeDetails[0].cityName}</option>
				  		
                     <c:forEach var="cityList" items="${cityList}">
                      <c:choose>
                       <c:when test="${employeeDetails[0].cityName ne cityList.cityName}">
                    		<option value="${cityList.cityName}">${cityList.cityName}</option>
                       </c:when>
                      </c:choose>
				     </c:forEach>
				       
                  </select>
                     <span id="cityNameSpan" style="color:#FF0000"></span>
				  </div> 
				    <div class="col-xs-6">
				    <label>Area </label> <label class="text-red">* </label>
                  <select class="form-control" name="locationareaName" id="locationareaName"onchange="getpinCode(this.value)">
                  
				  			<option selected="" value="${employeeDetails[0].locationareaName}">${employeeDetails[0].locationareaName}</option>
				  			
                 	  <c:forEach var="locationareaList" items="${locationareaList}">
	                 	  <c:choose>
		                 	    <c:when test="${employeeDetails[0].locationareaName ne locationareaList.locationareaName}">
		                    		<option value="${locationareaList.locationareaName}">${locationareaList.locationareaName}</option>
		                    	</c:when>
	                      </c:choose>
				      </c:forEach>
				      
                  </select>
                     <span id="locationareaNameSpan" style="color:#FF0000"></span>
				  </div> 
              </div>
            </div>
			
			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			   
                  <label for="pincode">Pin Code </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="areaPincode" placeholder="Pin Code" name="areaPincode" value="${employeeDetails[0].areaPincode}">
			       <span id="areaPincodeSpan" style="color:#FF0000"></span>
			     </div> 
				  
              </div>
            </div>
				   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			   
                  <label for="emailid">Email ID </label> <label class="text-red">* </label>
				   <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control" placeholder="Email" id="employeeEmail" name="employeeEmail" value="${employeeDetails[0].employeeEmail}">
                <span id="employeeEmailSpan" style="color:#FF0000"></span>
              </div>
			     </div> 
				  <div class="col-xs-6">
			    <label>Mobile No </label> <label class="text-red">* </label>
			    
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="employeeMobileno" id="employeeMobileno" value="${employeeDetails[0].employeeMobileno}">
                   <span id="employeeMobilenoSpan" style="color:#FF0000"></span>
                </div>
				 </div> 
              </div>
            </div>
			
				<div class="form-group">
               
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label for="pancardno">Pan Card No </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="employeePancardno" placeholder="Pan Card No " name="employeePancardno" value="${employeeDetails[0].employeePancardno}">
                   <span id="employeePancardnoSpan" style="color:#FF0000"></span>
			     </div> 
				  <div class="col-xs-6">
			      <label for="adharcardno">Adhar Card No</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="employeeAadharcardno" placeholder="Adhar Card No " name="employeeAadharcardno" value="${employeeDetails[0].employeeAadharcardno}" onchange="getuniqueaadharnumberName(this.value)" >
			        <span id="employeeAadharcardnoSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>
			   
			   
              </div>
			  
             
            </div>
          
            <div class="col-md-6">
			
			 
			   <div class="box-body">
              <div class="row">
			  
                  <div class="col-xs-4">
			   	  
			   <div class="pull-left image">
          <img src="#" id="profile-img-tag" class="img-circle" width="200px alt="User Image">
        </div>
			     </div> 
			       <div class="col-xs-1">
			   	
			     </div>
				  <div class="col-xs-6">
			   <div class="form-group">
			   </br></br>
			   </br></br>
                  <label >Select Photograph</label>
				  <input type="file" id="profile-img" name="profile-img" accept="image/x-png,image/gif,image/jpeg">

                  <p class="help-block">Only select PNG, JPEG, and GIF formats.</p>
                </div>
			     </div> 
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
				   
			   <label for="joiningdate">Joining Date</label> <label class="text-red">* </label>
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control pull-right" id="datepicker" name="employeeJoiningdate" value="${employeeDetails[0].employeeJoiningdate}">
                <span id="employeeJoiningdateSpan" style="color:#FF0000"></span>
                </div>
				</div> 
				  
              </div>
            </div>
			
			
			 <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
				   
			   <label for="basicpay">Basic Pay </label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="employeeBasicpay" placeholder="Basic Pay" name="employeeBasicpay" value="${employeeDetails[0].employeeBasicpay}">
                   <span id="employeeBasicpaySpan" style="color:#FF0000"></span>
			     </div> 
				  <div class="col-xs-6">
			    <label for="hra">HRA </label>
                <input type="text" class="form-control" id="employeeHra" placeholder="HRA " name="employeeHra" value="${employeeDetails[0].employeeHra}">
                   <span id="employeeHraSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>
             
			 
			    <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label for="da">DA </label>
                <input type="text" class="form-control" id="employeeDa" placeholder="DA" name="employeeDa" value="${employeeDetails[0].employeeDa}">
                   <span id="employeeDaSpan" style="color:#FF0000"></span>
			     </div> 
				  <div class="col-xs-6">
			   <label for="ca">CA </label>
                <input type="text" class="form-control" id="employeeCa" placeholder="CA" name="employeeCa" value="${employeeDetails[0].employeeCa}">
                   <span id="employeeCaSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>
			 
			    <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label for="pfno">P. F. A/C No</label>
                <input type="text" class="form-control" id="employeePfacno" placeholder="P. F. A/C No " name="employeePfacno" value="${employeeDetails[0].employeePfacno}">
                   <span id="employeePfacnoSpan" style="color:#FF0000"></span>
			     </div> 
				  <div class="col-xs-6">
			   <label for="estno">ESI No </label>
                <input type="text" class="form-control" id="employeeEsino" placeholder="ESI No" name="employeeEsino" value="${employeeDetails[0].employeeEsino}">
                   <span id="employeeEsinoSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>
			 
			 
			    <div class="box-body">
              <div class="row">
                   
				  <div class="col-xs-6">
			    <label>Bank Name </label>
              <select class="form-control" name="employeeBankacname" id="employeeBankacname" onchange="getBranchList(this.value)">
				 <option selected="selected" value="${employeeDetails[0].employeeBankacname}">${employeeDetails[0].employeeBankacname}</option>
				  		
                    <c:forEach var="bankList" items="${bankList}">
                      <c:choose>
	                     <c:when test="${employeeDetails[0].employeeBankacname ne bankList.bankName}">
		                    <option value="${bankList.bankName}">${bankList.bankName}</option>
		                 </c:when>
		              </c:choose>
	                 </c:forEach>
	            
                  </select>
                     <span id="employeeBankacnameSpan" style="color:#FF0000"></span>
			     </div> 
			     <div class="col-xs-6">
			    <label>Bank Branch Name </label> <label class="text-red">* </label>
			    <select class="form-control" id="branchName" name="branchName" onchange="getBranchIfsc(this.value)">
			        <option selected="selected" value="${employeeDetails[0].branchName}">${employeeDetails[0].branchName}</option>
                    <c:forEach var="bankList" items="${bankList}">
                      <c:choose>
	                     <c:when test="${employeeDetails[0].branchName ne bankList.branchName}">
		                    <option value="${bankList.branchName}">${bankList.branchName}</option>
		                 </c:when>
		              </c:choose>
	                 </c:forEach>
			         </select>
				 <span id="bankBranchNameSpan" style="color:#FF0000"></span>
			     </div> 
			  
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                 <div class="col-xs-6">
			    <label>IFSC </label> <label class="text-red">* </label>
                 <input type="text" class="form-control" id="bankifscCode" placeholder="Bank IFSC No " name="bankifscCode" value="${employeeDetails[0].bankifscCode}" readonly>
			     <span id="bankifscCodeSpan" style="color:#FF0000"></span>
			     </div> 
                 <div class="col-xs-6">
			    <label for="employeeBankacno">Bank A/C No </label>
                <input type="text" class="form-control" id="employeeBankacno" placeholder="Bank A/C No "name="employeeBankacno" value="${employeeDetails[0].employeeBankacno}">
                 <span id="employeeBankacnoSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>
			 </br></br>
			    <div class="box-body">
			    <h3>Leaves  Details</h3>
              <div class="row">
                  <div class="col-xs-6">
			     </div> 
			  </div>
            </div>
			
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-4">
			   	<label for="pl">PL </label>
                <input type="text" class="form-control" id="employeePLleaves" placeholder="PL " name="employeePLleaves" value="${employeeDetails[0].employeePLleaves}">
                 <span id="employeePLleavesSpan" style="color:#FF0000"></span>
			     </div> 
				  <div class="col-xs-4">
			   	 <label for="sl">SL </label>
                <input type="text" class="form-control" id="employeeSLleaves" placeholder="SL " name="employeeSLleaves" value="${employeeDetails[0].employeeSLleaves}">
                 <span id="employeeSLleavesSpan" style="color:#FF0000"></span>
			     </div>
					<div class="col-xs-4">
			   	 <label for="cl">CL </label>
                <input type="text" class="form-control" id="employeeCLleaves" placeholder="CL " name="employeeCLleaves" value="${employeeDetails[0].employeeCLleaves}">
                 <span id="employeeCLleavesSpan" style="color:#FF0000"></span>
			     </div> 
              </div>
            </div>
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			   	<label for="">Emplyee Status </label>
                    <select class="form-control" name="status" id="status" >
				  		<option value="Active">Active</option>
				  		<option value="InActive">InActive</option>
                  </select>
                 <span id="StatusSpan" style="color:#FF0000"></span>
			     </div> 
						
              </div>
            </div>
			 </br>
			 
			  <input type="hidden" id="employeeStatus" name="employeeStatus" value="${employeeStatus}">	
			  <input type="hidden" id="creationDate" name="creationDate" value="${employeeDetails[0].creationDate}">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
		
              <!-- /.form-group -->
            </div>
			
            <!-- /.col -->
			
          </div>
		  	      <div class="box-body">
              <div class="row">
              </br>
                
					<div class="col-xs-4">
	                	<a href="EmployeeMaster"><button type="button" class="btn btn-block btn-primary" value="reset" style="width:90px">Back</button></a>
				    </div>
				    
					<div class="col-xs-2">
			  			<button type="submit" class="btn btn-info pull-right" name="submit">Update</button>
			        </div> 
			     
				    <!-- 
			       <div class="col-xs-6">
			         <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"><a href="ImportNewEmployee"> Import From Excel File</a></button>
             </div>
              -->
                  
              </div>
			  </div>
			  
			  
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

    </section>
	</form>
    <!-- /.content -->
  </div>
  

  <!-- Control Sidebar -->
  <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>
function clearall()
{
	$('#employeefirstNameSpan').html('');
	$('#employeemiddleNameSpan').html('');
	$('#employeelastNameSpan').html('');
	$('#employeeDesignationSpan').html('');
	$('#employeeDobSpan').html('');
	$('#employeeGenderSpan').html('');
	$('#employeeAddressSpan').html('');
	$('#countryNameSpan').html('');
	$('#stateNameSpan').html('');
	$('#cityNameSpan').html('');
	$('#locationareaNameSpan').html('');
	$('#areaPincodeSpan').html('');
	$('#employeeEmailSpan').html('');

	$('#employeeMobilenoSpan').html('');
	$('#employeePancardnoSpan').html('');
	$('#employeeAadharcardnoSpan').html('');
	$('#employeeJoiningdateSpan').html('');
	$('#employeeBasicpaySpan').html('');
	$('#employeeHraSpan').html('');
	$('#employeeDaSpan').html('');
	$('#employeeCaSpan').html('');
	$('#employeePfacnoSpan').html('');
	$('#employeeEsinoSpan').html('');
	$('#employeeBankacnoSpan').html('');
	$('#employeeBankacnameSpan').html('');
	$('#employeePLleavesSpan').html('');
	$('#employeeSLleavesSpan').html('');
	$('#employeeCLleavesSpan').html('');
}
function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
   
 function validate()
 {
	 clearall();
	    //validation for employee first name
		if(document.employeeform.employeefirstName.value=="")
		{
			$('#employeefirstNameSpan').html('First name should not be blank..!');
			document.employeeform.employeefirstName.focus();
			return false;
		}
		else if(document.employeeform.employeefirstName.value.match(/^[\s]+$/))
		{
			$('#employeefirstNameSpan').html('First name should not be blank..!');
			document.employeeform.employeefirstName.value="";
			document.employeeform.employeefirstName.focus();
			return false;
		}
	
	    
	    //validation for employee middle name
	    if(document.employeeform.employeemiddleName.value.length!=0)
	    {
	    	if(document.employeeform.employeemiddleName.value.match(/^[\s]+$/))
	    	{
	    		document.employeeform.employeemiddleName.value="";
	    	}
	    
	    }
	    
	    //validation for employee last name
		if(document.employeeform.employeelastName.value=="")
		{
			$('#employeelastNameSpan').html('Last name should not be blank..!');
			document.employeeform.employeelastName.focus();
			return false;
		}
		else if(document.employeeform.employeelastName.value.match(/^[\s]+$/))
		{
			$('#employeelastNameSpan').html('Last name should not be blank..!');
			document.employeeform.employeelastName.value="";
			document.employeeform.employeelastName.focus();
			return false;
		}
	
	    //validation for employee designation
		if(document.employeeform.employeeDesignation.value=="Default")
		{
			$('#employeeDesignationSpan').html('Please, select employee designation..!');
			document.employeeform.employeeDesignation.focus();
			return false;
		}
	    
	    
		 //validation employee for date of birth
		if(document.employeeform.employeeDob.value=="")
		{
			$('#employeeDobSpan').html('Please, enter employee date of birth');
			document.employeeform.employeeDob.value="";
			document.employeeform.employeeDob.focus();
			return false;
		}
		else
		{
			var today = new Date();
				var dd1 = today.getDate();
				var mm1 = today.getMonth()+1;
				var yy1 = today.getFullYear();
				
		    var dob= new Date(document.employeeform.employeeDob.value);
		    	var dd2 = dob.getDate();
		    	var mm2 = dob.getMonth()+1;
		    	var yy2 = dob.getFullYear();
		    	
		    	var yydiff = yy1-yy2;
		    	var mmdiff = mm1-mm2;
		    	
		    if(yydiff<=18)
		    {
		    	if((mmdiff)<=0)
		    	{
		    		$('#employeeDobSpan').html(' date of birth should be 18 or 18+ years..!');
		    		document.employeeform.employeeDob.value="";
		    		document.employeeform.employeeDob.focus();
		    		return false;
		    	}
		    }
		}
	    
	    //validation for gender selection
	    if(( document.employeeform.employeeGender[0].checked == false ) && ( document.employeeform.employeeGender[1].checked == false ) )
		{
	    	$('#employeeGenderSpan').html('Please, choose your Gender: Male or Female..!');
			document.employeeform.employeeGender[0].focus();
			return false;
		}
	    
	    //validation for employee address----------------------
		if(document.employeeform.employeeAddress.value=="")
		{
			$('#employeeAddressSpan').html('Please, enter employee address name..!');
			document.employeeform.employeeAddress.focus();
			return false;
		}
		else if(document.employeeform.employeeAddress.value.match(/^[\s]+$/))
		{
			$('#employeeAddressSpan').html('Please, enter employee address ..!');
			document.employeeform.employeeAddress.focus();
			return false;
		}
	
	    
	    //validation for employee country
		if(document.employeeform.countryName.value=="Default")
		{
			$('#countryNameSpan').html('Please, select employee country name..!');
			document.employeeform.countryName.focus();
			return false;
		}
	    
	    //validation for employee state
		if(document.employeeform.stateName.value=="Default")
		{
			$('#stateNameSpan').html('Please, select employee state name..!');
			document.employeeform.stateName.focus();
			return false;
		}
	    
	    //validation for employee city
		if(document.employeeform.cityName.value=="Default")
		{
			$('#cityNameSpan').html('Please, select employee city name..!');
			document.employeeform.cityName.focus();
			return false;
		}
	    
	    //validation for employee location area
		if(document.employeeform.locationareaName.value=="Default")
		{
			$('#locationareaNameSpan').html('Please, select employee area name..!');
			document.employeeform.locationareaName.focus();
			return false;
		}
	    
	
	    
	   
	    //validation for employee email address
		if(document.employeeform.employeeEmail.value=="")
		{
			$('#employeeEmailSpan').html('Email Id should not be blank..!');
			document.employeeform.employeeEmail.focus();
			return false;
		}
		else if(!document.employeeform.employeeEmail.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-zA-Z]{2,4}$/))
		{
			$('#employeeEmailSpan').html(' enter valid email id..!');
			document.employeeform.employeeEmail.value="";
			document.employeeform.employeeEmail.focus();
			return false;
		}
		
		//validation for employee mobile number------------------------------
		if(document.employeeform.employeeMobileno.value=="")
		{
			$('#employeeMobilenoSpan').html('Please, enter employee mobile number..!');
			document.employeeform.employeeMobileno.value="";
			document.employeeform.employeeMobileno.focus();
			return false;
		}
		else if(!document.employeeform.employeeMobileno.value.match(/^[(]{1}[+]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
		{
			$('#employeeMobilenoSpan').html(' enter valid employee mobile number..!');
			document.employeeform.employeeMobileno.value="";
			document.employeeform.employeeMobileno.focus();
			return false;	
		}
		
		//validation for employee pan card
		if(document.employeeform.employeePancardno.value=="")
		{
			$('#employeePancardnoSpan').html('Please, enter Pancard number..!');
			document.employeeform.employeePancardno.focus();
			return false;
		}
		else if(!document.employeeform.employeePancardno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
		{
			$('#employeePancardnoSpan').html('PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
			document.employeeform.employeePancardno.value="";
			document.employeeform.employeePancardno.focus();
			return false;
		}
		
		//validation for employee aadhar card number
		if(document.employeeform.employeeAadharcardno.value=="")
		{
			$('#employeeAadharcardnoSpan').html('Please, enter Aadhar card number..!');
			document.employeeform.employeeAadharcardno.focus();
			return false;
		}
		else if(!document.employeeform.employeeAadharcardno.value.match(/^\d{4}\d{4}\d{4}$/))
		{
			$('#employeeAadharcardnoSpan').html('Aadhar card number should be 12 digit number only..!');
			document.employeeform.employeeAadharcardno.value="";
			document.employeeform.employeeAadharcardno.focus();
			return false;
		}
		
		//validation for joining date
		if(document.employeeform.employeeJoiningdate.value=="")
		{
			$('#employeeJoiningdateSpan').html('Please, select joining date..!');
			document.employeeform.employeeJoiningdate.focus();
			return false;
		}
		
		//validation for employee basic pay
		if(document.employeeform.employeeBasicpay.value=="")
		{
			$('#employeeBasicpaySpan').html('Please, enter basic pay amount..!');
			document.employeeform.employeeBasicpay.focus();
			return false;
		}
		else if(!document.employeeform.employeeBasicpay.value.match(/^[0-9]+$/))
		{
			$('#employeeBasicpaySpan').html('Please, enter valid Basic Pay amount..!');
			document.employeeform.employeeBasicpay.value="";
			document.employeeform.employeeBasicpay.focus();
			return false;
		}
		
		//validation for employee HRA
		if(document.employeeform.employeeHra.value.length!=0)
		{
			if(!document.employeeform.employeeHra.value.match(/^[0-9]+[.]{1}[0-9]{1,2}$/))
			{
				$('#employeeHraSpan').html('Please, enter valid HRA amount..!');
				document.employeeform.employeeHra.value="";
				document.employeeform.employeeHra.focus();
				return false;
			}
		}
		
		//validation for employee DA
		if(document.employeeform.employeeDa.value.length!=0)
		{
			if(!document.employeeform.employeeDa.value.match(/^[0-9]+[.]{1}[0-9]{1,2}$/))
			{
				$('#employeeDaSpan').html('Please, enter valid DA amount..!');
				document.employeeform.employeeDa.value="";
				document.employeeform.employeeDa.focus();
				return false;
			}
		}
		
		//validation for employee CA
		if(document.employeeform.employeeCa.value.length!=0)
		{
			if(!document.employeeform.employeeCa.value.match(/^[0-9]+[.]{1}[0-9]{1,2}$/))
			{
				$('#employeeCaSpan').html('Please, enter valid CA amount..!');
				document.employeeform.employeeCa.value="";
				document.employeeform.employeeCa.focus();
				return false;
			}
		}
		
		//validation for employee PF a/c no
		if(document.employeeform.employeePfacno.value.length!=0)
		{
			if(!document.employeeform.employeePfacno.value.match(/^[A-Za-z0-9/]$/))
			{
				$('#employeePfacnoSpan').html('Please, enter valid P.F. a/c number..!');
				document.employeeform.employeePfacno.value="";
				document.employeeform.employeePfacno.focus();
				return false;
			}
		}
		
		//validation for employee ESI no
		if(document.employeeform.employeeEsino.value.length!=0)
		{
			if(!document.employeeform.employeeEsino.value.match(/^[0-9]{2}[-]{1}[0-9]{2}[-]{1}[0-9]{6}[-]{1}[0-9]{3}[-]{1}[0-9]{4}$/))
			{
				$('#employeeEsinoSpan').html('Please, enter valid ESI number..!');
				document.employeeform.employeeEsino.value="";
				document.employeeform.employeeEsino.focus();
				return false;
			}
		}
		
		//validation for employee bank a/c no
		if(document.employeeform.employeeBankacno.value.length!=0)
		{
			if(!document.employeeform.employeeBankacno.value.match(/^[0-9]{6,18}$/))
			{
				$('#employeeBankacnoSpan').html('Please, enter valid Bank a/c number..!');
				document.employeeform.employeeBankacno.value="";
				document.employeeform.employeeBankacno.focus();
				return false;
			}
		}
		
		//validation for employee bank name
		if(document.employeeform.employeeBankacno.value.length!=0)
		{
		if(document.employeeform.employeeBankacname.value == "Default")
		{
			$('#employeeBankacnameSpan').html('Please, select bank name..!');
			document.employeeform.employeeBankacname.focus();
			return false;
		}
		}
		//validation for PL leaves
		if(document.employeeform.employeePLleaves.value.length!=0)
		{
			if(!document.employeeform.employeePLleaves.value.match(/^[0-9]{1,2}$/))
			{
				$('#employeePLleavesSpan').html('Please, enter valid PL leaves..!');
				document.employeeform.employeePLleaves.value="";
				document.employeeform.employeePLleaves.focus();
				return false;
			}
		}
		
		//validation for SL leaves 
		if(document.employeeform.employeeSLleaves.value.length!=0)
		{
			if(!document.employeeform.employeeSLleaves.value.match(/^[0-9]{1,2}$/))
			{
				$('#employeeSLleavesSpan').html('Please, enter valid SL leaves..!');
				document.employeeform.employeeSLleaves.value="";
				document.employeeform.employeeSLleaves.focus();
				return false;
			}
		}
		
		//validation for CL leaves
		if(document.employeeform.employeeCLleaves.value.length!=0)
		{
			if(!document.employeeform.employeeCLleaves.value.match(/^[0-9]{1,2}$/))
			{
				$('#employeeCLleavesSpan').html('Please, enter valid CL leaves..!');
				document.employeeform.employeeCLleaves.value="";
				document.employeeform.employeeCLleaves.focus();
				return false;
			}
		}
		
		
}
 
function init()
{
	 clearall();
	 var n = new Date();
	 var y = n.getFullYear();
	 var m = n.getMonth()+1;
	 var d = n.getDate();
	 
	 
	 document.getElementById("updateDate").value = d + "/"+ m + "/" + y;
	 
	 if(document.employeeform.employeeStatus.value == "Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.employeeform.employeeStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record saved successfully..!');
	 }
	 
     document.employeeform.employeefirstName.focus();
}


function getpinCode()
{

	 $("#areaPincode").empty();
	 var locationareaName = $('#locationareaName').val();
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		//data : { locationareaName : locationareaName, cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								if(result[i].countryName==countryName)
									{
									if(result[i].stateName==stateName)
										{
											if(result[i].cityName==cityName)
											{
												if(result[i].locationareaName==locationareaName)
												{
													 $('#areaPincode').val(result[i].pinCode);
												}
											}
										}
									}
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getStateList()
{
	 $("#stateName").empty();
	 $("#cityName").empty();
	 $("#locationareaName").empty();
	 
	 var countryName = $('#countryName').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateName").append(option);
							
							var option1 = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityName").append(option1);
							
							var option2 = $('<option/>');
							option.attr('value',"Default").text("-Select Location-");
							$("#locationareaName").append(option2);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateName).text(result[i].stateName);
							    $("#stateName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityName").empty();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityName).text(result[i].cityName);
							    $("#cityName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List

function getLocationAreaList()
{var cityName = $('#secondcityName').val();
}//end of get locationarea List


function getuniqueaadharnumberName()
{
	
	 var employeeAadharcardno = $('#employeeAadharcardno').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getaadharList',
			type : 'Post',
			data : { employeeAadharcardno : employeeAadharcardno},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									
									if(result[i].employeeAadharcardno==employeeAadharcardno)
										{
										
											  document.employeeform.employeeAadharcardno.focus();
											  $('#employeeAadharcardnoSpan').html('This aadhar number is already exist..!');
										}
									
									
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			});	
}

function getBranchList()
{
	 
	 $("#branchName").empty();
	 var bankName = $('#employeeBankacname').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#employeeBankacname').val();
     var branchName = $('#branchName').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
							/* for(var i=0;i<result.length;i++)
							{
								 var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#bankifscCode").append(option); 
							}   */
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}



  $(function ()
  {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
     $('#datepicker1').datepicker({
      autoclose: true
    })
 $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>