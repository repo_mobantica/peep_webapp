<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Task</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Task Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Task</li>
      </ol>
    </section>

    <!-- Main content -->
    <form name="taskform" action="${pageContext.request.contextPath}/AddTask" onSubmit="return validate()" method="post">

    
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
              <!-- /.form-group -->
             
			
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label for="taskId">Task Id</label>
                  <input type="text" class="form-control" id="taskId" placeholder="ID" name="taskId"  value="${taskCode}" readonly>
                </div>               
                
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label>Project </label> <label class="text-red">* </label>
                 <select class="form-control" name="projectName" id="projectName" onchange="getBuldingList(this.value)">
				  <option selected="" value="Default">-Select Project-</option>
                     <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectName}">${projectList.projectName}</option>
				     </c:forEach>
                  </select>
                </div>               
                <div class="col-xs-6">
				 <label>Project Building </label> <label class="text-red">* </label>
                    <select class="form-control" name="buildingName" id="buildingName" onchange="getFloorTypeList(this.value)">
				  		<option selected="" value="Default">-Select a Project Unit-</option>
                     <c:forEach var="projectbuildingList" items="${projectbuildingList}">
                    	<option value="${projectbuildingList.buildingName}">${projectbuildingList.buildingName}</option>
				     </c:forEach>
                  </select>
				</div> 
              </div>
            </div>
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label>Floor </label> <label class="text-red">* </label>
                 <select class="form-control" name="floortypeName" id="floortypeName" >
				 		 <option selected="" value="Default">-Select Floor-</option>
                      <c:forEach var="floortypeList" items="${floortypeList}">
                    		<option value="${floortypeList.floortypeName}">${floortypeList.floortypeName}</option>
				      </c:forEach>
                  </select>
                </div>               
                <div class="col-xs-6">
				 <label>Flat </label> <label class="text-red">* </label>
                  <select class="form-control" name="flatNumber">
				    <option>All</option>
                 
                  </select>
				</div> 
              </div>
            </div>
				   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			      <label>Task Name </label> <label class="text-red">* </label>
                  <select class="form-control" name="taskName">
				  <option selected="" value="Default">-Select Task Name-</option>
                     <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                    
                  </select>
			     </div> 
				 
				 </div>
            </div>
				
				
				    <div class="box-body">
				<div class="row">
                  <div class="col-xs-12">
			   <label for="taskDescription">Task Description </label> <label class="text-red">* </label>
                 <textarea class="form-control" rows="2" id="taskDescription" placeholder="Task Description" name="taskDescription"></textarea>
			     </div> 
				  
              </div>
            </div>
				
				
				
				
				
				
			
             
            </div>
          
		  
            <div class="col-md-6">
			</br>
			</br>
			</br></br>
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label>Task Assign From </label> <label class="text-red">* </label>
                  <select class="form-control" name="taskassignFrom">
				  <option selected="" value="Default">-Select Name-</option>
                     <c:forEach var="employeeList" items="${employeeList}">
                    	<option value="${employeeList.employeefirstName} ${employeeList.employeemiddleName} ${employeeList.employeelastName}">${employeeList.employeefirstName} ${employeeList.employeemiddleName} ${employeeList.employeelastName}</option>
				     </c:forEach>
                  </select>
                </div>               
                <div class="col-xs-6">
				 <label>Task Assign To </label> <label class="text-red">* </label>
                  <select class="form-control" name="taskassignTo">
				  <option selected="" value="Default">-Select Emp Name-</option>
                 <c:forEach var="employeeList" items="${employeeList}">
                    	<option value="${employeeList.employeefirstName} ${employeeList.employeemiddleName} ${employeeList.employeelastName}">${employeeList.employeefirstName} ${employeeList.employeemiddleName} ${employeeList.employeelastName}</option>
				     </c:forEach>
                  </select>
				</div> 
              </div>
            </div>
			 <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			   
                  <label for="emailid">Task Assing Date</label> <label class="text-red">* </label>
				    <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                    <input type="text" class="form-control pull-right" id="datepicker1" name="taskassingDate">
              
                </div>
			     </div> 
				  <div class="col-xs-6">
			    <label>Task End Date</label> <label class="text-red">* </label>
			     <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                 <input type="text" class="form-control pull-right" id="datepicker" name="taskendDate">
                </div>
				 </div> 
              </div>
            </div>
			 
			  <div class="box-body">
              <div class="row">
			     
                  <div class="col-xs-6">
                  <label>Status</label> <label class="text-red">* </label>
                  <select class="form-control" name="TaskStatus">
                    <option>Not Started</option>
                    <option>In-Process</option>
					 <option>In-Complete</option>
                    <option>Complete</option>
                  </select>
                </div>               
              </div>
            </div>
			  	
			 </br></br>	  
			
			
            </div>
			
            <!-- /.col -->
			
          </div>
		   	 <div class="box-body">
              <div class="row">
              </br>
                  <div class="col-xs-3">
                  </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Previus Tasks</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Task Id</th>
                  <th>Project Name</th>
                  <th>Unit Name</th>
                  <th>Floor</th>
				   <th>Flat</th>
                  <th>Task Assign By</th>
                  <th>Task Assign To</th>
                  <th>Task Name</th>
				   <th>Task Assign Date</th>
                  <th>Task End Date</th>
                  <th>Task Description</th>
                  <th>Status</th>
                </tr>
                </thead>
				
                <tbody>
               
                <tr>
                  <td>101</td>
                  <td>Mobantica</td>
                  <td>Java</td>
                  <td>All</td>
				  <td>All</td>
                  <td>Manish</td>
                  <td>Admin</td>
                  <td>UI</td>
                  <td>12/28/2017</td>
				  <td>12/29/2017</td>
                  <td>-</td>
                  <td>IN-Process</td>
                </tr>
             
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
  

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function getBuldingList()
{
	 $("#buildingName").empty();
	 var projectName = $('#projectName').val();

	 $.ajax({

		url : '/getBuildingList',
		type : 'Post',
		data : { projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingName).text(result[i].buildingName);
							    $("#buildingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Building List

function getFloorTypeList()
{
	 $("#floortypeName").empty();
	 var buildingName = $('#buildingName').val();
    
	 $.ajax({

		url : '/getFloorTypeList',
		type : 'Post',
		data : { buildingName : buildingName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Type-");
							$("#floortypeName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#floortypeName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Floor Type List





function validate()
{ 
		if(document.taskform.project.value=="Default")
		{
			alert("Please Select Project Name");
			document.taskform.project.focus();
			return false;
		}
		if(document.taskform.projectunit.value=="Default")
		{
			alert("Please Select Project Building Name");
			document.taskform.projectunit.focus();
			return false;
		}
		if(document.taskform.floor.value=="")
		{
			alert("Please Select Floor");
			document.taskform.floor.focus();
			return false;
		}
		if(document.taskform.flat.value=="")
		{
			alert("Please Select Flat");
			document.taskform.flat.focus();
			return false;
		}
		if(document.taskform.taskName.value=="Default")
		{
			alert("Please Select Task Name");
			document.taskform.taskName.focus();
			return false;
		}
		if(document.taskform.taskDescription.value=="")
		{
			alert("Please Fill Task Description");
			document.taskform.taskDescription.focus();
			return false;
		}
		
		if(document.taskform.taskassignFrom.value=="Default")
		{
			alert("Please Select Task Assigning Name");
			document.taskform.taskassignFrom.focus();
			return false;
		}
		if(document.taskform.taskassignTo.value=="Default")
		{
			alert("Please Select Task Assigned Emp Name");
			document.taskform.taskassignTo.focus();
			return false;
		}
		if(document.taskform.taskassingDate.value=="")
		{
			alert("Please Fill Task Assign Date");
			document.taskform.taskassingDate.focus();
			return false;
		}
		if(document.taskform.taskendDate.value=="")
		{
			alert("Please Fill Task Assign End Date");
			document.taskform.taskendDate.focus();
			return false;
		}
		if(document.taskform.taskStatus.value=="Default")
		{
			alert("Please Select Task Status");
			document.taskform.taskStatus.focus();
			return false;
		}
		
		
		
		
}
function init()
{
  document.taskform.project.focus();
}

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker1').datepicker({
      autoclose: true
    })
 $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
