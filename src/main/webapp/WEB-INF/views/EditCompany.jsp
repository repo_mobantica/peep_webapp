<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Edit Company</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
         
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  
  <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Company Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Company</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="companyform" action="${pageContext.request.contextPath}/EditCompany" method="Post" onSubmit="return validate()">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
          <span id="statusSpan" style="color:#FF0000"></span>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
          <div class="col-md-12">
              
              <!-- /.form-group -->
			  
			  
			     <div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
			     <label for="companyId">Company Id</label>
                  <input type="text" class="form-control"  id="companyId" name="companyId"  value="${companyDetails[0].companyId}" readonly>
                
			     </div> 
				</div>
            </div>
			  
			  
			     <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
                  <label for="companyName">Company Name</label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="companyName" placeholder="Enter Company Name" name="companyName" value="${companyDetails[0].companyName}" onchange="getuniquecompanyName(this.value)">
                  <span id="companyNameSpan" style="color:#FF0000"></span>
			     </div> 
			       <div class="col-xs-3">
                  <label>Select Company Type</label> <label class="text-red">* </label>
                   <select class="form-control" name="companyType" id="companyType">
                    			<option selected="" value="${companyDetails[0].companyType}">${companyDetails[0].companyType}</option>
	                    <c:choose>
			                 <c:when test="${companyDetails[0].companyType ne 'Private'}">
			                    <option value="Private">Private</option>
			                 </c:when>
			            </c:choose>
			            <c:choose>
			                 <c:when test="${companyDetails[0].companyType ne 'Public'}">
			                    <option value="Public">Public</option>
			                 </c:when>
			            </c:choose>
						<c:choose>
			                 <c:when test="${companyDetails[0].companyType ne 'Proprietary'}">
			                    <option value="Proprietary">Proprietary</option>
			                 </c:when>
	                    </c:choose>
                  </select>
                    <span id="companyTypeSpan" style="color:#FF0000"></span>
			     </div> 
			        <div class="col-xs-3">
			       <label for="companyPanno">PAN Number </label> <label class="text-red">* </label>
                <input type="text" class="form-control" id="companyPanno" placeholder="PAN No " name="companyPanno"value="${companyDetails[0].companyPanno}">
                  <span id="companyPannoSpan" style="color:#FF0000"></span>
                   </div> 
                   
                   <div class="col-xs-3">
			       <label for="companyRegistrationno">Company Registration No </label>
                <input type="text" class="form-control" id="companyRegistrationno" placeholder="Company Reg No " name="companyRegistrationno" value="${companyDetails[0].companyRegistrationno}">
                   <span id="companyRegistrationnoSpan" style="color:#FF0000"></span>
                  </div>
				</div>
            </div>
			  		<div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			       <label for="companyTanno">TAN Number </label> 
                <input type="text" class="form-control" id="companyTanno" placeholder="TAN No " name="companyTanno"value="${companyDetails[0].companyTanno}">
                  <span id="companyTannoSpan" style="color:#FF0000"></span>
                   </div> 
                   
                   <div class="col-xs-3">
			       <label for="companyPfno">Company PF A/c No </label>
                		<input type="text" class="form-control" id="companyPfno" placeholder="PF No " name="companyPfno" value="${companyDetails[0].companyPfno}">
                   <span id="companyPfnoSpan" style="color:#FF0000"></span>
                  </div>
                      <div class="col-xs-3">
			    <label for="companyGstno">GST No </label> <label class="text-red">* </label>
               <input type="text" class="form-control" id="companyGstno" placeholder="GST No " name="companyGstno" value="${companyDetails[0].companyGstno}">
                  <span id="companyGstnoSpan" style="color:#FF0000"></span>
			     </div>
              </div>
            </div>
			  
			  
			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			   <label for="companyAddress">Address </label> <label class="text-red">* </label>
                  <textarea class="form-control" rows="1" placeholder="Enter Address" name="companyAddress" id="companyAddress">${companyDetails[0].companyAddress}</textarea>
			    <span id="companyAddressSpan" style="color:#FF0000"></span>
			     </div> 
			
                  <div class="col-xs-3">
                  <label>Country</label> <label class="text-red">* </label>
                             <select class="form-control" id="countryName" name="countryName" onchange="getStateList(this.value)">
                  <option selected="" value="${companyDetails[0].countryName}">${companyDetails[0].countryName}</option>
                    <c:forEach var="countryList" items="${countryList}">
                     <c:choose>
                      <c:when test="${companyDetails[0].countryName ne countryList.countryName}">
	                    <option value="${countryList.countryName}">${countryList.countryName}</option>
	                  </c:when>
	                 </c:choose>
	                 </c:forEach>
                  </select>
                    <span id="countryNameSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-3">
				 <label>State</label> <label class="text-red">* </label>
                    <select class="form-control" id="stateName" name="stateName" onchange="getCityList(this.value)">
                  <option selected="" value="${companyDetails[0].stateName}">${companyDetails[0].stateName}</option>
                    <c:forEach var="stateList" items="${stateList}">
                      <c:choose>
                       <c:when test="${companyDetails[0].stateName ne stateList.stateName}">
                    	    <option value="${stateList.stateName}">${stateList.stateName}</option>
                       </c:when>
                      </c:choose>
				    </c:forEach>  
                  </select>
                    <span id="stateNameSpan" style="color:#FF0000"></span>
				</div> 
				     <div class="col-xs-3">
				  <label>City</label> <label class="text-red">* </label>
                        <select class="form-control" name="cityName" id="cityName" onchange="getLocationAreaList(this.value)">
				 <option selected="" value="${companyDetails[0].cityName}">${companyDetails[0].cityName}</option>
                     <c:forEach var="cityList" items="${cityList}">
                      <c:choose>
                        <c:when test="${companyDetails[0].cityName ne cityList.cityName}">
                    		<option value="${cityList.cityName}">${cityList.cityName}</option>
                        </c:when>
                      </c:choose>
				     </c:forEach>  
                  </select>
                    <span id="cityNameSpan" style="color:#FF0000"></span>
				  </div>
			
              </div>
            </div>
				
			
			
		
			
			  <div class="box-body">
              <div class="row">
              	      <div class="col-xs-3">
				    <label>Area</label> <label class="text-red">* </label>
                    	<select class="form-control"name="locationareaName" id="locationareaName" onchange="getpinCode(this.value)">
				  	<option selected="" value="${companyDetails[0].locationareaName}">${companyDetails[0].locationareaName}</option>
				  	  <c:forEach var="locationareaList" items="${locationareaList}">
				  	   <c:choose>
				  	     <c:when test="${companyDetails[0].locationareaName ne locationareaList.locationareaName}">
                    			<option value="${locationareaList.locationareaName}">${locationareaList.locationareaName}</option>
                    	 </c:when>
                       </c:choose>
				      </c:forEach> 
                  	</select>
                    <span id="locationareaNameSpan" style="color:#FF0000"></span>
				  </div>
                  <div class="col-xs-3">
                  <label for="companyPincode">Pin Code</label> <label class="text-red">* </label>
                   <input type="text" class="form-control" id="companyPincode" placeholder="Enter Pin Code" name="companyPincode" value="${companyDetails[0].companyPincode}" readonly>
			       <span id="companyPincodeSpan" style="color:#FF0000"></span>
			     </div> 
				  
				  <div class="col-xs-3">
			    <label for="companyPhoneno">Office Phone No</label> <label class="text-red">* </label>
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                   <input type="text" class="form-control" data-inputmask='"mask": "(999) 999-99999"' data-mask name="companyPhoneno" id="companyPhoneno" value="${companyDetails[0].companyPhoneno}">
                <span id="companyPhonenoSpan" style="color:#FF0000"></span>
                </div>
			     </div> 
				  
              </div>
            </div>
				
			
			<div class="box-body">
              <div class="row">
              	<div class="col-xs-3">
			    <label>Bank Name </label> <label class="text-red">* </label>
                 <select class="form-control" name="companyBankname" id="companyBankname" onchange="getBranchList(this.value)">
				  <option selected="" value="Default">-Select Bank-</option>
                  <c:forEach var="bankList" items="${bankList}">
                  <option value="${bankList.bankName}">${bankList.bankName}</option>
				  </c:forEach> 
                  </select>
			     <span id="companyBanknameSpan" style="color:#FF0000"></span>
			     </div> 
			     <div class="col-xs-3">
			    <label>Bank Branch Name </label> <label class="text-red">* </label>
			         <select class="form-control" id="branchName" name="branchName" onchange="getBranchIfsc(this.value)">
                      		<option selected="selected" value="Default">-Select Bank Branch-</option>
                      	 <c:forEach var="locationareaList" items="${bankBranchList}">
                    	    <option value="${bankBranchList.branchName}">${bankBranchList.branchName}</option>
				         </c:forEach>
                        </select>
				 <span id="branchNameSpan" style="color:#FF0000"></span>
			     </div> 
			     <div class="col-xs-3">
			    <label>IFSC </label> <label class="text-red">* </label>
                 <input type="text" class="form-control" id="bankifscCode" placeholder="Bank IFSC No " name="bankifscCode" readonly>
			     <span id="bankifscCodeSpan" style="color:#FF0000"></span>
			     </div> 
                  <div class="col-xs-3">
			    <label for="companyBankacno">Bank A/C No </label> <label class="text-red">* </label>
                <div class="input-group">
         		 <input type="text" class="form-control" id="companyBankacno" placeholder="Bank A/C No " name="companyBankacno">
                <span id="companyBankacnoSpan" style="color:#FF0000"></span>
         		 <span class="input-group-btn">
            	  <button type="button" class="btn btn-success" onclick="AddCompanyBank()"><i class="fa fa-plus"></i>Add </button>
              	</span>
       			 </div>
                </div> 
			    
			     </div>
			     </div>		    
			
			<div class="box-body">
              <div class="row">
               <div class="col-xs-12">
               
              <table class="table table-bordered" id="comapnyBanktable">
              <thead>
	                  <tr bgcolor=#4682B4>
	                 <td>Bank Name</td>
             		 <td>Branch Name</td>
             		 <td>IFSC</td>
             		 <td>Account number</td>
	                 <td style="width:50px">Action</td> 
	                  </tr>
                  </thead>
              
           
              <tbody>
                   <c:forEach items="${companyBankList}" var="companyBankList" varStatus="loopStatus">
                    <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
	                        <td>${companyBankList.companyBankname}</td>
	                        <td>${companyBankList.branchName}</td>
	                        <td>${companyBankList.bankifscCode}</td>
	                         <td>${companyBankList.companyBankacno}</td>
	                        <td><a onclick="DeleteCompanyBank(${companyBankList.companyBankId})" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a></td>
                      </tr>
					</c:forEach>
                 
                 </tbody>
              
              </table>
              </div>
              </div>
			  
			  <input type="hidden" id="companyStatus" name="companyStatus" value="${companyStatus}">	
			  <input type="hidden" id="companyFinancialyear" name="companyFinancialyear" value="">
			  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="Admin">
			
			
			     <div class="box-body">
              <div class="row">
              </br>
                 <div class="col-xs-4">
                <a href="CompanyMaster"><button type="button" class="btn btn-block btn-primary" style="width:90px">Back</button></a>
			     </div>
			     
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
				  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
              </div>
			  </div>
	    </div>
      	
          </div>
		  
		  
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#companyprofile-img").change(function(){
        readURL(this);
    });
    function AddCompanyBank()
    {
    	clearall();
    	
    	//validation for company bank name
    	if(document.companyform.companyBankname.value=="Default")
    	{
    		$('#companyBanknameSpan').html('Please, select company bank name..!');
    		document.companyform.companyBankname.focus();
    		return false;
    	}
    	
    	//validation for company bank name
    	if(document.companyform.branchName.value=="Default")
    	{
    		$('#branchNameSpan').html('Please, select bank branch name..!');
    		document.companyform.branchName.focus();
    		return false;
    	}

    	//validation for company bank account number
    	if(document.companyform.companyBankacno.value=="")
    	{
    		$('#companyBankacnoSpan').html('Please, enter bank A/C number..!');
    		document.companyform.companyBankacno.focus();
    		return false;
    	}
    	else if(!document.companyform.companyBankacno.value.match(/^[0-9]{6,18}$/))
    	{
    		$('#companyBankacnoSpan').html(' bank A/C number must be number ranges from 6 to 18 digit..!');
    		document.companyform.companyBankacno.value="";
    		document.companyform.companyBankacno.focus();
    		return false;
    	}
    	
    	
    	

    	$("#comapnyBanktable td").detach();
    	var companyId = $('#companyId').val();
    	var bankName = $('#companyBankname').val();
    	var branchName = $('#branchName').val();
    	var bankifscCode = $('#bankifscCode').val();
    	var companyBankacno = $('#companyBankacno').val();
    	$.ajax({

    		url : '${pageContext.request.contextPath}/AddCompanyBank',
    		type : 'Post',
    		data : { companyId : companyId, bankName : bankName, branchName : branchName,bankifscCode : bankifscCode, companyBankacno : companyBankacno},
    		dataType : 'json',
    		success : function(result)
    				  {
    						if (result) 
    						{
    							 
    						$('#comapnyBanktable').append('<tr style="background-color: #4682B4;"><td style="width:150px">Bank Name</td> <td style="width:150px">Branch Name</td><td style="width:150px">IFSC</td> <td style="width:150px">Account number  </td><td style="width:150px">Action </td>');
    						
    							for(var i=0;i<result.length;i++)
    							{ 
    								if(i%2==0)
    								{
    									var id = result[i].companyBankId;
    									$('#comapnyBanktable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyBankname+'</td><td>'+result[i].branchName+'</td><td>'+result[i].bankifscCode+'</td><td>'+result[i].companyBankacno+'</td><td><a onclick="DeleteCompanyBank('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>');
    								}
    								else
    								{
    									var id = result[i].companyBankId;
    									$('#comapnyBanktable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyBankname+'</td><td>'+result[i].branchName+'</td><td>'+result[i].bankifscCode+'</td><td>'+result[i].companyBankacno+'</td><td><a onclick="DeleteCompanyBank('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>');
    								}
    							
    							 } 
    							
    						} 
    						else
    						{
    							alert("failure111");
    						}

    					}
    		});	
    	
    	
    }

    function DeleteCompanyBank(bankId)
    {
    	alert("fk");
        var companyId = $('#companyId').val();
        var companyBankId = bankId;
    	$('#bankEmployeeListTable tr').detach();
        $.ajax({

    		 url : '${pageContext.request.contextPath}/DeleteCompanyBank',
    		type : 'Post',
    		data : { companyId : companyId, companyBankId : companyBankId },
    		dataType : 'json',
    		success : function(result)
    				  {
    					   if (result) 
    					   { 

    							 
    					        $('#comapnyBanktable').append('<tr style="background-color: #4682B4;"><td>Bank Name</td> <td>Branch Name</td><td>IFSC</td> <td>Account number  </td>');
    				
    					for(var i=0;i<result.length;i++)
    					{ 
    						if(i%2==0)
    						{
    							var id = result[i].companyBankId;
    							$('#comapnyBanktable').append('<tr style="background-color: #F0F8FF;"><td>'+result[i].companyBankname+'</td><td>'+result[i].branchName+'</td><td>'+result[i].bankifscCode+'</td><td>'+result[i].companyBankacno+'</td><td><a onclick="DeleteCompanyBank('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>');
    						}
    						else
    						{
    							var id = result[i].companyBankId;
    							$('#comapnyBanktable').append('<tr style="background-color: #CCE5FF;"><td>'+result[i].companyBankname+'</td><td>'+result[i].branchName+'</td><td>'+result[i].bankifscCode+'</td><td>'+result[i].companyBankacno+'</td><td><a onclick="DeleteCompanyBank('+id+')" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>');
    						}
    					
    					 } 
    					
    						} 
    						else
    						{
    							alert("failure111");
    						}
    				  } 

    		});
    }
    function getpinCode()
    {

    	 $("#companyPincode").empty();
    	 var locationareaName = $('#locationareaName').val();
    	 var cityName = $('#cityName').val();
    	 var stateName = $('#stateName').val();
    	 var countryName = $('#countryName').val();
    	 $.ajax({

    		 url : '${pageContext.request.contextPath}/getallAreaList',
    		type : 'Post',
    		//data : { locationareaName : locationareaName, cityName : cityName, stateName : stateName, countryName : countryName},
    		dataType : 'json',
    		success : function(result)
    				  {
    						if (result) 
    						{
    							
    							for(var i=0;i<result.length;i++)
    							{
    								if(result[i].countryName==countryName)
    									{
    									if(result[i].stateName==stateName)
    										{
    											if(result[i].cityName==cityName)
    											{
    												if(result[i].locationareaName==locationareaName)
    												{
    													 $('#companyPincode').val(result[i].pinCode);
    												}
    											}
    										}
    									}
    								
    							 } 
    						
    						} 
    						else
    						{
    							alert("failure111");
    							//$("#ajax_div").hide();
    						}

    					}
    		});
    	
    }

 function getuniquecompanyName()
 {
	 var companyName = $('#companyName').val();
		$.ajax({

			url : '${pageContext.request.contextPath}/getcompanyList',
			type : 'Post',
			data : { companyName : companyName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									
									if(result[i].companyName==companyName)
										{
											  document.companyform.companyName.focus();
											  $('#companyNameSpan').html('This company is already exist..!');
											  $('#companyName').val("");
										}
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			});	 
	 
 }
 
function validate()
{ 
		clearall();
		
	    //validation for company name
		if(document.companyform.companyName.value=="")
		{
			$('#companyNameSpan').html('Please, enter company name..!');
			document.companyform.companyName.focus();
			return false;
		}
		else if(document.companyform.companyName.value.match(/^[\s]+$/))
		{
			$('#companyNameSpan').html('Please, enter company name..!');
			document.companyform.companyName.value="";
			document.companyform.companyName.focus();
			return false;
		}
	
	    
		//validation for company pan no
		if(document.companyform.companyPanno.value=="")
		{
			$('#companyPannoSpan').html('Please, enter pan number..!');
			document.companyform.companyPanno.focus();
			return false;
		}
		else if(!document.companyform.companyPanno.value.match(/^[A-Za-z]{5}[0-9]{4}[A-z]{1}$/))
		{
			$('#companyPannoSpan').html(' PAN number must start with 5 alphabets follwed by 4 digit number and 1 alphabet..!');
			document.companyform.companyPanno.value="";
			document.companyform.companyPanno.focus();
			return false;
		}
		
		//validation for company service tax no
		if(document.companyform.companyRegistrationno.value.length!=0)
		{			
			if(!document.companyform.companyRegistrationno.value.match(/^[0-9]{15}$/))
			{
				$('#companyRegistrationnoSpan').html('service tax number must be 15 digit numbers only..!');
				document.companyform.companyRegistrationno.value="";
				document.companyform.companyRegistrationno.focus();
				return false;
			}
		}
		
		//validation for company GST no
		if(document.companyform.companyGstno.value=="")
		{
			$('#companyGstnoSpan').html('Please, Enter GST number..!');
			document.companyform.companyGstno.focus();
			return false;
		}
		else if(!document.companyform.companyGstno.value.match(/^[0-9]{2}[A-Za-z]{5}[0-9]{4}[A-z]{1}[0-9]{1}[Zz]{1}[0-9]{1}$/))
		{
			$('#companyGstnoSpan').html('GST number must match format like(22AAAAA0000A1Z5)..!');
			document.companyform.companyGstno.value="";
			document.companyform.companyGstno.focus();
			return false;
		}
		
	    
	    //validation for company address
		if(document.companyform.companyAddress.value=="")
		{
			$('#companyAddressSpan').html('Please, enter company address..!');
			document.companyform.companyAddress.focus();
			return false;
		}
		else if(document.companyform.companyAddress.value.match(/^[\s]+$/))
		{
			$('#companyAddressSpan').html('Please, enter company address..!');
			document.companyform.companyAddress.value="";
			document.companyform.companyAddress.focus();
			return false;
		}
	
	    
	    //validation for company country
		if(document.companyform.countryName.value=="Default")
		{
			$('#countryNameSpan').html('Please, select country name..!');
			document.companyform.countryName.focus();
			return false;
		}
	    
	    //validation for company state
		if(document.companyform.stateName.value=="Default")
		{
			$('#stateNameSpan').html('Please, select state name..!');
			document.companyform.stateName.focus();
			return false;
		}
	    
	    //validation for company city
		if(document.companyform.cityName.value=="Default")
		{
			$('#cityNameSpan').html('Please, select city name..!');
			document.companyform.cityName.focus();
			return false;
		}
	    
	    //validation for company area
		if(document.companyform.locationareaName.value=="Default")
		{
			$('#locationareaNameSpan').html('Please, select area name..!');
			document.companyform.locationareaName.focus();
			return false;
		}
	    
	    
		
	    //validation for company office phone no
		if(document.companyform.companyPhoneno.value=="")
		{
			$('#companyPhonenoSpan').html('Please, enter company phone number..!');
			document.companyform.companyPhoneno.focus();
			return false;
		}
		else if(!document.companyform.companyPhoneno.value.match(/^\(?([0-9]{3})\)?[\s. ]?([0-9]{3})[-. ]?([0-9]{5})$/))
		{
			$('#companyPhonenoSpan').html(' phone number must be 10 digit numbers only..!');
			document.companyform.companyPhoneno.value="";
			document.companyform.companyPhoneno.focus();
			return false;
		}
	
		
}

function clearall()
{
	$('#companyNameSpan').html('');
	$('#companyAddressSpan').html('');
	$('#countryNameSpan').html('');
	$('#stateNameSpan').html('');
	$('#cityNameSpan').html('');
	$('#locationareaNameSpan').html('');
	$('#companyPincodeSpan').html('');
	$('#companyPhonenoSpan').html('');
	$('#companyPannoSpan').html('');
	$('#companyServicetaxnoSpan').html('');
	$('#companyGstnoSpan').html('');
	$('#companyBankacnoSpan').html('');
	$('#companyBanknameSpan').html('');	
	$('#branchNameSpan').html('');
}

function init()
{
	clearall();
	 var n = new Date();
	 var y = n.getFullYear();
	 var m = n.getMonth()+1;
	 var d = n.getDate();
	 
	 document.getElementById("updateDate").value = d + "/"+ m + "/" + y;
	 
	 document.getElementById("companyFinancialyear").value = (y-1)+"-"+y;
	 
	 if(document.companyform.companyStatus.value == "Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.companyform.companyStatus.value == "Success")
	 {
		 $('#statusSpan').html('Record saved successfully..!');
	 }
	 
   	 document.companyform.companyName.focus();
}


function getBranchList()
{
	 $("#branchName").empty();
	 var bankName = $('#companyBankname').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#companyBankname').val();
     var branchName = $('#branchName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
							/* for(var i=0;i<result.length;i++)
							{
								 var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#bankifscCode").append(option); 
							}   */
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

function getStateList()
{
	$("#stateName").empty();
	$("#cityName").empty();
	$("#locationareaName").empty();
	
	var countryName = $('#countryName').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateName").append(option);
							
							var option1 = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityName").append(option1);
							
							var option2 = $('<option/>');
							option.attr('value',"Default").text("-Select Location-");
							$("#locationareaName").append(option2);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateName).text(result[i].stateName);
							    $("#stateName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityName").empty();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityName).text(result[i].cityName);
							    $("#cityName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List

function getLocationAreaList()
{var cityName = $('#secondcityName').val();
	
}//end of get locationarea List





  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>