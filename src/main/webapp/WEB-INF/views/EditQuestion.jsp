<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PEEP |Add Correct Option</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="styleshee
  t" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="addOptions()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-resubmitdata");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">
  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Correct Option :
    
      </h1>
    </section>

    <!-- Main content -->
	
<form name="correctOptionform" action="${pageContext.request.contextPath}/SaveEditQuestionData" onSubmit="return validate()" method=GET>
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
          <%--     <div class="box-body">
          <div class="row">
            <div class="col-md-4">
                  <label for="countyId">Country Id</label>
                  <input type="text" class="form-control" id="countryId" name="countryId"  value="${countryCode}" readonly>
                  </div>
                  </div>
                </div> --%>
			
		<input type="hidden" class="form-control" id="videoLink" name="videoLink"  value="${videoLink}" readonly>
                  
				
		<div class="box-body">
          <div class="row">
            <div class="col-md-12">
          
	              		
	            			
             				 <div class="col-xs-12"><br>
                 				 <label for="activityName">Activity Name </label> <label class="text-red">* </label>
                 				 	<select class="form-control" id="activityId" name="activityId">
                 				 	<%-- <c:if test="${activityDetails[0].activityId} eq ${questionDetails[0].activityId}"> --%>
                 				 	 	
							 			<option value="0">Select Activity</option> 
							 			<c:forEach items="${activityDetailsList}" var="activity">
							 			<c:if test="${ activity.activityId ne questionDetails[0].activityId}">
	                   					
  								 			<option value="${activity.activityId}">${activity.activityName}</option> 
  								 											
  					   						
  					   						</c:if> 
  					   						</c:forEach>
  					   					<c:forEach items="${activityDetailsList}" var="activity">
  					   					<c:if test="${activity.activityId eq questionDetails[0].activityId}">
							 			
	                   					
  								 			<option value="${activity.activityId}" selected>${activity.activityName}</option> 								
  					   						
  					   					</c:if> 
  					   					</c:forEach>
  					  				 </select>
  					  				 
                 				<span id="activityNameSpan" style="color:#FF0000"></span>
              				</div>
              				
              				<div class="col-xs-12"><br>
                 				 <label for="questionName">Question  Name </label> <label class="text-red">* </label>
                 				 <input type="text" class="form-control" id="questionName" name="questionName" placeholder="Question  Name" value="${questionDetails[0].questionName}">
                 				<span id="questionNameSpan" style="color:#FF0000"></span>
              				</div>
              		
	    				 <div  class="col-xs-12"><br>
                					<label for="optionName">Add Options </label> <label class="text-red">* </label><br>
                				    <input type="text" class="form-control" id="optionName" name="optionName" placeholder="Option Name" >
                		<span id="optionNameSpan" style="color:#FF0000"></span>
                		</div>
                 
                		<div class="col-xs-12"><br>
                				<label for="optionName123">&nbsp;</label> <label class="text-red">    </label><br>
                				<input type="button" class="btn btn-success" onclick="addnewOption()" value="Add More Options"/><br><br>
                		</div>
			               			
			               			
			             <div class="col-xs-12"><br>
               				<table id="myTable"  width="200" style="table-layout:fixed" border="1" align="left">
								<tr><th bgcolor="#3c8dbc"> <center>Question Name </center>    </th><th bgcolor="#3c8dbc">  <center>   Action </center>     </th>
										<c:forEach items="${optionDetailsList}" var="optionDetailsList">
										<col width="200">
										<col width="200">
										
  								 			<td>${optionDetailsList.optionName}</td> 
  								 			<td> <a class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>  <a onclick="myEditFunction()" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>
	 										</td><br>								
  					   						</c:forEach>
									
								</tr>
							</table>
	              		</div> 
	                 			
	             			
                		
			   			  <div class="col-xs-12"><br><br><br><br>
						        <label>Choose Correct Option </label><label class="text-red">* </label><br>
			             	    <select  class="form-control" name="correctOptionId" id="correctOptionId">
							  	
			                   	 <option value="0" selected="selected">--Select Correct Option--</option>
			                   	 <%-- 	<option value="${optionList.optionId}">${optionList.optionName}</option> --%>
							 <c:forEach items="${optionDetailsList}" var="option">
							 			<c:if test="${ option.optionName ne questionDetails[0].correctOptionId}">
	                   					
  								 			<option value="${option.optionId}">${option.optionName}</option> 
  								 											
  					   						
  					   						</c:if> 
  					   						</c:forEach>
  					   					
  					   					<c:forEach items="${optionDetailsList}" var="option">
							 			<c:if test="${ option.optionName eq questionDetails[0].correctOptionId}">
	                   					
  								 			<option value="${option.optionId}" selected >${option.optionName}</option> 
  								 											
  					   						
  					   						</c:if> 
  					   						</c:forEach>
			                    </select>
                   			 <span id="correctOptionIdSpan" style="color:#FF0000"></span>
			      		</div> 
			    
			      
                 </div>
               </div>
           </div>
		<input type="hidden" id="status" name="status" value="1">	
		<input type="hidden" id="createdDate" name="createdDate" >
		<input type="hidden" id="updateDate" name="updateDate" >
		<input type="hidden" id="optionNames" name="optionNames" >
			<input type="hidden" class="form-control" id="questionId" name="questionId" placeholder="Question  Name" value="${questionDetails[0].questionId}">
                 			
			<!-- 	<input type="hidden" id="userName" name="userName" placeholder="Admin"> -->
			
			
			<input type="hidden" id="correctOption" name="correctOption" >
				
				<!-- //	<input type="hidden" id="deleteOptions" name="deleteOptions"  value> -->
				
              <!-- /.form-group -->
            </div>
            
			
          </div>
			  
			  <div class="box-body">
              <div class="row"><br><br>
                <div class="col-xs-2"></div><div class="col-xs-1"></div>
                      <div class="col-xs-2">
                	<a href="QuestionMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-2">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-1">
			  				<button type="submit" id="submit" class="btn btn-info " name="submit">Submit</button>
			        </div> 
                 </div>
			  </div>
			  
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
           
      </div>
      <!-- /.box -->
			
			<!-- Design For View Table Of Country List -->
    
   
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
  
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
   
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function validate()
{
	 clearAll();
	 if(document.correctOptionform.activityId.value=="0"  )
		{
		  $('#activityNameSpan').html('Please, enter Activity Name..!');
			document.correctOptionform.activityId.focus();
			return false;
		}
	 if(document.correctOptionform.questionName.value=="")
		{
		  $('#questionNameSpan').html('Please, enterRed Question Name..!');
			document.correctOptionform.questionName.focus();
			return false;
		}
	 if(document.correctOptionform.optionName.value=="")
		{
		  $('#optionNameSpan').html('Please, Enter Atleat One Option..!');
			document.correctOptionform.optionName.focus();
			return false;
		}
	  
	  if(document.correctOptionform.correctOptionId.value=="0" || document.redFlagform.correctOptionId.value=="" )
		{
		  $('#correctOptionIdSpan').html('Please, enter Correct Option Id..!');
			document.correctOptionform.correctOptionId.focus();
			return false;
		}
	 
		
	 
}

function clearAll()
{
	  $('#questionNameSpan').html('');
	  $('#optionNameSpan').html('');
	  $('#correctOptionIdSpan').html(' ');
	  $('#activityNameSpan').html(' ');
}
function addnewOption() { 
	var txtval = document.getElementById("optionName").value;
	if(txtval=="")
	{
			 $('#optionNameSpan').html('Option Name Cannot be Blank');
	}else
	{
	    var table = document.getElementById("myTable");
	    var row = table.insertRow(1);
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	   
	    
	    cell1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ txtval;
	   // cell2.innerHTML = '<center><input type="button" class="remove_row" value="Delete"/>  &nbsp; <input type="button"  onclick="myEditFunction()" value="Edit" />&nbsp;</center>';
	   cell2.innerHTML = ' <a class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>  <a onclick="myEditFunction()" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>';
         
	   document.getElementsByTagName("td")[0].setAttribute("style", "word-wrap: break-word;"); 
	   
	  
	   	 var textToAdd  = document.getElementById("optionName").value;
	      var x	= document.getElementById("correctOptionId");
	      var option 		 = document.createElement("option");
	      option.text    = textToAdd;
	      x.add(option);
	    document.getElementById("optionName").value="";
	}
   
}

function myEditFunction() {
	
	var table = document.getElementById('myTable');
    
    for(var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
            rIndex = this.rowIndex;
            document.getElementById("myTable").deleteRow(rIndex);
        	 var optionName=this.cells[0].innerHTML;
        	 optionName=optionName.replace(/&nbsp;/g, '');
             document.getElementById("optionName").value = optionName;
            
        };
    }
   
}
</script>
<script>
$(document).ready(function(){

	 $("#myTable").on('click','.btn	',function(){
		 var select = document.getElementById("correctOptionId");
		    select.options.length = 0;
		    $(this).closest('tr').remove();
		  var table = document.getElementById("myTable");
		  var alloptions="";
		  for (var i = 1; i < table.rows.length; i++) {
		    	
		      var x	= document.getElementById("correctOptionId");
		      var option 		 = document.createElement("option");
		      options=table.rows[i].cells[0].innerHTML;
		      options=options.replace(/&nbsp;/g, '');
		      option.text    = options;
		      x.add(option);
		     
		    }  
	      
		  for (var i = 1; i < table.rows.length; i++) {
		    	
			  alloptions=alloptions+table.rows[i].cells[0].innerHTML+"###!###";
			  alloptions=alloptions.replace(/&nbsp;/g, '');
		     
		    }
		  //  alert(options);
		    document.getElementById("alloptions").value=alloptions;
		  //  document.getElementById("alloptions").value=alloptions;
	     });
	
	});
	
$(document).ready(function(){

	 $("#myTable1").on('click','.btn',function(){
	       $(this).closest('tr').remove();
	     });

	});
</script>

<script>
(function() {
  document.getElementById("submit").onclick = function() {
    var table = document.getElementById("myTable");
   
    var options="";
    var videoLink="";
    
    for (var i = 1; i < table.rows.length; i++) {
    	
      options=options+table.rows[i].cells[0].innerHTML+"###!###";
      options=options.replace(/&nbsp;/g, '');
     
    }
   
    //alert(options);
    document.getElementById("optionNames").value=options;

    document.getElementById("optionName").value=options;
    var select =  $( "#correctOptionId option:selected").text(); 
    
    document.getElementById("correctOption").value=select;
  };
})();



</script>
<script type="text/javascript">

function getActivityDetails(value)
{
	//alert("hii");
	  var moduleId = value;
	  //alert("moduleId"+moduleId);
    var moduleTypeId = moduleTypeId;    
    $.ajax({
		 url : '${pageContext.request.contextPath}/getActivityWithModule',
		type : 'Post',
		data : {
			moduleId:moduleId,
			},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
						//alert(result);
						   var option3 = $('<option />');
			 				option3.attr('value',"").text("Select Activity");
			 				$("#activityId").append(option3); 
						
							 for(var i=0;i<result.length;i++){
								var option = $('<option />');
							//	alert(result[i].activityId)
							option.attr('value',result[i].activityId).text(result[i].activityName);
							$("#activityId").append(option);
							 }
							 $('#activityId option[value=""]').remove();
						 //  window.location = "${pageContext.request.contextPath}/AddCorrectOption";
						} 
						else
						{
							alert("failure111");
						}					
				  } 
		});
}

</script>
</body>
</html>
