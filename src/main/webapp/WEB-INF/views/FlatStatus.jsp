<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Flat Status</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
 <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">

    <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Flat Status Details:
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Report</a></li>
        <li class="active">Flat Status</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="flatmasterform" action="${pageContext.request.contextPath}/FlatStatus" method="post">
    <section class="content">
   
     
      <div class="box box-default">
        
       
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
      
          <div class="box-body">
          <div class="row">
               <div class="col-md-3">
                  <label>Project</label><label class="text-red">* </label>
                  
                  <select class="form-control" name="projectName" id="projectName" onchange="getBuldingList(this.value)">
                  	 <option selected="" value="Default">-Select Project-</option>
                  	  <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectName}">${projectList.projectName}</option>
				     </c:forEach>
                   
				  </select>
                  </div>
                 <div class="col-xs-3">
			      <label>Project Building Name  </label> <label class="text-red">* </label>
                  <select class="form-control" name="buildingName" id="buildingName" onchange="getWingNameList(this.value)">
				  		<option selected="" value="Default">-Select Project Building-</option>
                     <c:forEach var="projectbuildingList" items="${projectbuildingList}">
                    	<option value="${projectbuildingList.buildingName}">${projectbuildingList.buildingName}</option>
				     </c:forEach>
                  </select>
			     </div> 
                <div class="col-xs-3">
			      <label>Wing </label> <label class="text-red">* </label>
              <select class="form-control" name="wingName" id="wingName" >
				 	 	<option selected="" value="Default">-Select Wing Name-</option>
                    <c:forEach var="projectwingList" items="${projectwingList}">
                    	<option value="${projectwingList.wingName}">${projectwingList.wingName}</option>
				     </c:forEach>
                  </select>
                 </div> 
                <div class="col-xs-2">
			      <br/>
			      	<button type="button" class="btn btn-success" onclick="SearchAllFlat()">Search</button>
            	  </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      
	
				
<div class="box box-default">
	<div class="box-body">
		<div class="col-xs-8">
		</div>
		
		<div class="col-xs-2">
		 <i class="fa fa-text-width"></i> <label>Instructions</label>
		 </br><font size="4"><label class="label label-danger"> .</label> - Sold</font>
		 </br>  <font size="4"><label class="label label-success">.</label> - UnSold</font>
		 </div>	
		 <div class="col-xs-2">
		<label>.</label>
		 </br><font size="4"><label class="label label-info"> .</label> - Agreement</font>
		 </br>  <font size="4"><label class="label label-warning">.</label> - Hold</font>
		 </div>	
		 																
	</div>
     <font size="4"> <label id="setProjectName">Project Name ${projectName}</label> </font>		 	
            	 	
<div class="box-body">

 <div class="table-responsive">
    
    <!-- 
    <table  class="table table-bordered" id="flatStatus">
     <s:forEach items="${projectList}" var="projectList" varStatus="loopStatus">
        <tr>
           <td style="width:100px" align="center">
          <label> ${projectList.projectName} </label> </td>
              
           <td>
              <table >
             	 <tr>
	                <s:forEach items="${wingList}" var="wingList" varStatus="loopStatus">
	            		<tr>
	            		  <s:choose>
	            		 <s:when test="${wingList.projectName eq projectList.projectName}">
	              		  <td  style="width:150px" height="40">${wingList.buildingName} - ${wingList.wingName} </td>
	                 	  <td>
	                			<c:forEach items="${flatList}" var="flatList" varStatus="loopStatus">
	                			  <s:choose>
	                  				 <s:when test="${flatList.wingName eq wingList.wingName && projectList.projectName eq flatList.projectName}">
	                  		         <c:choose>
					         			 <c:when test="${flatList.flatstatus eq 'Booking Completed'}">
					          			 	<font size="4"><label class="label label-danger">${flatList.flatNumber}</label></font>
					        		 	 </c:when>
								        <c:otherwise>  
								            &nbsp <font size="4"><label class="label label-success"> ${flatList.flatNumber}</label></font>
								        </c:otherwise>
				       				 </c:choose>
				       				 </s:when>
	               				 </s:choose>
	           					</c:forEach>
	           			 </td>
	           			</s:when> 
	           			</s:choose> 
	        	 	  </tr>
	               </s:forEach>
            	 </tr> 
              </table>
            </td>
          </tr>
       </s:forEach>
     </table>
     
     -->
     
     <table  class="table table-bordered" id="flatStatus" style="width:1300px">
        <tr>
         	
             	 <s:forEach items="${wingList}" var="wingList" varStatus="loopStatus">
             	  <tr>
             	 <td style="width:150px"><b>${wingList.buildingName}-${wingList.wingName}</b></td>
             	 <td >
             	 <c:forEach items="${flatList}" var="flatList" varStatus="loopStatus">
             	 <s:choose>
             	 <c:when test="${flatList.wingName eq wingList.wingName}">
	                  		         <c:choose>
					         			 <c:when test="${flatList.flatstatus eq 'Booking Completed'}">
					          			 &nbsp<font size="4"><label class="label label-danger">${flatList.flatNumber}</label></font>
					        		 	 </c:when>
								        <c:otherwise>  
								         &nbsp<font size="4"><label class="label label-success">${flatList.flatNumber}</label></font>
								        </c:otherwise>
				       				 </c:choose>
				       </c:when>
				       </s:choose>				 
	           		</c:forEach>
             	 </td>
             	  </tr>
             	 </s:forEach>
             
          </tr>
     </table>
     </div>
 </div>
 </div>
</section>
</form>
</div>
 
  <!-- Control Sidebar -->
     <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
   
<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function SearchAllFlat()
{
	 var projectName = $('#projectName').val();
	 var buildingName = $('#buildingName').val();
	 var wingName = $('#wingName').val();
	 document.getElementById('setProjectName').innerHTML ="Project Name: "+projectName+" - "+buildingName+" - "+wingName;
	 $("#flatStatus tr").detach();

	 $.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getWingWiseflatStatus',
		type : 'Post',
		data : { projectName : projectName, buildingName : buildingName, wingName : wingName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							for(var i=0;i<result.length;i++)
							{ 
								$('#flatStatus').append('<tr style="width:500px">');		
								if(result[i].flatstatus=="Booking Completed")
								{
								$('#flatStatus').append(' &nbsp <td > <font size="4"><label class="label label-danger">'+result[i].flatNumber+'</label></font></td>');
								}
								else
								{
								$('#flatStatus').append(' &nbsp <td > <font size="4"><label class="label label-success">'+result[i].flatNumber+'</label></font></td>');
								}								
									 
								$('#flatStatus').append("<br> </tr>") 		
							 } 
							// $('#flatStatus').append("</tr>")
							  
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getBuldingList()
{
	 $("#buildingName").empty();
	 var projectName = $('#projectName').val();

	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBuildingList',
		type : 'Post',
		data : { projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building Name-");
							$("#buildingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingName).text(result[i].buildingName);
							    $("#buildingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	 

	 
	 /*
		$("#flatStatus tr").detach();
		 var projectName = $('#projectName').val();
		 var wingName = [];
		// var flag=0;
		 //$("#buildingName").empty();
		$.ajax({
			//alert("hjhjh");

			url : '${pageContext.request.contextPath}/getProjectWiseflatStatus',
			type : 'Post',
			data : { projectName : projectName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
								for(var i=0;i<result.length;i++)
									{
									var flag=0;
									if(wingName.length==0)
										{
										wingName.push(result[i].wingName);
										
										}
									else
										{
										for(var k=0;k<=wingName.length;k++)
											{
											if(wingName[k]==result[i].wingName)
												{
												flag=1;
												}
											}
										if(flag==0)
											{
											wingName.push(result[i].wingName);
											}
										}
									}
								$('#flatStatus').append('<tr style="width:500px">');
								
								for(var i=0;i<wingName.length;i++)
								{ 
									$('#flatStatus').append('<td style="width:250px" >'+wingName[i]+' </td>');
										for(var j=0;j<result.length;j++)
										{
											if(result[j].wingName==wingName[i])
												{
												if(result[j].flatstatus=="Booking Completed")
													{
													$('#flatStatus').append(' &nbsp <td > <font size="4"><label class="label label-danger">'+result[j].flatNumber+'</label></font></td>');
													}
												else
													{
													$('#flatStatus').append(' &nbsp <td > <font size="4"><label class="label label-success">'+result[j].flatNumber+'</label></font></td>');
													}
												}
										 }
										
								 } 
								// $('#flatStatus').append("</tr>")
								  $('#flatStatus').append("<br> </tr>") 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		*/
	
}//end of get Building List



function getWingNameList()
{
	 $("#wingName").empty();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingName).text(result[i].wingName);
							    $("#wingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	 /*
		$("#flatStatus tr").detach();
		 var buildingName = $('#buildingName').val();
		 var projectName = $('#projectName').val();
		 //$("#floortypeName").empty();
		$.ajax({
			//alert("hjhjh");

			url : '${pageContext.request.contextPath}/getBuildingWiseFlatList',
			type : 'Post',
			data : { buildingName : buildingName , projectName : projectName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{ 
								$('#flatStatus').append('<tr style="background-color: #4682B4;"><th style="width:150px">Flat Id</th><th style="width:150px">Project Name</th> <th style="width:150px">Building Name</th> <th style="width:150px">Wing Name</th>  <th style="width:150px">Flat Number</th> <th style="width:100px">Flat Facing</th><th style="width:150px">Flat Type</th>	');
							
								for(var i=0;i<result.length;i++)
								{ 
									if(i%2==0)
									{
										$('#flatStatus').append('<tr style="background-color: #F0F8FF;"><td onclick="setFlatId()" style="color: blue;">'+result[i].flatId+'</td><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td>');
									}
									else
									{
										$('#flatStatus').append('<tr style="background-color: #CCE5FF;"><td onclick="setFlatId()" style="color: blue;">'+result[i].flatId+'</td><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td>');
									}
								
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			});
		*/
}


function getFloorNameList()
{
	 $("#floortypeName").empty();
	 var wingName = $('#wingName').val();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingName : wingName, buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#floortypeName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	 
	 
	 $("#flatStatus tr").detach();
	 var wingName = $('#wingName').val();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 //$("#floortypeName").empty();
	$.ajax({
		//alert("hjhjh");

		url : '${pageContext.request.contextPath}/getWingWiseFlatList',
		type : 'Post',
		data : {wingName : wingName, buildingName : buildingName , projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{ 
							$('#flatStatus').append('<tr style="background-color: #4682B4;"><th style="width:150px">Flat Id</th><th style="width:150px">Project Name</th> <th style="width:150px">Building Name</th> <th style="width:150px">Wing Name</th>  <th style="width:150px">Flat Number</th> <th style="width:100px">Flat Facing</th><th style="width:150px">Flat Type</th>	');
						
							for(var i=0;i<result.length;i++)
							{ 
								if(i%2==0)
								{
									$('#flatStatus').append('<tr style="background-color: #F0F8FF;"><td onclick="setFlatId()" style="color: blue;">'+result[i].flatId+'</td><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td>');
								}
								else
								{
									$('#flatStatus').append('<tr style="background-color: #CCE5FF;"><td onclick="setFlatId()" style="color: blue;">'+result[i].flatId+'</td><td>'+result[i].projectName+'</td><td>'+result[i].buildingName+'</td><td>'+result[i].wingName+'</td><td>'+result[i].flatNumber+'</td><td>'+result[i].flatfacingName+'</td><td>'+result[i].flatType+'</td>');
								}
							
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

</script>
</body>
</html>
