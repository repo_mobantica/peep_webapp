<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PEEP |Edit Module Type</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="styleshee
  t" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">

	<%@ include file="headerpage.jsp" %>
  
  	<%@ include file="menu.jsp" %>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit RedFlag Question Details:
      
      </h1>
    </section>

    <!-- Main content -->
	
<form name="redFlagform" action="${pageContext.request.contextPath}/EditRedFlagQuestionData"  onSubmit="return validate()" method="GET">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12s">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
                          <input type="hidden" class="form-control" id="redFlagQuestionId" name="redFlagQuestionId"  value="${redFlagQuestionDetails[0].redFlagQuestionId}" readonly>
					<div class="box-body">
          				<div class="row">
                 		
	              			 <div class="col-md-6">
	             				   <div class="col-xs-12"><br>
	               						 <label for="redFlagQuestionName"> Questions </label> <label class="text-red">* </label>
	                 					 <input type="text" class="form-control" id="redFlagQuestionName" name="redFlagQuestionName" onchange="clearAll()"  value="${redFlagQuestionDetails[0].redFlagQuestionName}" placeholder="Question Name" >
	                 					 
	                					 <span id="redFlagQuestionNameSpan" style="color:#FF0000"></span>
              						</div>
	             			</div> 
	             			<div class="col-md-6"></div>
                		</div>
                			<div class="row">
                 			 
	            			<div class="col-md-6">
	             			 	 <div class="col-xs-12"><br>
				    				 
	                  						<label for="redFlagOptionName"> Options </label> <label class="text-red">* </label><br>
	                  						 <input type="text" class="form-control" id="redFlagOptionName" name="redFlagOptionName" onchange="clearAll()"  placeholder="Option Name" >
                   					
	                   
	                  				
	                  						<label for="optionName123">&nbsp;</label> <label class="text-red">    </label><br>
	                  						 
	                  						<input type="button"  class="btn btn-success" onclick="myFunction()" value="Add More Options"/><br><br>
                   					
			               			 <span id="redFlagOptionNameSpan" style="color:#FF0000"></span>
	                 			</div>
	             			</div> 
                		</div>
                		<div class="row">
                 			
	            			<div class="col-md-6">
	             			 	<div class="col-xs-12"><br>
	                 					<table id="myTable"  width="200" style="table-layout:fixed" border="1" align="left">
										<tr><th bgcolor="#3c8dbc"> <center>Option Name </center>    </th><th bgcolor="#3c8dbc">  <center>   Action </center>     </th>
											<col width="200">
  											<col width="200">
  										</tr>
  									<c:forEach items="${redFlagOptionList}" var="redFlagOptionList">
  										<tr>
  										 
  										<%--  <td>${redFlagOptionList.redFlagOptionName}</td><td><center><input type="button" onClick= "myEditFunction()" value="Edit"/></center></td> --%> 
  									 <td>${redFlagOptionList.redFlagOptionName}</td>
  									 <td>	<a class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>  <a onclick="myEditFunction()" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a></td>
  										</tr>
  										</c:forEach>
  									</table>
	              				</div> 
	             			</div> 
                		</div>
                		 <div class="row">
                		 <div class="col-xs-6"><br><br><br><br>
						        <label>Choose Correct Option </label><label class="text-red">* </label><br>
			             	    <select  class="form-control" name="correctOptionId" id="correctOptionId">
							  	
			                   	 <option value="0" selected="selected">--Select Correct Option--</option>
							 <c:forEach items="${redFlagOptionList}" var="option">
							 			<c:if test="${ option.redFlagOptionName ne redFlagQuestionDetails[0].correctOptionId}">
	                   					
  								 			<option value="${option.redFlagOptionId}">${option.redFlagOptionName}</option> 
  								 											
  					   						
  					   						</c:if> 
  					   						</c:forEach>
  					   					
  					   					<c:forEach items="${redFlagOptionList}" var="option">
							 			<c:if test="${ option.redFlagOptionName eq redFlagQuestionDetails[0].correctOptionId}">
	                   					
  								 			<option value="${option.redFlagOptionId}" selected >${option.redFlagOptionName}</option> 
  								 											
  					   						
  					   						</c:if> 
  					   						</c:forEach>
			                    </select>
                   			 <span id="correctOptionIdSpan" style="color:#FF0000"></span>
			      		</div> 
                		
                		</div>
                	
                		
                		
                		
                </div>
				<input type="hidden" id="status" name="status" value="${questionStatus}">	
				<input type="hidden" id="createdDate" name="createdDate" >
				<input type="hidden" id="updateDate" name="updateDate" >
				<input type="hidden" id="optionName1" name="optionName1" >
				<input type="hidden" id="correctOption" name="correctOption" >
			<input type="hidden" class="form-control" id="redFlagQuestionId" name="redFlagQuestionId" placeholder="Question  Name" value="${redFlagQuestionDetails[0].redFlagQuestionId}">
            
			<!-- 	<input type="hidden" id="userName" name="userName" placeholder="Admin"> -->
				
					
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		  	    <!--  <div class="box-body">
              <div class="row">
                      <div class="col-xs-4">
                	<a href="RedFlagQuestionMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  				<button type="submit" id="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			        </div> 
                 </div>
			  </div> -->
			  
			  <!--   <div class="box-body">
              <div class="row"><br><br>
                      <div class="col-xs-2">
                	<a href="RedFlagQuestionMaster"><button type="button" class="btn btn-block  btn-primary" value="Back" style="width:90px">Back</button></a>
                	
			     </div>
				  <div class="col-xs-2">
                
              	<button type="submit" id="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div>
					<div class="col-xs-2">
			  			<button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			        </div> 
                 </div>
			  </div>
			   -->
			  
			  
			  <div class="box-body">
              <div class="row"><br><br>
                <div class="col-xs-2"></div><div class="col-xs-1"></div>
                      <div class="col-xs-2">
                	<a href="RedFlagQuestionMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-2">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-1">
			  				<button type="submit" id="submit" class="btn btn-info " name="submit">Submit</button>
			        </div> 
                 </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
           
      </div>
      <!-- /.box -->
			
			<!-- Design For View Table Of Country List -->
    
   
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
  
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
   
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
 <script>

function init()
{	
	
	var n =  new Date();
	var y = n.getFullYear();
	var m = n.getMonth() + 1;
	var d = n.getDate();
	document.getElementById("createdDate").value = d + "/" + m + "/" + y;
	document.getElementById("updateDate").value = d + "/" + m + "/" + y;


}

</script> 
<script>
$(document).ready(function(){

	 $("#myTable").on('click','.btn',function(){
	       $(this).closest('tr').remove();
	     });

	});
	
$(document).ready(function(){

	 $("#myTable1").on('click','.btn',function(){
	       $(this).closest('tr').remove();
	     });

	});
</script>




<script>
(function() {
  document.getElementById("submit").onclick = function() {
    var table = document.getElementById("myTable");

    var options="";

    
    for (var i = 1; i < table.rows.length; i++) {
    	
      options=options+table.rows[i].cells[0].innerHTML+"###!###";
      options=options.replace(/&nbsp;/g, '');
     
    }
   

    document.getElementById("optionName1").value=options;

    document.getElementById("redFlagOptionName").value=options;
 var select =  $( "#correctOptionId option:selected").text(); 
    
    document.getElementById("correctOption").value=select;
   
  };
})();
</script>
<script>

function myFunction() { 
	var txtval = document.getElementById("redFlagOptionName").value;
	
		    var table = document.getElementById("myTable");
		    var row = table.insertRow(1);
		    var cell1 = row.insertCell(0);
		    var cell2 = row.insertCell(1);
		    cell1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ txtval;
		  /*   cell2.innerHTML = '<center><input type="button" class="remove_row" value="Delete"/></center>'; */
		cell2.innerHTML = ' <a class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a>  <a onclick="myEditFunction()" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>';
		 var textToAdd  = document.getElementById("redFlagOptionName").value;
	      var x	= document.getElementById("correctOptionId");
	      var option 		 = document.createElement("option");
	      option.text    = textToAdd;
	      x.add(option);
			
  	 document.getElementById("redFlagOptionName").value="";
   // cell1.innerHTML = txtval;
   // cell2.innerHTML = '<a href="javascript:removeFunction()" class="btn btn-info btn-lg"> <span class="glyphicon glyphicon-plus-sign"></span>  </a>';
    //cell2.innerHTML = '<input type="button" class="remove_row" value="Delete"/>';
}

function validate()
{
	 clearAll();
	 if(document.redFlagform.redFlagQuestionName.value=="")
		{
		  $('#redFlagQuestionNameSpan').html('Please, enterRed Question Name..!');
			document.redFlagform.redFlagQuestionName.focus();
			return false;
		}
	 if(document.redFlagform.redFlagOptionName.value=="")
		{
		  $('#redFlagOptionNameSpan').html('Please, Enter Atleat One Option..!');
			document.redFlagform.redFlagOptionName.focus();
			return false;
		}
	  
	  if(document.redFlagform.correctOptionId.value=="0" || document.redFlagform.correctOptionId.value=="" )
		{
		  $('#correctOptionIdSpan').html('Please, enter Correct Option Id..!');
			document.redFlagform.correctOptionId.focus();
			return false;
		}
		
		
	 
}

function clearAll()
{
	  $('#redFlagQuestionNameSpan').html('');
	  $('#redFlagOptionNameSpan').html('');
	  $('#correctOptionIdSpan').html(' ');
}
var editOptions="";
function myEditFunction() {

	
	var table = document.getElementById('myTable');
    
    for(var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
        	
            rIndex = this.rowIndex;
            document.getElementById("myTable").deleteRow(rIndex);
        	 var questionName=this.cells[0].innerHTML;
        	 questionName=questionName.replace(/&nbsp;/g, '');
        	 editOptions=editOptions+this.cells[0].innerHTML+"###!###";
             document.getElementById("redFlagOptionName").value = questionName;
             document.getElementById("editOptionName").value = editOptions;
             //alert(editOptions);
            
        };
    }
   
}

</script>
<script>
$(document).ready(function(){

	 $("#myTable").on('click','.btn',function(){
		 var select = document.getElementById("correctOptionId");
		    select.options.length = 0;
		    $(this).closest('tr').remove();
		  var table = document.getElementById("myTable");
		  var alloptions="";
		  for (var i = 1; i < table.rows.length; i++) {
		    	
		      var x	= document.getElementById("correctOptionId");
		      var option 		 = document.createElement("option");
		      options=table.rows[i].cells[0].innerHTML;
		      options=options.replace(/&nbsp;/g, '');
		      option.text    = options;
		      x.add(option);
		     
		    }  
	      
		  for (var i = 1; i < table.rows.length; i++) {
		    	
			  alloptions=alloptions+table.rows[i].cells[0].innerHTML+"###!###";
			  alloptions=alloptions.replace(/&nbsp;/g, '');
		     
		    }
		  //  alert(options);
		    document.getElementById("alloptions").value=alloptions;
		  //  document.getElementById("alloptions").value=alloptions;
	     });
	
	});
	
$(document).ready(function(){

	 $("#myTable1").on('click','.remove_row',function(){
	       $(this).closest('tr').remove();
	     });

	});
</script>
</body>
</html>
