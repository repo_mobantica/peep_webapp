<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Booking</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#enquirydbStatusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">
<%@ include file="headerpage.jsp" %>
 
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Booking Details
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Booking</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="bookingform" action="${pageContext.request.contextPath}/AddNewBooking" onSubmit="return validate()" method="Post" >
    <section class="content">
    
      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
       <div  class="panel box box-danger"/>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
              <!-- /.form-group -->
             
			<span id="statusSpan" style="color:#FF0000"></span>
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-2">
                  <label for="enquiryId">Enquiry Id</label>
                  <input type="text" class="form-control" id="enquiryId" placeholder="ID" name="enquiryId" value="${enquiryCode}"  readonly>
                </div>               
                    <div class="col-xs-2">
                  <label for="bookingId">Booking Id</label>
                  <input type="text" class="form-control" id="bookingId" placeholder="ID" name="bookingId" value="${bookingCode}"  readonly>
                </div>
              </div>
            </div>
				
				
				<div class="box-body">
              <div class="row">
                <div class="col-xs-2">
				<label for="bookingfirstname">First Name</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingfirstname" placeholder="First Name" name="bookingfirstname" >
                  <span id="bookingfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="bookingmiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="bookingmiddlename" placeholder="Middle Name"  name="bookingmiddlename">
                 <span id="bookingmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="bookinglastname">Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookinglastname" placeholder="Last Name" name="bookinglastname">
                 <span id="bookinglastnameSpan" style="color:#FF0000"></span>
                </div>
                
                
                <!-- <div id="maidenName1" hidden="hidden">
                 <div class="col-xs-2">
				<label for="bookingmaidenfirstname">Maiden First Name</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingmaidenfirstname" placeholder="First Name" name="bookingmaidenfirstname" >
                  <span id="bookingmaidenfirstnameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="bookingmaidenmiddlename">Maiden Middle Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingmaidenmiddlename" placeholder="Middle Name"  name="bookingmaidenmiddlename" >
                 <span id="bookingmaidenmiddlenameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-xs-2">
				<label for="bookingmaidenlastname">Maiden Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingmaidenlastname" placeholder="Last Name" name="bookingmaidenlastname" >
                 <span id="bookingmaidenlastnameSpan" style="color:#FF0000"></span>
                </div>
                </div> -->
               
              </div>
            </div>
            
				
				   <div class="box-body">
              <div class="row">
                 
				    <div class="col-xs-3">
			    <label>Mobile No(Primary)</label><label class="text-red">* </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="bookingmobileNumber1" data-mask id="bookingmobileNumber1" value="${enquiryDetails[0].enqmobileNumber1}" >
                </div>
                   <span id="bookingmobileNumber1Span" style="color:#FF0000"></span>
				 </div>
				  <div class="col-xs-3">
			    <label>Mobile No 2</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="bookingmobileNumber2" data-mask id="bookingmobileNumber2" value="${enquiryDetails[0].enqmobileNumber2}">
                </div>
                 <span id="bookingmobileNumber2Span" style="color:#FF0000"></span>
				 </div> 
				 <div class="col-xs-3">
			     <label for="bookingEmail">Email ID </label><label class="text-red">* </label>
				   <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="text" class="form-control" placeholder="Email" name ="bookingEmail" id="bookingEmail" value="${enquiryDetails[0].enqEmail}">
              </div>
               <span id="bookingEmailSpan" style="color:#FF0000"></span>
			     </div> 
			      <div class="col-xs-3">
			    <label>Budget (Rs.)</label><label class="text-red">* </label>
			     <select class="form-control" name="flatBudget" id="flatBudget">
				  	  <option selected="" value="Default">-Select Budget-</option>
                   	  <c:forEach var="budgetList" items="${budgetList}">
                   	  <option value="${budgetList.budgetCost}">${budgetList.budgetCost}</option>
				      </c:forEach>
                  </select>
                    <span id="flatBudgetSpan" style="color:#FF0000"></span>
			     </div> 
				 </div>
            </div>
		
				
				
				
				    <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			   <label for="bookingaddress">Address </label><label class="text-red">* </label>
                 <textarea class="form-control" rows="1" id="bookingaddress" placeholder="Address" name="bookingaddress"> </textarea>
                  <span id="bookingaddressSpan" style="color:#FF0000"></span>
			     </div> 
				   <div class="col-xs-2">
                  <label>Country </label><label class="text-red">* </label>
    			  <select class="form-control" name="countryName" id="countryName" onchange="getStateList(this.value)">
				<option selected="" value="Default">-Select Country-</option>
                   <c:forEach var="countryList" items="${countryList}">
	                    <option value="${countryList.countryName}">${countryList.countryName}</option>
	                </c:forEach>
                  </select>
                   <span id="countryNameSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-2">
				 <label>State </label><label class="text-red">* </label>
				  <select class="form-control" name="stateName" id="stateName" onchange="getCityList(this.Value)">
			  		  <option selected="" value="Default">-Select State-</option>
                     <c:forEach var="stateList" items="${stateList}">
                       <option value="${stateList.stateName}">${stateList.stateName}</option>
				    </c:forEach>
                  </select>
                   <span id="stateNameSpan" style="color:#FF0000"></span>
               </div> 
               <div class="col-xs-2">
				  <label>City </label><label class="text-red">* </label>
                      <select class="form-control" name ="cityName" id="cityName" onchange="getLocationAreaList(this.value)">
			  		<option selected="" value="Default">-Select City-</option>
                    <c:forEach var="cityList" items="${cityList}">
                    	<option value="${cityList.cityName}">${cityList.cityName}</option>
				    </c:forEach>
                  </select>
                  <span id="cityNameSpan" style="color:#FF0000"></span>
				  </div> 
                 
				    <div class="col-xs-2">
				    <label>Area </label><label class="text-red">* </label>
				        <select class="form-control" name="locationareaName" id="locationareaName" onchange="getpinCode(this.value)">
			     <option selected="" value="Default">-Select Area-</option>
                     <c:forEach var="locationareaList" items="${locationareaList}">
                    	<option value="${locationareaList.locationareaName}">${locationareaList.locationareaName}</option>
				     </c:forEach>
				   </select>
                   <span id="locationareaNameSpan" style="color:#FF0000"></span>
				  </div> 
				   <div class="col-xs-1">
                  <label for="bookingPincode">Pin Code </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="bookingPincode" placeholder="Pin Code" name="bookingPincode" readonly>
                    <span id="bookingPincodeSpan" style="color:#FF0000"></span>
			     </div> 
				   
              </div>
            </div>
				
			
			
			
			
			  <div class="box-body">
              <div class="row">
                
                 <div class="col-xs-3">
			   	 <label for="sl">Enquiry Source </label><label class="text-red">* </label>
				 <select class="form-control" name="enqSource" id="enqSource" onchange="getSubEnquiryList(this.value)">
				  		<option selected="" value="Default">-Select Enquiry Source-</option>
                   	 <c:forEach var="enquirySourceList" items="${enquirySourceList}">
                    	 <option value="${enquirySourceList.enquirysourceName}">${enquirySourceList.enquirysourceName}</option>
				     </c:forEach> 
                  </select>
                <span id="enqSourceSpan" style="color:#FF0000"></span>
			     </div>
			      <div class="col-xs-3">
			   	 <label for="sl">Sub-Enquiry Source </label><label class="text-red">* </label>
			       <select class="form-control" name="subenqSource" id="subenqSource">
				 	 	<option selected="" value="Default">-Select Sub Enquiry Source-</option>
                    <c:forEach var="subenquiryList" items="${subenquiryList}">
                    	<option value="${subenquiryList.subenquirysourceName}">${subenquiryList.subenquirysourceName}</option>
				     </c:forEach>
                  </select>
                <span id="subenqSourceSpan" style="color:#FF0000"></span>
			     </div>   
                  <div class="col-xs-3">
			    <label for="bookingOccupation">Occupation </label><label class="text-red">* </label>
			     <select class="form-control" name="bookingOccupation" id="bookingOccupation" >
				  <option selected="" value="Default">-Select Occupation-</option>
					<c:forEach var="occupationList" items="${occupationList}">
                    <option value="${occupationList.occupationName}">${occupationList.occupationName}</option>
				    </c:forEach>				
				 </select>
                 <span id="bookingOccupationSpan" style="color:#FF0000"></span>
                </div> 
                   
                   <%-- <div class="col-xs-3">
			   	<label for="pl">Prefer Bank </label><label class="text-red">* </label>
               <select class="form-control" id="bankName" name="bankName" onchange="getBranchList(this.value)">
				  	<option selected="" value="Default">-Select Bank-</option>
                  <c:forEach var="bankList" items="${bankList}">
                  <option value="${bankList.bankName}">${bankList.bankName}</option>
				  </c:forEach>	
                  	  </select>
                  <span id="bankNameSpan" style="color:#FF0000"></span>
			     </div> 
			      --%>
				</div>
            </div>
         
          </div>
          
       		  <input type="hidden" id="enqStatusSpan" name="enqStatusSpan" value="Booking completed">
         	  <input type="hidden" id="bookingstatus" name="bookingstatus" value="Booking">
			  <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value=""> 

     </div>
     </div>
     </div>
     
    
      <div class="box box-default">
       <div class="box-body">
          <div class="row">
            <div class="col-md-12">
		 
			  <div class="box-body">
              <div class="row">
              <h3>Project And Flat Details</h3>
                  <div class="col-xs-3">
				<label>Project Name</label><label class="text-red">* </label>
				   <select class="form-control" name="projectName" id="projectName" onchange="getBuildingList(this.value)">
				  		<option selected="" value="Default">-Select Project-</option>
                     <c:forEach var="projectList" items="${projectList}">
                    	<option value="${projectList.projectName}">${projectList.projectName}</option>
				     </c:forEach>
                  </select>
                    <span id="projectNameSpan" style="color:#FF0000"></span>
			     </div> 
			         <div class="col-xs-3">
			     <label>Project Building</label><label class="text-red">* </label>
	            <select class="form-control" name="buildingName" id="buildingName" onchange="getWingNameList(this.value)">
					  		<option selected="" value="Default">-Select Project Building-</option>
	                    <c:forEach var="buildingList" items="${buildingList}">
                    		<option value="${buildingList.buildingName}">${buildingList.buildingName}</option>
				        </c:forEach>
	                  </select>
	               <span id="buildingNameSpan" style="color:#FF0000"></span>
			     </div> 
			    <div class="col-xs-3">
			      <label>Wing </label> <label class="text-red">* </label>
              <select class="form-control" name="wingName" id="wingName" onchange="getFloorNameList(this.value)">
				 	 <option selected="" value="Default">-Select Wing Name-</option>
                    <c:forEach var="projectwingList" items="${projectwingList}">
                    <option value="${projectwingList.wingName}">${projectwingList.wingName}</option>
				     </c:forEach>
                  </select>
                   <span id="wingNameSpan" style="color:#FF0000"></span>
                 </div>
                   <div class="col-xs-3">
			      <label>Floor Name</label><label class="text-red">* </label>
               <select class="form-control" name="floortypeName" id="floortypeName" onchange="getFlatNumberList(this.value)">
				 	 <option selected="" value="Default">-Select Floor-</option>
                      <c:forEach var="floortypeList" items="${floortypeList}">
                    <option value="${floortypeList.floortypeName}">${floortypeList.floortypeName}</option>
				      </c:forEach>
                  </select>
                    <span id="floortypeNameSpan" style="color:#FF0000"></span>
			     </div>
				 </div>
            </div>


			  <div class="box-body">
              <div class="row">
                 
                 
                 <div class="col-xs-3">
			    	<label>Flat Number  </label><label class="text-red">* </label>
			    <select class="form-control" name="flatNumber" id="flatNumber" onchange="getFlatFacing(this.value)">
				  	<option selected="" value="Default">-Select Flat Number-</option>
                     <c:forEach var="flatnumberList" items="${flatnumberList}">
                    <option value="${flatnumberList.flatNumber}">${flatnumberList.flatNumber}</option>
				    </c:forEach>
                  </select>
                    <span id="flatNumberSpan" style="color:#FF0000"></span>
			     </div>
			     
			     
			       <div class="col-xs-3">
			    <label>Flat Type  </label><label class="text-red">* </label>
				<input type="text" class="form-control" id="flatType" placeholder="" name="flatType" value=""readonly>
                    <span id="flatTypeSpan" style="color:#FF0000"></span>
			     </div> 
			     <div class="col-xs-3">
                  <label>Flat Facing  </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatFacing" placeholder="Flat Facing " name="flatFacing" value=""readonly>
			     <span id="flatFacingSpan" style="color:#FF0000"></span>
			     </div> 
			     
			      <div class="col-xs-3">
                  <label for="flatarea">Flat Area(SQ.FT)</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatareainSqFt" placeholder="Flat Area in SQ.FT " name="flatareainSqFt" value=""readonly>
			       <span id="flatareainSqFtSpan" style="color:#FF0000"></span>
			     </div>
			        </div>
            </div>
			
			 <div class="box-body">
              <div class="row"> 
			     	<div class="col-xs-3">
			 	   <label for="flatCostwithotfloorise">Flat Cost(Sq.Ft)</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatCostwithotfloorise" placeholder="Flat Total Cost" name="flatCostwithotfloorise" readonly>
                   <span id="flatCostwithotflooriseSpan" style="color:#FF0000"></span>
			     </div> 
           			<div class="col-xs-3">
			 	   <label for="floorRise">Floor Rise</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="floorRise" placeholder="Flat Total Cost" name="floorRise" readonly>
                   <span id="floorRiseSpan" style="color:#FF0000"></span>
			     </div> 
			   
                  <div class="col-xs-3">
			    <label for="flatCost">Net Flat Cost(Sq.Ft)</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatCost" placeholder="Flat Total Cost" name="flatCost" onchange="getflatBasicCost(this.value)">
                   <span id="flatCostSpan" style="color:#FF0000"></span>
                 <input type="hidden" id="flatminimumCost" name="flatminimumCost">
			     </div>
			       <div class="col-xs-3">
                  <label for="flatcost">Flat Basic Cost </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="flatbasicCost" placeholder="Flat Cost " name="flatbasicCost" value=""readonly>
			     <span id="flatbasicCostSpan" style="color:#FF0000"></span>
			     </div>
              </div>
            </div>
            
               <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			    <label for="parkingFloor">Parking Floor</label> <label class="text-red">* </label>
                    <select class="form-control" name="parkingFloor" id="parkingFloor" onchange="getParkingNumber(this.value)">
				  	<option selected="" value="Default">-Select Parking Floor-</option>
                  </select>
                   <span id="parkingFloorSpan" style="color:#FF0000"></span>
			     </div>
			       <div class="col-xs-3">
			    <label for="parkingNumber">Parking Zone</label> 
                    <select class="form-control" name="parkingNumber" id="parkingNumber" >
				  	<option selected="" value="Default">-Select Parking Number-</option>
                  </select>
                   <span id="parkingNumberSpan" style="color:#FF0000"></span>
			     </div>
			     
			       <div class="col-xs-3">
                  <label for="flatcost">Agent Farm Name </label>
 				 <select class="form-control" name="agentName" id="agentName">
				  		<option selected="" value="Default">-Select Agent Name -</option>
                     <c:forEach var="agentList" items="${agentList}">
                    	<option value="${agentList.agentfirmName}">${agentList.agentfirmName}</option>
				     </c:forEach>
                  </select>
			     <span id="agentNameSpan" style="color:#FF0000"></span>
			     </div>
              </div>
            </div>
            
          </div>
        
	 
    </div>
    </div>
    
               <div class="box-body">
               
              <div class="row">
                  <div class="col-xs-12">
                  	<div class="box-body">
              <div class="row">
              <h4><label>Amount Details</label></h4>
                  <div class="col-xs-3">
                   <div class="col-sm-12">
                  <label for="infrastructureCharge">Infrastructure Charges </label><label class="text-red">* </label>
                  </div>
                   <div class="col-sm-12">
                    <div class="form-group">
				<input type="text" class="form-control" id="infrastructureCharge" placeholder="Infrastructure Charges" name="infrastructureCharge" onchange="calculate(this.value)">      
				<span id="infrastructureChargeSpan" style="color:#FF0000"></span>     
                </div>
                </div>
                  </div>
                  
                  <div class="col-xs-3">
                   <div class="col-sm-12">
                 <label for="aggreementValue" >Aggreement Value</label><label class="text-red">* </label>
                  </div>
                   <div class="form-group">
                  <div class="col-sm-12">
                    <input type="text" class="form-control" id="aggreementValue1" placeholder="Aggreement Value " name="aggreementValue1" onchange="calculate(this.value)" readonly>
                    <span id="aggreementValue1Span" style="color:#FF0000"></span>
                  </div>
                </div>
                  </div>
                 
                   <div class="col-xs-3">
                   <div class="col-sm-12">
                  <label for="stampDuty" >Stamp Duty (6%)</label><label class="text-red">* </label>
                  </div>
                  <div class="form-group">
				   <div class="col-sm-4">
					<input type="text" class="form-control" id="stampDutyPer" name="stampDutyPer"  onchange="calculate(this.value)">  
					<span id="stampDutyPerSpan" style="color:#FF0000"></span>  
				  </div>
                  <div class="col-sm-8">
				<input type="text" class="form-control" id="stampDuty1" placeholder="Stamp Duty" name="stampDuty1" onchange="calculate(this.value)" readonly>   
				<span id="stampDuty1Span" style="color:#FF0000"></span>       
				  </div>
                </div>
			</div>
				  <div class="col-xs-3">
                   <div class="col-sm-12">
                   <label for="registrationCost" >Registration Cost(1%)</label><label class="text-red">* </label>
                  </div>
                    <div class="form-group">
					<div class="col-sm-4">
					<input type="text" class="form-control" id="registrationPer" name="registrationPer"  onchange="calculate(this.value)">  
					<span id="gstCostSpan" style="color:#FF0000"></span>  
				  </div>
                  <div class="col-sm-8">
					<input type="text" class="form-control" id="registrationCost1" placeholder="Registration" name="registrationCost1" onchange="calculate(this.value)" readonly>  
					<span id="registrationCost1Span" style="color:#FF0000"></span>        
				  </div>
                </div>
					</div>
					
                  </div>
                  </div> 
               <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                   <div class="col-sm-12">
                     <label for="handlingCharges" >Handling Charges</label><label class="text-red">* </label>
                  </div>
                 <div class="form-group">
                  <div class="col-sm-12">
					<input type="text" class="form-control" id="handlingCharges" placeholder="Handling Charges" name="handlingCharges" onchange="calculate(this.value)"> 
					<span id="handlingChargesSpan" style="color:#FF0000"></span>       
				  </div>
                </div>
				</div>
					
                   <div class="col-xs-3">
                    <div class="col-sm-12">
                  <label for="gstCost" >GST Amount </label><label class="text-red">* </label>
                  </div>
                   
                  <div class="form-group">
 				<div class="col-sm-4">
					<input type="text" class="form-control" id="gstCost" name="gstCost" onchange="calculate(this.value)">  
					<span id="gstCostSpan" style="color:#FF0000"></span>  
				  </div>
                  <div class="col-sm-8">
					<input type="text" class="form-control" id="gstAmount1" placeholder="GST Amount" name="gstAmount1" readonly>     
					<span id="gstAmount1Span" style="color:#FF0000"></span>  
				  </div>
                </div>
				</div>
           
                <div class="col-xs-3">
                   <div class="col-sm-12">
                 <label for="grandTotal">Grand Total</label><label class="text-red">* </label>
                  </div>
                     <div class="form-group">
                  <div class="col-sm-12">
				<input type="text" class="form-control" id="grandTotal1" placeholder="Grand Total" name="grandTotal1" readonly>         
			  <span id="grandTotal1Span" style="color:#FF0000"></span>
				  </div>
                </div>
                </div>
                
                <div class="col-xs-3">
                   <div class="col-sm-12">
                 <label for="grandTotal">TDS 1% Amount</label><label class="text-red">* </label>
                  </div>
                     <div class="form-group">
                  <div class="col-sm-12">
				<input type="text" class="form-control" id="tds" placeholder="TDS Amount" name="tds" readonly>         
			  <span id="grandTotal1Span" style="color:#FF0000"></span>
				  </div>
                </div>
                </div>
                
                </div>
                </div>
                  </div>
                  </div>
                  </div>
</div>
         
      
         <div class="box-body">
              <div class="row">
              </br>
                  <div class="col-xs-4">
                	<a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
			     
              </div>
			</div>
    </section>
	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>
/* 
function MaidenFemale()
{
	if((document.bookingform.bookingMarried[0].checked == true ) && (document.bookingform.bookingGender[1].checked == true ))
	{
 		document.getElementById('maidenName1').style.display ='block';
	}
}

function MaidenMale()
{
	 document.getElementById('maidenName1').style.display ='none';
} */
/*
function chequePayment(){
	document.getElementById("labelchequeNumber").innerHTML="Cheque Number";
	  document.getElementById('cheque').style.display ='block';
	  document.bookingform.chequeNumber.value="";
	}
function ddPayment(){
	document.getElementById("labelchequeNumber").innerHTML="DD Number";
	  document.getElementById('cheque').style.display ='block';
	  document.bookingform.chequeNumber.value="";
	}
function onlinePayment(){
	document.getElementById("labelchequeNumber").innerHTML="RTGS/NEFT/IMPS Number";
	  document.getElementById('cheque').style.display ='block';
	  document.bookingform.chequeNumber.value="";
	}
function cashPayment(){
	  document.getElementById('cheque').style.display ='none';
	  document.bookingform.chequeNumber.value="";
	}

*/

function clearall(){
	$('#bookingfirstNameSpan').html('');
	$('#bookingmiddlenameSpan').html('');
	$('#bookinglastnameSpan').html('');
	$('#bookingaddressSpan').html('');
	$('#countryNameSpan').html('');
	$('#stateNameSpan').html('');
	$('#cityNameSpan').html('');
	$('#locationareaNameSpan').html('');
	$('#bookingPincodeSpan').html('');
	$('#bookingEmailSpan').html('');
	$('#bookingmobileNumber1Span').html('');
	$('#bookingmobileNumber2Span').html('');
	$('#bookingOccupationSpan').html('');
	
	$('#projectNameSpan').html('');
	$('#buildingNameSpan').html('');
	$('#wingNameSpan').html('');
	$('#floortypeNameSpan').html('');
	$('#flatTypeSpan').html('');
	$('#flatNumberSpan').html('');
	$('#flatFacingSpan').html('');
	$('#flatareainSqFtSpan').html('');
	$('#flatbasicCostSpan').html('');
	$('#aggreementValue1Span').html('');
	$('#stampDuty1Span').html('');
	$('#registrationCost1Span').html('');
	$('#gstAmount1Span').html('');
	$('#handlingChargesSpan').html('');
	$('#infrastructureChargeSpan').html('');
	
	$('#grandTotal1Span').html('');
	$('#paymentModeSpan').html('');
	$('#chequeNumberSpan').html('');
	$('#chequebankNameSpan').html('');
	
}
function changeStatus()
{
	//$('#enquirydbStatusSpan').html('');
}
function calculate()
{
	
	 var flatbasicCost = Number($('#flatbasicCost').val());
	 var registrationPer = Number($('#registrationPer').val());
	 var stampDutyPer = Number($('#stampDutyPer').val());
	 
	var infrastructureCharge = Number($('#infrastructureCharge').val());
	var aggreementValue1 = flatbasicCost+infrastructureCharge;
	
	var gstCost =Number($('#gstCost').val());
	var stampDuty1 = (aggreementValue1/100)*stampDutyPer;
	var registrationCost2=0;
	var tds=0;
	if(aggreementValue1>3000000)
		{
		registrationCost2=30000;
		}
	else
		{
		registrationCost2= (aggreementValue1/100)*registrationPer;
		}
	var registrationCost1 =registrationCost2;
	
	
	var gstAmount1 = (aggreementValue1/100)*gstCost;
	var handlingCharges = Number($('#handlingCharges').val());
	var grandTotal1 = aggreementValue1+ stampDuty1+registrationCost1+handlingCharges+gstAmount1;
	var bookingAmount1=(aggreementValue1/100)*10;
	
	aggreementValue1=aggreementValue1.toFixed(0);
	registrationCost1=registrationCost1.toFixed(0);
	stampDuty1=stampDuty1.toFixed(0);
	grandTotal1=grandTotal1.toFixed(0);
	bookingAmount1=bookingAmount1.toFixed(0);
	gstAmount1=gstAmount1.toFixed(0);
	
	document.bookingform.gstAmount1.value=gstAmount1;
	document.bookingform.registrationCost1.value=registrationCost1;
	document.bookingform.handlingCharges.value=handlingCharges;
	document.bookingform.stampDuty1.value=stampDuty1;
	document.bookingform.aggreementValue1.value=aggreementValue1;
	document.bookingform.grandTotal1.value=grandTotal1;
	//document.bookingform.bookingAmount1.value=bookingAmount1;
	
	if(grandTotal1>5000000)
		{
		tds=(grandTotal1)/100;
		tds=tds.toFixed(0);
		}
	else 
		{
		tds=0;
		}
	document.bookingform.tds.value=tds;
}

function validate()
{ 
	
	clearall();
		//validation for first name
		if(document.bookingform.bookingfirstname.value=="")
		{
			$('#bookingfirstnameSpan').html('First name should not be empty..!');
			document.bookingform.bookingfirstname.focus();
			return false;
		}
		else if(document.bookingform.bookingfirstname.value.match(/^[\s]+$/))
		{
			$('#bookingfirstnameSpan').html('First name should not be empty..!');
			document.bookingform.bookingfirstname.value="";
			document.bookingform.bookingfirstname.focus();
			return false;
		}

		
		//validation for last name
		if(document.bookingform.bookinglastname.value=="")
		{
			 $('#bookinglastnameSpan').html('Last name should not be empty..!');
			document.bookingform.bookinglastname.focus();
			return false;
		}
		else if(document.bookingform.bookinglastname.value.match(/^[\s]+$/))
		{
			 $('#bookinglastnameSpan').html('Last name should not be empty..!');
			document.bookingform.bookinglastname.focus();
			return false;
		}
		
		//validation for address
		if(document.bookingform.bookingaddress.value=="")
		{
			 $('#bookingaddressSpan').html('Please, enter address..!');
			document.bookingform.bookingaddress.focus();
			return false;
		}
		else if(document.bookingform.bookingaddress.value.match(/^[\s]+$/))
		{
			 $('#bookingaddressSpan').html('Please, enter employee address name..!');
			document.bookingform.bookingaddress.focus();
			return false;
		}
	
		
		//validation for country name
		if(document.bookingform.countryName.value=="Default")
		{
			 $('#countryNameSpan').html('Please, select country name..!');
			document.bookingform.countryName.focus();
			return false;
		}
		
		//validation for state name
		if(document.bookingform.stateName.value=="Default")
		{
			 $('#stateNameSpan').html('Please, select state name..!');
			document.bookingform.stateName.focus();
			return false;
		}
		
		//validation for city name
		if(document.bookingform.cityName.value=="Default")
		{
			 $('#cityNameSpan').html('Please, select city name..!');
			document.bookingform.cityName.focus();
			return false;
		}
		
		//validation for location area name
		if(document.bookingform.locationareaName.value=="Default")
		{
			 $('#locationareaNameSpan').html('Please, select location name..!');
			document.bookingform.locationareaName.focus();
			return false;
		}
		
		
		
		//validation for email
		if(document.bookingform.bookingEmail.value=="")
		{
			 $('#bookingEmailSpan').html('Email Id should not be blank..!');
			document.bookingform.bookingEmail.focus();
			return false;
		}
		else if(!document.bookingform.bookingEmail.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		{
			 $('#bookingEmailSpan').html(' enter valid email id..!');
			document.bookingform.bookingEmail.value="";
			document.bookingform.bookingEmail.focus();
			return false;
		}
		
		
		//validation for mobile number 1
		if(document.bookingform.bookingmobileNumber1.value=="")
		{
			 $('#bookingmobileNumber1Span').html('Please, enter primary mobile number..!');
			document.bookingform.bookingmobileNumber1.value="";
			document.bookingform.bookingmobileNumber1.focus();
			return false;
		}
		else if(!document.bookingform.bookingmobileNumber1.value.match(/^[(]{1}[+]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
		{
			 $('#bookingmobileNumber1Span').html(' enter valid primary mobile number..!');
			document.bookingform.bookingmobileNumber1.value="";
			document.bookingform.bookingmobileNumber1.focus();
			return false;	
		}
		
		//validation for mobile number 2
		if(document.bookingform.bookingmobileNumber2.value.length!=0)
		{
			if(!document.bookingform.bookingmobileNumber2.value.match(/^[(]{1}[+]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
			{
				 $('#bookingmobileNumber2Span').html(' enter valid secondary mobile number..!');
				document.bookingform.bookingmobileNumber2.value="";
				document.bookingform.bookingmobileNumber2.focus();
				return false;	
			}
		}
		
		
		
		//validation for occupation
		if(document.bookingform.bookingOccupation.value=="Default")
		{
			 $('#bookingOccupationSpan').html('Please, select proper occupation..!');
			document.bookingform.bookingOccupation.focus();
			return false;
		}
	
		//validation for budget cost
		if(document.bookingform.flatBudget.value=="Default")
		{
			 $('#flatBudgetSpan').html('Please, select budget..!');
			document.bookingform.flatBudget.focus();
			return false;
		}
		
		//validation for bank name
		if(document.bookingform.bankName.value=="Default")
		{
			 $('#bankNameSpan').html('Please, select bank name..!');
			document.bookingform.bankName.focus();
			return false;
		}
		
		//validation for enquiry source
		if(document.bookingform.enqSource.value=="Default")
		{
			 $('#followupDateSpan').html('Please, select enquiry source..!');
			document.bookingform.enqSource.focus();
			return false;
		}
		
			
		//validation for project name-----------------------------------------------------
		if(document.bookingform.projectName.value=="Default")
		{
			 $('#projectNameSpan').html('Please, select project name..!');
			document.bookingform.projectName.focus();
			return false;
		}
		
		//validation for project building
		if(document.bookingform.buildingName.value=="Default")
		{
			 $('#buildingNameSpan').html('Please, select project building name..!');
			document.bookingform.buildingName.focus();
			return false;
		}
		//validation for project wing
		if(document.bookingform.wingName.value=="Default")
		{
			 $('#wingNameSpan').html('Please, select project wing name..!');
			document.bookingform.wingName.focus();
			return false;
		}
		//validation for floor
		if(document.bookingform.floortypeName.value=="Default")
		{
			 $('#floortypeNameSpan').html('Please, select floor..!');
			document.bookingform.floortypeName.focus();
			return false;
		}
		
		//validation for flat type
		if(document.bookingform.flatType.value=="Default")
		{
			 $('#flatTypeSpan').html('Please, select flat type..!');
			document.bookingform.flatType.focus();
			return false;
		}
		
		//validation for flat number
		if(document.bookingform.flatNumber.value=="Default")
		{
			 $('#flatNumberSpan').html('Please, select flat number..!');
			document.bookingform.flatNumber.focus();
			return false;
		}
	
		if(document.bookingform.parkingNumber.value=="Default")
		{
			 $('#parkingNumberSpan').html('Please, select parking number..!');
			document.bookingform.parkingNumber.focus();
			return false;
		}
		
}
function getParkingNumber()
{
	 
	 $("#parkingNumber").empty();
	 var projectName = $('#projectName').val();
	 var buildingName = $('#buildingName').val();
	 var wingName = $('#wingName').val();
	 var floortypeName = $('#parkingFloor').val();
	 
	 $.ajax({

			url : '${pageContext.request.contextPath}/getParkingNumber',
			type : 'Post',
			data : { projectName : projectName, buildingName : buildingName, wingName: wingName, floortypeName : floortypeName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select parking Number-");
								$("#parkingNumber").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].parkingNumber).text(result[i].parkingNumber);
								    $("#parkingNumber").append(option);
								} 
							} 
							else
							{
								alert("failure111");
							}

						}
			});	
}/* 
function getBranchList()
{
	 $("#branchName").empty();
	 var bankName = $('#bankName').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
} */

function getpinCode()
{

	 $("#bookingPincode").empty();
	 var locationareaName = $('#locationareaName').val();
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		//data : { locationareaName : locationareaName, cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								if(result[i].countryName==countryName)
									{
									if(result[i].stateName==stateName)
										{
											if(result[i].cityName==cityName)
											{
												if(result[i].locationareaName==locationareaName)
												{
													 $('#bookingPincode').val(result[i].pinCode);
												}
											}
										}
									}
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}
/*
function getsecondpinCode()
{

	$("#secondPincode").empty();
	 var locationareaName = $('#locationareaName').val();
	 var cityName = $('#secondcityName').val();
	 var stateName = $('#secondstateName').val();
	 var countryName = $('#secondcountryName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getallAreaList',
		type : 'Post',
		//data : { locationareaName : locationareaName, cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
							for(var i=0;i<result.length;i++)
							{
								if(result[i].countryName==countryName)
									{
									if(result[i].stateName==stateName)
										{
											if(result[i].cityName==cityName)
											{
												if(result[i].locationareaName==locationareaName)
												{
													 $('#secondPincode').val(result[i].pinCode);
												}
											}
										}
									}
								
							 } 
						
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}
*/
function init()
{
	//$('#enquirydbStatusSpan').html('');
	//clearall();

	$.ajax({

		url : '${pageContext.request.contextPath}/getGstCostList',
		type : 'Post',
		data : { },
		dataType : 'json',
		success : function(result)
				  {
			 var gstCost=0;
						if (result) 
						{
							for(var i=0;i<result.length;i++)
							{
								gstCost=result[result.length-1].taxPercentage;
							} 
						} 
						else
						{
							alert("failure111");
						}
						document.bookingform.gstCost.value=gstCost;
					}
		});
	
	 var n = new Date();
	 var y = n.getFullYear();
	 var m = n.getMonth()+1;
	 var d = n.getDate();
	 
	 document.getElementById("creationDate").value = d + "/"+ m + "/" + y;
	 
	 document.getElementById("updateDate").value = d + "/"+ m + "/" + y;
	
		document.bookingform.stampDutyPer.value=6;
		 document.bookingform.registrationPer.value=1;
}

function getStateList()
{

	 $("#stateName").empty();
	 $("#cityName").empty();
	 $("#locationareaName").empty();
	 
	 var countryName = $('#countryName').val();

		$.ajax({

			url : '${pageContext.request.contextPath}/getStateList',
			type : 'Post',
			data : { countryName : countryName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{

								var option = $('<option/>');
								option.attr('value',"Default").text("-Select Location Area-");
								$("#locationareaName").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select City-");
								$("#cityName").append(option);
								
								var option = $('<option/>');
								option.attr('value',"Default").text("-Select State-");
								$("#stateName").append(option);
								
								for(var i=0;i<result.length;i++)
								{
									var option = $('<option />');
								    option.attr('value',result[i].stateName).text(result[i].stateName);
								    $("#stateName").append(option);
								 } 
							} 
							else
							{
								alert("failure111");
								//$("#ajax_div").hide();
							}

						}
			});
		
}//end of get State List


function getCityList()
{

	$("#cityName").empty();
	$("#locationareaName").empty();
	var stateName = $('#stateName').val();
	var countryName = $('#countryName').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaName").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityName).text(result[i].cityName);
							    $("#cityName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List

function getLocationAreaList()
{
	$("#locationareaName").empty();
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#locationareaName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaName).text(result[i].locationareaName);
							    $("#locationareaName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get locationarea List



function getBankCode()
{
	
	 $("#branchName").empty();
	 $("#bankifscCode").empty();
	 var bankName = $('#chequebankName').val();
	 $.ajax({

		 url : '${pageContext.request.contextPath}/getBankCode',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							
								$('#branchName').val(result[0].branchName);
								$('#bankifscCode').val(result[0].bankifscCode);
							
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}



function getSubEnquiryList()
{
	 $("#subenqSource").empty();
	 var enqSource = $('#enqSource').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getSubEnquiryList',
		type : 'Post',
		data : { enqSource : enqSource},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Sub Enquiry Source-");
							$("#subenqSource").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].subenquirysourceName).text(result[i].subenquirysourceName);
							   
							    $("#subenqSource").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}
/*
function getsecondStateList()
{

	$("#secondstateName").empty();
	 $("#secondcityName").empty();
	 $("#secondlocationareaName").empty();
	 var countryName = $('#secondcountryName').val();
	 
	 
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#secondlocationareaName").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#secondcityName").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#secondstateName").append(option);
							
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateName).text(result[i].stateName);
							    $("#secondstateName").append(option);
							 } 
							
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}





function getsecondCityList()
{
	$("#secondcityName").empty();
	 $("#secondlocationareaName").empty();
	 
	 var stateName = $('#secondstateName').val();
	 var countryName = $('#secondcountryName').val();
	 
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#secondlocationareaName").append(option);
							
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#secondcityName").append(option);
							
						
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityName).text(result[i].cityName);
							    $("#secondcityName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
	 
}//end of get City List

function getsecondLocationAreaList()
{
	$("#secondlocationareaName").empty();
	 var cityName = $('#secondcityName').val();
	 var stateName = $('#secondstateName').val();
	 var countryName = $('#secondcountryName').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getLocationAreaList',
		type : 'Post',
		data : { cityName : cityName, stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Location Area-");
							$("#secondlocationareaName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].locationareaName).text(result[i].locationareaName);
							    $("#secondlocationareaName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get locationarea List


*/
function getWingNameList()
{
	 $("#wingName").empty();
	 $("#floortypeName").empty();
	 $("#flatNumber").empty();
	 $("#flatType").empty();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 document.bookingform.flatFacing.value="";
	 document.bookingform.flatareainSqFt.value="";
	 document.bookingform.flatbasicCost.value="";
	 document.bookingform.flatCostwithotfloorise.value="";
	 document.bookingform.floorRise.value="";
	 $.ajax({

		url : '${pageContext.request.contextPath}/getprojectwingList',
		type : 'Post',
		data : { buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							var option = $('<option/>');

			                var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].wingName).text(result[i].wingName);
							    $("#wingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}

function getBuildingList()
{
	 $("#buildingName").empty();
	 $("#wingName").empty();
	 $("#floortypeName").empty();
	 $("#flatNumber").empty();
	 $("#flatType").empty();
	 document.bookingform.flatFacing.value="";
	 document.bookingform.flatareainSqFt.value="";
	 document.bookingform.flatbasicCost.value="";
	 document.bookingform.flatCostwithotfloorise.value="";
		document.bookingform.floorRise.value="";
	 var projectName = $('#projectName').val();
	 
	$.ajax({

		url : '${pageContext.request.contextPath}/getBuildingList1',
		type : 'Post',
		data : { projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Wing Name-");
							$("#wingName").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);

							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Building-");
							$("#buildingName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].buildingName).text(result[i].buildingName);
							    $("#buildingName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
	
	$("#parkingFloor").empty();
	$.ajax({

		url : '${pageContext.request.contextPath}/getParkingFloor',
		type : 'Post',
		data : { projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
						
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Parking Floor-");
							$("#parkingFloor").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#parkingFloor").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Building List  


function getFloorNameList()
{
	 $("#floortypeName").empty();
	 $("#flatNumber").empty();
	 $("#flatType").empty();
	 var wingName = $('#wingName').val();
	 var buildingName = $('#buildingName').val();
	 var projectName = $('#projectName').val();
	 document.bookingform.flatFacing.value="";
		document.bookingform.flatareainSqFt.value="";
		document.bookingform.flatbasicCost.value="";
		document.bookingform.flatCostwithotfloorise.value="";
		document.bookingform.floorRise.value="";
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getwingfloorNameList',
		type : 'Post',
		data : {wingName : wingName, buildingName : buildingName, projectName : projectName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Floor Name-");
							$("#floortypeName").append(option);
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);
							
												
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].floortypeName).text(result[i].floortypeName);
							    $("#floortypeName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
} 

function getFlatNumberList()
{
	
	document.bookingform.flatFacing.value="";
	document.bookingform.flatareainSqFt.value="";
	document.bookingform.flatbasicCost.value="";
	document.bookingform.flatCostwithotfloorise.value="";
	document.bookingform.floorRise.value="";
	 $("#flatNumber").empty();
	// var flatType = $('#flatType').val();
	 var floortypeName = $('#floortypeName').val();
		var wingName = $('#wingName').val();
		var buildingName = $('#buildingName').val();
		var projectName = $('#projectName').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getFlatNumberList',
		type : 'Post',
		data : {floortypeName: floortypeName ,wingName : wingName, buildingName : buildingName, projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Flat Number-");
							$("#flatNumber").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].flatNumber).text(result[i].flatNumber);
							    $("#flatNumber").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Flat Number List   

/*
function getflattype()
{	
	document.enquiryform.flatFacing.value="";
document.enquiryform.flatareainSqFt.value="";
document.enquiryform.flatbasicCost.value="";
$("#flatNumber").empty();
$("#flatType").empty();

var option = $('<option/>');
option.attr('value',"Default").text("-Select Flat Number-");
$("#flatNumber").append(option);

var option = $('<option/>');
option.attr('value',"Default").text("-Select Flat Type-");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"RK").text("RK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"1 BHK").text("1 BHK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"2 BHK").text("2 BHK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"3 BHK").text("3 BHK");
$("#flatType").append(option);
var option = $('<option/>');
option.attr('value',"4 BHK").text("4 BHK");
$("#flatType").append(option);

}
*/
function getFlatFacing()
{
	document.bookingform.flatFacing.value="";
	document.bookingform.flatareainSqFt.value="";
	document.bookingform.flatbasicCost.value="";
	document.bookingform.flatCost.value="";
	
	document.bookingform.flatCostwithotfloorise.value="";
	document.bookingform.floorRise.value="";
	
	
	var flatNumber = $('#flatNumber').val();
	var wingName = $('#wingName').val();
	var buildingName = $('#buildingName').val();
	var projectName = $('#projectName').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getFlatDetail',
		type : 'Post',
		data : {flatNumber : flatNumber , wingName : wingName, buildingName : buildingName, projectName : projectName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
						  for(var i=0;i<result.length;i++)
						  {
							  $('#flatFacing').val(result[i].flatfacingName);
							  $('#flatareainSqFt').val(result[i].flatAreawithLoadingInFt);
							  $('#flatbasicCost').val(result[i].flatbasicCost);
							  $('#flatCost').val(result[i].flatCost); 
							  $('#flatminimumCost').val(result[i].flatminimumCost); 
							  $('#flatType').val(result[i].flatType); 
							  
							  $('#flatCostwithotfloorise').val(result[i].flatCostwithotfloorise); 
							  $('#floorRise').val(result[i].floorRise);
						  }
							
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
	
}//end of get Flat Number List

function getflatBasicCost()
{
	$('#flatCostSpan').html('');
	 var flatareainSqFt1 = $('#flatareainSqFt').val();
	 var flatCost1 = $('#flatCost').val();
	 var flatminimumCost1 = $('#flatminimumCost').val();
	 var floorRise1 = $('#floorRise').val();
	 var flatareainSqFt= parseInt(flatareainSqFt1);
	 var flatCost= parseInt(flatCost1);
	 var flatminimumCost= parseInt(flatminimumCost1);
	 
	 var floorRise= parseInt(floorRise1);
	 var flatCostwithotfloorise;
	 var flatBasiccost;
	 
	 if(flatCost<flatminimumCost)
		 {
		 flatBasiccost=flatareainSqFt*flatminimumCost;
		 flatCostwithotfloorise=flatminimumCost-floorRise;
		 document.bookingform.flatCost.value=flatminimumCost;
		 $('#flatCostSpan').html('Value should be '+flatminimumCost+' or above..!');
		 }
	 else
		 {
		 flatBasiccost=flatareainSqFt*flatCost;
		 flatCostwithotfloorise=flatCost-floorRise;
		 }
	 	document.bookingform.flatbasicCost.value=flatBasiccost;
		document.bookingform.flatCostwithotfloorise.value=flatCostwithotfloorise;
		calculate();
}
$(function () 
 {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    
    //Date picker
    $('#secondDob').datepicker({
      autoclose: true
    })
 $('#datepicker').datepicker({
      autoclose: true
    })
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
