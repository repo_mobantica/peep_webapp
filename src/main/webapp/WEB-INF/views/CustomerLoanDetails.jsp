<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Customer Loan Details</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
  <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
          
</head>
<body class="hold-transition skin-blue sidebar-mini"  onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">

  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
  <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Customer Loan Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Customer Loan Details</li>
      </ol>
    </section>

<form name="CustomerLoanDetailsform" action="${pageContext.request.contextPath}/CustomerLoanDetails" onSubmit="return validate()" method="POST">
    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              
              <!-- /.form-group -->
			  
			   <span id="statusSpan" style="color:#FF0000"></span>
			   
			     <div class="box-body">
              <div class="row">
               <div class="col-xs-2">
			     <label for="customerloanId">Loan Id</label>
			    <input type="text" class="form-control" id="customerloanId" name="customerloanId" value="${loanCode}" readonly>
                 </div> 
                  <div class="col-xs-2">
			     <label for="bookingId">Booking Id</label>
                  <input type="text" class="form-control" id="bookingId" name="bookingId" value="${bookingList[0].bookingId}" readonly>
                 </div> 
				</div>
            </div>
			  
			  
			     <div class="box-body">
              <div class="row">
                  <div class="col-xs-4">
                  <label for="customerName">Customer Name  </label>  <label class="text-red">* </label>
                  <input type="text" class="form-control" id="customerName" name="customerName"  value="${bookingList[0].bookingfirstname} ${bookingList[0].bookingmiddlename} ${bookingList[0].bookinglastname}" readonly>
                 <span id="customerNameSpan" style="color:#FF0000"></span>
			     </div> 
				</div>
            </div>
			  
				
				    <div class="box-body">
              <div class="row">
                  <div class="col-xs-3">
			   <label for="totalAmount">Total Amount </label>  <label class="text-red">*</label>
                  <input type="text" class="form-control" id="totalAmount" name="totalAmount"  value="${bookingList[0].grandTotal1}" readonly>
			      <span id="totalAmountSpan" style="color:#FF0000"></span>
			     </div> 
				     <div class="col-xs-3">
			   <label for="totalAmount">Aggreement Amount </label>  <label class="text-red">*</label>
                  <input type="text" class="form-control" id="aggreementValue1" name="aggreementValue1"  value="${bookingList[0].aggreementValue1}" readonly>
			      <span id="totalAmountSpan" style="color:#FF0000"></span>
			     </div> 
			      <div class="col-xs-3">
                  <label>Loan Amount</label>  <label class="text-red">* </label>
        		 <input type="text" class="form-control" id="loanAmount" name="loanAmount" onchange="getremainingAmount(this.value)">
                   <span id="loanAmountSpan" style="color:#FF0000"></span>
                </div>               
                <div class="col-xs-3">
				 <label>Own Contribution Amount</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="remainingAmount" name="remainingAmount" readonly>
                   <span id="remainingAmountSpan" style="color:#FF0000"></span>
				</div>
              </div>
            </div>
				
				
	
            
						
				<div class="box-body">
              <div class="row">
                    <div class="col-xs-3">
			   	<label for="pl">Prefer Bank </label><label class="text-red">* </label>
               <select class="form-control" id="bankName" name="bankName" onchange="getBranchList(this.value)">
				  	<option selected="" value="Default">-Select Bank-</option>
                  <c:forEach var="bankList" items="${bankList}">
                  <option value="${bankList.bankName}">${bankList.bankName}</option>
				  </c:forEach>	
                  	  </select>
                  <span id="bankNameSpan" style="color:#FF0000"></span>
			     </div> 
			     <div class="col-xs-3">
			    <label>Bank Branch Name </label> <label class="text-red">* </label>
   				<select class="form-control" id="branchName" name="branchName" onchange="getBranchIfsc(this.value)">
                 <option selected="selected" value="Default">-Select Bank Branch-</option>
                  <c:forEach var="bankBranchList" items="${bankBranchList}">
                 <option value="${bankBranchList.branchName}">${bankBranchList.branchName}</option>
				  </c:forEach>
                 </select>                
 				<span id="branchNameSpan" style="color:#FF0000"></span>
			     </div> 
			      <div class="col-xs-3">
			    <label>IFSC </label> <label class="text-red">* </label>
                 <input type="text" class="form-control" id="bankifscCode" placeholder="IFSC "name="bankifscCode" readonly="readonly">
                 <span id="bankifscCodeSpan" style="color:#FF0000"></span>
			     </div> 
			     </div>              
              </div>
            </div>
			
			
			
              <!-- /.form-group -->
            </div>
			
          </div>
		   <div class="box-body">
              <div class="row">
              </br>
                <div class="col-xs-4">
                	<a href="BookingMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
			     
			    
              </div>
			  </div>
          <!-- /.row -->
		  
		  
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->

<script>

function getBranchList()
{
	 $("#branchName").empty();
	 var bankName = $('#bankName').val();
	
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#bankName').val();
     var branchName = $('#branchName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
							/* for(var i=0;i<result.length;i++)
							{
								 var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#bankifscCode").append(option); 
							}   */
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

function clearall()
{
	$('#loanAmountSpan').html('');
	$('#bankNameSpan').html('');
}
function getremainingAmount()
{
	 var aggreementValue1 = $('#aggreementValue1').val(); 
	 var loanAmount = $('#loanAmount').val();
	 var remainingAmount = aggreementValue1-loanAmount;
	 document.getElementById("remainingAmount").value = remainingAmount ; 
	 
}
function validate()
{
	claerall();
	
	if(document.CustomerLoanDetailsform.loanAmount.value=="")
	{
		$('#loanAmountSpan').html('Please, enter valid digit loan amount..!');
		document.CustomerLoanDetailsform.loanAmount.value="";
		document.CustomerLoanDetailsform.loanAmount.focus();
		return false;
	}
	else if(!document.CustomerLoanDetailsform.loanAmount.value.match(/^[0-9]+$/))
	{
		$('#loanAmountSpan').html(' loan amount must be digit numbers only..!');
		document.CustomerLoanDetailsform.loanAmount.value="";
		document.CustomerLoanDetailsform.loanAmount.focus();
		return false;
	}
	//validation for bank name	
	if(document.CustomerLoanDetailsform.bankName.value=="Default")
	{
		$('#bankNameSpan').html('Please, select bank name..!');
		document.CustomerLoanDetailsform.bankName.focus();
		return false;
	}
	if(document.editcustomerloanform.branchName.value=="Default")
	{
		$('#branchNameSpan').html('Please, select bank branch name..!');
		document.editcustomerloanform.branchName.focus();
		return false;
	}
}

function init()
{
	clearall();
	 var n = new Date();
	 var y = n.getFullYear();
	 var m = n.getMonth()+1;
	 var d = n.getDate();
	 
	 document.getElementById("creationDate").value = d + "/"+ m + "/" + y;
	 
	 document.getElementById("updateDate").value = d + "/"+ m + "/" + y;
	 
	 document.getElementById("userName").value = "Admin" ; 

	
     document.CustomerLoanDetailsform.loanAmount.focus();
}


</script>
</body>
</html>
