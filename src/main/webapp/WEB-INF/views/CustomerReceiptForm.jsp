<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Customer Receipt Form</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
   <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
    
 	<script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
    </script>

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>

<div class="wrapper">

   <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Payment Scheduler Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Customer Receipt Form</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="customerreceiptform" action="${pageContext.request.contextPath}/CustomerReceiptForm" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
      <div class="box box-danger"></div>
        
         <div class="box-body" id="paymentStatus" hidden="hidden">
           <div class="callout callout-success">
                <h4><i class="icon fa fa-check"></i>Amount payed by  ${bookingDetails[0].bookingfirstname} ${bookingDetails[0].bookingmiddlename} ${bookingDetails[0].bookinglastname}</h4>

              </div>
	 </div>
	 <div class="box-body">
	           <div class="form-horizontal">
			
		
				<div class="col-md-4 col-xs-12">
				 
				 <tabel>
				 </tabel>
				 
				 <div class="form-group">
                     <h4><label for="bookingId" class="col-sm-4 control-label"><span class="fa fa-star"></span> </label></h4> 
                       <h4> <div class="col-sm-8 control-label" style="text-align:left;">
                      <label >Booking Id : ${bookingDetails[0].bookingId}</label>
                      </div></h4>
					</div>
		
			</div>
				
				<div class="col-md-6 col-xs-12">
			  		  <div class="form-group">
                     <h4><label for="customerName" class="col-sm-4 control-label"><span class="fa fa-user"></span> </label></h4> 
                       <h4> <div class="col-sm-8 control-label" style="text-align:left;">
                      <label>Customer Name : ${bookingDetails[0].bookingfirstname} ${bookingDetails[0].bookingmiddlename} ${bookingDetails[0].bookinglastname}</label>
                      </div></h4>
					</div>
			</div>
				<div class="col-md-6 col-xs-12">
			  	
			</div>
            </div><!-- /.box-body -->
          </div>
          
          
          
          
          <div class="box-body">
	           <div class="form-horizontal">
			<div class="col-md-4 col-xs-6">
			</br>
			<div class="box-body">
              <div class="table-responsive">
              <table id="bankListTable" class="table table-bordered ">

                  <thead>
	                  <tr bgcolor=#4682B4>
	                    <th>Particulars</th>
	                    <th>Amount(Rs.)</th>
	                    
	                    <!-- <th>Work Stage</th>
	                    <th>Total Due as per work stage</th>
	                    <th>Less Total Receipt Till Date</th>
	                    <th>Total due to us as on date(Excluding GST)</th>
	                    <th>GST due on above amount</th>
	                    <th>Total Due including GST</th> -->
	                    
	                  </tr>
                  </thead>
                  <tbody>
                      
                      <tr>
             		    <td>Agreement Value</td>
             		    <td>${demandDetails.agreementValue}</td>
             		  </tr>
             		  <tr>
             		    <td>Work Steg</td>
             		    <td>${demandDetails.completedWorkSteg}</td>
             		  </tr>
             		  <tr>
             		    <td>Total Due as per work stage</td>
             		    <td>${demandDetails.dueAsPerWork}</td>
             		  </tr>
             		  <tr>
             		    <td>Less Total Receipt Till Date</td>
             		    <td>${demandDetails.totalTillDate}</td>
             		  </tr>
             		  <tr>
             		    <td>Total due to us as on date(Excluding GST)</td>
             		    <td>${demandDetails.totalExcludingGST}</td>
             		  </tr>
             		  <tr>
             		    <td>GST due on above amount</td>
             		    <td>${demandDetails.GSTdue}</td>
             		  </tr>
             		  <tr>
             		    <td>Total Due including GST</td>
             		    <td>${demandDetails.totalIncludingGST}</td>
             		  </tr>
             		  
                 </tbody>
                 
                </table>
              </div>
            </div>
			
			
			
			
			</div>
				<div class="col-md-8 col-xs-6">
				 <table>
				  <thead>
                  <tr >
              	 <td style="width:400px">
				    <div class="form-group">
                       <h4><div class="col-sm-8 control-label" style="text-align:left;"></div></h4>
					</div>
				 </td>
                  	 <td style="width:150px">
				  <div class="form-group">
                       <h4> <div class="col-sm-8 control-label" style="text-align:left;">
                      <label><font color="red"> Total Amount </font></label>
                      </div></h4>
					</div>
				 </td>
				 <td style="width:150px">
				    <div class="form-group">
                       <h4> <div class="col-sm-8 control-label" style="text-align:left;">
                      <label> <font color="red"> Payed Amount </font></label>
                      </div></h4>
					</div>
				 </td>
				 <td style="width:150px">
				    <div class="form-group">
                       <h4> <div class="col-sm-7 control-label" style="text-align:left;">
                      <label> <font color="red"> Remaining Amount </font></label>
                      </div></h4>
					</div>
				  </td>
				 <td style="width:10px">
				      <span id="buttonSpan" style="color:#FF0000"></span>
				 </td>
                  </tr>
                  </thead>
                  <tbody >
				 <tr>
				 <td>
				  <div class="form-group">
                     <h4><label for="bookingId" class="col-sm-4 control-label"><span class="fa fa-inr"></span> </label></h4> 
                       <h4> <div class="col-sm-8 control-label" style="text-align:left;">
                      <label> <font color="green"> Aggreement Amount :</font> </label>
                      </div></h4>
					</div>
				 </td>
				 <td>
				<label >${bookingDetails[0].aggreementValue1}</label>
				 </td>
				  <td>
				<label > ${customerPayment[0].totalpayAggreement}</label>
				 </td>
				   <td>
				<label id="reamaingAggreement"> </label>
				 </td>
				   <td id="aggreementButton">
				   <a onclick = "Aggreement()" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add Amount"><i class="glyphicon glyphicon-edit"></i></a>
				 </td>
				 </tr>
				 
				 <tr>
				 <td>
				     <div class="form-group">
                     <h4><label for="customerName" class="col-sm-4 control-label"><span class="fa fa-inr"></span> </label></h4> 
                       <h4> <div class="col-sm-8 control-label" style="text-align:left;">
                      <label><font color="green"> Stamp Duty Amount (6%)  :</font></label>
                      </div></h4>
					</div>
				 </td>
				  <td>
				<label > ${bookingDetails[0].stampDuty1} </label>
				 </td>
				  <td>
				<label > ${customerPayment[0].totalpayStampDuty}</label>
				 </td>
				   <td>
				<label id="reamaingstampDuty"> </label>
				 </td>
				 <td id="stampDutyButton">
				   <a onclick = "StampDuty()" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add Amount"><i class="glyphicon glyphicon-edit"></i></a>
				 </td>
				 </tr>
				 
				 <tr>
				 <td>
				 <div class="form-group">
                     <h4><label for="customerName" class="col-sm-4 control-label"><span class="fa fa-inr"></span> </label></h4> 
                       <h4> <div class="col-sm-8 control-label" style="text-align:left;">
                      <label> <font color="green"> Registration Amount (1%) :</font></label>
                      </div></h4>
					</div>
				 </td>
				  <td>
				<label > ${bookingDetails[0].registrationCost1} </label>
				 </td>
				  <td>
				<label > ${customerPayment[0].totalpayRegistration} </label>
				 </td>
				   <td>
				<label id="reamaingRegistration"> </label>
				 </td>
				  <td id="registrationButton">
				   <a onclick = "Registration()" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add Amount"><i class="glyphicon glyphicon-edit"></i></a>
				 </td>
				 </tr>
				 
				 <tr style="border-bottom: 1px solid #000;">
				 <td >
				 	<div class="form-group">
                     <h4><label for="customerName" class="col-sm-4 control-label"><span class="fa fa-inr"></span> </label></h4> 
                       <h4> <div class="col-sm-8 control-label" style="text-align:left;">
                      <label> <font color="green"> GST Amount  :</font></label>
                      </div></h4>
					</div>
				 </td>
				  <td>
				<label > ${bookingDetails[0].gstAmount1} </label>
				 </td>
				  <td>
				<label > ${customerPayment[0].totalpaygstAmount}</label>
				 </td>
				   <td>
				<label id="reamaingGstAmount"> </label>
				 </td>
				  <td id="gstButton">
				   <a onclick = "GstAmount()" class="btn btn-success btn-sm" data-toggle="tooltip" title="Add Amount"><i class="glyphicon glyphicon-edit"></i></a>
				 </td>
				 </tr>
				 
				 <tr>
				 <td >
				 	<div class="form-group">
                     <h4><label for="customerName"class="col-sm-4 control-label"><span class="fa fa-inr"></span> </label></h4> 
                       <h4> <div class="col-sm-8 control-label" style="text-align:left;">
                      <label> <font color="green"> Total Amount  :</font></label>
                      </div></h4>
					</div>
				 </td>
				  <td>
			<h4><label > ${bookingDetails[0].grandTotal1}</label></h4>
				 </td>
				  <td>
				<label id="totalPayedAmount1"></label>
				 </td>
				  <td>
				<label id="totalRemainingAmount">  </label>
				 </td>
				 </tr>
				</tbody>
				 </table>
				 
				 
				</div>
            </div>
          
     
      
         
    </div>
        
              <input type="hidden" class="form-control" id="paymentType" name="paymentType" value="">
              <input type="hidden" class="form-control" id="paymentAmount" name="paymentAmount" value="">
              
              <input type="hidden" class="form-control" id="bookingId" name="bookingId" value="${bookingDetails[0].bookingId}">
              <input type="hidden" class="form-control" id="id" name="id" value="${customerPayment[0].id}">
              <input type="hidden" class="form-control" id="receiptId" name="receiptId" value="${receiptCode}">
              <input type="hidden" class="form-control" id="totalpayAggreement" name="totalpayAggreement" value="${customerPayment[0].totalpayAggreement}">
              <input type="hidden" class="form-control" id="totalpayStampDuty" name="totalpayStampDuty" value="${customerPayment[0].totalpayStampDuty}">
              <input type="hidden" class="form-control" id="totalpayRegistration" name="totalpayRegistration" value=" ${customerPayment[0].totalpayRegistration} ">
              <input type="hidden" class="form-control" id="totalpaygstAmount" name="totalpaygstAmount" value=" ${customerPayment[0].totalpaygstAmount}">
                             
              <input type="hidden" class="form-control" id="totalPayedAggreement1" name="totalPayedAggreement1" value="${customerPayment[0].totalpayAggreement}">
              <input type="hidden" class="form-control" id="totalPayedstampDuty1" name="totalPayedstampDuty1" value="${customerPayment[0].totalpayStampDuty}">
              <input type="hidden" class="form-control" id="totalPayedRegistration1" name="totalPayedRegistration1" value=" ${customerPayment[0].totalpayRegistration} ">
              <input type="hidden" class="form-control" id="totalPayedGstAmount1" name="totalPayedGstAmount1" value=" ${customerPayment[0].totalpaygstAmount}">
               
               
               
               
              <input type="hidden" class="form-control" id="totalAggreement1" name="totalAggreement1" value="${bookingDetails[0].aggreementValue1}">
              <input type="hidden" class="form-control" id="totalstampDuty1" name="totalstampDuty1" value="${bookingDetails[0].stampDuty1} ">
              <input type="hidden" class="form-control" id="totalRegistration1" name="totalRegistration1" value="${bookingDetails[0].registrationCost1} ">
              <input type="hidden" class="form-control" id="totalGstAmount1" name="totalGstAmount1" value=" ${bookingDetails[0].gstAmount1}">
               
               
              <input type="hidden" id="creationDate" name="creationDate" value="">
			  <input type="hidden" id="updateDate" name="updateDate" value="">
			  <input type="hidden" id="userName" name="userName" value="Admin">                             
	  </div>	
	  
	  
	      <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                  <span id="statusSpan" style="color:#FF0000"></span>
                   
                  <div class="box-body">
                     <div class="row">
                     <div class="col-xs-3">
                     </div>
                       <div class="col-xs-4" id="abc">
					   <label for="radiobutton">Payment Mode</label><label class="text-red">* </label> <span id="paymentModeSpan" style="color:#FF0000"></span></br>
		                <input type="radio"name="paymentMode" id="paymentMode" value="Cash"  onclick = "cashPayment()"> Cash
		                   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
		                 <input type="radio"  name="paymentMode" id="paymentMode" value="Cheque" onclick = "chequePayment()"> Cheque
						  &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
		                 <input type="radio"  name="paymentMode" id="paymentMode" value="DD" onclick = "ddPayment()"> DD
						    &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
		                 <input type="radio"  name="paymentMode" id="paymentMode" value="Online" onclick = "onlinePayment()"> Online
				  	</div> 
				  	
		 
			 
		<div class="col-xs-3">
          <div id="aggreement" hidden="hidden">
			    <label>Add Aggreement Amount </label> <label class="text-red">* </label>
                 <input type="text" class="form-control" id="aggreementAmount" placeholder="Aggreement Amount " name="aggreementAmount" >
			     <span id="aggreementAmountSpan" style="color:#FF0000"></span>
			
		</div>
          <div id="stampDuty" hidden="hidden">
			    <label>Add Stamp Duty Amount </label> <label class="text-red">* </label>
                 <input type="text" class="form-control" id="stampDutyAmount" placeholder="Stamp Duty Amount " name="stampDutyAmount" >
			     <span id="stampDutyAmountSpan" style="color:#FF0000"></span>
			 
		</div>
          <div id="registration" hidden="hidden">
			    <label>Add Registration Amount </label> <label class="text-red">* </label>
                 <input type="text" class="form-control" id="registrationAmount" placeholder="Registration Amount " name="registrationAmount" >
			     <span id="registrationAmountSpan" style="color:#FF0000"></span>
			
		</div>
          <div id="gstamount" hidden="hidden">
			    <label>Add GST Amount </label> <label class="text-red">* </label>
                 <input type="text" class="form-control" id="gstAmount" placeholder="GST Amount " name="gstAmount" >
			     <span id="gstAmountSpan" style="color:#FF0000"></span>
			     </div> 
			     
			     </div>

		 
				  </div>
				  </div>	
         		<div class="box-body"id="cheque" hidden="hidden">
               <div class="row">
            	<div class="col-md-3">
                  <label for="bankName">Bank Name </label><label class="text-red">* </label>
    				<select class="form-control" name="bankName" id="bankName" onchange="getBranchList(this.value)">
				  <option selected="" value="Default">-Select Prefer Bank-</option>
                   <c:forEach var="bankList" items="${bankList}">
                  <option value="${bankList.bankName}">${bankList.bankName}</option>
				  </c:forEach>
                  </select>                     
		            <span id="bankNameSpan" style="color:#FF0000"></span>
		           </div>
           	    <div class="col-xs-3">
			    <label>Bank Branch Name </label> <label class="text-red">* </label>
   				<select class="form-control" id="branchName" name="branchName" onchange="getBranchIfsc(this.value)">
                 <option selected="selected" value="Default">-Select Bank Branch-</option>
                  <c:forEach var="bankBranchList" items="${bankBranchList}">
                 <option value="${bankBranchList.branchName}">${bankBranchList.branchName}</option>
				  </c:forEach>
                 </select>                
 				<span id="branchNameSpan" style="color:#FF0000"></span>
			     </div>  
			    
			    <div class="col-md-3">
                  <label id="labelchequeNumber" >Cheque/DD/RTGS/NEFT/IMPS Number</label><label class="text-red">* </label>
                  <input type="text" class="form-control" name="chequeNumber" id="chequeNumber" placeholder="Enter Number">
                  <span id="chequeNumberSpan" style="color:#FF0000"></span>
        	    </div>
			    
			    <div class="col-xs-3">
			      <label>IFSC </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="bankifscCode" placeholder="IFSC "name="bankifscCode" readonly="readonly">
                  <span id="bankifscCodeSpan" style="color:#FF0000"></span>
			    </div> 
              	  </div>
            	</div> 
            	  
            	  
	      </div>               
         </div>
        </div> 
	  
	  
	    <div class="box-body">
           <div class="row">
         
              </br>
                 <div class="col-xs-5">
                   <a href="BookingMaster"><button type="button" class="btn btn-primary" value="Back" style="width:90px">Back</button></a>
                 </div>
				 <div class="col-xs-3">
                   <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
			     </div>
				 
				 <div class="col-xs-3" id="submitButton">
			        <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			     </div> 
			
            </div>
        </div>
      </section>
	</form>
 </div>
    <%@ include file="footer.jsp" %>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>

<script>

function getBranchList()
{
	
	 $("#branchName").empty();
	 var bankName = $('#bankName').val();
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchList',
		type : 'Post',
		data : { bankName : bankName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select Branch Name-");
							$("#branchName").append(option);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#branchName").append(option);
							} 
						} 
						else
						{
							alert("failure111");
						}

					}
		});	
}

function getBranchIfsc()
{
	 //$("#bankifscCode").val('');
	 var bankName = $('#bankName').val();
     var branchName = $('#branchName').val();
	 
	 $.ajax({

		url : '${pageContext.request.contextPath}/getBranchIfsc',
		type : 'Post',
		data : { bankName : bankName, branchName : branchName },
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							$('#bankifscCode').val(result[0].bankifscCode);
							/* for(var i=0;i<result.length;i++)
							{
								 var option = $('<option />');
							    option.attr('value',result[i].branchName).text(result[i].branchName);
							    $("#bankifscCode").append(option); 
							}   */
						} 
						else
						{
							alert("failure111");
						}

					}
		});
}

function chequePayment(){
	document.getElementById("labelchequeNumber").innerHTML="Cheque Number";
	  document.getElementById('cheque').style.display ='block';
	  document.customerreceiptform.chequeNumber.value="";
	}
function ddPayment(){
	document.getElementById("labelchequeNumber").innerHTML="DD Number";
	  document.getElementById('cheque').style.display ='block';
	  document.customerreceiptform.chequeNumber.value="";
	}
function onlinePayment(){
	document.getElementById("labelchequeNumber").innerHTML="RTGS/NEFT/IMPS Number";
	  document.getElementById('cheque').style.display ='block';
	  document.customerreceiptform.chequeNumber.value="";
	}
function cashPayment(){
	  document.getElementById('cheque').style.display ='none';
	  document.customerreceiptform.chequeNumber.value="";
	}
function Aggreement()
{
	
	document.customerreceiptform.gstAmount.value="";
	document.customerreceiptform.registrationAmount.value="";
	document.customerreceiptform.stampDutyAmount.value="";
	document.customerreceiptform.aggreementAmount.value="";

	
	document.getElementById('registration').style.display ='none';
	document.getElementById('stampDuty').style.display ='none';
	document.getElementById('gstamount').style.display ='none';
	document.getElementById('aggreement').style.display ='block';
	document.getElementById('abc').style.display ='block';
}
function StampDuty()
{
	document.customerreceiptform.registrationAmount.value="";
	document.customerreceiptform.stampDutyAmount.value="";
	document.customerreceiptform.aggreementAmount.value="";
	document.getElementById('aggreement').style.display ='none';
	document.getElementById('registration').style.display ='none';
	document.getElementById('gstamount').style.display ='none';
	document.getElementById('stampDuty').style.display ='block';
	document.getElementById('abc').style.display ='block';
}
function Registration()
{
	document.customerreceiptform.registrationAmount.value="";
	document.customerreceiptform.stampDutyAmount.value="";
	document.customerreceiptform.aggreementAmount.value="";
	document.getElementById('aggreement').style.display ='none';
	document.getElementById('stampDuty').style.display ='none';
	document.getElementById('gstamount').style.display ='none';
	document.getElementById('registration').style.display ='block';
	document.getElementById('abc').style.display ='block';
}
function GstAmount()
{
	document.customerreceiptform.registrationAmount.value="";
	document.customerreceiptform.stampDutyAmount.value="";
	document.customerreceiptform.aggreementAmount.value="";
	document.getElementById('aggreement').style.display ='none';
	document.getElementById('stampDuty').style.display ='none';
	document.getElementById('registration').style.display ='none';
	document.getElementById('gstamount').style.display ='block';
	document.getElementById('abc').style.display ='block';
}

function init()
{
	
	 var n = new Date();
	 var y = n.getFullYear();
	 var m = n.getMonth()+1;
	 var d = n.getDate();
	 
	 document.getElementById("creationDate").value = d + "/"+ m + "/" + y;
	 
	 document.getElementById("updateDate").value = d + "/"+ m + "/" + y;
	// document.getElementById("userName").value = "Admin" ; 
	
	var totalPayedAggreement1= Number($('#totalPayedAggreement1').val());
	var totalPayedstampDuty1= Number($('#totalPayedstampDuty1').val());
	var totalPayedRegistration1= Number($('#totalPayedRegistration1').val());
	var totalPayedGstAmount1= Number($('#totalPayedGstAmount1').val());
	var totalPayedAmount1=totalPayedAggreement1+totalPayedstampDuty1+totalPayedRegistration1+totalPayedGstAmount1;
	document.getElementById('totalPayedAmount1').innerHTML = totalPayedAmount1;
	
	
	  
var totalAggreement1=Number($('#totalAggreement1').val());
var totalstampDuty1=Number($('#totalstampDuty1').val());
var totalRegistration1=Number($('#totalRegistration1').val());
var totalGstAmount1=Number($('#totalGstAmount1').val());

var reamaingAggreement=totalAggreement1-totalPayedAggreement1;
var reamaingstampDuty=totalstampDuty1-totalPayedstampDuty1;
var reamaingRegistration= totalRegistration1-totalPayedRegistration1;
var reamaingGstAmount=totalGstAmount1-totalPayedGstAmount1;

document.getElementById('reamaingAggreement').innerHTML = reamaingAggreement;
document.getElementById('reamaingstampDuty').innerHTML =reamaingstampDuty ;
document.getElementById('reamaingRegistration').innerHTML =reamaingRegistration;
document.getElementById('reamaingGstAmount').innerHTML = reamaingGstAmount;

document.getElementById('totalRemainingAmount').innerHTML = reamaingAggreement+reamaingstampDuty+reamaingRegistration+reamaingGstAmount;

if(reamaingAggreement<=0)
	{
	document.getElementById('aggreementButton').style.display ='none';
	}

if(reamaingstampDuty<=0)
	{
	document.getElementById('stampDutyButton').style.display ='none';
	}

if(reamaingRegistration<=0)
	{
	document.getElementById('registrationButton').style.display ='none';
	}
if(reamaingGstAmount<=0)
{
document.getElementById('gstButton').style.display ='none';
}


if(reamaingAggreement<=0 && reamaingstampDuty<=0 && reamaingRegistration<=0 && reamaingGstAmount<=0)
{
document.getElementById('submitButton').style.display ='none';
document.getElementById('paymentStatus').style.display ='block';
}
}

function validate()
{
	var totalPayedAggreement1= Number($('#totalPayedAggreement1').val());
	var totalPayedstampDuty1= Number($('#totalPayedstampDuty1').val());
	var totalPayedRegistration1= Number($('#totalPayedRegistration1').val());
	var totalPayedGstAmount1= Number($('#totalPayedGstAmount1').val());
	var totalPayedAmount1=totalPayedAggreement1+totalPayedstampDuty1+totalPayedRegistration1+totalPayedGstAmount1;
	document.getElementById('totalPayedAmount1').innerHTML = totalPayedAmount1;
	
	
	  
var totalAggreement1=Number($('#totalAggreement1').val());
var totalstampDuty1=Number($('#totalstampDuty1').val());
var totalRegistration1=Number($('#totalRegistration1').val());
var totalGstAmount1=Number($('#totalGstAmount1').val());

var reamaingAggreement=totalAggreement1-totalPayedAggreement1;
var reamaingstampDuty=totalstampDuty1-totalPayedstampDuty1;
var reamaingRegistration= totalRegistration1-totalPayedRegistration1;
var reamaingGstAmount=totalGstAmount1-totalPayedGstAmount1;



	$('#buttonSpan').html('');
	if(document.customerreceiptform.aggreementAmount.value.length==0)
		{
		if(document.customerreceiptform.stampDutyAmount.value.length==0)
			{
			if(document.customerreceiptform.registrationAmount.value.length==0)
				{
					if(document.customerreceiptform.gstAmount.value.length==0)
					{
						$('#buttonSpan').html('Please, add any one entry or select button..!');
						return false;
					}
				}
			}
		}
	
	 if(document.customerreceiptform.aggreementAmount.value!="")
		{
		 var paymentAmount= Number($('#aggreementAmount').val());
		 if(document.customerreceiptform.aggreementAmount.value.match(/^[0-9]+$/))
			 {
			 	document.customerreceiptform.paymentType.value="";
				document.customerreceiptform.paymentType.value="Aggreement";
			 	var paymentAmount= Number($('#aggreementAmount').val());
				document.customerreceiptform.paymentAmount.value="";
				document.customerreceiptform.paymentAmount.value=paymentAmount;
				
				var abc= Number($('#totalPayedAggreement1').val());
				document.customerreceiptform.totalpayAggreement.value=abc+paymentAmount;
			}
		 else
			 {
			 document.customerreceiptform.aggreementAmount.value="";
			 	$('#aggreementAmountSpan').html('Please, enter the only digit values ..!');
			 	return false;
			 }
			if(reamaingAggreement<paymentAmount)
				{
				$('#aggreementAmountSpan').html('Please, enter correct amount ..!');
				return false;
				}
		}
	else if(document.customerreceiptform.stampDutyAmount.value!="")
		{
		var paymentAmount= Number($('#stampDutyAmount').val());
		 if(document.customerreceiptform.stampDutyAmount.value.match(/^[0-9]+$/))
			 {
				document.customerreceiptform.paymentType.value="";
				document.customerreceiptform.paymentType.value="Stamp Duty";
				
				document.customerreceiptform.paymentAmount.value="";
				document.customerreceiptform.paymentAmount.value=paymentAmount;
			
				var abc= Number($('#totalPayedstampDuty1').val());
				document.customerreceiptform.totalpayStampDuty.value=abc+paymentAmount;
			}
		 else
			 {
			 document.customerreceiptform.stampDutyAmount.value="";
				 $('#stampDutyAmountSpan').html('Please, enter the only digit values ..!');
			 }
		 if(reamaingstampDuty<paymentAmount)
			{
			$('#stampDutyAmountSpan').html('Please, enter correct amount ..!');
			return false;
			}
		}
	else if(document.customerreceiptform.registrationAmount.value!="")
		{
		var paymentAmount= Number($('#registrationAmount').val());
		 if(document.customerreceiptform.registrationAmount.value.match(/^[0-9]+$/))
			 {
				document.customerreceiptform.paymentType.value="";
				document.customerreceiptform.paymentType.value="Registration";
				var paymentAmount= Number($('#registrationAmount').val());
				document.customerreceiptform.paymentAmount.value="";
				document.customerreceiptform.paymentAmount.value=paymentAmount;
				
				var abc= Number($('#totalPayedRegistration1').val());
				document.customerreceiptform.totalpayRegistration.value=abc+paymentAmount;
			}
		 else
			 {
			 document.customerreceiptform.registrationAmount.value="";
				 $('#registrationAmountSpan').html('Please, enter the only digit values ..!');
			 }
		 if(reamaingRegistration<paymentAmount)
			{
			$('#registrationAmountSpan').html('Please, enter correct amount ..!');
			return false;
			}
		}
	else if(document.customerreceiptform.gstAmount.value!="")
		{
		var paymentAmount= Number($('#gstAmount').val());
		 if(document.customerreceiptform.gstAmount.value.match(/^[0-9]+$/))
			 {
				document.customerreceiptform.paymentType.value="";
				document.customerreceiptform.paymentType.value="GST Amount";
				var paymentAmount= Number($('#gstAmount').val());
				document.customerreceiptform.paymentAmount.value="";
				document.customerreceiptform.paymentAmount.value=paymentAmount;
				var abc= Number($('#totalPayedGstAmount1').val());
				document.customerreceiptform.totalpaygstAmount.value=abc+paymentAmount;
			}
		 else
			 {
			 document.customerreceiptform.gstAmount.value="";
			 $('#gstAmountSpan').html('Please, enter the only digit values ..!');
			 }
		 if(reamaingGstAmount<paymentAmount)
			{
			$('#gstAmountSpan').html('Please, enter correct amount ..!');
			return false;
			}
		}
	 
		if(( document.customerreceiptform.paymentMode[0].checked == false ) && ( document.customerreceiptform.paymentMode[1].checked == false ) && ( document.customerreceiptform.paymentMode[2].checked == false )  && ( document.customerreceiptform.paymentMode[3].checked == false )  )
		{
	    	$('#paymentModeSpan').html('Please, select payment mode..!');
			document.customerreceiptform.paymentMode[0].focus();
			return false;
		}
	  if(( document.customerreceiptform.paymentMode[1].checked == true ) && ( document.customerreceiptform.paymentMode[2].checked == true ) && ( document.customerreceiptform.paymentMode[3].checked == true ))
		  {
		  if(document.customerreceiptform.chequeNumber.value=="")
			{
				 $('#chequeNumberSpan').html('Please, enter cheque number');
				document.customerreceiptform.chequeNumber.focus();
				return false;
			}
		  
		  if(document.customerreceiptform.bankName.value=="Default")
			{
				 $('#bankNameSpan').html('Please, select Bank..!');
				document.customerreceiptform.bankName.focus();
				return false;
			}
		  }
  }
</script>
</body>
</html>