<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PEEP | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style>
body {
background-image: url("${pageContext.request.contextPath}/resources/dist/img/patient.jpg");
height: 94%;
width: 100%;
background-size: cover;
    background-position: center;
background-repeat: no-repeat;
}
.login-box, .register-box {
    margin: 0% auto;
    position: relative;
    top: 18%;
}
</style>
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<%-- 
<body background="${pageContext.request.contextPath}/resources/dist/img/patient.jpg" onload="init()">

 --%>

<div class="login-box">
  <div class="login-logo">
    
  </div>
  <!-- /.login-logo -->
  
  <div class="login-box-body">
  
<%--    <img src="${pageContext.request.contextPath}/resources/dist/img/loginlogo.png" height="139" width="320"> --%>
  </br></br>
    <form name="loginform" action="${pageContext.request.contextPath}/login" method="post">
    
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="userName" id="userName" placeholder="Username" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="passWord" id="passWord" placeholder="Password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <!-- <label>
              <input type="checkbox"> Remember Me
            </label> -->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="submit" id="submit" class="btn btn-primary ">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
      
     		<input type="hidden" id="loginStatus" name="loginStatus" value="${loginStatus}"/>
     		
    </form>
    
  </br>
  
    <a href="#">I forgot my password</a><br>

  </div>
  
  <!-- /.login-box-body -->
  
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>

<script>

function init()
{
	if(document.getElementById("loginStatus").value == "Fail")
	{
		alert("Failed to login..!");
	}
}

  $(function () 
  {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  
</script>
</body>
</html>
