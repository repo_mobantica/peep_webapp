<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
  
  <section class="sidebar">
   
      <ul class="sidebar-menu" data-widget="tree">
	<!-- 
		 <li class="treeview ">
          <a href="#">
              <li><a href="home"><i class="fa fa-home"></i> Home</a></li>
            <span class="pull-right-container">
            </span>
          </a>          
        </li> -->
        		
		<li class="treeview ">
             
               <li><a href="PatienteMaster"><i class="fa fa-files-o"></i> Patient</a></li>
               
               
                <li class="treeview">
		              <a href="#"><i class="glyphicon glyphicon-hand-right"></i> Module Details
		                <span class="pull-right-container">
		                  <i class="fa fa-angle-left pull-right"></i>
		                </span>
		              </a>
	             	 <ul class="treeview-menu">
			            <li><a href="ModuleTypeMaster"><i class="fa fa-circle-o"></i> Module </a></li>		         
			            <li><a href="ActivityMaster"><i class="fa fa-circle-o"></i> Activity </a></li> 
			             <li><a href="QuestionMaster"><i class="fa fa-circle-o"></i> Questions </a></li>
			             <li><a href="VideoMaster"><i class="fa fa-circle-o"></i> Videos </a></li>		         
		              </ul>
              </li>
           
              
                <li><a href="RedFlagQuestionMaster"><i class="fa fa-files-o"></i>Red FLag </a></li>
           
                <li><a href="AnswerMaster"><i class="fa fa-files-o"></i> User Question Answer Report</a></li> 
                <li><a href="VideoWatchReport"><i class="fa fa-files-o"></i> User Video Watch Report</a></li> 
                <li><a href="FeedBackMaster"><i class="fa fa-files-o"></i>FeedBack</a></li> 
                <li><a href="ResourceMaster"><i class="fa fa-files-o"></i>Video Resources</a></li> 
                <li><a href="ArticleMaster"><i class="fa fa-files-o"></i>Article Resources </a></li> 
                  <!--   <li><a href="VideoAnswerMaster"><i class="fa fa-files-o"></i> Watch Video Report</a></li> 
                 <li><a href="RedFlagQuestionMaster"><i class="fa fa-files-o"></i> Red Flag Question</a></li>   -->                 
                    
		</ul>
    </section>
    
  </aside>
 

