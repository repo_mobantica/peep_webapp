<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PEEP |Add Patient Information</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="styleshee
  t" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">
  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Module Type Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Module Type</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="countryform" action="${pageContext.request.contextPath}/AddPatientInformationData" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
          <%--     <div class="box-body">
          <div class="row">
            <div class="col-md-4">
                  <label for="countyId">Country Id</label>
                  <input type="text" class="form-control" id="countryId" name="countryId"  value="${countryCode}" readonly>
                  </div>
                  </div>
                </div> --%>
			
								
				
		<div class="box-body">
					
      <!--  <div class="row">
            <div class="col-md-12">
                  <label for="moduleType">Module Type </label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="moduleType" name="moduleType" placeholder="Module Type" >
                  
                  <label for="firstName">First Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" ><br>
                  
                  
                 <span id="moduleTypeSpan" style="color:#FF0000"></span>
             </div>
           </div>  -->
                
                 <div class="row">
          <h2>Personal Information</h2><br>
            <div class="col-md-6">
             	<div >
                  		 <label for="firstName">First Name</label> <label class="text-red">* </label>
                  		<input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" ><br>
                   		 <span id="fistNameSapn" style="color:#FF0000"></span>
                 </div> 
                 
               	  <div>
                  	 	<label for="lastName">Last Name</label> <label class="text-red">* </label>
              	  		<input type="text" class="form-control" id="lastName" name="lastName" placeholder="Last Name"><br>
              	   		<span id="lastNameSpan" style="color:#FF0000"></span>
              	  </div>
              	  
              	  <div>
              	   		<label for="emailId">Email ID</label> <label class="text-red">* </label>
                  	 	<input type="text" class="form-control" id="emailId" name="emailId" placeholder="Email Id" ><br>
                   		 <span id="emailIdSpan" style="color:#FF0000"></span>
                  </div>
                   
                  <div>
                 		<label for="phoneNUmber">Phone Number</label> <label class="text-red">* </label>  
                   		<input type="text" class="form-control" id="phoneNUmber" name="phoneNUmber" placeholder="Phone Numaber" > <br>
                   		<span id="phoneNUmberSpan" style="color:#FF0000"></span>
                   </div>
                   
                   
                   <div>
                		<label for="socialSecurityNumber">Social Security Number</label> <label class="text-red">* </label>  
                   		<input type="text" class="form-control" id="socialSecurityNumber" name="socialSecurityNumber" placeholder="Social Security Number" ><br>
               		  	<span id="socialSecurityNumberSpan" style="color:#FF0000"></span>
                   </div>
                   
                   <div>
		                   <label for="cancerTypeId">Type of Cancer</label> <label class="text-red">* </label>  
		                   <br>
		                  <c:forEach items="${cancerTypeList}" var="cancer">
		                  <input type="radio"  id="cancerTypeId" name="cancerTypeId" value="${cancer.id}"> ${cancer.cancerType}
							</c:forEach>
		                 <br>
		                 <span id="cancerTypeIdSpan" style="color:#FF0000"></span>
                 </div>
                 
                 <div>
	                   <br><br><label for="hospitalName">Name of Hospital where care is going on </label> <label class="text-red">* </label>
	                  	<input type="text" class="form-control" id="hospitalName" name="hospitalName" placeholder="Name of Hospital where care is going on" ><br>
	                 	<span id="hospitalNameSpan" style="color:#FF0000"></span>
                 </div>
                 
                   <div>
	                  <label for="hospitalEmailid">Hospital Emailid</label> <label class="text-red">* </label>  
	                   <input type="text" class="form-control" id="hospitalEmailid" name="hospitalEmailid" placeholder="Hospital Emailid" ><br>
	                   <span id="hospitalEmailidSpan" style="color:#FF0000"></span>
	               </div>
	               
                 <div>
	                   <label for="password">Password</label> <label class="text-red">* </label>  
	                   <input type="password" class="form-control" id="password" name="password" placeholder="Password" ><br>
	                     <span id="passwordSpan" style="color:#FF0000"></span>
	              </div>
                   
               </div>
                
                  
                  
                  
           <div class="col-md-6">
                   <div>
	                     <label for="dateOfBirth">Year Of Birth</label> <label class="text-red">* </label> 
	                    <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" placeholder="Year Of Birth">
	                    <br>
	                      <span id="dateOfBirthSpan" style="color:#FF0000"></span>
                    </div>
                    
                    <div>
		                  
	                     <label for="countryName">Country</label> <label class="text-red">* </label>   
	                     <input type="text" class="form-control" id="country" name="country" placeholder="Country" ><br>
		            	 <span id="countryNameSpan" style="color:#FF0000"></span>
                    </div>
		             
		             <div>
		             
	              		 <label for="zipCode">Zip Code</label> <label class="text-red">* </label> 
	                     <input type="text" class="form-control" id="zipCode" name="zipCode" placeholder="Zip Code" ><br>
                    	<span id="zipCodeSpan" style="color:#FF0000"></span>
                    </div>
                   
                   <div>
	                    <label for="gender">Gender</label> <label class="text-red">* </label>     
	                     <input type="text" class="form-control" id="gender" name="gender" placeholder="Gender" ><br>
	                 	<span id="genderSpan" style="color:#FF0000"></span>
                    </div>
                    <div>  
	                     <label for="language"> Prefferd Language</label> <label class="text-red">* </label>  
	                     <input type="text" class="form-control" id="language" name="language" placeholder=" Prefferd Language" ><br>
	                     <span id="languageSpan" style="color:#FF0000"></span>
                    </div>
                    
                    <div>  
	                       <label for="stageOfChemotherapy"> Stage of Chemotherapy </label> <label class="text-red">* </label> <br> 
	                     	<div class="col-md-12">
	                     	<div class="col-sm-4">
	                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Receiving" >Receiving
	                     	</div>
	                     	<div class="col-sm-4">
	 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Received" >Received 
	 						</div>
	 						</div>
	 						
	 						<div class="col-md-12">
	                     	<div class="col-sm-4">
	                     		<input type="radio" id="stageOfChemotherapy" name="stageOfChemotherapy" value="Planned" >Planned
	                     	</div>
	                     	<div class="col-sm-4">
	 							<input type="radio"  id="stageOfChemotherapy" name="stageOfChemotherapy" value="Not sure" >Not sure
	 						</div>
	 						</div>
	 						
	 						<span id="stageOfChemotherapySpan" style="color:#FF0000"></span>
                    </div>
 					
 					<div>	
		 					<br><br> <br><label for="hospitalPhysician">Name Of Physician</label> <label class="text-red">* </label>
		              	  <input type="text" class="form-control" id="hospitalPhysician" name="hospitalPhysician" placeholder="Name Of Physician"><br>
		            		<span id="hospitalPhysicianSpan" style="color:#FF0000"></span>
                    </div>
 					 <div> 	  	
		                    <label for="repassword">Repeat Password</label> <label class="text-red">* </label>  
		                   <input type="password" class="form-control" id="repassword" name="repassword" placeholder="Repeat Password" ><br>
		             		<span id="repasswordSpan" style="color:#FF0000"></span>
                    </div>      
                   
             
             </div>
                  
                  
                  
                </div>
                
                
                </div>
				<input type="hidden" id="status" name="status" value="${moduletypeStatus}">	
				<input type="hidden" id="createdDate" name="createdDate" >
				<input type="hidden" id="updateDate" name="updateDate" >
			<!-- 	<input type="hidden" id="userName" name="userName" placeholder="Admin"> -->
				
					
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		  	     <div class="box-body">
              <div class="row">
                      <div class="col-xs-4">
                	<a href="ModuleTypeMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  				<button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
			        </div> 
                 </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
           
      </div>
      <!-- /.box -->
			
			<!-- Design For View Table Of Country List -->
    
   
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
  
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
   
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script>
  function validate()
  {
	/*    if(document.countryform.firstName.value=="" )
		{
		  if( document.countryform.firstName.value.match(/^[\s]+$/) )
			  {
			  
			  $('#fistNameSapn').html('Please, enter First Name.!');
				document.countryform.firstName.value="";
				document.countryform.firstName.focus();
				return false; 	
			  }
			  $('#fistNameSapn').html('Please, enter First Name.!');
			document.countryform.firstName.value="";
			document.countryform.firstName.focus();
			return false; 	
		} */
		 if(document.countryform.firstName.value=="")
			{
			  $('#firstNameSpan').html('Please, enter First Name.!');
				document.countryform.firstName.focus();
				return false;
			}
			else if(document.countryform.firstName.value.match(/^[\s]+$/))
			{
				$('#firstNameSpan').html('Please, enter First Name..!');
				document.countryform.firstName.value="";
				document.countryform.firstName.focus();
				return false; 	
			}
	   
			 if(document.countryform.lastName.value=="")
			{
				  $('#lastNameSpan').html('Please, enter Last Name.!');
					document.countryform.lastName.focus();
					return false;
			}
			else if(document.countryform.lastName.value.match(/^[\s]+$/))
			{
				$('#lastNameSpan').html('Please, enter Last Name..!');
				document.countryform.lastName.value="";
				document.countryform.lastName.focus();
				return false; 	
			}
	  
			  if(document.countryform.countryName.value=="")
			{
			  $('#countryNameSpan').html('Please, enter Country Name.!');
				document.countryform.countryName.focus();
				return false;
			}
			else if(document.countryform.countryName.value.match(/^[\s]+$/))
			{
				$('#countryNameSpan').html('Please, enter Country Name..!');
				document.countryform.countryName.value="";
				document.countryform.countryName.focus();
				return false; 	
			}
			  
			 if(document.countryform.socialSecurityNumber.value=="")
			{
			  $('#socialSecurityNumberSpan').html('Please, enter Last Name.!');
				document.countryform.socialSecurityNumber.focus();
				return false;
			}
			  
		  if(document.countryform.hospitalName.value=="")
			{
			  $('#hospitalNameSpan').html('Please, enter Hospital Name.!');
				document.countryform.hospitalName.focus();
				return false;
			}
			 else if(document.countryform.hospitalName.value.match(/^[\s]+$/))
			{
				$('#hospitalNameSpan').html('Please, enter Hospital Name..!');
				document.countryform.hospitalName.value="";
				document.countryform.hospitalName.focus();
				return false; 	
			} 
	  
	  if(document.countryform.emailId.value=="")
		{
		  
			 
		  $('#emailIdSpan').html('Please, enter Email Id.!');
			document.countryform.emailId.focus();
			return false;
		}
	  else
		  {
		  if(!document.countryform.emailId.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		  {
				 $('#emailIdSpan').html('Please, enter  Valid Email Id.!');
				 document.countryform.emailId.value="";
				document.countryform.emailId.focus();
				return false;
			  }
		  
		  }
	  
	 /*  if(document.countryform.hospitalEmailid.value=="")
		{
		  
			 
		  $('#hospitalEmailidSpan').html('Please, enter Email Id.!');
			document.countryform.hospitalEmailid.focus();
			return false;
		}
	  else
		  {
		  if(!document.countryform.hospitalEmailid.value.match(/^(([\-\w]+)\.?)+@(([\-\w]+)\.?)+\.[a-z]{2,4}$/))
		  {
				 $('#hospitalEmailidSpan').html('Please, enter  Valid Email Id.!');
				 document.countryform.hospitalEmailid.value="";
				document.countryform.hospitalEmailid.focus();
				return false;
			  }
		  
		  } */
	  
	  if(document.countryform.phoneNUmber.value=="")
		{
			 $('#phoneNUmberSpan').html('Please, enter phone  number..!');
			document.countryform.phoneNUmber.value="";
			document.countryform.phoneNUmber.focus();
			return false;
		}
		else if(!document.countryform.phoneNUmber.value.match(/^[(]{1}[+]{1}[0-9]{2}[)]{1}[\s]{1}[0-9]{10}$/))
		{
			 $('#phoneNUmberSpan').html(' enter valid phone number..!');
			document.countryform.phoneNUmber.value="";
			document.countryform.phoneNUmber.focus();
			return false;	
		}
	  
	/*    if(document.countryform.dateOfBirth.value=="")
		{
			 $('#dateOfBirthSpan').html('Please, enter Year of Birth..!');
			document.countryform.dateOfBirth.value="";
			document.countryform.dateOfBirth.focus();
			return false;
		}
		else if(!document.countryform.dateOfBirth.value.match(^[0-9]{4}$))
		{
			 $('#dateOfBirthSpan').html(' enter valid Year of Birth..!');
			document.countryform.dateOfBirth.value="";
			document.countryform.dateOfBirth.focus();
			return false;	
		}
	
		 if(document.countryform.zipCode.value=="")
			{
				 $('#zipCodeSpan').html('Please, enter ZipCode..!');
				document.countryform.zipCode.value="";
				document.countryform.zipCode.focus();
				return false;
			}
			else if(!document.countryform.dateOfBirth.value.match(^[0-9])
			{
				 $('#zipCodeSpan').html(' enter valid ZipCode..!');
				document.countryform.zipCode.value="";
				document.countryform.zipCode.focus();
				return false;	
			} 
	 */
  }
function init()
{	
	
	var n =  new Date();
	var y = n.getFullYear();
	var m = n.getMonth() + 1;
	var d = n.getDate();
	document.getElementById("createdDate").value = d + "/" + m + "/" + y;
	document.getElementById("updateDate").value = d + "/" + m + "/" + y;
	/* document.getElementById("userName").value ="Admin"; */
	
	
	 if(document.countryform.status.value=="Fail")
	 {
	  //	alert("Sorry, record is present already..!");
	 }
	 else if(document.countryform.status.value=="Success")
	 {
		 
		 $('#statusSpan').html('Record added successfully..!');
	    
	 }
  	 document.countryform.countryName.focus();
}

</script>
</body>
</html>
