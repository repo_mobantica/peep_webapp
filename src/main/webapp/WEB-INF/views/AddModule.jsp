<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PEEP|Add Module</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="styleshee
  t" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
     <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()" >

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>
<div class="wrapper">
  <%@ include file="headerpage.jsp" %>
  <!-- Left side column. contains the logo and sidebar -->
   <%@ include file="menu.jsp" %>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Module Type Details:

      </h1>
     
    </section>

    <!-- Main content -->
	
<form name="moduleform" action="${pageContext.request.contextPath}/SaveModuleData"  onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12s">
                <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
        
					<div class="box-body">
          				<div class="row">
                 			 <div class="col-md-6">
	                				<div class="col-xs-12"><br>
	                 					 <label for="moduleType">Module Type </label> <label class="text-red">* </label>
	                 					 	<input type="text" class="form-control" id="moduleType" name="moduleType" placeholder="Module Type" >
	                 					
	                 					<span id="moduleTypeSpan" style="color:#FF0000"></span>
	            					  </div>
	              			</div>
	            			
                		</div>
                		
                		
                		
                		
                		
                		
                </div>
				<input type="hidden" id="status" name="status" value="1">	
				<input type="hidden" id="createdDate" name="createdDate" >
				<input type="hidden" id="updateDate" name="updateDate" >
			<!-- 	<input type="hidden" id="userName" name="userName" placeholder="Admin"> -->
				
					
				
              <!-- /.form-group -->
            </div>
            
            <!-- /.col -->
			
          </div>
		  	     <div class="box-body">
              <div class="row"><br><br>
                <div class="col-xs-2"></div><div class="col-xs-1"></div>
                      <div class="col-xs-2">
                	<a href="ModuleTypeMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-2">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-1">
			  				<button type="submit" id="submit" class="btn btn-info " name="submit">Submit</button>
			        </div> 
                 </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
           
      </div>
      <!-- /.box -->
			
			<!-- Design For View Table Of Country List -->
    
   
     
    </section>
	</form>
    <!-- /.content -->
    
  </div>
  
  <!-- Control Sidebar -->
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="https://code.jquery.com/jquery-3.0.0.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
   
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
 <script>
  function validate()
  {
	  
	  clearAll();
	  if(document.moduleform.moduleType.value=="")
		{
		  $('#moduleTypeSpan').html('Please, Enter Module Type..!');
			document.moduleform.moduleType.focus();
			return false;
		}
		
	  
	 
		
  }
function init()
{	
	
	var n =  new Date();
	var y = n.getFullYear();
	var m = n.getMonth() + 1;
	var d = n.getDate();
	document.getElementById("createdDate").value = d + "/" + m + "/" + y;
	document.getElementById("updateDate").value = d + "/" + m + "/" + y;
	/* document.getElementById("userName").value ="Admin"; */
	
	
	 if(document.moduleform.status.value=="Fail")
	 {
	  //	alert("Sorry, record is present already..!");
	 }
	 else if(document.moduleform.status.value=="Success")
	 {
		 
		 $('#statusSpan').html('Record added successfully..!');
	    
	 }
  	
}
function clearAll()
{
	
	$('#moduleTypeSpan').html(' ');
	  
}

</script> 
<script>
$(document).ready(function(){

	 $("#questionTable").on('click','.btn',function(){
	       $(this).closest('tr').remove();
	     });

	});
	
$(document).ready(function(){

	 $("#videoTable").on('click','.btn',function(){
	       $(this).closest('tr').remove();
	     });

	});

</script>




<script>
(function() {
  document.getElementById("submit").onclick = function() {
    var table = document.getElementById("questionTable");
    var table1 = document.getElementById("videoTable");
    var options="";
    var videoLink="";
    
    for (var i = 1; i < table.rows.length; i++) {
    	
      options=options+table.rows[i].cells[0].innerHTML+"###!###";
      options=options.replace(/&nbsp;/g, '');
     
    }
    for (var i = 1; i < table1.rows.length; i++) {
    	
    	videoLink=videoLink+table1.rows[i].cells[0].innerHTML+"###!###";
    	videoLink=videoLink.replace(/&nbsp;/g, '');
     
    }
  //  alert(options);
    document.getElementById("videoLink1").value=videoLink;
    document.getElementById("optionName1").value=options;
    document.getElementById("videoLink").value=videoLink;
    document.getElementById("questionName").value=options;
   
   
  };
})();
</script>
<script>

function addMoreQuestion() { 
	var txtval = document.getElementById("questionName").value;
	if(txtval=="")
	{
			 $('#questionNameSpan').html('Question Name Cannot be Blank');
	}else
	{
	    var table = document.getElementById("questionTable");
	    var row = table.insertRow(1);
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	    var cell3 = row.insertCell(2);
	    var cell4 = row.insertCell(3);
	    cell1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ txtval;
	    cell2.innerHTML = '<a class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></a> &nbsp;<a onclick="questionEditFunction()" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a> &nbsp;<a onclick="addOptions()" class="btn btn-warning btn-sm" data-toggle="tooltip" title="AddOption"><i class="glyphicon glyphicon-plus"></i></a>';
	    cell3.innerHTML = txtval;
	    cell4.innerHTML =  txtval;
	    cell3.style="display:none";
	    cell4.style="display:none";
	    document.getElementsByTagName("td")[0].setAttribute("style", "word-wrap: break-word;"); 
	    document.getElementById("questionName").value="";
	}
  
}
function addMoreVideo() { 
	var txtval = document.getElementById("videoLink").value;
	if(txtval=="")
	{
			 $('#videoLinkSpan').html('VideoLink Cannot be Blank');
	}else
	{
	    var table = document.getElementById("videoTable");
	    var row = table.insertRow(1);
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	    cell1.innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ txtval;
	   cell2.innerHTML = '<a class="btn btn-danger btn-sm" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-trash"></i></a> &nbsp;<a onclick="videoEditFunction()" class="btn btn-info btn-sm" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-edit"></i></a>';
		  
	    document.getElementsByTagName("td")[0].setAttribute("style", "word-wrap: break-word;"); 
	    document.getElementById("videoLink").value="";
	}
}





function questionEditFunction() {
	
	var table = document.getElementById('questionTable');
    
    for(var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
            rIndex = this.rowIndex;
            document.getElementById("questionTable").deleteRow(rIndex);
        	 var questionName=this.cells[0].innerHTML;
        	 questionName=questionName.replace(/&nbsp;/g, '');
             document.getElementById("questionName").value = questionName;
            
        };
    }
   
}

function videoEditFunction() {
	
	var table = document.getElementById('videoTable');
    
    for(var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
            rIndex = this.rowIndex;
            document.getElementById("videoTable").deleteRow(rIndex);
        	 var videoLink=this.cells[0].innerHTML;
        	 videoLink=videoLink.replace(/&nbsp;/g, '');
             document.getElementById("videoLink").value = videoLink;
            
        };
    }
   
}
 function addOptions() {
	   var videoLink="";
	   var questionNames="";
	   var questionoptions="";
	   var questioncorrectoptions="";
	   var table = document.getElementById("questionTable");
	   
	   var table1 = document.getElementById("videoTable");
	   for (var i = 1; i < table1.rows.length; i++) {
	    	
	    	videoLink=videoLink+table1.rows[i].cells[0].innerHTML+"###!###";
	    	videoLink=videoLink.replace(/&nbsp;/g, '');
	     
	    }
	  
	  for (var i = 1; i < table.rows.length; i++) {	    	
		  questionNames=questionNames+table.rows[i].cells[0].innerHTML+"###!###";
		  questionoptions=table.rows[i].cells[0].innerHTML+"***#***"+questionoptions+table.rows[i].cells[2].innerHTML+"***#***"+table.rows[i].cells[3].innerHTML+"$$$$";
	      questionNames=questionNames.replace(/&nbsp;/g, '');	     
	    }
	  
	var moduleType = document.getElementById('moduleType').value;
	var activityName = document.getElementById('activityName').value;
	var videoLink1 =videoLink;
	var multiQuestionNames =questionNames;
	//alert(questionNames1);
	var table = document.getElementById('questionTable');
    for(var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function()
        {
           	 rIndex = this.rowIndex;
        	 var questionName=this.cells[0].innerHTML;
        	 questionName=questionName.replace(/&nbsp;/g, '');
             document.getElementById("questionName").value = questionName;
            /*   sessionStorage.setItem("questionName11", "gusruoifegafuierg");
             sessionStorage.setItem("moduleType11", moduleType);
             sessionStorage.setItem("activityName11", "hdfghudhgui");
             sessionStorage.setItem("videoLink11", videoLink); */
 	       sendData(questionName,moduleType,activityName,videoLink1,multiQuestionNames,questionoptions);
              	       
        };
    }
   
}   

</script>
<script>

function sendData(questionName,moduleType,activityName,videoLink1,multiQuestionNames,questionoptions)
{
	//alert("hii");
    var moduleTypeId = moduleTypeId;    
    $.ajax({
		 url : '${pageContext.request.contextPath}/AddOptions',
		type : 'Post',
		data : {questionName: questionName,
			moduleType:moduleType,
			activityName:activityName,
			videoLink:videoLink1,
			multiQuestionNames:multiQuestionNames,
			questionoptions:questionoptions
			},
		dataType : 'json',
		success : function(result)
				  {
					   if (result) 
					   { 
						
						   window.location = "${pageContext.request.contextPath}/AddCorrectOption";
						} 
						else
						{
							alert("failure111");
						}					
				  } 
		});
}

</script>
</body>
</html>
