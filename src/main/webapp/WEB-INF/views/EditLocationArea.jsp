<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Edit Location Area</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
    <style type="text/css">
   tr.odd {background-color: #CCE5FF}
tr.even {background-color: #F0F8FF}
    </style>
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">

	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
    		
		if (session != null) 
		{
			if (session.getAttribute("user") == null) 
			{
				response.sendRedirect("login");
			} 
		}
	%>


<div class="wrapper">

	<%@ include file="headerpage.jsp" %>
  
  	<%@ include file="menu.jsp" %>
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Location Area Details:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Edit Area</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="locationareaform" action="${pageContext.request.contextPath}/EditLocationArea" onSubmit="return validate()" method="post">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <span id="statusSpan" style="color:#FF0000"></span>
              <!-- /.form-group -->
              <div class="box-body">
          <div class="row">
            <div class="col-md-2">
                  <label >Area Id</label>
                  <input type="text" class="form-control" id="locationareaId" name="locationareaId" value="${locationDetails[0].locationareaId}" readonly>
                </div>
			 </div>
            </div>
								
		 <div class="box-body">
          <div class="row">
            <div class="col-md-4">
                  <label>Country</label> <label class="text-red">* </label>
                  <select class="form-control" name="countryName" id="countryName" onchange="getStateList(this.value)">
                  
				 	 	<option selected="" value="${locationDetails[0].countryName}">${locationDetails[0].countryName}</option>
				 	 	
                     <s:forEach var="CountryList" items="${CountryList}">
                       <s:choose>
                       	 <s:when test="${locationDetails[0].countryName ne CountryList.countryName}">
                    	  	<option value="${CountryList.countryName}">${CountryList.countryName}</option>
                    	 </s:when>
                       </s:choose>
                     </s:forEach> 
                    
                  </select>
                   <span id="countryNameSpan" style="color:#FF0000"></span>
                </div>
            
            <div class="col-md-4">
                  <label>State</label> <label class="text-red">*</label>
                  <select class="form-control" name="stateName" id="stateName" onchange="getCityList(this.value)">
                  
				  	        <option selected="" value="${locationDetails[0].stateName}">${locationDetails[0].stateName}</option>
				  	        
                    <s:forEach var="stateList" items="${stateList}">
                      <s:choose>
                        <s:when test="${locationDetails[0].stateName ne stateList.stateName}">
                            <option value="${stateList.stateName}">${stateList.stateName}</option>
                         </s:when>
                      </s:choose>
				    </s:forEach>
				    
				  </select>
				   <span id="stateNameSpan" style="color:#FF0000"></span>
                </div>
                <div class="col-md-4">
                  <label>City</label> <label class="text-red">* </label>
                  
                  <select class="form-control" name="cityName" id="cityName" onchange="getuniqueCityList(this.value)">
                  
				  		<option selected="" value="${locationDetails[0].cityName}">${locationDetails[0].cityName}</option>
				  		
                    <s:forEach var="cityList" items="${cityList}">
                      <s:choose>
                         <s:when test="${locationDetails[0].cityName ne cityList.cityName}">
                    		<option value="${cityList.cityName}">${cityList.cityName}</option>
                    	 </s:when>
                      </s:choose>
				    </s:forEach>
				    
                  </select>
                   <span id="cityNameSpan" style="color:#FF0000"></span>
                   </div>
                 </div>
            </div>
            <div class="box-body">
          <div class="row">
            <div class="col-md-4">
                  <label for="locationareaName">Location Area Name</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="locationareaName" placeholder="Enter Area Name" name="locationareaName" value="${locationDetails[0].locationareaName}">
               <span id="locationareaNameSpan" style="color:#FF0000"></span>
                </div>
                    <div class="col-md-4">
                  <label for="pinCode">Pin Code</label> <label class="text-red">* </label>
                  <input type="text" class="form-control" id="pinCode" placeholder="Pin code" name="pinCode" value="${locationDetails[0].pinCode}">
               <span id="pinCodeSpan" style="color:#FF0000"></span>
                </div>
				<input type="hidden" id="locationAreaStatus" name="locationAreaStatus" value="${locationAreaStatus}">	
				<input type="hidden" id="creationDate" name="creationDate" value="${locationDetails[0].creationDate}">
				<input type="hidden" id="updateDate" name="updateDate" value="">
				<input type="hidden" id="userName" name="userName" value="<%= session.getAttribute("user") %>">
              <!-- /.form-group -->
            </div>
            </div>
             
            
            
          <div class="box-body">
              <div class="row">
              <br/>
                     <div class="col-xs-4">
                	<a href="LocationAreaMaster"><button type="button" class="btn btn-block btn-primary" value="Back" style="width:90px">Back</button></a>
			     </div>
				  <div class="col-xs-3">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Update</button>
              
			     </div> 
              </div>
			  </div>
			  </div>
          </div>
		   	  	     
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div>
    
      </section>
	</form>
    <!-- /.content -->
    
  </div>
   <%@ include file="footer.jsp" %>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="${pageContext.request.contextPath}/resources/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/moment/min/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="${pageContext.request.contextPath}/resources/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="${pageContext.request.contextPath}/resources/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="${pageContext.request.contextPath}/resources/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="${pageContext.request.contextPath}/resources/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/resources/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="${pageContext.request.contextPath}/resources/dist/js/demo.js"></script>
<!-- Page script -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(1000).fadeOut();
      });
</script>

<script>
function clearall()
{
	$('#locationareaNameSpan').html('');
	$('#cityNameSpan').html('');
	$('#stateNameSpan').html('');
	$('#countryNameSpan').html('');
	$('#pinCodeSpan').html('');
}

function validate()
{ 
		clearall();
		
		if(document.locationareaform.countryName.value=="Default")
		{
			$('#countryNameSpan').html('Please, select country name..!');
			document.locationareaform.countryName.focus();
			return false;
		}
		
		if(document.locationareaform.stateName.value=="Default")
		{
			$('#stateNameSpan').html('Please, select state name..!');
			document.locationareaform.stateName.focus();
			return false;
		}
		
		if(document.locationareaform.cityName.value=="Default")
		{
			$('#cityNameSpan').html('Please, select city name..!');
			document.locationareaform.cityName.focus();
			return false;
		}
		
		//validation for the location area name
		if(document.locationareaform.locationareaName.value=="")
		{
			document.locationareaform.locationareaName.value="";
			document.locationareaform.locationareaName.focus();
			return false;
		}
		else if(document.locationareaform.locationareaName.value.match(/^[\s]+$/))
		{
			$('#locationareaNameSpan').html('Please, enter location area name..!');
			document.locationareaform.locationareaName.value="";
			document.locationareaform.locationareaName.focus();
			return false; 	
		}

		
		 //validation for pincode
		if(document.locationareaform.pinCode.value=="")
		{
			$('#pinCodeSpan').html('Please, enter valid 6 digit pincode');
			document.locationareaform.pinCode.focus();
			return false;
		}
		
}
function init()
{
	clearall();
	 var n = new Date();
	 var y = n.getFullYear();
	 var m = n.getMonth()+1;
	 var d = n.getDate();
	 
	 document.getElementById("updateDate").value = d + "/"+ m + "/" + y;
	 
	 if(document.locationareaform.locationAreaStatus.value == "Fail")
	 {
	  	alert("Sorry, record is present already..!");
	 }
	 else if(document.locationareaform.locationAreaStatus.value == "Success")
	 {
		 $('#locationareaNameSpan').html('Record saved successfully..!');
	 }
	 
  	document.locationareaform.countryName.focus();
}


function getStateList()
{
	 $("#stateName").empty();
	 $("#cityName").empty();
	 
	 var countryName = $('#countryName').val();
	
	$.ajax({

		url : '${pageContext.request.contextPath}/getStateList',
		type : 'Post',
		data : { countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select State-");
							$("#stateName").append(option);
							
							var option1 = $('<option/>');
							option1.attr('value',"Default").text("-Select City-");
							$("#cityName").append(option1);
							
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].stateName).text(result[i].stateName);
							    $("#stateName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
						}

					}
		});
	
}//end of get State List


function getCityList()
{
	 $("#cityName").empty();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
	$.ajax({

		url : '${pageContext.request.contextPath}/getCityList',
		type : 'Post',
		data : { stateName : stateName, countryName : countryName},
		dataType : 'json',
		success : function(result)
				  {
						if (result) 
						{
							var option = $('<option/>');
							option.attr('value',"Default").text("-Select City-");
							$("#cityName").append(option);
							for(var i=0;i<result.length;i++)
							{
								var option = $('<option />');
							    option.attr('value',result[i].cityName).text(result[i].cityName);
							    $("#cityName").append(option);
							 } 
						} 
						else
						{
							alert("failure111");
							//$("#ajax_div").hide();
						}

					}
		});
}//end of get City List


function getuniqueCityList()
{
	var locationareaName = $('#locationareaName').val();
	 var cityName = $('#cityName').val();
	 var stateName = $('#stateName').val();
	 var countryName = $('#countryName').val();
		$.ajax({

			url : '${pageContext.request.contextPath}/getallAreaList',
			type : 'Post',
			data : { locationareaName : locationareaName},
			dataType : 'json',
			success : function(result)
					  {
							if (result) 
							{
								 
								for(var i=0;i<result.length;i++)
								{
									
									if(result[i].countryName==countryName)
										{
										if(result[i].stateName==stateName)
											{
												if(result[i].cityName==cityName)
												{
													if(result[i].locationareaName==locationareaName)
													{
													  document.locationareaform.locationareaName.focus();
													  $('#locationareaNameSpan').html('This location area is already exist..!');
													  $('#locationareaName').val("");
													}
												}
											}
										}
									
									
								 } 
							} 
							else
							{
								alert("failure111");
							}

						}
			});		
}


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>