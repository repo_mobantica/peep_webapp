package com.peep.configuration;

import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.peep")
public class ApplicationConfiguration  extends WebMvcConfigurerAdapter
{
	
	@Bean(name="multipartResolver") 
        public CommonsMultipartResolver getResolver() throws IOException
	{
                CommonsMultipartResolver resolver = new CommonsMultipartResolver();
                 
                //Set the maximum allowed size (in bytes) for each individual file.
                resolver.setMaxUploadSizePerFile(5242880);//5MB
                
                //You may also set other available properties.
                return resolver;
        }
	
    @Bean(name = "PEEP")
    public ViewResolver viewResolver()
    {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        return viewResolver;
    }
    
    public void addResourceHandlers(ResourceHandlerRegistry registry) 
    {
        registry.addResourceHandler("/webapp/resources/**").addResourceLocations("/webapp/resources/");
    }
    
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer)
    {
        configurer.enable();
    }
}