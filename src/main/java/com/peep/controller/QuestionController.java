package com.peep.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;
import com.peep.bean.OptionDetails;
import com.peep.bean.QuestionDetails;
import com.peep.bean.VideoDetails;
import com.peep.repository.ActivityRepository;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.OptionRepository;
import com.peep.repository.QuestionRepository;
import com.peep.repository.VideoRepository;

@Controller
@RequestMapping("/")
public class QuestionController
{
	@Autowired
	ActivityRepository activityRepository;
	
	@Autowired
	ModuleTypeRepository moduleTypeRepository;
	
	
	@Autowired
	VideoRepository videoRepository;
	
	@Autowired
	QuestionRepository questionRepository;
	
	@Autowired
	OptionRepository optionRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String countryCode;
	    
	    
/* @ResponseBody
	    @RequestMapping("/searchCountryNameWiseCountryList")
	    public List<Country> SearchCountryNameWise(@RequestParam("countryName") String countryName)
	    {
	    	Query query = new Query();
	    	
	    	List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryName").regex("^"+countryName+".*","i")), Country.class);
	    	return countryList;
	    }*/
	    
	    @RequestMapping("/QuestionMaster")
	    public String VideoMaster(ModelMap model)
	    {
 	    	 
	    	 Query query=new Query();
		    	List<QuestionDetails> questionList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), QuestionDetails.class);
		    	query=new Query();
		    	List<ActivityDetails> activityList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), ActivityDetails.class);
			    
	    	 for(int k=0;k<questionList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{
		    		
		    		
		    		//	System.out.println(activityList.get(i).getActivityId()+" "+questionList.get(k).getActivityId());
		    			if(activityList.get(i).getActivityId().equals(questionList.get(k).getActivityId()))
		    			{
		    				questionList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
		    		
		    		
			    	 }
		    	}
		    	
 	    	 model.addAttribute("questionList", questionList);
 	    	 
	    	return "QuestionMaster";
	    }
	    
	    //Request Mapping For Module
	    @RequestMapping("/AddQuestion")
	    public String AddQuestion(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 
	    	 List<ActivityDetails> activityList = activityRepository.findAll();
	    	 
	    	 model.addAttribute("activityList", activityList);
	         return "AddQuestion";
	    }
	    
	        
	    @RequestMapping(value = "/AddQuestionData", method = RequestMethod.POST)
	    public String countrySave(@ModelAttribute QuestionDetails questionDetails, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
	    	//	System.out.println("hioiiiiiiiiiiiiiiiiii");
	    	//	moduleType.setStatus("1");
	    	//	moduleType.setCreatedDate(new Date());
	    		questionDetails = new QuestionDetails(questionDetails.getQuestionId(),"1", questionDetails.getCreatedDate()
	    				,questionDetails.getActivityId(),questionDetails.getCorrectOptionId(),questionDetails.getQuestionName());
	    		questionRepository.save(questionDetails);		        
		        model.addAttribute("questionStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("questionStatus", "Fail");
	    	}	    	
	    	 List<QuestionDetails> questionList= questionRepository.findAll();
	    	 
	    	 model.addAttribute("questionList", questionList);
	    	
	        return " redirect:QuestionMaster";
	    }
	    
	    @RequestMapping(value="/EditQuestion",method=RequestMethod.GET)
	    public String EditQuestion(@RequestParam("questionId") String questionId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<QuestionDetails> questionDetails = mongoTemplate.find(query.addCriteria(Criteria.where("questionId").is(questionId)), QuestionDetails.class);
	    	model.addAttribute("questionDetails",questionDetails);
	    	query = new Query();
	    	List<OptionDetails >optionDetailsList = mongoTemplate.find((query.addCriteria(Criteria.where("questionId").is(questionId).and("status").is("1"))), OptionDetails.class);
	    	model.addAttribute("optionDetailsList",optionDetailsList);
	    	query = new Query();
	    	String deleteOptionIds="";
	    	List<OptionDetails> optionDetails = mongoTemplate.find((query.addCriteria(Criteria.where("questionId").is(questionId))), OptionDetails.class);
	    	for(int i=0;i<optionDetails.size();i++ )
	    	{
		    	/*optionDetails.get(i).setStatus("2");
	    		optionRepository.save(optionDetails);*/
	    		deleteOptionIds=deleteOptionIds+optionDetails.get(i).getOptionId()+"##";
	    	}
	      	/*query = new Query();
	    	QuestionDetails questionDetailsList = mongoTemplate.findOne(query.addCriteria(Criteria.where("questionId").is(questionId)), QuestionDetails.class);
	      	query = new Query();
	    	List<ActivityDetails> activityDetails = mongoTemplate.find((query.addCriteria(Criteria.where("status").is("1").and("activityId").is(questionDetailsList.getActivityId()))), ActivityDetails.class);

	    	model.addAttribute("activityDetails",activityDetails);*/
	    	query = new Query();
	    	List<ActivityDetails> activityDetailsList = mongoTemplate.find((query.addCriteria(Criteria.where("status").is("1"))), ActivityDetails.class);
	    	model.addAttribute("deleteOptionIds",deleteOptionIds);
	    	model.addAttribute("activityDetailsList",activityDetailsList);
	    	model.addAttribute("deleteOptionIds",deleteOptionIds);
	    	return "EditQuestion";
	    }
	    
	    @RequestMapping(value="/SaveEditQuestionData",method=RequestMethod.GET)
	    public String EditCountryPostMethod( ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
	    		String alloptions=req.getParameter("optionNames");
	    		String activityId=req.getParameter("activityId");
	    		String questionName=req.getParameter("questionName");
	    		String correctOption=req.getParameter("correctOption");
	    		String questionId=req.getParameter("questionId");
	    		String deleteOptions=req.getParameter("deleteOptions");
	    		QuestionDetails question =questionRepository.findByStatusAndQuestionId("1",questionId);
	    		question.setActivityId(activityId);
	    		question.setCorrectOptionId(correctOption);
	    		question.setStatus("1");
	    		question.setQuestionName(questionName);
	    		question.setQuestionId(questionId);
	    		question.setUpdatedDate(new Date());
        		questionRepository.save(question);
	    		
	    		if (alloptions.endsWith("###!###")) {
	    			alloptions = alloptions.substring(0, alloptions.length() - 7);
	    			}
	    		/*if (deleteOptions.endsWith("##")) {
	    			deleteOptions = deleteOptions.substring(0, alloptions.length() - 2);
	    			}*/
	    		String[] optionArray = alloptions.split("###!###");
	    		String[] deleteOptionArray = alloptions.split("##");
	    		Query query= new Query();
	    		List<OptionDetails> optionDetails = mongoTemplate.find((query.addCriteria(Criteria.where("questionId").is(questionId))), OptionDetails.class);
		    	for(int i=0;i<optionDetails.size();i++ )
		    	{
			    	optionDetails.get(i).setStatus("2");
		    		optionRepository.save(optionDetails);
		    		
		    	}
	    		for(int i=0;i<optionArray.length;i++)
	    		{
	    			///System.out.println(array[i]);
	    			OptionDetails optionDetails1 = new OptionDetails(optionArray[i],"1", new Date()
	        				,questionId);
	        		optionRepository.save(optionDetails1);		        
	        		
	    			
	    		}
		        model.addAttribute("questionStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("questionStatus", "Fail");
	    	}
	    	
	    	 List<QuestionDetails> questionList = questionRepository.findAll();

 	    	 model.addAttribute("questionList", questionList);
 	    	 
	    	return "redirect:QuestionMaster";
	    }
	    
	    
	    @RequestMapping("/CorrectOptionMaster")
	    public String CorrectOptionMaster(ModelMap model)
	    {
	    	 List<ActivityDetails> activityList = activityRepository.findAll();
 	    	 
	    	 List<QuestionDetails> questionList = questionRepository.findAll();
	      	 List<OptionDetails> optionList = optionRepository.findAll();
	    	 for(int k=0;k<questionList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{
		    		
		    		
		    			//System.out.println(activityList.get(i).getActivityId()+" "+questionList.get(k).getActivityId());
		    			if(activityList.get(i).getActivityId().equals(questionList.get(k).getActivityId()))
		    			{
		    				questionList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
		    		
		    		
			    }
		    	for(int i=0;i<optionList.size();i++)
		    	{
		    		
		    		
		    		//	System.out.println(optionList.get(i).getOptionId()+" "+questionList.get(k).getCorrectOptionId());
		    			if(optionList.get(i).getOptionId().equals(questionList.get(k).getCorrectOptionId()))
		    			{
		    				questionList.get(k).setOptionName(optionList.get(i).getOptionName());
		    			}
		    		
		    		
			    }
		    }
		    	
 	    	 model.addAttribute("questionList", questionList);
 	    	 
	    	return "CorrectOptionMaster";
	    }
	    
	    
	    
	  //Request Mapping For Module
	    /*@RequestMapping("/AddCorrectOption")
	    public String AddCorrectOption(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 
	    	 List<ActivityDetails> activityList = activityRepository.findAll();
	    	 List<QuestionDetails> questionList = questionRepository.findAll();
	    	 List<OptionDetails> optionList = optionRepository.findAll();
	    	 
	    	// System.out.println(activityList.size());
	    	 model.addAttribute("activityList", activityList);
	    	 model.addAttribute("questionList", questionList);
	    	 model.addAttribute("optionList", optionList);
	    	 
	         return "AddCorrectOption";
	    }
	    */
	 
	    
	    
	    @RequestMapping(value="/EditCorrectOption",method=RequestMethod.GET)
	    public String EditCorrectOption(@RequestParam("questionId") String questionId,ModelMap model)
	    {
	    	Query query = new Query();
	    	Query query1 = new Query();
	    	List<QuestionDetails> questionDetails = mongoTemplate.find(query.addCriteria(Criteria.where("questionId").is(questionId)), QuestionDetails.class);
	    	List<ActivityDetails> activityDetails = activityRepository.findAll();
	    	List<OptionDetails> optionList =mongoTemplate.find(query1.addCriteria(Criteria.where("questionId").is(questionId)), OptionDetails.class);
	    	for(int i=0;i<activityDetails.size();i++)
	    	{
	    		
	    		
	    		//	System.out.println(activityList.get(i).getActivityId()+" "+questionList.get(k).getActivityId());
	    			if(activityDetails.get(i).getActivityId().equals(questionDetails.get(0).getActivityId()))
	    			{
	    				questionDetails.get(0).setActivityName(activityDetails.get(i).getActivityName());
	    			}
	    		
	    		
		    }
	    	
	    	
	    	
	    	 
	    	model.addAttribute("activityDetails",activityDetails);
	    	model.addAttribute("questionDetails",questionDetails);
	    	 model.addAttribute("optionList", optionList);
	    	
	    	return "EditCorrectOption";
	    }
	    
	    @RequestMapping(value="/EditCorrectOptionData",method=RequestMethod.POST)
	    public String EditCorrectOptionData(@ModelAttribute QuestionDetails questionDetails, ModelMap model)
	    {
	    	try
	    	{
	    		
	    		//System.out.println(questionDetails.getQuestionId()+" "+questionDetails.getActivityId()+" " +questionDetails.getCorrectOptionId()+" " +questionDetails.getQuestionName());
		    	
	    		questionDetails = new QuestionDetails(questionDetails.getQuestionId(), "1", new Date(),questionDetails.getActivityId()
	    				,questionDetails.getCorrectOptionId(),questionDetails.getQuestionName());
	    		questionRepository.save(questionDetails);	
		        
		        model.addAttribute("questionStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("questionStatus", "Fail");
	    	}
	    	
	    	/* List<QuestionDetails> questionList = questionRepository.findAll();

 	    	 model.addAttribute("questionList", questionList);*/
 	    	 
	        return "redirect:CorrectOptionMaster";
	    }
	    
	    
	    
	    @ResponseBody
	    @RequestMapping(value="/DeleteQuestion")
	    public List<ModuleType> DeleteQuestion(@RequestParam("questionId") String questionId,ModelMap model)
	    {
	    	
	    	/*ModuleType moduleType= new ModuleType();
	    	moduleTypeRepository.findAll();
	    	moduleType= moduleTypeRepository.findById(moduleTypeId);
	    	*/
	    	QuestionDetails questionDetails = new QuestionDetails();
	    	
    		Query query = new Query();
    		/*moduleType = mongoTemplate.findOne(Query.query(Criteria.where("id").is(moduleTypeId)), ModuleType.class);
    		moduleType.setStatus("2");
    		moduleTypeRepository.save(moduleType);*/
    	    query = new Query();

    	    questionDetails = mongoTemplate.findOne(Query.query(Criteria.where("questionId").is(questionId)), QuestionDetails.class);
    	    questionDetails.setStatus("2");
     		questionRepository.save(questionDetails);
     		
     		query = new Query();
     		List<OptionDetails> optionDetails = mongoTemplate.find((query.addCriteria(Criteria.where("questionId").is(questionId))), OptionDetails.class);
	    	for(int i=0;i<optionDetails.size();i++ )
	    	{
		    	optionDetails.get(i).setStatus("2");
	    		optionRepository.save(optionDetails);
	    		
	    	}
     		List<ModuleType> moduleTypeList = moduleTypeRepository.findByStatus("1");
	    	List<ActivityDetails> activityList = activityRepository.findByStatus("1");
	    	
	    	for(int k=0;k<moduleTypeList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{	
		    			if(activityList.get(i).getModuleId().equals(moduleTypeList.get(k).getId()))
		    			{
		    				moduleTypeList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
			    }
		    
		    }
 	    	 
	    	return moduleTypeList;
	    	
	    }
	    
	    
	    
	    
	    
}
