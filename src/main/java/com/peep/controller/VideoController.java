package com.peep.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;
import com.peep.bean.PatientInformation;
import com.peep.bean.VideoDetails;
import com.peep.bean.VideoDetailsUserwise;
import com.peep.repository.ActivityRepository;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.VideoRepository;

@Controller
@RequestMapping("/")
public class VideoController
{
	@Autowired
	ActivityRepository activityRepository;
	
	@Autowired
	ModuleTypeRepository moduleTypeRepository;
	
	
	@Autowired
	VideoRepository videoRepository;
	
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String countryCode;
	    
	    
/* @ResponseBody
	    @RequestMapping("/searchCountryNameWiseCountryList")
	    public List<Country> SearchCountryNameWise(@RequestParam("countryName") String countryName)
	    {
	    	Query query = new Query();
	    	
	    	List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryName").regex("^"+countryName+".*","i")), Country.class);
	    	return countryList;
	    }*/
	    
	    @RequestMapping("/VideoMaster")
	    public String VideoMaster(ModelMap model)
	    {
	    	Query query=new Query();
	    	List<VideoDetails> videoList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), VideoDetails.class);
	    	query=new Query();
	    	List<ActivityDetails> activityList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), ActivityDetails.class);
	    	 	    	 
	    	 
	    	 for(int k=0;k<videoList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{
		    		
		    			//System.out.println(activityList.get(i).getActivityId()+" "+videoList.get(k).getActivityId());
		    			if(activityList.get(i).getActivityId().equals(videoList.get(k).getActivityId()))
		    			{
		    				videoList.get(k).setActivityName(activityList.get(i).getActivityName());
		    				break;
		    			}
		    		
		    		
			    	 }
		    	}
		    	
 	    	 model.addAttribute("videoList", videoList);
 	    	 
	    	return "VideoMaster";
	    }
	    
	    //Request Mapping For Module
	    @RequestMapping("/AddVideo")
	    public String addCountry(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 
	    	
	    	 Query query=new Query();
		    	List<ActivityDetails> activityList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), ActivityDetails.class);
	    	// List<ActivityDetails> activityList = activityRepository.findAll();
	    	/// System.out.println(activityList.size());
	    	 model.addAttribute("activityList", activityList);
	         return "AddVideo";
	    }
	    
	        
	    @RequestMapping(value = "/AddVideoData", method = RequestMethod.POST)
	    public String countrySave(@ModelAttribute VideoDetails videoDetails, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
	    	//	System.out.println("hioiiiiiiiiiiiiiiiiii");
	    	//	moduleType.setStatus("1");
	    	//	moduleType.setCreatedDate(new Date());
	    		videoDetails = new VideoDetails(videoDetails.getVideoLink(), "1", new Date(),videoDetails.getActivityId());
	    		videoRepository.save(videoDetails);		        
		        model.addAttribute("videoStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("videoStatus", "Fail");
	    	}	    	
	    	 List<VideoDetails> videoList= videoRepository.findAll();
	    	 
	    	 model.addAttribute("videoList", videoList);
	    	
	        return "redirect:VideoMaster";
	    }
	    
	    @RequestMapping(value="/EditVideo",method=RequestMethod.GET)
	    public String EditCountry(@RequestParam("videoId") String videoId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<VideoDetails> videoDetails = mongoTemplate.find(query.addCriteria(Criteria.where("videoId").is(videoId)), VideoDetails.class);
	    	query=new Query();
		    List<ActivityDetails> activityDetails = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), ActivityDetails.class);
	    	//List<ActivityDetails> activityDetails = activityRepository.findAll();
	    	model.addAttribute("activityDetails",activityDetails);
	    	model.addAttribute("videoDetails",videoDetails);
	    	
	    	return "EditVideo";
	    }
	    @RequestMapping(value="/EditVideoData",method=RequestMethod.POST)
	    public String EditCountryPostMethod(@ModelAttribute VideoDetails videoDetails, ModelMap model)
	    {
	    	try
	    	{
	    		//System.out.println("hiii");
	    		videoDetails = new VideoDetails(videoDetails.getVideoId(),videoDetails.getVideoLink(),"1", videoDetails.getCreatedDate()
	    				,videoDetails.getActivityId());
	    		videoRepository.save(videoDetails);
		        
		        model.addAttribute("videoStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("videoStatus", "Fail");
	    	}
	    	
	    	return "redirect:VideoMaster";
	    }
	    
	    
	    @ResponseBody
	    @RequestMapping(value="/DeleteVideo")
	    public List<ModuleType> DeleteActivity(@RequestParam("videoId") String videoId,ModelMap model)
	    {
	    	
	    	VideoDetails videoDetails = new VideoDetails();
	    	
    		Query query = new Query();

    	    videoDetails = mongoTemplate.findOne(Query.query(Criteria.where("videoId").is(videoId)), VideoDetails.class);
    	    videoDetails.setStatus("2");
     		videoRepository.save(videoDetails);
     		query = new Query();
     		List<ModuleType> moduleTypeList = moduleTypeRepository.findByStatus("1");
	    	List<ActivityDetails> activityList = activityRepository.findByStatus("1");
	    	
	    	for(int k=0;k<moduleTypeList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{	
		    			if(activityList.get(i).getModuleId().equals(moduleTypeList.get(k).getId()))
		    			{
		    				moduleTypeList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
			    }
		    
		    }
 	    	 
	    	return moduleTypeList;
	    	
	    }
	    
	    
	    
	    @RequestMapping("/VideoWatchReport")
	    public String VideoWatchReport(ModelMap model)
	    {
	    	Query query=new Query();
	    	List<VideoDetailsUserwise> videoList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), VideoDetailsUserwise.class);
	    	    	 
	    	 for(int k=0;k<videoList.size();k++)
	    	 {
	    		 try
	    		 {
	    		 try {
	    		 query=new Query();
	 	    	List<VideoDetails> videoDetails = mongoTemplate.find(query.addCriteria(Criteria.where("videoId").is(videoList.get(k).getVideoId())), VideoDetails.class);
	 	    	videoList.get(k).setVideoLink(videoDetails.get(0).getVideoLink());
	    		 }
	    		 catch (Exception e) {
					// TODO: handle exception
				}
	    		 try {
	    		 query=new Query();
	 	    	List<ActivityDetails> activityList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1").and("activityId").is(videoList.get(k).getActivityId())), ActivityDetails.class);
	 	    	
	 	    	videoList.get(k).setActivityName(activityList.get(0).getActivityName());
	    		 }
	    		 catch (Exception e) {
					// TODO: handle exception
				}
	    		 try
	    		 {
	 	    	 query=new Query();
		 	     List<PatientInformation> userDetails = mongoTemplate.find(query.addCriteria(Criteria.where("id").is(videoList.get(k).getUserId())), PatientInformation.class);
		 	     videoList.get(k).setPatientName(userDetails.get(0).getFirstName()+" "+userDetails.get(0).getLastName());
	    		 }
	    		 catch (Exception e) {
					// TODO: handle exception
				}
	    		 }
	    		 catch (Exception e) {
				}
	 	    	
	    	/*	 
		    	for(int i=0;i<activityList.size();i++)
		    	{
		    		
		    		
		    			///System.out.println(activityList.get(i).getActivityId()+" "+videoList.get(k).getActivityId());
		    			if(activityList.get(i).getActivityId().equals(videoList.get(k).getActivityId()))
		    			{
		    				videoList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
		    		
		    		
			    	 }*/
		    	}
		    	
 	    	 model.addAttribute("videoList", videoList);
 	    	 
	    	return "VideoWatchReport";
	    }    
	    
}
