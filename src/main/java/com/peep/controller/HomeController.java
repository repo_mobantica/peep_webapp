package com.peep.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping("/")
public class HomeController 
{
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	
	
    //Request Mapping For Home
    @RequestMapping("/home")
    public String homeRedirect(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {
    
        return "home";
    }
}
