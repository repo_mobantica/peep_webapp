package com.peep.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;
import com.peep.bean.OptionDetails;
import com.peep.bean.PatientInformation;
import com.peep.bean.QuestionDetails;
import com.peep.bean.RedFlagOptionDetails;
import com.peep.bean.RedFlagQuestionDetails;
import com.peep.bean.VideoDetails;
import com.peep.repository.ActivityRepository;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.OptionRepository;
import com.peep.repository.QuestionRepository;
import com.peep.repository.RedFlagOptionRepository;
import com.peep.repository.RedFlagQuestionsRepository;
import com.peep.repository.VideoRepository;

@Controller
@RequestMapping("/")
public class RedFlagQuestionController
{
	
	
	
	@Autowired
	VideoRepository videoRepository;
	
	@Autowired
	RedFlagQuestionsRepository redFlagQuestionsRepository;
	
	@Autowired
	RedFlagOptionRepository redFlagOptionRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String countryCode;
	    
	    
/* @ResponseBody
	    @RequestMapping("/searchCountryNameWiseCountryList")
	    public List<Country> SearchCountryNameWise(@RequestParam("countryName") String countryName)
	    {
	    	Query query = new Query();
	    	
	    	List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryName").regex("^"+countryName+".*","i")), Country.class);
	    	return countryList;
	    }*/
	    
	    @RequestMapping("/RedFlagQuestionMaster")
	    public String RedFlagQuestionMaster(ModelMap model)
	    {
	    	
 	    	 
	    	 List<RedFlagQuestionDetails> redFlagQuestionList = redFlagQuestionsRepository.findByStatus("1");
	    	
		    	
 	    	 model.addAttribute("redFlagQuestionList", redFlagQuestionList);
 	    	 
	    	return "RedFlagQuestionMaster";
	    }
	    
	    //Request Mapping For Module
	    @RequestMapping("/AddRedFlagQuestion")
	    public String AddRedFlagQuestion(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 
	    	 List<RedFlagQuestionDetails> redFlagQuestionList = redFlagQuestionsRepository.findAll();
	    	
	    	 model.addAttribute("redFlagQuestionList", redFlagQuestionList);
	    	 
	         return "AddRedFlagQuestion";
	    }
	    
	        
	    @RequestMapping(value = "/SaveRedFlagQuestionData", method = RequestMethod.GET)
	    public String SaveRedFlagQuestionData(@ModelAttribute RedFlagQuestionDetails redFlagQuestionDetails, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
	    		String optionName1=req.getParameter("optionNames");
	    		String correctOptionId=req.getParameter("correctOption");
	    		redFlagQuestionDetails = new RedFlagQuestionDetails("1", redFlagQuestionDetails.getCreatedDate()
	    				,correctOptionId,redFlagQuestionDetails.getRedFlagQuestionName());
	    		redFlagQuestionsRepository.save(redFlagQuestionDetails);
	    		 List<RedFlagQuestionDetails> redFlagQuestionList = redFlagQuestionsRepository.findAll();
	    		 
	    		 RedFlagQuestionDetails e = redFlagQuestionList.get(redFlagQuestionList.size() - 1);
	    		 
	     		if (optionName1.endsWith("###!###")) {
	     			optionName1 = optionName1.substring(0, optionName1.length() - 7);
	     			}
	     		
	     		String[] array = optionName1.split("###!###");
	     		
	     		for(int i=0;i<array.length;i++)
	     		{
	     			RedFlagOptionDetails optionDeatils = new RedFlagOptionDetails(array[i], "1", new Date(),e.getRedFlagQuestionId());
	     			redFlagOptionRepository.save(optionDeatils);
	     		}
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("questionStatus", "Fail");
	    	}
	    	
	        return "redirect:RedFlagQuestionMaster";
	    }
	    
	    @RequestMapping(value="/EditRedFlagQuestionMaster",method=RequestMethod.GET)
	    public String EditRedFlagQuestion(@RequestParam("redFlagQuestionId") String redFlagQuestionId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<RedFlagQuestionDetails> redFlagQuestionDetails = mongoTemplate.find(query.addCriteria(Criteria.where("redFlagQuestionId").is(redFlagQuestionId)), RedFlagQuestionDetails.class);
	    	model.addAttribute("redFlagQuestionDetails",redFlagQuestionDetails);
	    	query = new Query();
	    	List<RedFlagOptionDetails> optionList =  mongoTemplate.find(query.addCriteria(Criteria.where("redFlagQuestionId").is(redFlagQuestionId).and("status").is("1")), RedFlagOptionDetails.class);
	        model.addAttribute("redFlagOptionList",optionList);
	        return "EditRedFlagQuestionMaster";
	    }                                                                                                                                                                                                                                                                                                                                                                     
	   
	    
	    
	    @RequestMapping(value="/EditRedFlagQuestionData",method=RequestMethod.GET)
	    public String EditCountryPostMethod( ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
	    		String alloptions=req.getParameter("optionName1");
	    		String questionName=req.getParameter("redFlagQuestionName");
	    		String correctOption=req.getParameter("correctOption");
	    		String questionId=req.getParameter("redFlagQuestionId");
	    		RedFlagQuestionDetails question =redFlagQuestionsRepository.findByStatusAndRedFlagQuestionId("1",questionId);
	    		question.setCorrectOptionId(correctOption);
	    		question.setStatus("1");
	    		question.setRedFlagQuestionName(questionName);
	    		question.setRedFlagQuestionId(questionId);
	    		question.setUpdatedDate(new Date());
	    		redFlagQuestionsRepository.save(question);
	    		
	    		if (alloptions.endsWith("###!###")) {
	    			alloptions = alloptions.substring(0, alloptions.length() - 7);
	    			}
	    		/*if (deleteOptions.endsWith("##")) {
	    			deleteOptions = deleteOptions.substring(0, alloptions.length() - 2);
	    			}*/
	    		String[] optionArray = alloptions.split("###!###");
	    		Query query= new Query();
	    		List<RedFlagOptionDetails> optionDetails = mongoTemplate.find((query.addCriteria(Criteria.where("redFlagQuestionId").is(questionId))), RedFlagOptionDetails.class);
		    	for(int i=0;i<optionDetails.size();i++ )
		    	{
			    	optionDetails.get(i).setStatus("2");
		    		redFlagOptionRepository.save(optionDetails);
		    		
		    	}
	    		for(int i=0;i<optionArray.length;i++)
	    		{
	    			///System.out.println(array[i]);
	    			RedFlagOptionDetails optionDetails1 = new RedFlagOptionDetails(optionArray[i],"1", new Date()
	        				,questionId);
	    			redFlagOptionRepository.save(optionDetails1);		        
	        		
	    			
	    		}
		        model.addAttribute("questionStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("questionStatus", "Fail");
	    	}
	    	/*
	    	 List<QuestionDetails> questionList = questionRepository.findAll();

 	    	 model.addAttribute("questionList", questionList);*/
 	    	 
	    	return "redirect:RedFlagQuestionMaster";
	    }
	    
	    
	    @ResponseBody
	    @RequestMapping("/DeleteRedFlagQuestion")
	    public List<RedFlagQuestionDetails> DeleteRedFlagQuestion(@RequestParam("redFlagQuestionId") String redFlagQuestionId, HttpSession session)
	    {
	    	RedFlagQuestionDetails p=new RedFlagQuestionDetails();
	    		Query query = new Query();
	    		p = mongoTemplate.findOne(Query.query(Criteria.where("redFlagQuestionId").is(redFlagQuestionId)), RedFlagQuestionDetails.class);
			   	p.setStatus("2");
			   	redFlagQuestionsRepository.save(p);
	    	     query = new Query();
	 		     List<RedFlagQuestionDetails> redFlagQuestionList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), RedFlagQuestionDetails.class);
	 		   	  
	    	     //List<RedFlagQuestionDetails> redFlagQuestionList = redFlagQuestionsRepository.findAll();
		    	return redFlagQuestionList;
	    
	    	
	    }
	    @RequestMapping("/RedFlagCorrectOptionMaster")
	    public String RedFlagCorrectOptionMaster(ModelMap model)
	    {
	    	 
	    	 List<RedFlagQuestionDetails> questionList = redFlagQuestionsRepository.findAll();
	      	 List<RedFlagOptionDetails> optionList = redFlagOptionRepository.findAll();
	      	
	    		
	    	 for(int k=0;k<questionList.size();k++)
	    	 {
		    
	    		 for(int i=0;i<optionList.size();i++)
	 	    	{
		    		
		    		//	System.out.println(optionList.get(i).getOptionId()+" "+questionList.get(k).getCorrectOptionId());
	    		 if(optionList.get(i).getRedFlagOptionId()!="" && questionList.get(k).getCorrectOptionId()!=null)
	    			{
		    			if(optionList.get(i).getRedFlagOptionId().equals(questionList.get(k).getCorrectOptionId()))
		    			{
		    				questionList.get(k).setRedFlagOptionName(optionList.get(i).getRedFlagOptionName());
		    			}
		    		
	    			}
			    }
		    }
		    	
 	    	 model.addAttribute("questionList", questionList);
 	    	 
	    	return "RedFlagCorrectOptionMaster";
	    }
	    
	    
	    
	  //Request Mapping For Module
	    @RequestMapping("/AddCorrectRedFlagOption")
	    public String AddCorrectRedFlagOption(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 
	    	
	    	 List<RedFlagQuestionDetails> questionList = redFlagQuestionsRepository.findAll();
	      	 List<RedFlagOptionDetails> optionList = redFlagOptionRepository.findAll();
	    	// System.out.println(activityList.size());
	    	
	    	 model.addAttribute("questionList", questionList);
	    	 model.addAttribute("optionList", optionList);
	    	 
	         return "AddCorrectRedFlagOption";
	    }
	    
	    @RequestMapping(value = "/AddCorrectRerdFlagOptionData", method = RequestMethod.POST)
	    public String AddCorrectOptionData(@ModelAttribute RedFlagQuestionDetails questionDetails, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
	    		questionDetails = new RedFlagQuestionDetails(questionDetails.getRedFlagQuestionId(), "1", new Date()
	    				,questionDetails.getCorrectOptionId(),questionDetails.getRedFlagQuestionName());
	    		redFlagQuestionsRepository.save(questionDetails);		        
		        model.addAttribute("questionStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("questionStatus", "Fail");
	    	}	    	
	    	 List<RedFlagQuestionDetails> questionList= redFlagQuestionsRepository.findAll();
	    	 
	    	 model.addAttribute("questionList", questionList);
	    	
	        return "RedFlagCorrectOptionMaster";
	    }
	    
	    
	    @RequestMapping(value="/EditCorrectRedFlagOption",method=RequestMethod.GET)
	    public String EditCorrectOption(@RequestParam("redFlagQuestionId") String redFlagQuestionId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<RedFlagQuestionDetails> questionDetails = mongoTemplate.find(query.addCriteria(Criteria.where("redFlagQuestionId").is(redFlagQuestionId)), RedFlagQuestionDetails.class);
	    	List<RedFlagOptionDetails> optionList = redFlagOptionRepository.findAll();
	    	System.out.println(optionList.size());
	    	model.addAttribute("redFlagQuestionDetails",questionDetails);
	    	 model.addAttribute("optionList", optionList);
	    	
	    	return "EditCorrectRedFlagOption";
	    }
	    
	    @RequestMapping(value="/EditCorrectRedFlagOptionData",method=RequestMethod.POST)
	    public String EditCorrectOptionData(@ModelAttribute RedFlagQuestionDetails questionDetails, ModelMap model)
	    {
	    	try
	    	{
	    		questionDetails = new RedFlagQuestionDetails(questionDetails.getRedFlagQuestionId(), "1", new Date()
	    				,questionDetails.getCorrectOptionId(),questionDetails.getRedFlagQuestionName());
	    		redFlagQuestionsRepository.save(questionDetails);
		        
		        model.addAttribute("questionStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("questionStatus", "Fail");
	    	}
	    	
	    	List<RedFlagQuestionDetails> questionList= redFlagQuestionsRepository.findAll();
	    	 
	    	 model.addAttribute("questionList", questionList);
	    	
	        return "redirct:RedFlagCorrectOptionMaster";
	    }
	    
	   
	    
}
