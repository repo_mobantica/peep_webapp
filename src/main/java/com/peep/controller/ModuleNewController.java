package com.peep.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.peep.bean.ModuleType;
import com.peep.bean.Resource;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.ResourceRepository;

public class ModuleNewController {
	
	@Autowired
	ModuleTypeRepository moduleRepository;
	 
    @RequestMapping("/ModuleMaster")
    public String ResourceMaster(ModelMap model)
    {
    	 List<ModuleType> moduleList = moduleRepository.findByStatus("1");
	    	 model.addAttribute("moduleList", moduleList);
	    	 
    	return "ResourceMaster";
    }
    
    //Request Mapping For Module
    @RequestMapping("/AddModuleName")
    public String addResource(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
    {	 
    	 List<ModuleType> moduleList =  moduleRepository.findAll();
    	// System.out.println(activityList.size());
    	 model.addAttribute("moduleList", moduleList);
         return "AddResource";
    }
    
     /*   
    @RequestMapping(value = "/AddResourceData", method = RequestMethod.POST)
    public String addResourceData(@ModelAttribute Resource resource, ModelMap model, HttpServletRequest req, HttpServletResponse res)
    {
    	try
    	{
    	
    		resource = new Resource(resource.getVideoUrl(), "1", new Date(),resource.getResourceName());
    		resourceRepository.save(resource);		        
	        model.addAttribute("resourceStatus", "Success");
	        
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("resourceStatus", "Fail");
    	}	    	

    	 List<Resource> resourceList = resourceRepository.findAll();

    	 model.addAttribute("resourceList", resourceList);
    	
        return "redirect:ResourceMaster";
    }
    
    @RequestMapping(value="/EditResource",method=RequestMethod.GET)
    public String EditResource(@RequestParam("resourceId") String resourceId,ModelMap model)
    {
    	Query query = new Query();
    	List<Resource> resource = mongoTemplate.find(query.addCriteria(Criteria.where("resourceId").is(resourceId)), Resource.class);
    	model.addAttribute("resource",resource);
    	
    	return "EditResource";
    }
    @RequestMapping(value="/EditResourceData",method=RequestMethod.POST)
    public String EditResourceData(@ModelAttribute Resource resource, ModelMap model)
    {
    	try
    	{
    		resource = new Resource(resource.getResourceId(),resource.getVideoUrl(), "1", new Date(),resource.getResourceName());
    		resourceRepository.save(resource);	
	        
	        model.addAttribute("resourceStatus", "Success");
	        
    	}
    	catch(DuplicateKeyException de)
    	{
    		model.addAttribute("resourceStatus", "Fail");
    	}
    	
    	 List<Resource> resourceList = resourceRepository.findAll();

	    	 model.addAttribute("resourceList", resourceList);
	    	 
    	return "redirect:ResourceMaster";
    }
    
    @ResponseBody
    @RequestMapping("/DeleteResource")
    public List<Resource> DeleteResource(@RequestParam("resourceId") String resourceId, HttpSession session)
    {
    	Resource resource=new Resource();
    		Query query = new Query();
    		resource = mongoTemplate.findOne(Query.query(Criteria.where("resourceId").is(resourceId)), Resource.class);
    		resource.setStatus("2");
		   	resourceRepository.save(resource);
    	     query = new Query();
 		     List<Resource> resourceList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), Resource.class);
 		   	  
    	     //List<RedFlagQuestionDetails> redFlagQuestionList = redFlagQuestionsRepository.findAll();
	    	return resourceList;
    
    	
    }*/
}
