package com.peep.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.peep.bean.ActivityDetails;
import com.peep.bean.AddDetails;
import com.peep.bean.FeedBackUserWise;
import com.peep.bean.ModuleType;
import com.peep.bean.PatientInformation;
import com.peep.repository.ActivityRepository;
import com.peep.repository.FeedBackUserwiseRepository;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.PatientRepository;

@Controller
@RequestMapping("/")
public class FeedBackController
{
	@Autowired
	FeedBackUserwiseRepository feedBackUserwiseRepository;
	
	@Autowired
    PatientRepository patientRepository;
	
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String countryCode;
	    
	    
	   /* @ResponseBody
	    @RequestMapping("/searchCountryNameWiseCountryList")
	    public List<Country> SearchCountryNameWise(@RequestParam("countryName") String countryName)
	    {
	    	Query query = new Query();
	    	
	    	List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryName").regex("^"+countryName+".*","i")), Country.class);
	    	return countryList;
	    }*/
	    
	    @RequestMapping("/FeedBackMaster")
	    public String FeedBackMaster(ModelMap model)
	    {
	    
	    	List<FeedBackUserWise> feedBackUserWiseList = feedBackUserwiseRepository.findAll();
	    	
	    	List<PatientInformation> patientInformation = patientRepository.findAll();
 	    	 
	    	for(int i=0;i<feedBackUserWiseList.size();i++)
	    	{
    	
	    		for(int k=0;k<patientInformation.size();k++)
		    	 {
	    			if(patientInformation.get(k).getId()!="" && feedBackUserWiseList.get(i).getUserId()!=null)
	    			{
			    		if(feedBackUserWiseList.get(i).getUserId().equals(patientInformation.get(k).getId()))
			    		{
			    			feedBackUserWiseList.get(i).setUsername(patientInformation.get(k).getFirstName()+patientInformation.get(k).getLastName());
			    		}
	    			}
		    	 }
	    	}
	    	
 	    	 model.addAttribute("feedBackUserWiseList", feedBackUserWiseList);
 	    	 
			return "FeedBackMaster";
 	    	
	    }
	    
	    @RequestMapping(value="/ViewFeedBack")
	    public String addCountry(@RequestParam("feedBackId") String feedBackId,ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 
	    	
	    	Query query = new Query();
	    	List<FeedBackUserWise> feedBackUserWiseDetails = mongoTemplate.find(query.addCriteria(Criteria.where("feedBackId").is(feedBackId)), FeedBackUserWise.class);
	    	model.addAttribute("feedBackUserWiseDetails",feedBackUserWiseDetails);
	    	
	    	
	         return "ViewFeedBack";
	    }

}
