package com.peep.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import com.peep.bean.ModuleType;
import com.peep.bean.PatientInformation;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.PatientRepository;
import com.peep.bean.CancerType;
import com.peep.repository.CancerTypeRepository;

@Controller
@RequestMapping("/")
public class PatientController
{
	@Autowired
    PatientRepository patientRepository;
	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	CancerTypeRepository cancerTypeRepository;
	
	    @RequestMapping("/PatienteMaster")
	    public String CountryMaster(ModelMap model)
	    {
	    	 List<PatientInformation> patientList;
	    	 patientList = patientRepository.findByStatus("1");
 	    	 
 	    	 model.addAttribute("patientList", patientList);
 	    	 
	    	return "PatienteMaster";
	    }
	  
	    @RequestMapping("/AddPatientInformation")
	    public String addCountry(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 

	    	
	    	List<CancerType> cancerType = cancerTypeRepository.findByStatus("1");
	    	model.addAttribute("cancerTypeList", cancerType);
	    	
	         return "AddPatientInformation";
	    }
	    
	   @RequestMapping(value = "/AddPatientInformationData", method = RequestMethod.POST)
	    public String addPatientInformationData(@ModelAttribute PatientInformation patientInformation, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
	    		String cancerTypeId=req.getParameter("cancerTypeId");
	    		patientInformation.setZipCode(patientInformation.getZipCode());
	    		patientInformation.setCancerTypeId(patientInformation.getCancerTypeId());
	    		patientInformation.setUsername(patientInformation.getEmailId());
	    		patientInformation.setStatus("1");
	    		patientInformation.setCreatedDate(new Date());
	    		String stageOfChemotherapy=req.getParameter("stageOfChemotherapy");
	    		if(stageOfChemotherapy.equals("Receiving"))
	    		{
	    			patientInformation.setStageOfChemotherapy("Receiving");
	    		}
	    		if(stageOfChemotherapy.equals("Received"))
	    		{
	    			patientInformation.setStageOfChemotherapy("Received");
	    		}
	    		if(stageOfChemotherapy.equals("Planned"))
	    		{
	    			patientInformation.setStageOfChemotherapy("Planned");
	    		}
	    		if(stageOfChemotherapy.equals("Not sure"))
	    		{
	    			patientInformation.setStageOfChemotherapy("Not sure");
	    		}	        
		        model.addAttribute("moduletypeStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("moduletypeStatus", "Fail");
	    	}	    	
	    	 List<PatientInformation> patientList;
	    	 patientList = patientRepository.findAll();
	    	 
	    	 model.addAttribute("patientList", patientList);
	    	
	        return "PatienteMaster";
	    }
	
	   
	   
	    @ResponseBody
	    @RequestMapping("/DeletePatientInformation")
	    public List<PatientInformation> DeletePatientInformation(@RequestParam("patientId") String patientId, HttpSession session)
	    {
	    		PatientInformation p=new PatientInformation();
	    		Query query = new Query();
	    		///System.out.println("patientId"+patientId);
	    		p = mongoTemplate.findOne(Query.query(Criteria.where("id").is(patientId)), PatientInformation.class);
			   	p.setStatus("2");
			   	patientRepository.save(p);
	    	     query = new Query();
	 		     List<PatientInformation> patientList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), PatientInformation.class);
	 		   	  
	 	    	 
		    	return patientList;
	    
	    	
	    }
	   
	   
	   
	   
	   
	   
	   
	 /*  
	    @ResponseBody
	    @RequestMapping(value="/DeletePatientInformation")
	    public String DeletePatientInformation(@RequestParam("patientId") String patientId, ModelMap model)
	    {
	    	System.out.println("jkf");
	    	PatientInformation p= new PatientInformation();
			patientRepository.findAll();
			p= patientRepository.findById(patientId);
			p.setStatus("2");
			p = patientRepository.save(p);
			
			 List<PatientInformation> patientList;
	    	 patientList = patientRepository.findByStatus("1");
 	    	 System.out.println(patientList.size());
 	    	// model.addAttribute("patientList", patientList);
 	    	 
 	    	 
 	    	 
 	    	 
 	    	 
 	    	 
 	        
	    }
	    */
	    
	    @RequestMapping(value="/EditPatientInformationData",method=RequestMethod.POST)
	    public String EditCountryPostMethod(@ModelAttribute PatientInformation patientInformation, ModelMap model)
	    {
	    	try
	    	{
	    		PatientInformation p= new PatientInformation();
	    		
	    			patientRepository.findAll();
	    			p= patientRepository.findById(patientInformation.getId());
	    			
	    			p.setFirstName(patientInformation.getFirstName());
			    	p.setLastName(patientInformation.getLastName());
			    	p.setEmailId(patientInformation.getEmailId());
			    	p.setPhoneNUmber(patientInformation.getPhoneNUmber());
			    	p.setSocialSecurityNumber(patientInformation.getSocialSecurityNumber());
		    		p.setDateOfBirth(patientInformation.getDateOfBirth());
		    		p.setZipCode(patientInformation.getZipCode());
		    		p.setCountry(patientInformation.getCountry());
		    		p.setGender(patientInformation.getGender());
		    		p.setLanguage(patientInformation.getLanguage());
		    		p.setCancerTypeId(patientInformation.getCancerTypeId());
		    		p.setStageOfChemotherapy(patientInformation.getStageOfChemotherapy());
		    		p.setHospitalName(patientInformation.getHospitalName());
		    		p.setHospitalEmailid(patientInformation.getHospitalEmailid());
		    		p.setHosipitalPhoneNumber(patientInformation.getHosipitalPhoneNumber());
		    		p.setHospitalPhysician(patientInformation.getHospitalPhysician());
		    		p.setAge(patientInformation.getAge());
		    		p.setUsername(patientInformation.getEmailId());
		    		p.setPassword(p.getPassword());
	    			p = patientRepository.save(p);
	    	
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("patientStatus", "Fail");
	    	}
	    	
	    	 List<PatientInformation> patientList;
	    	 patientList = patientRepository.findAll();

 	    	 model.addAttribute("patientList", patientList);
 	    	 
	    	return "redirect:PatienteMaster";
	    }
	    
	    @RequestMapping(value="/EditPatientInformation",method=RequestMethod.GET)
	    public String EditCountry(@RequestParam("patientId") String patientId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<PatientInformation> patientDetails = mongoTemplate.find(query.addCriteria(Criteria.where("id").is(patientId)), PatientInformation.class);
	    	List<CancerType> cancerType=new ArrayList();
	    	model.addAttribute("patientDetails",patientDetails);
	    	
	    		cancerType = cancerTypeRepository.findByStatus("1");
	    
	    	model.addAttribute("cancerTypeList", cancerType);
	    	
	    	return "EditPatientInformation";
	    }
	    
	    

	    @RequestMapping(value="/ActivationMail",method=RequestMethod.GET)
	    public String mailActivation(@RequestParam("emailId") String emailId,ModelMap model)
	    {
	    	PatientInformation p= new PatientInformation();
    		
			
			p= patientRepository.findByEmailId(emailId);
			
			p.setFirstName(p.getFirstName());
	    	p.setLastName(p.getLastName());
	    	p.setEmailId(p.getEmailId());
	    	p.setPhoneNUmber(p.getPhoneNUmber());
	    	p.setSocialSecurityNumber(p.getSocialSecurityNumber());
    		p.setDateOfBirth(p.getDateOfBirth());
    		p.setZipCode(p.getZipCode());
    		p.setCountry(p.getCountry());
    		p.setGender(p.getGender());
    		p.setLanguage(p.getLanguage());
    		p.setCancerTypeId(p.getCancerTypeId());
    		p.setStageOfChemotherapy(p.getStageOfChemotherapy());
    		p.setHospitalName(p.getHospitalName());
    		p.setHospitalEmailid(p.getHospitalEmailid());
    		p.setHosipitalPhoneNumber(p.getHosipitalPhoneNumber());
    		p.setHospitalPhysician(p.getHospitalPhysician());
    		p.setAge(p.getAge());
    		p.setUsername(p.getEmailId());
    		p.setStatus(p.getStatus());
    		p.setActivation_status("1");
    		p.setPassword(p.getPassword());
			p = patientRepository.save(p);
	
	    	
	    	return "ActivationMail";
	    }
}
