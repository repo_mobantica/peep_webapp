package com.peep.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.accept.ServletPathExtensionContentNegotiationStrategy;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;
import com.peep.bean.OptionDetails;
import com.peep.bean.PatientInformation;
import com.peep.bean.QuestionDetails;
import com.peep.bean.VideoDetails;
import com.peep.repository.ActivityRepository;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.OptionRepository;
import com.peep.repository.QuestionRepository;
import com.peep.repository.VideoRepository;
import com.peep.ws.wsQuestionsDetails;
@Controller
@RequestMapping("/")
public class ModuleController
{
	@Autowired
    ModuleTypeRepository moduleTypeRepository;
	
	@Autowired
	ActivityRepository activityRepository;
	
	@Autowired
	OptionRepository optionRepository;
	
	@Autowired
	QuestionRepository questionRepository;
	
	@Autowired
	VideoRepository videoRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String countryCode;
	   
	    @RequestMapping("/ModuleTypeMaster")
	    public String CountryMaster(ModelMap model,HttpSession session)
	    {
	    	 
	    	List<ModuleType> moduleList = moduleTypeRepository.findByStatus("1");
	    	List<ActivityDetails> activityList = activityRepository.findByStatus("1");	    	
	    	for(int k=0;k<moduleList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{	
		    			if(activityList.get(i).getModuleId().equals(moduleList.get(k).getId()))
		    			{
		    				moduleList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
			    }		    
		    }	    	
	    	session.removeAttribute("questionName");
	    	session.removeAttribute("moduleType");
	    	session.removeAttribute("activityName");
	    	session.removeAttribute("videoLink");
	    	session.removeAttribute("questionOptionList");
	    	session.removeAttribute("questionCorrectOptionList");	    	
 	    	model.addAttribute("moduleTypeList", moduleList); 	    	 
	    	return "ModuleTypeMaster";
	    }
	    
	    @RequestMapping("/ModuleList")
	    public String ModuleList(ModelMap model,HttpSession session)
	    {
	    	 
	    	List<ModuleType> moduleList = moduleTypeRepository.findByStatus("1");
	    	List<ActivityDetails> activityList = activityRepository.findByStatus("1");	    	
	    	for(int k=0;k<moduleList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{	
		    			if(activityList.get(i).getModuleId().equals(moduleList.get(k).getId()))
		    			{
		    				moduleList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
			    }		    
		    }	    	
	    	session.removeAttribute("questionName");
	    	session.removeAttribute("moduleType");
	    	session.removeAttribute("activityName");
	    	session.removeAttribute("videoLink");
	    	session.removeAttribute("questionOptionList");
	    	session.removeAttribute("questionCorrectOptionList");	    	
 	    	model.addAttribute("moduleTypeList", moduleList); 	    	 
	    	return "ModuleTypeMaster";
	    }
	    
	    @RequestMapping("/AddModuleDetails")
	    public String addCountry(ModelMap model, HttpServletRequest req, HttpServletResponse res,HttpSession session) 
	    {	 
	        if(session.getAttribute("activityName")!=null && session.getAttribute("questionName")!=null)
	    	{	
	        	String questionName= session.getAttribute("questionName").toString();	
		    	String moduleType=session.getAttribute("moduleType").toString();
		    	String activityName=session.getAttribute("activityName").toString();
		    	String videoLink=session.getAttribute("videoLink" ).toString();
		    	
		       	String questionOptionList=session.getAttribute("questionOptionList" ).toString();
		       	
		    	model.addAttribute("moduleType", moduleType);
		    	model.addAttribute("activityName", activityName);
		    	
		    	String[] videoArray = null;
		    			if(videoLink.length()>0)
		    				{
		    					videoArray=videoLink.split("###!###");
		    				}
		    	model.addAttribute("videoLink", videoArray);
	    	}	    
	        return "AddModule";
	    }
	 /*   questionName: questionName,
		moduleType:moduleType,
		activityName:activityName,
		videoLink:videoLink1*/
	 
	
	    @RequestMapping(value="/AddCorrectOption")
	    public String AddCorrectOption(ModelMap model,HttpSession session)
	    {	 
	    	 
	    	/*String questionName= session.getAttribute("questionName").toString();	
	    	String moduleType=session.getAttribute("moduleType").toString();
	    	String activityName=session.getAttribute("activityName").toString();
	    	String videoLink=session.getAttribute("videoLink" ).toString();
	     	String multiQuestionNames=session.getAttribute("multiQuestionNames" ).toString();
	     	String questionOptionList=session.getAttribute("questionOptionList" ).toString();
	     	String questionCorrectOptionList=session.getAttribute("questionCorrectOptionList" ).toString();
	     	//List<HashMap<String, String>> questionOptionList = (List) session.getAttribute("questionOptionList");
	     	System.out.println("questionOptionList"+questionOptionList);
	    	model.addAttribute("questionName", questionName);
	    	model.addAttribute("moduleType", moduleType);
	    	model.addAttribute("activityName", activityName);
	    	model.addAttribute("videoLink", videoLink);
	    	model.addAttribute("questionNames", multiQuestionNames);
	    	model.addAttribute("questionOptionList", questionOptionList);
	    	model.addAttribute("questionCorrectOptionList", questionCorrectOptionList);
	   // 	System.out.println("hiii");
*/	    	
	    	//List<ModuleType> moduleList = moduleTypeRepository.findAll();
	    	 Query query=new Query();
		    List<ModuleType> moduleList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), ModuleType.class);
	    	
	    	model.addAttribute("moduleList", moduleList);
	    	
	    	return "AddCorrectOption"; 
	        
	    }
	    @RequestMapping(value = "/AddCorrectOptionData", method = RequestMethod.GET)
	    public String AddCorrectOptionData( ModelMap model, HttpServletRequest req, HttpServletResponse res,HttpSession session)
	    {
	    	try
	    	{
	    		
	    		String alloptions=req.getParameter("optionNames");
	    		String moduleId=req.getParameter("moduleId");
	    		String activityId=req.getParameter("activityId");
	    		String questionName=req.getParameter("questionName");
	    		String correctOption=req.getParameter("correctOption");
	    		QuestionDetails questionDetails = new QuestionDetails(questionName,"1", new Date()
        				,activityId,correctOption);
        		questionRepository.save(questionDetails);
	    		
	    		if (alloptions.endsWith("###!###")) {
	    			alloptions = alloptions.substring(0, alloptions.length() - 7);
	    			}
	    		
	    		String[] optionArray = alloptions.split("###!###");
	    		List<QuestionDetails> questionDetailsList = questionRepository.findAll();
	    		QuestionDetails e1 = questionDetailsList.get(questionDetailsList.size() - 1);
	    		for(int i=0;i<optionArray.length;i++)
	    		{
	    			///System.out.println(array[i]);
	    			OptionDetails optionDetails = new OptionDetails(optionArray[i],"1", new Date()
	        				,e1.getQuestionId());
	        		optionRepository.save(optionDetails);		        
	        		
	    			
	    		}  
	    		
	    		
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("questionStatus", "Fail");
	    	}	    	
	    	

	    	
	        return "redirect:QuestionMaster";
	    }
	    
	    @RequestMapping(value="/SaveModuleData" , method = RequestMethod.POST)
	    public String AddActivityData(@ModelAttribute ModuleType moduleType, ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {
	    	String activityName=moduleType.getActivityName();
	    	String videoLink1=req.getParameter("videoLink1");
	    	String questionName=moduleType.getQuestionName();
	     	String questionName1=req.getParameter("optionName1");
	    	moduleType=new ModuleType(moduleType.getModuleType(), "1", new Date(),"");
    		moduleTypeRepository.save(moduleType);
    		List<ModuleType> moduleList = moduleTypeRepository.findAll();
    		ModuleType e = moduleList.get(moduleList.size() - 1);
    		
    	   	/*ActivityDetails	activityDetails = new ActivityDetails();
	    	activityDetails.setActivityName(activityName);
    		activityDetails.setStatus("1");
    		activityDetails.setCreatedDate(new Date());
    		activityDetails.setModuleId(e.getId());
    		activityRepository.save(activityDetails);
    		
    		List<ActivityDetails> activityDetailsList = activityRepository.findAll();
    		ActivityDetails e1 = activityDetailsList.get(activityDetailsList.size() - 1);
    		
    		if (videoLink1.endsWith(",")) {
    			videoLink1 = videoLink1.substring(0, videoLink1.length() - 1);
        			}
    		String[] videoArray = videoLink1.split(",");
    		
    		for(int i=0;i<videoArray.length;i++)
    		{
    			VideoDetails videoDetails = new VideoDetails(videoArray[i], "1", new Date(),e1.getActivityId());
        		videoRepository.save(videoDetails);	
    		}
    	
    		
    		if (questionName1.endsWith(",")) {
    			questionName1 = questionName1.substring(0, questionName1.length() - 1);
    			}
    		
    
    		String[] array = questionName1.split(",");
    		
    		for(int i=0;i<array.length;i++)
    		{
    			///System.out.println(array[i]);
    			QuestionDetails questionDetails = new QuestionDetails(array[i],"1", new Date()
        				,e1.getActivityId(),"");
        		questionRepository.save(questionDetails);		        
        		
    			
    		}
    		*/
	         return "redirect:ModuleTypeMaster";
	    }
	 
	 
	    
	    @RequestMapping(value="/EditModuleType",method=RequestMethod.GET)
	    public String EditCountry(@RequestParam("moduleId") String moduleId,ModelMap model)
	    {
	    	Query query = new Query();
	   
	  
	    	List<QuestionDetails> questionDetails= new ArrayList<QuestionDetails>() ;
	    	List<VideoDetails> videoDetails =new ArrayList<VideoDetails>() ;
	    	List<OptionDetails> optionDetails =new ArrayList<OptionDetails>();

	    	List<ModuleType> moduleDetails = mongoTemplate.find(query.addCriteria(Criteria.where("id").is(moduleId)), ModuleType.class);
	    	query = new Query();
	    	List<ActivityDetails> activityDetails = mongoTemplate.find(query.addCriteria(Criteria.where("moduleId").is(moduleId)), ActivityDetails.class);
	    	
	    	for(int y=0;y<activityDetails.size();y++)
		    	 {
	    			query = new Query();
	    			questionDetails= mongoTemplate.find(query.addCriteria(Criteria.where("activityId").is(activityDetails.get(y).getActivityId())), QuestionDetails.class);
	    			query = new Query();
	    			videoDetails = mongoTemplate.find(query.addCriteria(Criteria.where("activityId").is(activityDetails.get(y).getActivityId())), VideoDetails.class);
		    	 }
	    	for(int z=0;z<questionDetails.size();z++)
	    	 {
	    		query = new Query();
				optionDetails = mongoTemplate.find(query.addCriteria(Criteria.where("questionId").is(questionDetails.get(z).getQuestionId())), OptionDetails.class);
   			
	    	 }
	   
	    	model.addAttribute("moduleDetails",moduleDetails);
	    	model.addAttribute("activityDetails",activityDetails);
	    	model.addAttribute("questionDetails",questionDetails);
	    	model.addAttribute("videoDetails",videoDetails);
	    	model.addAttribute("optionDetails",optionDetails);
	    	
	    	return "EditModuleType";
	    }
	  
	    
	    @RequestMapping(value="/SaveEditedModuleData" , method = RequestMethod.POST)
	    public String SaveEditdModuleData(@ModelAttribute ModuleType moduleType, ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {
	    	
	    	ModuleType m=moduleTypeRepository.findById(moduleType.getId());
	    	m.setModuleType(moduleType.getModuleType());
	    	m.setModuleIcon("");
	    	m.setStatus("1");
	    	m.setUpdatedDate(new Date());
	    	
    		moduleTypeRepository.save(m);
    		
	         return "redirect:ModuleTypeMaster";
	    }
	 
	    
	    
	    
	    
	    
	    
	    @ResponseBody
	    @RequestMapping(value="/AddOptions")
	    public List<ModuleType> AddOptions(@RequestParam("questionName") String questionName,@RequestParam("moduleType") String moduleType,@RequestParam("activityName") String activityName,
	    		@RequestParam("videoLink") String videoLink,@RequestParam("multiQuestionNames") String multiQuestionNames,@RequestParam("questionoptions") String questionoptions,  ModelMap model,HttpSession session)
	    {
	    	session.setAttribute("questionName", questionName);	
	    	session.setAttribute("moduleType", moduleType);
	    	session.setAttribute("activityName", activityName);
	    	session.setAttribute("videoLink", videoLink);
	    	session.setAttribute("multiQuestionNames", multiQuestionNames);
	    	session.setAttribute("questionOptionList", "");
	    	session.setAttribute("questionCorrectOptionList", "");
	    	session.setAttribute("questionoptions", questionoptions);
     		List<ModuleType> moduleTypeList = moduleTypeRepository.findByStatus("1");
	    	List<ActivityDetails> activityList = activityRepository.findByStatus("1");
	    	
	    	for(int k=0;k<moduleTypeList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{	
		    			if(activityList.get(i).getModuleId().equals(moduleTypeList.get(k).getId()))
		    			{
		    				moduleTypeList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
			    }
		    
		    }
	    	
	    	// model.addAttribute("moduleTypeList", moduleTypeList);
 	    	 
	    	return moduleTypeList;
	    	
	    }
	    
	    
	    @ResponseBody
	    @RequestMapping(value="/DeleteModuleType")
	    public List<ModuleType> DeleteModuleType(@RequestParam("moduleTypeId") String moduleTypeId,ModelMap model)
	    {
	    	
	    	ModuleType moduleType= new ModuleType();
	    	moduleTypeRepository.findAll();
	    	moduleType= moduleTypeRepository.findById(moduleTypeId);
	    	
	    	ActivityDetails activityDetails = new ActivityDetails();
	    	
    		Query query = new Query();
    		moduleType = mongoTemplate.findOne(Query.query(Criteria.where("id").is(moduleTypeId)), ModuleType.class);
    		moduleType.setStatus("2");
    		moduleTypeRepository.save(moduleType);
    	    query = new Query();

    	 /*   activityDetails = mongoTemplate.findOne(Query.query(Criteria.where("moduleId").is(moduleTypeId)), ActivityDetails.class);
     		moduleType.setStatus("2");
     		activityRepository.save(activityDetails);
     		query = new Query();*/
     		List<ModuleType> moduleTypeList = moduleTypeRepository.findByStatus("1");
	    	List<ActivityDetails> activityList = activityRepository.findByStatus("1");
	    	
	    	for(int k=0;k<moduleTypeList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{	
		    			if(activityList.get(i).getModuleId().equals(moduleTypeList.get(k).getId()))
		    			{
		    				moduleTypeList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
			    }
		    
		    }
	    	
	    	// model.addAttribute("moduleTypeList", moduleTypeList);
 	    	 
	    	return moduleTypeList;
	    	
	    }
}
