package com.peep.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.peep.bean.ActivityDetails;
import com.peep.bean.Answers;
import com.peep.bean.ModuleType;
import com.peep.bean.OptionDetails;
import com.peep.bean.PatientInformation;
import com.peep.bean.QuestionDetails;
import com.peep.bean.QuestionDetailsUserwise;
import com.peep.bean.VideoDetails;
import com.peep.bean.VideoDetailsUserwise;
import com.peep.repository.ActivityRepository;
import com.peep.repository.AnswerRepository;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.OptionRepository;
import com.peep.repository.PatientRepository;
import com.peep.repository.QuestionDetailsUserWiseRepository;
import com.peep.repository.QuestionRepository;
import com.peep.repository.VideoRepository;
import com.peep.repository.VideoUserwiseRepository;

@Controller
@RequestMapping("/")
public class AnswerController
{
	@Autowired
	ActivityRepository activityRepository;
	
	@Autowired
	PatientRepository patientRepository;
	
	@Autowired
	VideoRepository videoRepository;
	
	@Autowired
	VideoUserwiseRepository videoUserwiseRepository;
	
	@Autowired
	OptionRepository optionRepository;
	
	@Autowired
	QuestionRepository questionRepository;
	
	@Autowired
	AnswerRepository answerRepository; 
	@Autowired
	QuestionDetailsUserWiseRepository questionDetailsUserWiseRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String countryCode;
	    
	    
/* @ResponseBody
	    @RequestMapping("/searchCountryNameWiseCountryList")
	    public List<Country> SearchCountryNameWise(@RequestParam("countryName") String countryName)
	    {
	    	Query query = new Query();
	    	
	    	List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryName").regex("^"+countryName+".*","i")), Country.class);
	    	return countryList;
	    }*/
	    
	    @RequestMapping("/AnswerMaster")
	    public String answerMaster(ModelMap model)
	    {
	    	 List<QuestionDetailsUserwise> answerList = questionDetailsUserWiseRepository.findAll();
	    	 List<PatientInformation> patientInformationList = patientRepository.findAll();
	    	 List<ActivityDetails> activityList = activityRepository.findAll();
	    	 List<QuestionDetails> questionList = questionRepository.findAll();
	    	 List<OptionDetails> optionList = optionRepository.findAll();
		    	
	    	 Query query=new Query();
	    	 for(int k=0;k<answerList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{	
		    			if(activityList.get(i).getActivityId().equals(answerList.get(k).getActivityId()))
		    			{
		    				answerList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
		    		
		    		
			    }
		    	for(int i=0;i<patientInformationList.size();i++)
		    	{	
		    			if(patientInformationList.get(i).getId().equals(answerList.get(k).getUserId()))
		    			{
		    				answerList.get(k).setPatientName(patientInformationList.get(i).getFirstName()+ " "+patientInformationList.get(i).getLastName());
		    			}
		    		
			    }
		    	
		    	
		    	/*
		    	query=new Query();
		 	    List<QuestionDetails> questionList = mongoTemplate.find(query.addCriteria(Criteria.where("questionId").is("5b7ec1547c1fc34296c0f35c")), QuestionDetails.class);
		 	    	
		 	    	answerList.get(k).setQuestionName(questionList.get(0).getQuestionName());
    				answerList.get(k).setCorrectOptionName(questionList.get(0).getCorrectOptionId());
		    	*/
		   	
		    	for(int i=0;i<questionList.size();i++)
		    	{	
		    			if(questionList.get(i).getQuestionId().equals(answerList.get(k).getQuestionId()))
		    			{
		    				answerList.get(k).setQuestionName(questionList.get(i).getQuestionName());
		    				
		    				answerList.get(k).setCorrectOptionName(questionList.get(i).getCorrectOptionId());
		    			}
			    }
		    	
		    	for(int i=0;i<optionList.size();i++)
		    	{	
		    			if(optionList.get(i).getOptionId().equals(answerList.get(k).getAnswerId()))
		    			{
		    				answerList.get(k).setAnswerName(optionList.get(i).getOptionName());
		    			}
		    		
		    		
			    }
		    	
		    	
		    	 SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		    	  String result = formater.format(answerList.get(k).getCreatedDate());
		    	  answerList.get(k).setCreatedDate1(result);
		    	//  System.out.println(result);
		    }
		    	
	    
 	    	 model.addAttribute("answerList", answerList);
 	    	 
	    	return "AnswerMaster";
	    }
	    
	    
	    
	    
	    @RequestMapping("/VideoAnswerMaster")
	    public String videoAnswerMaster(ModelMap model)
	    {
	    	
	    	 List<PatientInformation> patientInformationList = patientRepository.findAll();
	    	 List<ActivityDetails> activityList = activityRepository.findAll();
	    	 List<VideoDetailsUserwise> videoUerWiseList = videoUserwiseRepository.findAll();
	    	
	    	 for(int k=0;k<videoUerWiseList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{	
		    			if(activityList.get(i).getActivityId().equals(videoUerWiseList.get(k).getActivityId()))
		    			{
		    				videoUerWiseList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
		    		
		    		
			    }
		    	for(int i=0;i<patientInformationList.size();i++)
		    	{	
		    			if(patientInformationList.get(i).getId().equals(videoUerWiseList.get(k).getUserId()))
		    			{
		    				videoUerWiseList.get(k).setPatientName(patientInformationList.get(i).getFirstName()+ " "+patientInformationList.get(i).getLastName());
		    			}
		    		
		    		
			    }
		    
		    	  SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		    	  String result = formater.format(videoUerWiseList.get(k).getCreatedDate());
		    	  videoUerWiseList.get(k).setCreatedDate1(result);
		    	
		    	 
		    	
		    }
		    	
	    
 	    	 model.addAttribute("videoUerWiseList", videoUerWiseList);
 	    	 
	    	return "VideoAnswerMaster";
	    }
	    
}