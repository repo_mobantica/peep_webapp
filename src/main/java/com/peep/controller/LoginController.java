package com.peep.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.peep.bean.Login;
import com.peep.repository.LoginRepository;

@Controller
@RequestMapping("/")
public class LoginController
{
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	
	
    //Request Mapping For Login
	@RequestMapping(method = RequestMethod.GET)
	public String loginRedirect(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
	   return "login";
	}
	    
	@RequestMapping(value="/login",method = RequestMethod.GET)
	public String LoginReturnByLogout(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	{
	   return "login";
	}
	
	@RequestMapping(value="/login", method=RequestMethod.POST) 
	public String checkLogin(@ModelAttribute Login login, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		String userName = req.getParameter("userName");
		String passWord = req.getParameter("passWord");
		
		try
		{
			List<Login> loginUser= new ArrayList<Login>();
			
			Query query = new Query();
			
			loginUser = mongoTemplate.find(query.addCriteria(Criteria.where("userName").is(userName).and("passWord").is(passWord)), Login.class);
			
		   if(loginUser.size()==1)
		   {
			 HttpSession session = req.getSession(true);
			 session.setAttribute("user",userName);
			   
			 return "redirect:PatienteMaster";
		   }
		   else
		   {
			   model.addAttribute("loginStatus", "Fail");
				
			   return "login";
		   }
		}
		catch(Exception e)
		{
			
			return "login";
		}
	}
	
	
	@RequestMapping("/logout")
	public String logout(HttpServletRequest req,HttpServletResponse res)
	{
		HttpSession session = req.getSession(false);
		
		session.removeAttribute("user");
		
		return "redirect:login";
	}
	
}