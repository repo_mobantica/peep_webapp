package com.peep.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;
import com.peep.bean.OptionDetails;
import com.peep.bean.QuestionDetails;
import com.peep.bean.VideoDetails;
import com.peep.repository.ActivityRepository;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.OptionRepository;
import com.peep.repository.QuestionRepository;
import com.peep.repository.VideoRepository;

@Controller
@RequestMapping("/")
public class OptionController
{
	@Autowired
	ActivityRepository activityRepository;
	
	@Autowired
	ModuleTypeRepository moduleTypeRepository;
	
	
	@Autowired
	QuestionRepository questionRepository;
	
	@Autowired
	OptionRepository optionRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String countryCode;
	    
	    
/* @ResponseBody
	    @RequestMapping("/searchCountryNameWiseCountryList")
	    public List<Country> SearchCountryNameWise(@RequestParam("countryName") String countryName)
	    {
	    	Query query = new Query();
	    	
	    	List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryName").regex("^"+countryName+".*","i")), Country.class);
	    	return countryList;
	    }*/
	    
	    @RequestMapping("/OptionMaster")
	    public String OptionMaster(ModelMap model)
	    {
	    	 List<QuestionDetails> questionList = questionRepository.findAll();
 	    	 
	    	 List<OptionDetails> optionList = optionRepository.findAll();
	    	 for(int k=0;k<optionList.size();k++)
	    	 {
		    	for(int i=0;i<questionList.size();i++)
		    	{
		    		
		    		
		    			//System.out.println(questionList.get(i).getQuestionId()+" "+optionList.get(k).getQuestionId());
		    			if(questionList.get(i).getQuestionId().equals(optionList.get(k).getQuestionId()))
		    			{
		    				optionList.get(k).setQuestionName(questionList.get(i).getQuestionName());
		    			}
		    		
		    		
			    	 }
		    	}
		    	
 	    	 model.addAttribute("optionList", optionList);
 	    	 
	    	return "OptionMaster";
	    }
	    
	    //Request Mapping For Module
	    @RequestMapping("/AddOption")
	    public String addCountry(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 
	    	 List<QuestionDetails> questionList = questionRepository.findAll();
	    	// System.out.println(activityList.size());
	    	 model.addAttribute("questionList", questionList);
	         return "AddOption";
	    }
	    
	        
	    @RequestMapping(value = "/AddOptionData", method = RequestMethod.POST)
	    public String countrySave(@ModelAttribute OptionDetails optionDeatils, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
	    		optionDeatils = new OptionDetails(optionDeatils.getOptionName(), "1", new Date(),optionDeatils.getQuestionId());
	    		optionRepository.save(optionDeatils);		        
		        model.addAttribute("optionStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("optionStatus", "Fail");
	    	}	    	
	    	 List<OptionDetails> optionList= optionRepository.findAll();
	    	 
	    	 model.addAttribute("optionList", optionList);
	    	
	        return "redirect:OptionMaster";
	    }
	    
	    @RequestMapping(value="/EditOption",method=RequestMethod.GET)
	    public String EditCountry(@RequestParam("optionId") String optionId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<OptionDetails> optionDetails = mongoTemplate.find(query.addCriteria(Criteria.where("optionId").is(optionId)), OptionDetails.class);
	    	List<QuestionDetails> questionDetails = questionRepository.findAll();
	    	model.addAttribute("questionDetails",questionDetails);
	    	model.addAttribute("optionDetails",optionDetails);
	    	
	    	return "EditOption";
	    }
	    @RequestMapping(value="/EditOptionData",method=RequestMethod.POST)
	    public String EditCountryPostMethod(@ModelAttribute OptionDetails optionDetails, ModelMap model)
	    {
	    	try
	    	{
	    		optionDetails = new OptionDetails(optionDetails.getOptionId(),optionDetails.getOptionName(),"1", optionDetails.getCreatedDate()
	    				,optionDetails.getQuestionId());
	    		optionRepository.save(optionDetails);
		        
		        model.addAttribute("optionStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("optionStatus", "Fail");
	    	}
	    	
	    	 List<OptionDetails> optionList = optionRepository.findAll();

 	    	 model.addAttribute("optionList", optionList);
 	    	 
	    	return "redirect:OptionMaster";
	    }
}
