package com.peep.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.peep.bean.ActivityDetails;
import com.peep.bean.AddDetails;
import com.peep.bean.ModuleType;
import com.peep.repository.ActivityRepository;
import com.peep.repository.ModuleTypeRepository;

@Controller
@RequestMapping("/")
public class ActivityController
{
	@Autowired
	ActivityRepository activityRepository;
	
	@Autowired
	ModuleTypeRepository moduleTypeRepository;
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String countryCode;
	    
	    
	   /* @ResponseBody
	    @RequestMapping("/searchCountryNameWiseCountryList")
	    public List<Country> SearchCountryNameWise(@RequestParam("countryName") String countryName)
	    {
	    	Query query = new Query();
	    	
	    	List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryName").regex("^"+countryName+".*","i")), Country.class);
	    	return countryList;
	    }*/
	    
	    @RequestMapping("/ActivityMaster")
	    public String ActivityMaster(ModelMap model)
	    {
	    
	    	
 	    	 Query query=new Query();
	    	List<ModuleType> moduleList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), ModuleType.class);
	    	query=new Query();
	    	List<ActivityDetails> activityList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), ActivityDetails.class);
		    	for(int i=0;i<activityList.size();i++)
		    	{
	    	
		    		for(int k=0;k<moduleList.size();k++)
			    	 {
		    			if(activityList.get(i).getModuleId()!="" && activityList.get(i).getModuleId()!=null)
		    			{
				    		if(activityList.get(i).getModuleId().equals(moduleList.get(k).getId()))
				    		{
				    			activityList.get(i).setModuleType(moduleList.get(k).getModuleType());
				    		}
		    			}
		    		
		    	}
	    	 }
		    	//System.out.println(activityList.size());
		    	//model.addAttribute("moduleDetails",moduleDetails);
 	    	 model.addAttribute("activityList", activityList);
 	    	 
	    	return "ActivityMaster";
	    }
	    
	    
	    
	    
	    @ResponseBody
	    @RequestMapping(value="/getActivityWithModule")
	    public List<ActivityDetails> getActivityWithModule(@RequestParam("moduleId") String moduleId,ModelMap model,HttpSession session)
	    {
	    	
     		List<ModuleType> moduleTypeList = moduleTypeRepository.findByStatusAndId("1",moduleId);
     		List<ActivityDetails> activityListOld = activityRepository.findByStatus("1");
	    	List<ActivityDetails> activityList = new ArrayList<ActivityDetails>();
	    	
	    	for(int k=0;k<moduleTypeList.size();k++)
	    	 {
		    	for(int i=0;i<activityListOld.size();i++)
		    	{	ActivityDetails ac =new ActivityDetails();
		    			if(activityListOld.get(i).getModuleId().equals(moduleTypeList.get(k).getId()))
		    				
		    			{
		    				ac.setActivityId(activityListOld.get(i).getActivityId());
		    				ac.setActivityName(activityListOld.get(i).getActivityName());
		    				activityList.add(ac);
		    			}
			    }
		    
		    }
	    	// model.addAttribute("moduleTypeList", moduleTypeList);
 	    	 
	    	return activityList;
	    	
	    }
	    
	    
	    
	    
	    
	    
	    
	    
	    //Request Mapping For Module
	    @RequestMapping(value="/AddActivity")
	    public String AddActivity(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 
	    	//List<ModuleType> moduleList = moduleTypeRepository.findAll();
	    	 Query query=new Query();
		    	List<ModuleType> moduleList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), ModuleType.class);
	    	
	    	model.addAttribute("moduleList",moduleList);
	    	
	         return "AddActivity";
	    }
	    
	   
	        
	    @RequestMapping(value ="/SaveActivityData", method = RequestMethod.POST)
	    public String SaveActivityData(@ModelAttribute ActivityDetails activityDetails, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	//System.out.println(activityDetails.getModuleId());
	    	try
	    	{
	    	
	    		//System.out.println("hoifdhgiuohdroigthoidhtgoiioss");
	    		
	    		activityDetails = new ActivityDetails(activityDetails.getActivityName(), "1", new Date(),activityDetails.getModuleId());
	    		activityRepository.save(activityDetails);		        
		        model.addAttribute("activityStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("activityStatus", "Fail");
	    	}	    	
	    	/* List<ActivityDetails> activityList;
	    	 activityList = activityRepository.findAll();
	    	 
	    	 model.addAttribute("activityList", activityList);*/
	    	
	        return "redirect:ActivityMaster";
	    }
	    
	    @RequestMapping(value="/EditActivity",method=RequestMethod.GET)
	    public String EditActivity(@RequestParam("activityId") String activityId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<ActivityDetails> activityDetails = mongoTemplate.find(query.addCriteria(Criteria.where("activityId").is(activityId)), ActivityDetails.class);
	    	query = new Query();
	    	List<ModuleType> moduleDetails = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), ModuleType.class);
	    	model.addAttribute("moduleDetails",moduleDetails);
	    	model.addAttribute("activityDetails",activityDetails);
	    	
	    	return "EditActivity";
	    }
	    @RequestMapping(value="/EditAtivityData",method=RequestMethod.POST)
	    public String EditAtivityData(@ModelAttribute ActivityDetails activityDetails, ModelMap model)
	    {
	    	try
	    	{
	    		activityDetails = new ActivityDetails(activityDetails.getActivityId(),activityDetails.getActivityName(),"1", activityDetails.getCreatedDate()
	    				,activityDetails.getModuleId());
	    		activityRepository.save(activityDetails);
		        
		        model.addAttribute("activityStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("activityStatus", "Fail");
	    	}
	    	
	    	 List<ActivityDetails> activityList;
	    	 activityList = activityRepository.findAll();

 	    	 model.addAttribute("activityList", activityList);
 	    	 
	    	return "redirect:ActivityMaster";
	    }
	    
	    
	    /*@ResponseBody
	    @RequestMapping(value="/DeleteActivity")
	    public List<ActivityDetails> DeleteActivity(@RequestParam("activityId") String activityId,ModelMap model)
	    {
	    	System.out.println("hiiiiiii"+activityId);
	    	
	    	
	    	ActivityDetails moduleType= new ActivityDetails();
	    	activityRepository.findAll();
	    	moduleType= activityRepository.findByActivityId(activityId);
	    	
	    	ActivityDetails activityDetails = new ActivityDetails();
	    	
    		Query query = new Query();
    		moduleType = mongoTemplate.findOne(Query.query(Criteria.where("activityId").is(activityId)), ActivityDetails.class);
    		moduleType.setStatus("2");
    		activityRepository.save(moduleType);
	    	
	    	
	    	
	    	List<ActivityDetails> activityList = activityRepository.findByStatus("1");
	    	
	    	
	    	// model.addAttribute("moduleTypeList", moduleTypeList);
 	    	 
	    	return activityList;
	    	
	    }*/
	    @ResponseBody
	    @RequestMapping(value="/DeleteActivity")
	    public List<ModuleType> DeleteActivity(@RequestParam("activityId") String activityId,ModelMap model)
	    {
	    	
	    	/*ModuleType moduleType= new ModuleType();
	    	moduleTypeRepository.findAll();
	    	moduleType= moduleTypeRepository.findById(moduleTypeId);
	    	*/
	    	ActivityDetails activityDetails = new ActivityDetails();
	    	
    		Query query = new Query();
    		/*moduleType = mongoTemplate.findOne(Query.query(Criteria.where("id").is(moduleTypeId)), ModuleType.class);
    		moduleType.setStatus("2");
    		moduleTypeRepository.save(moduleType);*/
    	    query = new Query();

    	    activityDetails = mongoTemplate.findOne(Query.query(Criteria.where("activityId").is(activityId)), ActivityDetails.class);
    	    activityDetails.setStatus("2");
     		activityRepository.save(activityDetails);
     		query = new Query();
     		List<ModuleType> moduleTypeList = moduleTypeRepository.findByStatus("1");
	    	List<ActivityDetails> activityList = activityRepository.findByStatus("1");
	    	
	    	for(int k=0;k<moduleTypeList.size();k++)
	    	 {
		    	for(int i=0;i<activityList.size();i++)
		    	{	
		    			if(activityList.get(i).getModuleId().equals(moduleTypeList.get(k).getId()))
		    			{
		    				moduleTypeList.get(k).setActivityName(activityList.get(i).getActivityName());
		    			}
			    }
		    
		    }
 	    	 
	    	return moduleTypeList;
	    	
	    }
	    
}
