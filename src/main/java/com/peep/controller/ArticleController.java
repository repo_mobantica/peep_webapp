package com.peep.controller;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.peep.bean.ActivityDetails;
import com.peep.bean.Articles;
import com.peep.bean.ModuleType;
import com.peep.bean.Resource;
import com.peep.bean.VideoDetails;
import com.peep.repository.ActivityRepository;
import com.peep.repository.ArticleRepository;
import com.peep.repository.ModuleTypeRepository;
import com.peep.repository.ResourceRepository;
import com.peep.repository.VideoRepository;

@Controller
@RequestMapping("/")
public class ArticleController
{
	@Autowired
	ArticleRepository articleRepository;
	
	@Autowired
	ResourceRepository resourceRepository;
	
	
	@Autowired
	VideoRepository videoRepository;
	
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	
	 String countryCode;
	    
	    
/* @ResponseBody
	    @RequestMapping("/searchCountryNameWiseCountryList")
	    public List<Country> SearchCountryNameWise(@RequestParam("countryName") String countryName)
	    {
	    	Query query = new Query();
	    	
	    	List<Country> countryList = mongoTemplate.find(query.addCriteria(Criteria.where("countryName").regex("^"+countryName+".*","i")), Country.class);
	    	return countryList;
	    }*/
	    
	    @RequestMapping("/ArticleMaster")
	    public String ArticleMaster(ModelMap model)
	    {
	    	 List<Articles> articleList = articleRepository.findByStatus("1");
 	    	 model.addAttribute("articleList", articleList);
 	    	 
	    	return "ArticleMaster";
	    }
	    
	    //Request Mapping For Module
	    @RequestMapping("/AddArticle")
	    public String addResource(ModelMap model, HttpServletRequest req, HttpServletResponse res) 
	    {	 
	    	 List<Articles> articleList = articleRepository.findAll();
 	    	 model.addAttribute("articleList", articleList);
	         return "AddArticles";
	    }
	    
	        
	    @RequestMapping(value = "/AddArticlesData", method = RequestMethod.POST)
	    public String addResourceData(@ModelAttribute Articles articles, ModelMap model, HttpServletRequest req, HttpServletResponse res)
	    {
	    	try
	    	{
	    		articles = new Articles(articles.getArticleName(),articles.getArticleUrl(), "1", new Date());
	    		articleRepository.save(articles);		        
		        model.addAttribute("articleStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("articleStatus", "Fail");
	    	}	    	
	    	 List<Articles> articleList = articleRepository.findAll();
 	    	 model.addAttribute("articleList", articleList);
	    	
	        return "redirect:ArticleMaster";
	    }
	    
	    @RequestMapping(value="/EditArticles",method=RequestMethod.GET)
	    public String EditCountry(@RequestParam("articleId") String articleId,ModelMap model)
	    {
	    	Query query = new Query();
	    	List<Articles> articles = mongoTemplate.find(query.addCriteria(Criteria.where("articleId").is(articleId)), Articles.class);
	    	model.addAttribute("articles",articles);
	    	
	    	return "EditArticles";
	    }
	    @RequestMapping(value="/EditArticlesData",method=RequestMethod.POST)
	    public String EditArticlesData(@ModelAttribute Articles articles, ModelMap model)
	    {
	    	try
	    	{
	    		articles = new Articles(articles.getArticleId(),articles.getArticleName(),articles.getArticleUrl(), "1", new Date());
	    		articleRepository.save(articles);		        
		        model.addAttribute("articleStatus", "Success");
		        
	    	}
	    	catch(DuplicateKeyException de)
	    	{
	    		model.addAttribute("articleStatus", "Fail");
	    	}
	    	
	    	 List<Articles> articleList = articleRepository.findAll();
 	    	 model.addAttribute("articleList", articleList);
 	    	 
	    	return "redirect:ArticleMaster";
	    }
	    
	    
	    @ResponseBody
	    @RequestMapping("/DeleteArticle")
	    public List<Articles> DeleteArticle(@RequestParam("articleId") String articleId, HttpSession session)
	    {
	    	Articles articles=new Articles();
	    		Query query = new Query();
	    		articles = mongoTemplate.findOne(Query.query(Criteria.where("articleId").is(articleId)), Articles.class);
	    		articles.setStatus("2");
	    		articleRepository.save(articles);
	    	     query = new Query();
	 		     List<Articles> articleList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("1")), Articles.class);
	 		   	  
	    	     //List<RedFlagQuestionDetails> redFlagQuestionList = redFlagQuestionsRepository.findAll();
		    	return articleList;
	    
	    	
	    }
}
