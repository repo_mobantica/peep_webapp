package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class RedFlagOptionDetails {

	
	@Id
	String redFlagOptionId;
	
	String redFlagOptionName;
	Date createdDate;
	Date updatedDate;
	String status;
	String redFlagQuestionId;

	public RedFlagOptionDetails() {

	}
	public RedFlagOptionDetails(String redFlagOptionName, String status, Date createdDate, String redFlagQuestionId) {
		this.redFlagOptionName=redFlagOptionName;
		this.status=status;
		this.createdDate=createdDate;
		this.redFlagQuestionId=redFlagQuestionId;
		
	}
	public RedFlagOptionDetails(String redFlagOptionId, String redFlagOptionName, String status, Date createdDate, String redFlagQuestionId) {
		
		this.redFlagOptionName=redFlagOptionName;
		this.status=status;
		this.createdDate=createdDate;
		this.redFlagQuestionId=redFlagQuestionId;
		this.redFlagOptionId=redFlagOptionId;
		
	}
	
	public RedFlagOptionDetails(Object redFlagOptionName, String status, Date date, String redFlagQuestionId) {
		this.redFlagOptionName=(String) redFlagOptionName;
		this.status=status;
		this.createdDate=date;
		this.redFlagQuestionId=redFlagQuestionId;
	
	}
	public String getRedFlagOptionId() {
		return redFlagOptionId;
	}
	public void setRedFlagOptionId(String redFlagOptionId) {
		this.redFlagOptionId = redFlagOptionId;
	}
	public String getRedFlagOptionName() {
		return redFlagOptionName;
	}
	public void setRedFlagOptionName(String redFlagOptionName) {
		this.redFlagOptionName = redFlagOptionName;
	}
	
	public String getRedFlagQuestionId() {
		return redFlagQuestionId;
	}
	public void setRedFlagQuestionId(String redFlagQuestionId) {
		this.redFlagQuestionId = redFlagQuestionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	

}
