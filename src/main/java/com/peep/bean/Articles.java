package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class Articles {

	
	@Id
	String articleId;
	String articleName;
	String articleUrl;
	String status;
	Date createdDate;
	Date updatedDate;
	public Articles()
	{
		
	}
	public Articles(String articleName,String articleUrl, String status, Date createdDate) {
		this.articleName=articleName;
		this.articleUrl=articleUrl;
		this.status=status;
		this.createdDate=createdDate;
	}
	public Articles(String articleId, String articleName, String articleUrl, String status, Date createdDate) {
		this.articleUrl=articleUrl;
		this.articleName=articleName;
		this.status=status;
		this.createdDate=createdDate;
		this.articleId=articleId;
	}
	public String getArticleId() {
		return articleId;
	}
	public void setArticleId(String articleId) {
		this.articleId = articleId;
	}
	
	public String getArticleName() {
		return articleName;
	}
	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}
	public String getArticleUrl() {
		return articleUrl;
	}
	public void setArticleUrl(String articleUrl) {
		this.articleUrl = articleUrl;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	

	
	

}
