package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class QuestionDetails {

	
	@Id
	String questionId;
	String questionName;
	Date createdDate;
	Date updatedDate;
	String status;
	String activityId;
	String correctOptionId;

	@Transient
	String activityName;
	
	@Transient
	String optionName;
	
	public QuestionDetails()
	{
		
	}
	public QuestionDetails(String questionName, String status, Date createdDate, String activityId,String correctOptionId) {
		this.questionName=questionName;
		this.status=status;
		this.createdDate=createdDate;
		this.activityId=activityId;
		this.correctOptionId=correctOptionId;
	
	}
	public QuestionDetails(String questionId, String questionName, String status, Date createdDate,
			String activityId) {
		this.questionName=questionName;
		this.status=status;
		this.createdDate=createdDate;
		this.activityId=activityId;
		this.questionId=questionId;
		
		
	}
	
	public QuestionDetails(String questionId, String status, Date createdDate, String activityId, String correctOptionId,String questionName) {
		this.correctOptionId=correctOptionId;
		this.questionName=questionName;
		this.status=status;
		this.createdDate=createdDate;
		this.activityId=activityId;
		this.questionId=questionId;
	}
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getCorrectOptionId() {
		return correctOptionId;
	}
	public void setCorrectOptionId(String correctOptionId) {
		this.correctOptionId = correctOptionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	

}
