package com.peep.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "login")
//@CompoundIndexes({@CompoundIndex(name="loginIndex", unique= true, def="{'userId':1}")})
public class Login 
{
	@Id
	private String userId;
	private String userName;
	private String passWord;
	private String type;
	private String employeeId;
	
	public Login() 
	{
		
	}

	public Login(String userId, String userName, String passWord, String type, String employeeId) 
	{
		super();
		this.userId = userId;
		this.userName = userName;
		this.passWord = passWord;
		this.type = type;
		this.employeeId = employeeId;
	}

	public String getUserId() 
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getUserName() 
	{
		return userName;
	}

	public void setUserName(String userName) 
	{
		this.userName = userName;
	}

	public String getPassWord() 
	{
		return passWord;
	}

	public void setPassWord(String passWord) 
	{
		this.passWord = passWord;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type) 
	{
		this.type = type;
	}

	public String getEmployeeId() 
	{
		return employeeId;
	}

	public void setEmployeeId(String employeeId)
	{
		this.employeeId = employeeId;
	}
	
}
