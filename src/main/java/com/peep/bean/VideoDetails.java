package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class VideoDetails {

	
	@Id
	String videoId;
	String videoLink;
	Date createdDate;
	Date updatedDate;
	String status;
	String activityId;
	
	@Transient
	String activityName;

	public VideoDetails()
	{
		
	}
	public VideoDetails(String videoLink, String status,Date createdDate, String activityId) {
		this.videoLink=videoLink;
		this.status=status;
		this.createdDate=createdDate;
		this.activityId=activityId;
	}
	public VideoDetails(String videoId, String videoLink, String status, Date createdDate, String activityId) {
		// TODO Auto-generated constructor stub
		this.videoLink=videoLink;
		this.status=status;
		this.createdDate=createdDate;
		this.activityId=activityId;
		this.videoId=videoId;
	}
	
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	public String getVideoLink() {
		return videoLink;
	}
	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	
	
	

}
