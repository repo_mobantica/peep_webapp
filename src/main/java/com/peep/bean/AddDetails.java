package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class AddDetails
{
	
	String id;
	String moduleType;
	String status;
	Date createdDate;
	Date updatedDate;
	String moduleIcon;
	String questionName;
	String activityName;
	String optionName;
	String videoLink;
	
	public AddDetails() 
	{
		
	}
	
	public AddDetails(String moduleType, String status, Date createdDate) 
	{
		super();
		this.moduleType = moduleType;
		this.status = status;
		this.createdDate = createdDate;	
	}
	
	public AddDetails(String id,String moduleType, String status, Date createdDate) 
	{
		super();
		this.id=id;
		this.moduleType = moduleType;
		this.status = status;
		this.createdDate = createdDate;	
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getModuleType() {
		return moduleType;
	}
	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}
	public String getStatus() {
		return status;
	}
	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public String getVideoLink() {
		return videoLink;
	}

	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getModuleIcon() {
		return moduleIcon;
	}
	public void setModuleIcon(String moduleIcon) {
		this.moduleIcon = moduleIcon;
	}
	
	
	

}
