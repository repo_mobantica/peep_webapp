package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class RedFlagQuestionDetails {

	
	@Id
	String redFlagQuestionId;
	String redFlagQuestionName;
	Date createdDate;
	Date updatedDate;
	String status;
	String correctOptionId;

	@Transient
	String redFlagOptionName;

	public RedFlagQuestionDetails()
	{
		
	}
	public RedFlagQuestionDetails(String redFlagQuestionId, String status, Date createdDate, String correctOptionId,
			String redFlagQuestionName) {
		this.redFlagQuestionId=redFlagQuestionId;
		this.status=status;
		this.createdDate=createdDate;
		this.correctOptionId=correctOptionId;
		this.redFlagQuestionName=redFlagQuestionName;
		
	}
	public RedFlagQuestionDetails(String status, Date createdDate, String correctOptionId, String redFlagQuestionName) {
		this.status=status;
		this.createdDate=createdDate;
		this.correctOptionId=correctOptionId;
		this.redFlagQuestionName=redFlagQuestionName;
	}
	public String getRedFlagQuestionId() {
		return redFlagQuestionId;
	}
	public void setRedFlagQuestionId(String redFlagQuestionId) {
		this.redFlagQuestionId = redFlagQuestionId;
	}
	public String getRedFlagQuestionName() {
		return redFlagQuestionName;
	}
	public void setRedFlagQuestionName(String redFlagQuestionName) {
		this.redFlagQuestionName = redFlagQuestionName;
	}
	public String getCorrectOptionId() {
		return correctOptionId;
	}
	public void setCorrectOptionId(String correctOptionId) {
		this.correctOptionId = correctOptionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public String getRedFlagOptionName() {
		return redFlagOptionName;
	}
	public void setRedFlagOptionName(String redFlagOptionName) {
		this.redFlagOptionName = redFlagOptionName;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	

}
