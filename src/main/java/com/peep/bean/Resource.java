package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class Resource {

	
	@Id
	String resourceId;
	String videoUrl;
	String resourceName;
	String status;
	Date createdDate;
	Date updatedDate;
	public Resource()
	{
		
	}
	public Resource(String videoUrl, String status, Date createdDate, String resourceName) {
	 this.videoUrl=videoUrl;
	 this.status=status;
	 this.createdDate=createdDate;
	 this.resourceName=resourceName;
	}
	public Resource(String resourceId, String videoUrl, String status, Date createdDate, String resourceName) {
		this.videoUrl=videoUrl;
		 this.status=status;
		 this.createdDate=createdDate;
		 this.resourceName=resourceName;
		 this.resourceId=resourceId;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	public String getVideoUrl() {
		return videoUrl;
	}
	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	

}
