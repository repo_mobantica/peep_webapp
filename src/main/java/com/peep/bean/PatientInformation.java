package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class PatientInformation {
	@Id
	String id;
	String firstName;
	String lastName;
	String emailId;
	String phoneNUmber;
	String socialSecurityNumber;
	String dateOfBirth;
	String zipCode;
	String country;
	String gender;
	String language;
	String cancerTypeId;
	String stageOfChemotherapy;
	String username;
	String password;
	String hospitalName;
	String hospitalPhysician;
	String hospitalEmailid;
	String hosipitalPhoneNumber;
	String age;
	String activation_status;
	String status;
	Date createdDate;
	Date updatedDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhoneNUmber() {
		return phoneNUmber;
	}
	public void setPhoneNUmber(String phoneNUmber) {
		this.phoneNUmber = phoneNUmber;
	}
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getCancerTypeId() {
		return cancerTypeId;
	}
	public void setCancerTypeId(String cancerTypeId) {
		this.cancerTypeId = cancerTypeId;
	}
	public String getStageOfChemotherapy() {
		return stageOfChemotherapy;
	}
	public void setStageOfChemotherapy(String stageOfChemotherapy) {
		this.stageOfChemotherapy = stageOfChemotherapy;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getHospitalName() {
		return hospitalName;
	}
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	public String getHospitalPhysician() {
		return hospitalPhysician;
	}
	public void setHospitalPhysician(String hospitalPhysician) {
		this.hospitalPhysician = hospitalPhysician;
	}
	public String getHospitalEmailid() {
		return hospitalEmailid;
	}
	public void setHospitalEmailid(String hospitalEmailid) {
		this.hospitalEmailid = hospitalEmailid;
	}
	public String getHosipitalPhoneNumber() {
		return hosipitalPhoneNumber;
	}
	public void setHosipitalPhoneNumber(String hosipitalPhoneNumber) {
		this.hosipitalPhoneNumber = hosipitalPhoneNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getActivation_status() {
		return activation_status;
	}
	public void setActivation_status(String activation_status) {
		this.activation_status = activation_status;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	
	
}
