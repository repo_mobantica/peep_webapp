package com.peep.bean;


import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class ActivityDetails
{
	@Id
	String activityId;
	String activityName;
	Date createdDate;
	Date updatedDate;
	String status;
	String moduleId;

	@Transient
	String moduleType;
	
	
	
	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public ActivityDetails()
	{
		
	}
	
	public ActivityDetails(String activityName, String status, Date createdDate, String moduleId) {
		this.activityName=activityName;
		this.status=status;
		this.createdDate=createdDate;
		this.moduleId=moduleId;
		
	}
	public ActivityDetails(String activityId, String activityName, String status, Date createdDate,
			String moduleId) {
		// TODO Auto-generated constructor stub
		this.activityName=activityName;
		this.status=status;
		this.createdDate=createdDate;
		this.moduleId=moduleId;
		this.activityId=activityId;
	}

	public ActivityDetails(String moduleType, String status, Date createdDate) {
		this.moduleType=moduleType;
		this.status=status;
		this.createdDate=createdDate;
	}



	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


}
