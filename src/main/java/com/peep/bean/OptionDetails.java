package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
@Document
public class OptionDetails {

	
	@Id
	String optionId;
	
	String optionName;
	Date createdDate;
	Date updatedDate;
	String status;
	String questionId;
	
	@Transient
	String questionName;
	
	

	public OptionDetails()
	{
		
	}
	public OptionDetails(String optionName, String status, Date createdDate, String questionId) {
		
		this.optionName=optionName;
		this.createdDate=createdDate;
		this.status=status;
		this.questionId=questionId;
	}
	public OptionDetails(String optionId, String optionName, String status, Date createdDate, String questionId) {
		this.optionName=optionName;
		this.createdDate=createdDate;
		this.status=status;
		this.questionId=questionId;
		this.optionId=optionId;
	}
	
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public String getOptionId() {
		return optionId;
	}
	public void setOptionId(String optionId) {
		this.optionId = optionId;
	}
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	

}
