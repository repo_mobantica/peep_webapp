package com.peep.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ModuleType
{
	@Id
	String id;
	String moduleType;
	String status;
	Date   createdDate;
	Date   updatedDate;
	String moduleIcon;
	
	@Transient
	String questionName;
	
	@Transient
	String createdDate1;
	
	@Transient
	String activityName;
	
	@Transient
	String optionName;
	
	@Transient
	String videoLink;
	
	public ModuleType() 
	{
		
	}

	public ModuleType(String id, String moduleType, String status, Date createdDate, Date updatedDate,
			String moduleIcon, String questionName, String activityName, String optionName, String videoLink)
	{
		super();
		this.id = id;
		this.moduleType = moduleType;
		this.status = status;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.moduleIcon = moduleIcon;
		this.questionName = questionName;
		this.activityName = activityName;
		this.optionName = optionName;
		this.videoLink = videoLink;
	}

	public ModuleType(String moduleType, String status, Date createdDate,String moduleIcon) {
		this.moduleType = moduleType;
		this.status = status;
		this.createdDate = createdDate;
		this.moduleIcon = moduleIcon;
	}

	public ModuleType(String moduleType, String status, Date createdDate, String moduleIcon, String id) {
		this.moduleType = moduleType;
		this.status = status;
		this.createdDate = createdDate;
		this.moduleIcon = moduleIcon;
		this.id = id;
	}

	public String getCreatedDate1() {
		return createdDate1;
	}

	public void setCreatedDate1(String createdDate1) {
		this.createdDate1 = createdDate1;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getModuleIcon() {
		return moduleIcon;
	}

	public void setModuleIcon(String moduleIcon) {
		this.moduleIcon = moduleIcon;
	}

	public String getQuestionName() {
		return questionName;
	}

	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public String getVideoLink() {
		return videoLink;
	}

	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}
	
}
