package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.peep.bean.ModuleType;



public interface ModuleTypeRepository extends MongoRepository<ModuleType, String>
{
   public List<ModuleType> findAll(Sort sort);

public ModuleType findById(String moduleTypeId);

public List<ModuleType> findByStatus(String string);

public List<ModuleType> findByStatusAndId(String string, String moduleId);
   
}