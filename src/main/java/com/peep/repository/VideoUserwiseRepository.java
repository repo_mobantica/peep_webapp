package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.transaction.annotation.Transactional;

import com.peep.bean.VideoDetails;
import com.peep.bean.VideoDetailsUserwise;



public interface VideoUserwiseRepository extends MongoRepository<VideoDetailsUserwise, String> {
	public List<VideoDetailsUserwise> findByStatus(String status);

	public List<VideoDetailsUserwise> findByStatusAndUserId(String string, String id);
	   public List<VideoDetailsUserwise> findAll(Sort sort);

	
}
