package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.peep.bean.ActivityDetails;
import com.peep.bean.Answers;
import com.peep.bean.ModuleType;
import com.peep.bean.QuestionDetailsUserwise;
import com.peep.bean.VideoDetails;



public interface QuestionDetailsUserWiseRepository extends MongoRepository<QuestionDetailsUserwise, String>
{
   public List<QuestionDetailsUserwise> findAll(Sort sort);
   
}