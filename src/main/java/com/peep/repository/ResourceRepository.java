package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;
import com.peep.bean.Resource;
import com.peep.bean.VideoDetails;



public interface ResourceRepository extends MongoRepository<Resource, String>
{
   public List<Resource> findAll(Sort sort);

public List<Resource> findByStatus(String string);
   
}