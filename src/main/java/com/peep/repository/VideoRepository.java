package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;
import com.peep.bean.VideoDetails;



public interface VideoRepository extends MongoRepository<VideoDetails, String>
{
   public List<VideoDetails> findAll(Sort sort);
   
}