package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.peep.bean.PatientInformation;



public interface PatientRepository extends MongoRepository<PatientInformation, String>
{
   public List<PatientInformation> findAll(Sort sort);

public PatientInformation findById(String id);

public List<PatientInformation> findByStatus(String string);

public PatientInformation findByEmailId(String emailId);
   
}