package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep.bean.RedFlagOptionDetails;
import com.peep.bean.RedFlagQuestionDetails;

public interface RedFlagQuestionsRepository extends MongoRepository<RedFlagQuestionDetails, String> {
	public List<RedFlagQuestionDetails> findByStatus(String status);
	public List<RedFlagQuestionDetails> findAll(Sort sort);
	public RedFlagQuestionDetails findByStatusAndRedFlagQuestionId(String string, String questionId);
	
}
