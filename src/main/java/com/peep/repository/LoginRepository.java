package com.peep.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.peep.bean.Login;

public interface LoginRepository extends MongoRepository<Login, String>
{
		
}
