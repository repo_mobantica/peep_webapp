package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep.bean.QuestionDetails;
import com.peep.bean.RedFlagOptionDetails;



@Transactional
public interface RedFlagOptionRepository extends MongoRepository<RedFlagOptionDetails, String> {
	public List<RedFlagOptionDetails> findByStatus(String status);

	public List<RedFlagOptionDetails> findByStatusAndRedFlagQuestionId(String string, String questionId);

	public List<RedFlagOptionDetails> findByStatusAndRedFlagOptionId(String string, String redFlagQuestionId);

	   public List<RedFlagOptionDetails> findAll(Sort sort);
	
}
