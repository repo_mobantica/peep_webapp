package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.transaction.annotation.Transactional;

import com.peep.bean.FeedBackUserWise;
import com.peep.bean.VideoDetails;
import com.peep.bean.VideoDetailsUserwise;



public interface FeedBackUserwiseRepository extends MongoRepository<FeedBackUserWise, String> {
	public List<FeedBackUserWise> findByStatus(String status);

	public List<FeedBackUserWise> findByStatusAndUserId(String string, String id);
	   public List<FeedBackUserWise> findAll(Sort sort);

	
}
