package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.peep.bean.ActivityDetails;
import com.peep.bean.Answers;
import com.peep.bean.ModuleType;
import com.peep.bean.VideoDetails;



public interface AnswerRepository extends MongoRepository<Answers, String>
{
   public List<Answers> findAll(Sort sort);
   
}