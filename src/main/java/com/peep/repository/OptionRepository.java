package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;
import com.peep.bean.OptionDetails;
import com.peep.bean.VideoDetails;



public interface OptionRepository extends MongoRepository<OptionDetails, String>
{
   public List<OptionDetails> findAll(Sort sort);
   
}