package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;



public interface ActivityRepository extends MongoRepository<ActivityDetails, String>
{
   public List<ActivityDetails> findAll(Sort sort);

public ActivityDetails findByStatusAndModuleId(String string, String moduleTypeId);

public List<ActivityDetails> findByStatus(String string);

public ActivityDetails findByActivityId(String activityId);
   
}