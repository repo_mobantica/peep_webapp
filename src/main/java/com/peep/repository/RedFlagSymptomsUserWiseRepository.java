package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep.bean.RedFlagSymptomsUserWise;


public interface RedFlagSymptomsUserWiseRepository extends MongoRepository<RedFlagSymptomsUserWise, String> {

	public List<RedFlagSymptomsUserWise> findAll(Sort sort);
	

	
}
