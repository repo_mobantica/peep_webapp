package com.peep.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peep.bean.CancerType;

@Transactional
public interface CancerTypeRepository extends MongoRepository<CancerType, String> {
	public List<CancerType> findByStatus(String status);

	public List<CancerType> findByStatusAndId(String string, String cancerTypeId);

	
}
