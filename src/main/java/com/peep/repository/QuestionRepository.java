package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.peep.bean.ActivityDetails;
import com.peep.bean.ModuleType;
import com.peep.bean.QuestionDetails;
import com.peep.bean.VideoDetails;



public interface QuestionRepository extends MongoRepository<QuestionDetails, String>
{
   public List<QuestionDetails> findAll(Sort sort);

public QuestionDetails findByStatusAndQuestionId(String string, String questionId);
   
}