package com.peep.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.peep.bean.ActivityDetails;
import com.peep.bean.Articles;
import com.peep.bean.ModuleType;
import com.peep.bean.VideoDetails;



public interface ArticleRepository extends MongoRepository<Articles, String>
{
   public List<Articles> findAll(Sort sort);

public List<Articles> findByStatus(String string);


   
}