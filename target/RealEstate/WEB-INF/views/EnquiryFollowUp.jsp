<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Enquiry Follow-up</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>

                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Alexander Pierce</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
        </div>
      </div>
    
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
	  
		 <li class="treeview">
          <a href="#">
              <li><a href="home.jsp"><i class="fa fa-home"></i> Home</a></li>
            <span class="pull-right-container">
            </span>
          </a>
          
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Administrator Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="treeview">
			    <li ><a href="AddCountry.jsp"><i class="fa fa-circle-o"></i> Add Country</a></li>
            <li><a href="AddState.jsp"><i class="fa fa-circle-o"></i> Add State</a></li>
            <li><a href="AddCity.jsp"><i class="fa fa-circle-o"></i> Add City</a></li>
            <li><a href="AddLocationArea.jsp"><i class="fa fa-circle-o"></i> Add Location</a></li>
			<li><a href="AddCurrency.jsp"><i class="fa fa-circle-o"></i> Add Currency</a></li>
			<li><a href="AddBank.jsp"><i class="fa fa-circle-o "></i> Add Bank</a></li>
			 <li><a href="AddCompany.jsp"><i class="fa fa-circle-o"></i> Add Company</a></li>
            <li><a href="AddDepartment.jsp"><i class="fa fa-circle-o"></i> Add Department</a></li>
            <li><a href="AddEmployee.jsp"><i class="fa fa-circle-o"></i> Add Employee</a></li>
            <li><a href="AddDesignation.jsp"><i class="fa fa-circle-o"></i> Add Designation</a></li>
			<li><a href="AddProject.jsp"><i class="fa fa-circle-o"></i> Add Project</a></li>
            <li><a href="AddProjectUnit.jsp"><i class="fa fa-circle-o"></i> Add Project Unit</a></li>
            <li><a href="AddFloor.jsp"><i class="fa fa-circle-o"></i> Add Floor</a></li>
            <li><a href="AddFloorType.jsp"><i class="fa fa-circle-o"></i> Add Floor Type</a></li>
            <li><a href="AddFlatFacingType.jsp"><i class="fa fa-circle-o"></i> Add Flat Facing Type</a></li>
            <li><a href="AddFlat.jsp"><i class="fa fa-circle-o"></i> Add Flat</a></li>
			<li><a href="AddStore.jsp"><i class="fa fa-circle-o"></i> Add Store</a></li>
			<li><a href="AddItemUnit.jsp"><i class="fa fa-circle-o"></i> Add Item Unit</a></li>
			<li><a href="AddItemCategory.jsp"><i class="fa fa-circle-o"></i> Add Item Category</a></li>
			<li><a href="AddItem.jsp"><i class="fa fa-circle-o"></i> Add Items</a></li>
            <li><a href="AddContractor.jsp"><i class="fa fa-circle-o"></i> Add Contractor</a></li>
            <li><a href="AddSupplierType.jsp"><i class="fa fa-circle-o"></i> Add Supplier Type</a></li>
            <li><a href="AddSupplier.jsp"><i class="fa fa-circle-o"></i> Add Supplier</a></li>
            <li><a href="AddTax.jsp"><i class="fa fa-circle-o"></i> Add Tax</a></li>
            <li><a href="AddEnquirySource.jsp"><i class="fa fa-circle-o"></i> Add Enquiry Source</a></li>
            <li><a href="AddOccupation.jsp"><i class="fa fa-circle-o"></i> Add Occupation</a></li>
            <li><a href="AddYear.jsp"><i class="fa fa-circle-o"></i> Add Financial Year</a></li>
            <li><a href="AddBudget.jsp"><i class="fa fa-circle-o"></i> Add Budget</a></li>
			<li><a href="AddChartofAccounts.jsp"><i class="fa fa-circle-o"></i> Add Chart Of Account</a></li>
            <li><a href="AddSubChartofAccounts.jsp"><i class="fa fa-circle-o"></i> Add Sub-Chart Of Account</a></li>
			  
			
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
           
            </li>
			 </ul>
		  </li>
		
		
		<li class="treeview  active">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Pre-Sale</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddEnquiry.jsp"><i class="fa fa-circle-o"></i>Add Enquiry</a></li>
            <li class="active" style="background-color:#48D1CC"><a href="EnquiryFollowUp.jsp"><i class="fa fa-circle-o"></i> Todays Follow-up</a></li>
			
          </ul>
        </li>
		
        
        
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Sales Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add Booking</a></li>
			
          </ul>
        </li>
      
     
		<li class="treeview">
          <a href="#">
            <i class="fa fa-tasks"></i>
            <span>Assign Task</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddTask.jsp"><i class="fa fa-circle-o"></i> Add Task</a></li>
			
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>HR Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> HR and Payroll</a></li>
			
          </ul>
        </li>
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Site Manager</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add </a></li>
			
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Project Engineering</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add</a></li>
		  </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Purchase management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Purchase</a></li>
		  </ul>
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Contractors Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Contractors Management</a></li>
		  </ul>
        </li>
		
			<li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Documentation</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add Documentation</a></li>
			
          </ul>
        </li>
	 </ul>
	  
	  
    </section>
    
    
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Enquiry Follow-up:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Enquiry Follow-up</li>
      </ol>
    </section>

    <!-- Main content -->
    
<form name="enquiryfollowupform" action="#" onSubmit="return validate()">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
              <!-- /.form-group -->
             
			
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label for="enquairyid">Enquiry Id</label>
                  <input type="text" class="form-control" id="enquairyid" placeholder="ID" name="enquiryid"  disabled>
                </div>               
                <div class="col-xs-6">
                  <label>Enquiry Date </label>
                     <input type="text" class="form-control" id="enquirydate" disabled>
                </div>  
              </div>
            </div>
				
				
				<div class="box-body">
              <div class="row">
                <div class="col-xs-12">
				<label for="enquairyfirstname">Full Name</label>
                  <input type="text" class="form-control" id="enquairyfirstname" placeholder="Full Name" name="enquairyfirstname"  disabled>
                </div>
                
              </div>
            </div>
				
			
				
				 <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			   <label for="radiobutton">Sex </label></br>
               <input type="radio" name="r1" class="minimal" checked>
			    <label for="radiobutton">Male</label>
				&nbsp &nbsp &nbsp &nbsp&nbsp&nbsp&nbsp
                <input type="radio" name="r2" class="minimal">
				  <label for="radiobutton">Female</label>
                 </div> 
			 </div>
            </div>
				
				
				
				    <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			   <label for="enquairyaddress">Address </label>
                 <textarea class="form-control" rows="1" id="enquairyaddress" placeholder="Address"  disabled></textarea>
			     </div> 
				  
              </div>
            </div>
				
				
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label>Country </label>
                     <input type="text" class="form-control" id="countryname" placeholder="Country Name"  disabled>
                </div>               
                <div class="col-xs-6">
				 <label>State </label>
                     <input type="text" class="form-control" id="statename" placeholder="State Name"  disabled>
				</div> 
              </div>
            </div>
				
			
			
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
				  <label>City </label>
                   <input type="text" class="form-control" id="cityname" placeholder="City Name"  disabled>
				  </div> 
				    <div class="col-xs-6">
				    <label>Area </label>
                    <input type="text" class="form-control" id="areaname" placeholder="Area Name"  disabled>
				  </div> 
              </div>
            </div>
			
			 
				   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			   
                  <label>Email Id </label>
                    <input type="text" class="form-control" id="emailid" placeholder="" name="emailid" disabled>
			     </div> 
				 
              </div>
            </div>
			
					<div class="form-group">
               
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label for="mobileno1">Mobile No(Primary)</label>
                    <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" name="mobileno1"  disabled>
                </div>
			     </div> 
				  <div class="col-xs-6">
			      <label for="officephoneno">Mobile Number</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" name="mobileno2"  disabled>
                </div>
			     </div> 
              </div>
            </div>
			   
			   
              </div>
		
			
			
			  
				<div class="form-group">
                <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label for="occupation">Occupation </label> 
                  <input type="text" class="form-control" id="occupation" placeholder="Occupation" name="occupation"  disabled>
				</div>
            </div>
			  </div>
			   </div>
			  
			<div class="form-group">
               
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label for="campanyname">Company Name </label>
                  <input type="text" class="form-control" id="companyname" placeholder="Company Name " name="companyname"  disabled>
                
			     </div> 
				  <div class="col-xs-6">
			      <label for="officephoneno">Office Phone Number</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" name="officephoneno"  disabled>
                </div>
			     </div> 
              </div>
            </div>
			   
			   
              </div>
			  
			  
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			   	<label for="preferbank">Prefer Bank </label>
                    <input type="text" class="form-control" id="preferbank" placeholder="Bank Name" name="preferbank"  disabled>
			     </div> 
				  <div class="col-xs-6">
			   	 <label for="preferbanks">Enquiry Source </label>
				   <input type="text" class="form-control" id="enquirysource" placeholder="Enquiry Source" name="enquirysource"  disabled>
              
			     </div>
				 
              </div>
            </div>
             
            </div>
          
            <div class="col-md-6">
			
			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
				<label>Project</label>
                     <input type="text" class="form-control" id="projectname" placeholder="Project Name" name="projectname"  disabled>
			     </div> 
              </div>
            </div>
			 
			 
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			     <label>Project Unit</label>
                    <input type="text" class="form-control" id="projectunitname" placeholder="Project Unit Name" name="projectunitname"  disabled>
			     </div> 
              </div>
            </div>
					
					
				


			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			      <label>Floor  </label>
                    <input type="text" class="form-control" id="floorname" placeholder="Floor Name" name="floorname"  disabled>
			     </div> 
              </div>
            </div>
			
			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			    <label>Flat Type  </label>
                     <input type="text" class="form-control" id="flattype" placeholder="Flat Type" name="flattype"  disabled>
			     </div> 
              </div>
            </div>
			
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
				  
                  <label>Flat Facing  </label>
                  <input type="text" class="form-control" id="flatfacing" placeholder="Flat Facing " name="flatfacing"  disabled>
			     </div> 
              </div>
            </div>
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
                  <label for="flatarea">Flat Area in SQ.FT </label>
                  <input type="text" class="form-control" id="flatarea" placeholder="Flat Area in SQ.FT " name="flatarea"  disabled>
			     </div> 
              </div>
            </div>
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
                  <label for="flatcost">Flat Cost </label>
                  <input type="text" class="form-control" id="flatcost" placeholder="Flat Cost " name="flatcost"  disabled>
			     </div> 
              </div>
            </div>
			
				
				
			   <div class="box-body">
              <div class="row">
				  <div class="col-xs-12">
			    <label>Budget</label>
                <input type="text" class="form-control" id="budget" placeholder="Budget" name="budget" disabled>
			     </div> 
              </div>
            </div>
			 
			   
			    <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			    <label>Next Follow-up Date</label><label class="text-red">* </label>
              <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker" name="nextfollowupdate">
                </div>
			     </div> 
              </div>
            </div>
			   
				
				    <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			   <label for="enquairyremark">Remark </label><label class="text-red">* </label>
                 <textarea class="form-control" rows="2" id="enquairyremark" placeholder="Remark" name="enquairyremark"></textarea>
			     </div> 
				  
              </div>
            </div>
			    <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			   <label for="status">Status </label><label class="text-red">* </label>
			   <select class="form-control" name="status">
				  <option selected="" value="Default">-Status-</option>
                    <option>In-Process</option>
                    <option>Site Visited</option>
                    <option>Booking</option>
                    <option>Cancel</option>
                  </select>
			     </div> 
				  
              </div>
            </div>
			 </br>
			
              <!-- /.form-group -->
			  
            </div>
			
            <!-- /.col -->
			
          </div>
          
		  	     <div class="box-body">
              <div class="row">
              </br>
                  <div class="col-xs-3">
                  </div>
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Previus Follow-up</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="" class="table table-bordered table-striped" >
                <thead>
                <tr>
                  <th>Sr.No</th>
                  <th>Follow-up Date</th>
                  <th>Remark</th>
                  <th>Status</th>
				  <th>Enquiry Taken User Name</th>
                </tr>
                </thead>
				
                <tbody>
               
                <tr>
                  <td>1</td>
                  <td>1/1/2018</td>
                  <td>Will call</td>
                  <td>Process</td>
                  <td>Bahirnath</td>
                </tr>
             
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
      <!-- /.box -->

     
    </section>
    </form>
    <!-- /.content -->
  </div>
  
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>

function validate()
{ 
		if(document.enquiryfollowupform.nextfollowupdate.value=="")
		{
			alert("Please Fill Nest Follow-up Date");
			document.enquiryfollowupform.nextfollowupdate.focus();
			return false;
		}
		if(document.enquiryfollowupform.enquairyremark.value=="")
		{
			alert("Please Fill Remark");
			document.enquiryfollowupform.enquairyremark.focus();
			return false;
		}
		if(document.enquiryfollowupform.status.value=="Default")
		{
			alert("Please Select Status");
			document.enquiryfollowupform.status.focus();
			return false;
		}
		
}


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
