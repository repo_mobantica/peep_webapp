<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate |Import New Project</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Sonigara</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>

                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Alexander Pierce</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
     <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
        </div>
      </div>
      
      <ul class="sidebar-menu" data-widget="tree">
		
		
		 <li class="treeview">
          <a href="#">
              <li><a href="home.jsp"><i class="fa fa-home"></i> Home</a></li>
            <span class="pull-right-container">
            </span>
          </a>
          
        </li>
		
		<li class="treeview active">
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Administrator Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview active">
             <li ><a href="AddCountry.jsp"><i class="fa fa-circle-o"></i> Add Country</a></li>
            <li><a href="AddState.jsp"><i class="fa fa-circle-o"></i> Add State</a></li>
            <li><a href="AddCity.jsp"><i class="fa fa-circle-o"></i> Add City</a></li>
            <li><a href="AddLocationArea.jsp"><i class="fa fa-circle-o"></i> Add Location</a></li>
			<li><a href="AddCurrency.jsp"><i class="fa fa-circle-o"></i> Add Currency</a></li>
			<li><a href="AddBank.jsp"><i class="fa fa-circle-o"></i> Add Bank</a></li>
			 <li><a href="AddCompany.jsp"><i class="fa fa-circle-o"></i> Add Company</a></li>
            <li><a href="AddDepartment.jsp"><i class="fa fa-circle-o"></i> Add Department</a></li>
            <li><a href="AddEmployee.jsp"><i class="fa fa-circle-o"></i> Add Employee</a></li>
            <li><a href="AddDesignation.jsp"><i class="fa fa-circle-o"></i> Add Designation</a></li>
			<li><a href="AddProject.jsp"><i class="fa fa-circle-o"></i> Add Project</a></li>
            <li class="active"  style="background-color:#48D1CC"><a href="AddProjectUnit.jsp"><i class="fa fa-circle-o"></i> Add Project Unit</a></li>
            <li><a href="AddFloor.jsp"><i class="fa fa-circle-o"></i> Add Floor</a></li>
            <li><a href="AddFloorType.jsp"><i class="fa fa-circle-o"></i> Add Floor Type</a></li>
            <li><a href="AddFlatFacingType.jsp"><i class="fa fa-circle-o"></i> Add Flat Facing Type</a></li>
            <li><a href="AddFlat.jsp"><i class="fa fa-circle-o"></i> Add Flat</a></li>
			<li><a href="AddStore.jsp"><i class="fa fa-circle-o"></i> Add Store</a></li>
			<li><a href="AddItemUnit.jsp"><i class="fa fa-circle-o"></i> Add Item Unit</a></li>
			<li><a href="AddItemCategory.jsp"><i class="fa fa-circle-o"></i> Add Item Category</a></li>
			<li><a href="AddItem.jsp"><i class="fa fa-circle-o"></i> Add Items</a></li>
            <li><a href="AddContractor.jsp"><i class="fa fa-circle-o"></i> Add Contractor</a></li>
            <li><a href="AddSupplierType.jsp"><i class="fa fa-circle-o"></i> Add Supplier Type</a></li>
            <li><a href="AddSupplier.jsp"><i class="fa fa-circle-o"></i> Add Supplier</a></li>
            <li><a href="AddTax.jsp"><i class="fa fa-circle-o"></i> Add Tax</a></li>
            <li><a href="AddEnquirySource.jsp"><i class="fa fa-circle-o"></i> Add Enquiry Source</a></li>
            <li><a href="AddOccupation.jsp"><i class="fa fa-circle-o"></i> Add Occupation</a></li>
            <li><a href="AddYear.jsp"><i class="fa fa-circle-o"></i> Add Financial Year</a></li>
            <li><a href="AddBudget.jsp"><i class="fa fa-circle-o"></i> Add Budget</a></li>
			<li><a href="AddChartofAccounts.jsp"><i class="fa fa-circle-o"></i> Add Chart Of Account</a></li>
            <li><a href="AddSubChartofAccounts.jsp"><i class="fa fa-circle-o"></i> Add Sub-Chart Of Account</a></li>
			  
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
           
            </li>
			 </ul>
		  </li>
		
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Pre-Sale</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddEnquiry.jsp"><i class="fa fa-circle-o"></i>Add Enquiry</a></li>
            <li><a href="EnquiryFollowUp.jsp"><i class="fa fa-circle-o"></i>Todays Follow-up</a></li>
			
          </ul>
        </li>
		
        
        
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Sales Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add Booking</a></li>
			
          </ul>
        </li>
      
     
		<li class="treeview">
          <a href="#">
            <i class="fa fa-tasks"></i>
            <span>Assign Task</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddTask.jsp"><i class="fa fa-circle-o"></i> Add Task</a></li>
			
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>HR & Payroll Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> HR and Payroll</a></li>
			
          </ul>
        </li>
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Site Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add </a></li>
			
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Project Engineering</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add</a></li>
		  </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Purchase management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Purchase</a></li>
		  </ul>
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Contractors Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Contractors Management</a></li>
		  </ul>
        </li>
		
			<li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Documentation</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add Documentation</a></li>
			
          </ul>
        </li>
		
       
	
		
      </ul>
    </section>
	
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Import Project Unit Details From Excle File:
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Import Project Unit From Excle File </li>
      </ol>
    </section>

<form name="importnewflatform" action="#" >
    <!-- Main content -->
    <section class="content">


      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        	<div class="box-body">
				<div class="form-horizontal">
											<div class="row">
					<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<label for="profile_img1" class="col-sm-4 control-label">Excel File
									</label>
								<div class="col-sm-5">
									<input type="file" accept=".xls" required="required" class="form-control" id="flat_excel" name="flat_excel"/>
								<label id="profile_img1-error"></label>
								</div>
								
								<div id="divchanged1">
                	
                                     </div>
							</div>
					</div>
				
						</div>
				</div>
					<div class="box-footer" style="text-align: center;">
			
				
				<span id="progressBar">
				<div class="progress progress-sm active">
                    <div class="progress-bar progress-bar-success progress-bar-striped" id="progBar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
               	</div>
				</span>
					<div class="box-body">
              <div class="row">
              </br>
                
				  <div class="col-xs-4">
				<button type="Cancle" class="btn btn-default" style="width:90px"><a href="AddProjectUnit.jsp">Cancle</a></button>
				
				</div>
				  <div class="col-xs-4">
				<button type="button" class="btn btn-success" onClick="uploadJqueryForm()">Import</button>&nbsp
	
				</div>
				</div>
				</div>
			</div>
			  <div class="box-header with-border">
                  <i class="fa fa-text-width"></i>
                  <h3 class="box-title">Import Instructions</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <dl class="dl-horizontal">
                    <dt>Template</dt>
                    <dd>Use Standard Template only for Import. You can download it from <a href="#">here</a>.</dd>
                  
                  </dl>
                 
                </div>
			</div>
        <!-- /.box-header -->
     
        <!-- /.box-body -->
             <div class="box">
            <div class="box-header">
              <h1 class="box-title">Selected Project Unit Details</h1>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="" class="table table-bordered table-striped" >
                <thead>
                <tr>
                  <th>Sr.No</th>
                  <th>Company Name</th>
                  <th>Project Name</th>
                  <th>Unit Name</th>
				  <th>No Of Foor</th>
				  <th>Status</th>
                </tr>
                </thead>
				
                <tbody>
               
                <tr>
                  <td>1</td>
                  <td>1/1/2018</td>
                  <td>Will call</td>
                  <td>Process</td>
                  <td>Bahirnath</td>
                  <td>1</td>
                </tr>
             
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
  
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
      

      </div>
	  
	  
	  
	  
	  
	  
	  
	  
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>


 


</script>
</body>
</html>
