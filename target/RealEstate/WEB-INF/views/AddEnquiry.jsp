<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Real Estate | Add Enquiry</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini" onLoad="init()">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Sonigara</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>

                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar"
                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Alexander Pierce</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
	
	
	
	
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
        </div>
      </div>
     
      <ul class="sidebar-menu" data-widget="tree">
	  
	  
		
		 <li class="treeview">
          <a href="#">
              <li><a href="home.jsp"><i class="fa fa-home"></i> Home</a></li>
            <span class="pull-right-container">
            </span>
          </a>
          
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Administrator Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview ">
               <li ><a href="AddCountry.jsp"><i class="fa fa-circle-o"></i> Add Country</a></li>
            <li><a href="AddState.jsp"><i class="fa fa-circle-o"></i> Add State</a></li>
            <li ><a href="AddCity.jsp"><i class="fa fa-circle-o"></i> Add City</a></li>
            <li><a href="AddLocationArea.jsp"><i class="fa fa-circle-o"></i> Add Location</a></li>
			<li><a href="AddCurrency.jsp"><i class="fa fa-circle-o"></i> Add Currency</a></li>
			<li><a href="AddBank.jsp"><i class="fa fa-circle-o"></i> Add Bank</a></li>
			 <li><a href="AddCompany.jsp"><i class="fa fa-circle-o"></i> Add Company</a></li>
            <li><a href="AddDepartment.jsp"><i class="fa fa-circle-o"></i> Add Department</a></li>
            <li><a href="AddEmployee.jsp"><i class="fa fa-circle-o"></i> Add Employee</a></li>
            <li><a href="AddDesignation.jsp"><i class="fa fa-circle-o"></i> Add Designation</a></li>
			<li><a href="AddProject.jsp"><i class="fa fa-circle-o"></i> Add Project</a></li>
            <li><a href="AddProjectUnit.jsp"><i class="fa fa-circle-o"></i> Add Project Unit</a></li>
            <li><a href="AddFloor.jsp"><i class="fa fa-circle-o"></i> Add Floor</a></li>
            <li><a href="AddFloorType.jsp"><i class="fa fa-circle-o"></i> Add Floor Type</a></li>
            <li><a href="AddFlatFacingType.jsp"><i class="fa fa-circle-o"></i> Add Flat Facing Type</a></li>
            <li><a href="AddFlat.jsp"><i class="fa fa-circle-o"></i> Add Flat</a></li>
			<li><a href="AddStore.jsp"><i class="fa fa-circle-o"></i> Add Store</a></li>
			<li><a href="AddItemUnit.jsp"><i class="fa fa-circle-o"></i> Add Item Unit</a></li>
			<li><a href="AddItemCategory.jsp"><i class="fa fa-circle-o"></i> Add Item Category</a></li>
			<li><a href="AddItem.jsp"><i class="fa fa-circle-o"></i> Add Items</a></li>
            <li><a href="AddContractor.jsp"><i class="fa fa-circle-o"></i> Add Contractor</a></li>
            <li><a href="AddSupplierType.jsp"><i class="fa fa-circle-o"></i> Add Supplier Type</a></li>
            <li><a href="AddSupplier.jsp"><i class="fa fa-circle-o"></i> Add Supplier</a></li>
            <li><a href="AddTax.jsp"><i class="fa fa-circle-o"></i> Add Tax</a></li>
            <li><a href="AddEnquirySource.jsp"><i class="fa fa-circle-o"></i> Add Enquiry Source</a></li>
            <li><a href="AddOccupation.jsp"><i class="fa fa-circle-o"></i> Add Occupation</a></li>
            <li><a href="AddYear.jsp"><i class="fa fa-circle-o"></i> Add Financial Year</a></li>
            <li><a href="AddBudget.jsp"><i class="fa fa-circle-o"></i> Add Budget</a></li>
			<li><a href="AddChartofAccounts.jsp"><i class="fa fa-circle-o"></i> Add Chart Of Account</a></li>
            <li><a href="AddSubChartofAccounts.jsp"><i class="fa fa-circle-o"></i> Add Sub-Chart Of Account</a></li>
			  
			
			
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
           
            </li>
			 </ul>
		  </li>
		
		
		<li class="treeview active">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Pre-Sale</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
		  <li class="treeview active">
            <li class="active"  style="background-color:#48D1CC"><a href="AddEnquiry.jsp"><i class="fa fa-circle-o"></i>Add Enquiry</a></li>
            <li><a href="EnquiryFollowUp.jsp"><i class="fa fa-circle-o"></i>Todays Follow-up</a></li>
			 </li>
          </ul>
        </li>
		
        
        
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Sales Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add Booking</a></li>
			
          </ul>
        </li>
      
     
		<li class="treeview">
          <a href="#">
            <i class="fa fa-tasks"></i>
            <span>Assign Task</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="AddTask.jsp"><i class="fa fa-circle-o"></i> Add Task</a></li>
			
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>HR & Payroll Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> HR and Payroll</a></li>
			
          </ul>
        </li>
		 <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Site Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add </a></li>
			
          </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Project Engineering</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add</a></li>
		  </ul>
        </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Purchase management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Purchase</a></li>
		  </ul>
        </li>
		
		<li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Contractors Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Contractors Management</a></li>
		  </ul>
        </li>
		
			<li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Documentation</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i>Add Documentation</a></li>
			
          </ul>
        </li>
		
		
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Enquiry Details
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master</a></li>
        <li class="active">Add Enquiry</li>
      </ol>
    </section>

    <!-- Main content -->
	
<form name="enquiryform" action="#" onSubmit="return validate()">
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              
              <!-- /.form-group -->
             
			
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label for="enquairyid">Enquiry Id</label>
                  <input type="text" class="form-control" id="enquairyid" placeholder="ID" name="enquairyid" disabled>
                </div>               
                
              </div>
            </div>
				
				
				<div class="box-body">
              <div class="row">
                <div class="col-xs-4">
				<label for="enquairyfirstname">First Name</label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="enquairyfirstname" placeholder="First Name" name="enquairyfirstname"  pattern="[A-Za-z\s]{1,32}" oninvalid="alert('Please Fill Only Aphabets!');"  required>
                </div>
                <div class="col-xs-4">
				<label for="enquairymiddlename">Middle Name </label>
                  <input type="text" class="form-control" id="enquairymiddlename" placeholder="Middle Name"  pattern="[A-Za-z\s]{1,32}" oninvalid="alert('Please Fill Only Aphabets!');">
                </div>
                <div class="col-xs-4">
				<label for="enquairylastname">Last Name </label><label class="text-red">* </label>
                  <input type="text" class="form-control" id="enquairylastname" placeholder="Last Name" name="enquairymiddlename"  pattern="[A-Za-z\s]{1,32}" oninvalid="alert('Please Fill Only Aphabets!');"  required>
                </div>
              </div>
            </div>
				
				
				   <div class="box-body">
              <div class="row">
                <div class="col-xs-6">
			      <label>Date Of Birth </label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="date" class="form-control"data-mask name="dob">
                </div>
				   </div>
				 </div>
            </div>
				
			 <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			   <label for="radiobutton">Gender</label></br>
                 <input type="radio" class="minimal" name="gender" value="male"> Male
				   &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
  					<input type="radio" class="minimal" name="gender" value="female"> Female
                 </div> 
			 </div>
            </div>
				
				
				
				    <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			   <label for="enquairyaddress">Address </label>
                 <textarea class="form-control" rows="2" id="enquairyaddress" placeholder="Address" name="enquairyaddress"></textarea>
			     </div> 
				  
              </div>
            </div>
				
				
				<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label>Country </label>
                  <select class="form-control" name="country">
				  <option selected="" value="Default">-Select Country-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>               
                <div class="col-xs-6">
				 <label>State </label>
                  <select class="form-control" name="state">
				  <option selected="" value="Default">-Select State-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
				</div> 
              </div>
            </div>
				
			
			
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
				  <label>City </label>
                  <select class="form-control" name ="city">
				  <option selected="" value="Default">-Select City-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
				  </div> 
				    <div class="col-xs-6">
				    <label>Area </label>
                  <select class="form-control" name="area">
				  <option selected="" value="Default">-Select Area-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
				  </div> 
              </div>
            </div>
			
			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			   
                  <label for="pincode">Pin Code </label>
                  <input type="text" class="form-control" id="pincode" placeholder="Pin Code" name="pincode" pattern="[0-9]{6}" maxlength="6" oninvalid="alert('Please Fill Only Six Digit Number!');">
			     </div> 
				  
              </div>
            </div>
				   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			   
                  <label for="emailid">Email ID </label><label class="text-red">* </label>
				   <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <input type="email" class="form-control" placeholder="Email" name ="email"  required>
              </div>
			     </div> 
				 
              </div>
            </div>
			
			
			
			
			
			 <div class="box-body">
              <div class="row">
                   <div class="col-xs-6">
			    <label>Mobile No(Primary)</label><label class="text-red">* </label>
			    
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="mobileno1" required>
                </div>
				 </div>
				  <div class="col-xs-6">
			    <label>Mobile No 2</label>
			    
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(+99) 9999999999"' data-mask name="mobileno2">
                </div>
				 </div> 
              </div>
            </div>
			
			
				<div class="form-group">
               
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label for="pancardno">Pan Card No </label>
                  <input type="text" class="form-control" id="pancardno" placeholder="Pan Card No " name="pancardno" pattern="[a-zA-Z0-9]{10}" oninvalid="alert('Please Fill Only Ten Digit Aphabets & Number!');">
                
			     </div> 
				  <div class="col-xs-6">
			      <label for="adharcardno">Adhar Card No</label>
                  <input type="text" class="form-control" id="adharcardno" placeholder="Adhar Card No " name="adharcardno" pattern="[0-9]{12}" oninvalid="alert('Please Fill Only Number!');">
			     </div> 
              </div>
            </div>
			   
			   
              </div>
			  
				<div class="form-group">
                <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label for="pancardno">Occupation </label> 
				<select class="form-control" name="occupation">
				  <option selected="" value="Default">-Select Occupation-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div> 
				</div>
            </div>
			  </div>
			  
			  
			<div class="form-group">
               
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label for="compaany">Company Name </label>
                  <input type="text" class="form-control" id="companyname" placeholder="Company Name " name="companyname" pattern="[a-zA-Z\s]+" oninvalid="alert('Please Fill Only Aphabets!');">
                
			     </div> 
				  <div class="col-xs-6">
			      <label for="adharcardno">Office Phone Number</label>
				  <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" >
                </div>
			     </div> 
              </div>
            </div>
			   
			   
              </div>
			  
			  
             
            </div>
          
            <div class="col-md-6">
			
			 </br>
			 
			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			    <h3>Project And Flat Details</h3>
				<label>Project</label><label class="text-red">* </label>
                  <select class="form-control" name="project">
				  <option selected="" value="Default">-Select Project-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
			     </div> 
              </div>
            </div>
			 
			 
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			     <label>Project Unit</label><label class="text-red">* </label>
                  <select class="form-control" name="projectunitname">
				  <option selected="" value="Default">-Select Project Unit-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
			     </div> 
              </div>
            </div>
					
					
				


			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			      <label>Floor  </label><label class="text-red">* </label>
                  <select class="form-control" name="floor">
				  <option selected="" value="Default">-Select Floor-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
			     </div> 
              </div>
            </div>
			
			  <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			    <label>Flat Type  </label><label class="text-red">* </label>
                  <select class="form-control" name="flattype">
				  <option selected="" value="Default">-Select Flat Type-</option>
                    <option>RK</option>
                    <option>1 BHK </option>
                    <option>2 BHK</option>
                    <option>3 BHK</option>
                  </select>
			     </div> 
              </div>
            </div>
			 <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			    <label>Flat Number  </label><label class="text-red">* </label>
                  <select class="form-control" name="flatno">
				  <option selected="" value="Default">-Select Flat Number-</option>
                    <option>Options 1</option>
                    <option>Options 2</option>
                    <option>Options 3</option>
                    <option>Options 4</option>
                  </select>
			     </div> 
              </div>
            </div>
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
				  
                  <label>Flat Facing  </label>
                  <input type="text" class="form-control" id="flatfacing" placeholder="Flat Facing " name="flatfacing" disabled>
			     </div> 
              </div>
            </div>
			<div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
                  <label for="flatarea">Flat Area in SQ.FT </label>
                  <input type="text" class="form-control" id="flatarea" placeholder="Flat Area in SQ.FT " name="flatarea" disabled>
			     </div> 
				 
				   <div class="col-xs-6">
                  <label for="flatcost">Flat Cost </label>
                  <input type="text" class="form-control" id="flatcost" placeholder="Flat Cost " name="flatcost">
			     </div> 
              </div>
			   
            </div>
			
			
				
				
			   <div class="box-body">
              <div class="row">
				  <div class="col-xs-12">
			    <label>Budget</label><label class="text-red">* </label>
              <select class="form-control" name="budget">
				  <option selected="" value="Default">-Select Budget-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
			     </div> 
              </div>
            </div>
			 
			   <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			   	<label for="pl">Prefer Bank </label><label class="text-red">* </label>
                 <select class="form-control" name="preferbank">
				  <option selected="" value="Default">-Select  Prefer Bank-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
			     </div> 
			      </div>
            </div>
			     
			     
			      <div class="box-body">
              <div class="row">
				  <div class="col-xs-12">
			   	 <label for="sl">Enquiry Source </label><label class="text-red">* </label>
				 <select class="form-control" name="enquairysource">
				  <option selected="" value="Default">-Select Enquiry Source-</option>
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
              
			     </div>
				 
              </div>
            </div>
			   
			    <div class="box-body">
              <div class="row">
                  <div class="col-xs-6">
			    <label>Follow-up Date</label><label class="text-red">* </label>
              <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="date" class="form-control pull-right" name="followupdate" required>
                </div>
			     </div> 
			       <div class="col-xs-6">
			   <label for="status">Status </label>
			   <select class="form-control" name="status">
				  <option selected="" value="Default">-Status-</option>
                    <option>In-Process</option>
                    <option>Site Visited</option>
                    <option>Booking</option>
                    <option>Cancel</option>
                  </select>
			     </div> 
			     
              </div>
            </div>
			   
				
				    <div class="box-body">
              <div class="row">
                  <div class="col-xs-12">
			   <label for="enquairyremark">Remark </label><label class="text-red">* </label>
                 <textarea class="form-control" rows="2" id="enquairyremark" placeholder="Remark" name="enquairyremark" required></textarea>
			     </div> 
				  
              </div>
            </div>
			    <div class="box-body">
              <div class="row">
                
				  
              </div>
            </div>
			 </br>
			  
              <!-- /.form-group -->
			  
            </div>
			
            <!-- /.col -->
			
          </div>
		   		    <div class="box-body">
              <div class="row">
              </br>
                
				  <div class="col-xs-4">
                <button type="reset" class="btn btn-default" value="reset" style="width:90px"> Reset</button>
              
			     </div>
					<div class="col-xs-2">
			  <button type="submit" class="btn btn-info pull-right" name="submit">Submit</button>
              
			     </div> 
			     
			     
			       <div class="col-xs-6">
			         <button type="button" class="btn btn-info pull-right" style="background:#48D1CC"><a href="ImportNewEnquiry.jsp"> Import From Excel File</a></button>
              
			       
                  </div>
              </div>
			  </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Today's Enquiry</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Sr.No</th>
				  <th>Enquiry Id</th>
                  <th>Name</th>
                  <th>Address</th>
				  <th>City</th>
                  <th>Email Id</th>
				  <th>Mobile No</th>
				  <th>Project name</th>
				  <th>Unit Name</th>
				  <th>Floor</th>
				  <th>Flat Number</th>
				  <th>Status</th>
				  </tr>
                </thead>
				
                <tbody>
               
                <tr>
                  <td>1</td>
                  <td>101</td>
                  <td>Bahirnath</td>
				   <th>Pipeline Road</th>
                  <td>Nagar</td>
				  <th>bhirnath@gmail.com</th>
				  <th>9822197411</th>
				  <th>ABC</th>
				  <th>A</th>
				  <th>Second</th>
                  <th>202</th>
				  <th>IN-Process</th>
                </tr>
             
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
      <!-- /.box -->

     
    </section>
	</form>
    <!-- /.content -->
  </div>
 

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page script -->
<script>


function validate()
{ 
		if(document.enquiryform.enquairyfirstname.value=="")
		{
			alert("Please Fill Name");
			document.enquiryform.enquairyfirstname.focus();
			return false;
		}
		if(document.enquiryform.enquairylastname.value=="")
		{
			alert("Please Fill Last Name");
			document.enquiryform.enquairylastname.focus();
			return false;
		}
		if(document.enquiryform.email.value=="")
		{
			alert("Please Fill emaill id");
			document.enquiryform.email.focus();
			return false;
		}
		if(document.enquiryform.mobileno1.value=="")
		{
			alert("Please Fill Mobile Number");
			document.enquiryform.mobileno1.focus();
			return false;
		}
		if(document.enquiryform.project.value=="Default")
		{
			alert("Please Select Project Name");
			document.enquiryform.project.focus();
			return false;
		}
		if(document.enquiryform.projectunitname.value=="Default")
		{
			alert("Please Select Project Unit Name");
			document.enquiryform.projectunitname.focus();
			return false;
		}
			if(document.enquiryform.floor.value=="Default")
		{
			alert("Please Select Floor");
			document.enquiryform.floor.focus();
			return false;
		}
		if(document.enquiryform.flattype.value=="Default")
		{
			alert("Please Select Flat Type");
			document.enquiryform.flattype.focus();
			return false;
		}
		if(document.enquiryform.flatno.value=="Default")
		{
			alert("Please Select Flat Number");
			document.enquiryform.flatno.focus();
			return false;
		}
		if(document.enquiryform.budget.value=="Default")
		{
			alert("Please Select Budget");
			document.enquiryform.budget.focus();
			return false;
		}
		if(document.enquiryform.preferbank.value=="Default")
		{
			alert("Please Select Bank");
			document.enquiryform.preferbank.focus();
			return false;
		}
		if(document.enquiryform.enquairysource.value=="Default")
		{
			alert("Please Fill Enquiry Source");
			document.enquiryform.enquairysource.focus();
			return false;
		}
		if(document.enquiryform.followupdate.value=="")
		{
			alert("Please Select Next Follow-up Date ");
			document.enquiryform.followupdate.focus();
			return false;
		}
		if(document.enquiryform.enquairyremark.value=="")
		{
			alert("Please Fill Remark ");
			document.enquiryform.enquairyremark.focus();
			return false;
		}
		
}
function init()
{
  document.enquiryform.enquairyfirstname.focus();
}

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
</body>
</html>
